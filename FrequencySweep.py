

# -*- coding: utf-8 -*-
# 21 septembre 2023 Update for pyqtgraph 0.13.1
"""
GUI for uRes photoacoustic
"""

import os
from collections import namedtuple

from pyqtgraph.dockarea import DockArea
from pyqtgraph.dockarea import Dock
from pyqtgraph import PlotWidget

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets, QtCore
from pyqtgraph.parametertree import interact, ParameterTree, Parameter

import numpy as np
import toolbox.instrument 
import toolbox.parameter_tree
import toolbox.sauvegarder
import toolbox.mother_class
import scipy.optimize
import time


# only if you want a white background for your graph
# pg.setConfigOption('background', 'w')



class MainWindow(QtWidgets.QMainWindow,toolbox.mother_class.MotherMainWindow): 
    def __init__(self):
        super().__init__()
        # the tuple version of a dictionary, use to store width and height of the screen 
        Screen = namedtuple('Screen', 'width height')
        # get screen's information (height and width)
        # CAUTION this attribut can be modified by other method like .show
        # it can put all the values at 0, even they were welle defined
        self.screen = Screen(width=1500,height=800) 
        # dictionary with all the graphic elements 
        self.obj_graph = {'plot': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                  },
                          'plot2': {'dock': None,
                                   'graph': None,
                                   'curve': None
                                   },         
                          'param': {'dock': None,
                                    'tree': None,
                                    'parameter': None}}                          

        self.on_init()       
        # change the size of the graphic interface
        self.resize(self.screen.width, self.screen.height)
        # my timer use for acquisition in real time
        self.my_timer = None
        self.period_timer = 50
        # create an attribute with all my instruments 
        self.instruments = MesInstrus(param_tree=self.parameters)
        # display the GUI
        self.show()

    def on_init(self):
        # for debug it is useful to separate the create         
        self.create_dock()
        self.create_contenu_dock_pg()
        self.create_contenu_dock_plot()


    def create_dock(self):
        area = DockArea()
        self.obj_graph['param']['dock'] = Dock("FS (Frequency Sweep) setting parameters")
        self.obj_graph['plot']['dock'] = Dock("Frequency sweep R function of Freq")
        self.obj_graph['plot2']['dock'] = Dock("Frequency sweep Phi function of Freq")
        area.addDock(self.obj_graph['param']['dock'], 'left')
        area.addDock(self.obj_graph['plot']['dock'],'right')
        area.addDock(self.obj_graph['plot2']['dock'],'bottom',self.obj_graph['plot']['dock'])
        self.setCentralWidget(area)

    def create_contenu_dock_plot(self):
        # creation of plot 1 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot']['curve'] = MesCourbes()
        self.obj_graph['plot']['graph'] = self.obj_graph['plot']['curve'].plot_widget 
        self.obj_graph['plot']['dock'].addWidget(self.obj_graph['plot']['graph'])
        self.obj_graph['plot']['curve'].add_curve('curve acq',(0,250,250),markers_on=True)
        self.obj_graph['plot']['curve'].add_curve('curve acq fit',(200,200,0),markers_on=False, linewidht=2)  
               
        # creation of plot 2 (graph 2)
        # a place for this graph has been defined in create_dock()   
        self.obj_graph['plot2']['curve'] = MesCourbes()
        self.obj_graph['plot2']['graph'] = self.obj_graph['plot2']['curve'].plot_widget
        self.obj_graph['plot2']['dock'].addWidget(self.obj_graph['plot2']['graph']) 
        self.obj_graph['plot2']['curve'].add_curve('curve acq',(0,200,200),markers_on=True)
        self.obj_graph['plot2']['curve'].add_curve('curve acq fit',(200,200,0),markers_on=False, linewidht=2)  

    def create_contenu_dock_pg(self):
        self.obj_graph['param']['tree'] = ParameterTree()
        self.create_parameter()
        self.obj_graph['param']['dock'].addWidget(self.obj_graph['param']
                                                  ['tree'])
    def create_parameter(self):
        # create a Class with all parameters use in the GUI
        self.parameters = MesParams()
        # build the GUI
        (self.obj_graph['param']
         ['parameter']) = Parameter.create(name='params',
                                           type='group',
                                           children=self.parameters.tree)
        (self.obj_graph['param']
         ['parameter'].sigTreeStateChanged.connect(self.catch_param_change))

        """
        setParameters
        Set the top-level :class:`Parameter <pyqtgraph.parametertree.Parameter>`
        to be displayed in this ParameterTree.

        If *showTop* is False, then the top-level parameter is hidden and only 
        its children will be visible. This is a convenience method equivalent 
        to::
        
            tree.clear()
            tree.addParameters(param, showTop)
        """
        self.obj_graph['param']['tree'].setParameters((self.obj_graph['param']
                                                       ['parameter']),
                                                      showTop=False)
        self.obj_graph['param']['tree'].setWindowTitle('Parameter Tree')

    def catch_param_change(self, _, changes):
        # call in create_parameter()
        for param, _, data in changes:
            path = self.obj_graph['param']['parameter'].childPath(param)
            if path is not None: 
                # print(path) -> ['Input- Zurich', 'time constant']
                child_name = '.'.join(path)
                # print(child_name) -> Input- Zurich.time constant
            else:
                child_name = param.name()
            print('  parameter: %s' % child_name)
            print('  change:    %s' % _)
            print('  data:      %s (%s)' % (str(data),type(data)))

            # who is the name of the parameter tree
            # what the name of the paramater of the parameter tree
            try:
                who, what = child_name.split('.')
            except:
                who = child_name
                what = child_name

            graph_1 = self.obj_graph['plot']['curve']
            graph_2 = self.obj_graph['plot2']['curve']

            ## INSTRUMENT CHANGE
            # CAUTION check it always in first
            if what in ['instrument']:
                # in this case data == name of the instrument
                print(who,data)
                self.instruments.update(who,data)

            ## CHANGE VALUES ASKED ON THE INSTRUMENT
            if who in ['Input','Generator','Laser Driver','Pulse module']:
                #if what in ['time constant','sensitivity','amplitude','on','frequency','external reference','current','temperature','internal modulation (Square signal)','frequency modulation','amplitude modulation','harmonic']:
                    # get the name of instrument asked in the parameter tree 
                instru = self.parameters.give_inst(who)
                self.instruments.set_value(instru,what,data)
                # if who == what:
                #     self.instruments.update(who,data)    
                    
            ## START ACQUISITION
            if (who+'.'+what) in 'Acquisition.start':
                # update old and new acquisition
                self.plot_ghost_curve()                
                # create frequency list
                graph_1.create_frequency_list(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                graph_2.create_frequency_list(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                # on lance un timer qui appelera 'timerEvent' toutes les period_timer
                if self.my_timer is None:
                    self.my_timer = self.startTimer(self.period_timer) #c une methode heritée
                # the method timeEvent will be executed each period_timer

            ## AVERAGE ACQUISITION
            if (who+'.'+what) in 'Acquisition.average':
                # average data
                graph_1.average(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                graph_2.average(
                    curve_id='curve acq',
                    param_tree=self.parameters)  
                # display graphs
                graph_1.display('curve acq','R')
                graph_2.display('curve acq','Phi')


            ## ERASE ACQUISITION
            if (who+'.'+what) in 'Acquisition.clear all':
                graph_1.remove_ghost()
                graph_2.remove_ghost()
                for k in ['acq','acq fit']:
                    # earase data
                    graph_1.clear_data('curve '+k)
                    graph_2.clear_data('curve '+k)
                    # display graphs
                    graph_1.display('curve '+k,'R')
                    graph_2.display('curve '+k,'Phi')
                # stop acquisition
                self.stop_timer()
                # remove list of curve in the paramter tree and a new one
                self.parameters.dico['Graph'].clearChildren()
                self.parameters.dico['Graph'].addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})

            ## FIT 
            # for MAG -> GRAPH 1
            if (who+'.'+what) in 'Fit.fit MAG':
                self.fit('R')               
            # for Phi -> GRAPH 2
            if (who+'.'+what) in 'Fit.fit PHASE':             
                self.fit('Phi')

            # FULL WIDTH
            if (who+'.'+what) in 'Fit.full width':
                self.full_width()

            # ## SAVE info resonator cahnge the list of  resonator if the design has been changed
            # if (who+'.'+what) in 'Save.design':
            #     design = self.parameters.give(who='Save',what='design')
            #     self.parameters.dico['Save'].Resonator(design)

            ## SAVE ACQUISITION
            if (who+'.'+what) in 'Save.save':
                # update only the parameters from the section Save and Graph
                graph_1.save_parameters_sauvegarder(curve_id='curve acq',param=self.parameters)
                # save everything                
                toolbox.sauvegarder.Save('Frequency sweep',self.obj_graph['plot']['curve'].curves,self.obj_graph['plot2']['curve'].curves)

    def fit(self,what):
        self.fit_freq_limit()
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        f_min = self.parameters.give('Fit','freq min')
        f_max = self.parameters.give('Fit','freq max')
        quality_factor = self.parameters.give('Fit','initial guess for Q')
        C0 = self.parameters.give('Fit','value of C0')
        amplitude = self.parameters.give('Fit','initial guess for amp')
        freq_0 = self.parameters.give('Fit','initial guess for f0')
        if what in 'R':
            graph = graph_1
        if what in 'Phi':
            graph = graph_2
        # earase data
        graph.clear_data('curve acq fit')  
        # compute fit
        graph.lorentzian_fit('curve acq',amp_screen=amplitude,f0=freq_0,
            Q=quality_factor,freq_min=f_min,freq_max=f_max,capacitance=C0,what=what)
        # update parametertree
        self.rajoute_info_fit(what)
        # display graphs
        graph.display('curve acq fit',what) 

    def fit_freq_limit(self):
        # adjuste frequency min and max used for the fit to avoid bug
        f_acq_min = self.parameters.dico['Acquisition'].param('freq min').value()
        f_acq_max = self.parameters.dico['Acquisition'].param('freq max').value()
        f_fit_min = self.parameters.dico['Fit'].param('freq min').value()
        f_fit_max = self.parameters.dico['Fit'].param('freq max').value()
        # print('FIT FREQ LIMIT: f_acq_min={:.3f} f_acq_max={:.3f} f_fit_min={:.3f} f_fit_max={:.3f}'.format(f_acq_min,f_acq_max,f_fit_min,f_fit_max))
        # f_fit_min > f_fit_max
        if f_fit_min > f_fit_max:
            # print('FIT FREQ LIMIT: f_fit_min > f_fit_max')
            self.parameters.dico['Fit'].param('freq min').setValue(f_fit_max)
            self.parameters.dico['Fit'].param('freq max').setValue(f_fit_min)
            f_fit_min = self.parameters.dico['Fit'].param('freq min').value()
            f_fit_max = self.parameters.dico['Fit'].param('freq max').value()
        # fit_min < fit_max < acq_min < acq_max             
        if (f_fit_min <= f_acq_min) and (f_fit_max <= f_acq_min):
            # print('FIT FREQ LIMIT: fit_min < fit_max < acq_min < acq_max ')
            self.parameters.dico['Fit'].param('freq min').setValue(f_acq_min)
            self.parameters.dico['Fit'].param('freq max').setValue(f_acq_max)
        # fit_min < acq_min < fit_max < acq_max 
        if (f_fit_min <= f_acq_min) and (f_fit_max >= f_acq_min) and (f_fit_max <= f_acq_max):
            # print('FIT FREQ LIMIT: fit_min < acq_min < fit_max < acq_max')
            self.parameters.dico['Fit'].param('freq min').setValue(f_acq_min)
        # acq_min < fit_min < acq_max < fit_max
        if (f_fit_min >= f_acq_min) and (f_fit_min <= f_acq_max) and (f_fit_max >= f_acq_max):
            # print('FIT FREQ LIMIT: acq_min < fit_min < acq_max < fit_max')
            self.parameters.dico['Fit'].param('freq max').setValue(f_acq_max)
        # acq_min < acq_max < fit_min < fit_max
        if (f_fit_min >= f_acq_max) and (f_fit_max >= f_acq_max):
            # print('FIT FREQ LIMIT: acq_min < acq_max < fit_min < fit_max')
            self.parameters.dico['Fit'].param('freq min').setValue(f_acq_min)
            self.parameters.dico['Fit'].param('freq max').setValue(f_acq_max)
        # fit_min < acq_min < acq_max < fit_max
        if (f_fit_min < f_acq_min) and (f_acq_max < f_fit_max):
            # print('FIT FREQ LIMIT: fit_min < acq_min < acq_max < fit_max')
            self.parameters.dico['Fit'].param('freq min').setValue(f_acq_min)
            self.parameters.dico['Fit'].param('freq max').setValue(f_acq_max)

    def full_width(self):
        self.fit_freq_limit()
        graph_1 = self.obj_graph['plot']['curve']
        graph_1.full_width()
        self.rajoute_info_curve()


    def guess_value(self):
        self.fit_freq_limit()
        graph_1 = self.obj_graph['plot']['curve']
        f0, q ,_ = graph_1.full_width()
        amp = max(graph_1.curves['curve acq']['data']['R'])
        self.parameters.dico['Fit'].param('initial guess for amp').setValue(amp)
        self.parameters.dico['Fit'].param('initial guess for f0').setValue(f0)
        self.parameters.dico['Fit'].param('initial guess for Q').setValue(q)

    def plot_ghost_curve(self):
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        # if test_no_restart is False that's mean that the button start has been pressed before the end of the acquisition. In this case no ghost will be ploted.
        test_no_restart = (len(graph_1.curves['curve acq']['data']['R']) == len(graph_1.curves['curve acq']['data']['Freq']))
        if (nbr_ghost > 0) and test_no_restart:
            graph_1.copy_curve('curve acq','curve ghost'+str(nbr_ghost))
            graph_1.display('curve ghost'+str(nbr_ghost),'R')
            graph_2.copy_curve('curve acq','curve ghost'+str(nbr_ghost))
            graph_2.display('curve ghost'+str(nbr_ghost),'Phi')
            graph_1.copy_curve('curve acq fit','curve ghost'+str(nbr_ghost)+' fit')
            graph_1.display('curve ghost'+str(nbr_ghost)+' fit','R')
            graph_2.copy_curve('curve acq fit','curve ghost'+str(nbr_ghost)+' fit')
            graph_2.display('curve ghost'+str(nbr_ghost)+' fit','Phi')
        # earase data               
        graph_1.clear_data('curve acq')
        graph_1.clear_data('curve acq fit')
        graph_2.clear_data('curve acq')
        graph_2.clear_data('curve acq fit')

    def rajoute_ghost_cuvre(self):
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # put and chack the max of curve
        curves_max = 9
        if nbr_ghost == curves_max:
            self.message_box(text='You have reached the maximum number of curves.')
        # color
        colors = self.color_map(curves_max+1) 
        # add curve
        graph_1.add_curve('curve ghost'+str(nbr_ghost+1),np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        graph_2.add_curve('curve ghost'+str(nbr_ghost+1),np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        graph_1.add_curve('curve ghost'+str(nbr_ghost+1)+' fit',np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        graph_2.add_curve('curve ghost'+str(nbr_ghost+1)+' fit',np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        # add a line on the parameter tree to give the value of the resonnace frequency
        self.parameters.dico['Graph'].addChild({'name': 'info curve '+str(nbr_ghost+1), 'type': 'str', 'value': '', 'readonly': True})
        self.parameters.dico['Graph'].addChild({'name': 'info fit R '+str(nbr_ghost+1), 'type': 'str', 'value': '', 'readonly': True})
        self.parameters.dico['Graph'].addChild({'name': 'info fit Phi '+str(nbr_ghost+1), 'type': 'str', 'value': '', 'readonly': True})
        # add a line on the parameter tree str(nbr_ghost+2) as a first legend curve has been created in the same time than the parametertree
        self.parameters.dico['Graph'].addChild({'name': 'legend curve '+str(nbr_ghost+2), 'type': 'str', 'value': ''})      

  
    def rajoute_info_curve(self):
        # add info about the acquisition on the parametre
        graph_1 = self.obj_graph['plot']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        if nbr_ghost > 0:
            df = graph_1.curves['curve acq']['data']['Delta f']
            f0 = graph_1.curves['curve acq']['data']['f0']
            Q = graph_1.curves['curve acq']['data']['Q']
            texte = 'full widt give give : f0={:.4f}kHz Q={:.0f} df={:.3f}Hz'.format(f0*1e-3,Q,df)
            self.parameters.dico['Graph'].param('info curve '+str(nbr_ghost)).setValue(texte)   

    def rajoute_info_fit(self,what):
        # add info about the acquisition on the parametre
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        if nbr_ghost > 0:
            if what in 'R':
                graph = graph_1
            if what in 'Phi':
                graph = graph_2
            f0_fit = graph.curves['curve acq fit']['data']['f0']
            f0_sigma = graph.curves['curve acq fit']['data']['f0 sigma']
            Q_fit = graph.curves['curve acq fit']['data']['Q']
            Q_sigma = graph.curves['curve acq fit']['data']['Q sigma']
            capa_fit = graph.curves['curve acq fit']['data']['C0']          
            texte = 'Fit of '+what+' give : f0={:.3f}kHz(+/-){:.3f}Hz Q={:.0f}(+/-){:.1f} C0={:.2f}pF'.format(f0_fit/1e3,f0_sigma,Q_fit,Q_sigma,capa_fit*1e12)
            self.parameters.dico['Graph'].param('info fit '+what+' '+str(nbr_ghost)).setValue(texte)

    def timerEvent(self, _):
        """
        code exécuté toutes les "period_timer"
        """
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # test to stop the timer i.e. the acquisition
        if len(graph_1.curves['curve acq']['data']['R']) == len(graph_1.curves['curve acq']['data']['Freq']):
            self.stop_timer()
            self.rajoute_ghost_cuvre()
            self.full_width()
            self.guess_value()
            self.fit('R')
            self.fit('Phi')
            # save param on the curve dictionary
            graph_1.save_parameters('curve acq',self.parameters)
            graph_2.save_parameters('curve acq',self.parameters)
            return
        # change frequency on the instrument to freq
        index = len(graph_1.curves['curve acq']['data']['R'])
        freq_i = graph_1.curves['curve acq']['data']['Freq'][index]
        instru = self.parameters.give_inst('Generator') #######A verifier
        instru2 = self.parameters.give_inst('Input')
        self.instruments.set_value(instru,'frequency',freq_i) #changement freq de modulation
        self.parameters.dico['Generator'].param('frequency').setValue(freq_i)  #displays the current frequency
        # wait
        time.sleep(self.parameters.waiting_time())
        # get X Y R and Phi from instrument
        try:   # in case poll lenght is not in the parameter tree
            t_poll = self.parameters.give('Input','poll length')
        except:
            t_poll = None
        temp = self.instruments.get_X_Y_R_Phi(instru2,poll_length=t_poll,freq=freq_i)
        # update data
        graph_1.update_X_Y_R_Phi('curve acq',temp)
        graph_2.update_X_Y_R_Phi('curve acq',temp)
        # plot data
        graph_1.display('curve acq','R')
        graph_2.display('curve acq','Phi')


class MesInstrus(toolbox.mother_class.MotherInstru):
    def __init__(self,param_tree):
        self.param_tree = param_tree
        # attribut with all the instrument created
        self.connected = []
        # dico that the code will fill with instru which can be called
        self.use = {}
      

class MesParams(toolbox.mother_class.MotherParam):
    def __init__(self):
        # dictionary of parameter tree used
        self.dico = {
            'Laser Doppler Vibrometer':toolbox.parameter_tree.LaserDopplerVibrometer(name='Laser Doppler Vibrometer'),
            'Input':toolbox.parameter_tree.Input(name='Input'),
            'Generator':toolbox.parameter_tree.Generator(name='Generator'),
            #'Pulse module':toolbox.parameter_tree.PulseModule(name='Pulse module'),
            'Laser Driver':toolbox.parameter_tree.LaserDriver(name='Laser Driver'),
            'Acquisition':toolbox.parameter_tree.Acquisition(name='Acquisition'),
            'Fit':toolbox.parameter_tree.FitFS(name='Fit'),
            'Graph':toolbox.parameter_tree.GraphList(name='Graph'),
            'Save':toolbox.parameter_tree.Save(name='Save')}
        # use the self.dico to build a list required for the GUI
        self.tree = []       
        for k in self.dico:
            self.tree.append(self.dico[k]) 

    def waiting_time(self):
        tc = self.dico['Input'].param('time constant').value()
        wt = self.dico['Acquisition'].param('waiting time').value()
        return tc*wt

class MesCourbes(toolbox.mother_class.MotherCourbe):
    def __init__(self):
        self.plot_widget = PlotWidget()
        self.plot_widget.showGrid(x=True,y=True)
        # self.plot_widget.getPlotItem().addLegend()
        self.plot_widget.setBackground((0, 0, 0))
        #  dictionary with all the curve and their data 
        # curve 0 is dedicated to the live/acquisition plot
        self.curves = {}

    def add_curve(self, curve_id, curve_color, markers_on=False, linewidht=3):
        curve_name = curve_id
        pen = pg.mkPen(curve_color, width=linewidht)
        symbol = "o"
        symbolPen = pg.mkPen(0,0,0)
        symbolBrush = curve_color
        symbolSize = 8
        # this adds the item to the plot and legend
        if markers_on:
            plot = self.plot_widget.plot(
                name=curve_name, pen=pen, symbol=symbol, symbolPen=symbolPen, 
                symbolBrush=symbolBrush, symbolSize=symbolSize
            )
        else:
            plot = self.plot_widget.plot(name=curve_name, pen=pen)
        self.curves[curve_id] = {
                    'plot':plot,
                    'data':{'Freq':[],'X':[],'Y':[],'R':[],'Phi':[]}
                        }

    def average(self,curve_id,param_tree):
        nbr_seqs = param_tree.give('Acquisition','nbr seqs')
        for k in ['X','Y','R','Phi','Freq']:
            temp = self.curves[curve_id]['data'][k]
            self.curves[curve_id]['data'][k] = toolbox.function.average(temp,nbr_seqs)

    def create_frequency_list(self,curve_id,param_tree):
        '''
        Create a array with all the frequency use for the frequency sweep
        It take into account if many sequences have been asked
        '''
        freq_min = param_tree.give('Acquisition','freq min')
        freq_max = param_tree.give('Acquisition','freq max')
        nbr_pts = param_tree.give('Acquisition','nbr pts')
        nbr_seqs = param_tree.give('Acquisition','nbr seqs')
        freq_list = np.linspace(freq_min,freq_max,nbr_pts)
        if nbr_seqs > 1:
            temp = freq_list
            i=0
            while i < int(nbr_seqs)-1:
                i+=1
                freq_list = np.append(freq_list,temp[::(-1)**i])
        self.curves[curve_id]['data']['Freq'] = freq_list

    def display(self,curve_id,what,adjust=True):
        X = self.curves[curve_id]['data']['Freq']
        Y = self.curves[curve_id]['data'][what]
        # during acquisition freq is longer than the others datas
        # so it is useful to reduce it
        if adjust is True:
            if len(X) != len(Y):
                X = self.curves[curve_id]['data']['Freq'][0:len(Y)]
        self.set_values(curve_id,X,Y)

    def find_freq_res(self, curve_id):
        # find the value of freq when R is maximum
        X = self.curves[curve_id]['data']['Freq']
        Y = self.curves[curve_id]['data']['R']
        idx = toolbox.function.find_nearest_index(max(Y),Y)
        f0 = X[idx]
        self.curves[curve_id]['data']['Freq0'] = f0
        return f0

    def full_width(self, curve_id='curve acq'):
        # find the quality factor by measuring the width at srt(2) the max
        try:
            X, Y, _ = self.interpolate(curve_id,'Freq','R')
            idx0 = np.argmax(Y)
            idx1 = toolbox.function.find_nearest_index(Y[:idx0],Y[idx0]/np.sqrt(2))
            idx2 = toolbox.function.find_nearest_index(Y[idx0:],Y[idx0]/np.sqrt(2))
            y0, f0 = Y[idx0], X[idx0]
            y1, f1 = Y[:idx0][idx1], X[:idx0][idx1]
            y2, f2 = Y[idx0:][idx2], X[idx0:][idx2]
            df = f2-f1
            q = f0/(f2-f1)
            print("Full width give f0={:.3f}kHz  df={:.3f}Hz  q={:.3f}".format(f0/1e3,df,q))
            self.curves[curve_id]['data']['f0'] = f0
            self.curves[curve_id]['data']['Q'] = q
            self.curves[curve_id]['data']['Delta f'] = df
            return f0, q ,df
        except:
            print('Can not estimate width.')
            for k in ['f0','Q','Delta f']:
                self.curves[curve_id]['data'][k] = float('nan')
            return float('nan'), float('nan'), float('nan')

    def lorentzian_fit(self,curve_data,amp_screen,f0,Q,freq_min,freq_max,capacitance,what='R'):
        capa = capacitance 
        # get data
        X = self.curves[curve_data]['data']['Freq'].copy()
        Y = self.curves[curve_data]['data'][what].copy()
        X, Y = toolbox.function.reduce_data_range(X,Y,freq_min,freq_max)
        # initial guess
        if what in 'R':
            # f0 = X[toolbox.function.find_nearest_index(Y, max(Y))]
            amp = amp_screen*f0**2/Q
        if what in 'Phi':
            # f0 = X[toolbox.function.find_nearest_index(abs(Y), max(abs(Y))/2)]    
            amp = 1.
            Cste = 0.
        print('Fit of {:s} was initialized  with : amp={:.3g}Vrms f0={:.3f}kHz Q={:.1f}'.format(what,amp/(f0**2/Q),f0/1e3,Q))
        # new lorentzian function without C0
        def lrtz_witout_C0(f,ampl,f0,Q):
            R = toolbox.function.lorentzianMAG(f,ampl,f0,Q,capa)
            return R
        # comute fit
        try:
            if what in 'R':
                popt, pcov = scipy.optimize.curve_fit(
                                    lrtz_witout_C0, 
                                    X,
                                    Y, 
                                    p0=(amp,f0,Q),)
                                   # bounds=(0,[float('Inf'),500e3,1e5]),
                                   # method='trf')
                amp_fit, f0_fit, Q_fit, capa_fit = np.append(popt, capa) 
            if what in 'Phi':
                popt, pcov = scipy.optimize.curve_fit(
                                    toolbox.function.lorentzianPHASE, 
                                    X,
                                    Y ,
                                    p0=(amp,f0,Q,capa,Cste))
                amp_fit, f0_fit, Q_fit, capa_fit, Cste_fit = popt
            perr = np.sqrt(np.diag(pcov))
            # record value
            self.curves['curve acq fit']['data']['amp'] = amp_fit
            self.curves['curve acq fit']['data']['amp sigma'] = perr[0]
            self.curves['curve acq fit']['data']['f0'] = f0_fit
            self.curves['curve acq fit']['data']['f0 sigma'] = perr[1]
            self.curves['curve acq fit']['data']['Q'] = Q_fit
            self.curves['curve acq fit']['data']['Q sigma'] = perr[2]
            self.curves['curve acq fit']['data']['C0'] = capa_fit
            # texte
            texte = 'Fit {} gives : f0={:.3f}kHz(+/-){:.3f}Hz Q={:.0f}(+/-){:.1f}'.format(what,f0_fit/1e3,perr[1],Q_fit,perr[2])
            print(texte)
            # compute graph of the fit
            X_fit = np.linspace(int(min(X)),int(max(X)),1000)
            if what in 'R':
                Y_fit = toolbox.function.lorentzianMAG(X_fit, amp_fit, f0_fit, Q_fit, capa_fit)
            if what in 'Phi':
                Y_fit = toolbox.function.lorentzianPHASE(X_fit, amp_fit, f0_fit, Q_fit, capa_fit, Cste_fit)            
            self.curves['curve acq fit']['data']['Freq'] = X_fit.copy() 
            self.curves['curve acq fit']['data'][what] = Y_fit.copy()
        except:
            print('Can not fit the curve, pas possible :-(')
            for k in ['amp','amp sigma','f0','f0 sigma','Q','Q sigma','C0']:
                self.curves['curve acq fit']['data'][k] = float('nan')


    def update_X_Y_R_Phi(self,curve_id,A):
        self.curves[curve_id]['data']['X'] = np.append(self.curves[curve_id]['data']['X'],A[0])
        self.curves[curve_id]['data']['Y'] = np.append(self.curves[curve_id]['data']['Y'],A[1])
        self.curves[curve_id]['data']['R'] = np.append(self.curves[curve_id]['data']['R'],A[2])
        self.curves[curve_id]['data']['Phi'] = np.append(self.curves[curve_id]['data']['Phi'],A[3]) 

if __name__ == "__main__":
    win = MainWindow()
    win.show()
    pg.exec()
