#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 22 15:06:50 2022

@author: michaelbahriz

lorentzian fit
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import toolbox.function
import os

# =============================================================================
# READ CSV
# =============================================================================

path = os.path.dirname(os.path.abspath(__file__))+'\\' # get the path of the file
print('path :',path)
file = 'YYY.csv'
sample_name = 'XXX'

data = pd.read_csv(path+file, sep=';', skiprows=[1])
print(data.head())

x = data['Frequency (Hz)']
y = data['r (Vrms)']

# =============================================================================
# GRAPH 1 - DATA
# =============================================================================

toolbox.function.load_mpl_params()
plt.plot(x, y, 'bo', label=sample_name)
plt.legend(loc=1)
plt.xlabel('frequency (Hz)')
plt.ylabel('magnitude (a.u.)')
plt.show()

# =============================================================================
# LORENTZIAN FIT
# =============================================================================

f0_init = 13.066e3
Q_init = 291
C0_init = 119e-12
amp_graph = 0.0012
amp_init = amp_graph*f0_init**2/Q_init

freq_min = 11000
freq_max = 13750

x, y = toolbox.function.reduce_data_range(x, y, freq_min, freq_max)

popt, pcov = scipy.optimize.curve_fit(
    toolbox.function.lorentzianMAG,
    x,
    y,
    p0=(amp_init, f0_init, Q_init, C0_init))
amp_fit, f0_fit, Q_fit, C0_fit = popt
perr = np.sqrt(np.diag(pcov))
amp_fit_sigma, f0_fit_sigma, Q_fit_sigma, C0_fit_sigma = perr

fit_text = 'The Lorentzian fit gives : f0={:.3f}kHz(+/-){:.3f}Hz Q={:.1f}(+/-){:.1f} C0={:.1f}(+/-){:.1f}pF'.format(
    f0_fit/1e3, f0_fit_sigma, Q_fit, Q_fit_sigma, C0_fit*1e12, C0_fit_sigma*1e12)
print(fit_text)

x_max = max(x)
x_min = min(x)
x_fit = np.linspace(int(x_min), int(x_max), num=1000)
y_fit = toolbox.function.lorentzianMAG(x_fit, amp_fit, f0_fit, Q_fit, C0_fit)

# =============================================================================
# GRAPH 2 - DATA + FIT
# =============================================================================

toolbox.function.load_mpl_params()
plt.plot(x, y, 'bo', label=sample_name)
plt.plot(x_fit, y_fit, 'r', label=fit_text)
plt.legend(loc=1)
plt.ylim(0, amp_graph*1.25)
plt.xlim(x_min, x_max)
plt.xlabel('frequency (Hz)')
plt.ylabel('magnitude (a.u.)')
plt.savefig(path+file.replace('.csv','')+'_lortz_fit.png')
plt.show()

