#!C:\\Users\\QEPAS\\.conda\\envs\\Interfero_Py37\\python.exe'
# -*- coding: utf-8 -*-
# 21 septembre 2023 Update for pyqtgraph 0.13.1
"""
GUI for uRes photoacoustic
use to optimize the amplitude of modualtion to reach the maximum of photoacoustic signal
"""

import os
from collections import namedtuple

from pyqtgraph.dockarea import DockArea
from pyqtgraph.dockarea import Dock
from pyqtgraph import PlotWidget

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets, QtCore
from pyqtgraph.parametertree import interact, ParameterTree, Parameter

import numpy as np
import toolbox.instrument 
import toolbox.parameter_tree
#import toolbox.function
import toolbox.sauvegarder
import toolbox.mother_class
import time


class MainWindow(QtWidgets.QMainWindow,toolbox.mother_class.MotherMainWindow): 
    def __init__(self):
        super().__init__()
        # the tuple version of a dictionary, use to store width and height of the screen 
        Screen = namedtuple('Screen', 'width height')
        # get screen's information (height and width)
        # CAUTION this attribut can be modified by other method like .show
        # it can put all the values at 0, even they were welle defined
        self.screen = Screen(width=1500,height=800) 
        # dictionary with all the graphic elements 
        self.obj_graph = {'plot': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                  },
                          'plot2': {'dock': None,
                                   'graph': None,
                                   'curve': None
                                   },         
                          'param': {'dock': None,
                                    'tree': None,
                                    'parameter': None}}                          

        self.on_init()       
        # change the size of the graphic interface
        self.resize(self.screen.width, self.screen.height)
        # my timer use for acquisition in real time
        self.my_timer = None
        self.period_timer = 50
        # create an attribute with all my instruments 
        self.instruments = MesInstrus(param_tree=self.parameters)
        # display the GUI
        self.show()

    def on_init(self):
        # for debug it is useful to separate the create         
        self.create_dock()
        self.create_contenu_dock_pg()
        self.create_contenu_dock_plot()


    def create_dock(self):
        area = DockArea()
        self.obj_graph['param']['dock'] = Dock("AOM (Amplitude Of Modulation) setting parameters")
        self.obj_graph['plot']['dock'] = Dock("R (Vrms) function of the amp. of mod. (V)")
        self.obj_graph['plot2']['dock'] = Dock("Phi (deg) function of the amp. of mod. (V)")
        area.addDock(self.obj_graph['param']['dock'], 'left')
        area.addDock(self.obj_graph['plot']['dock'],'right')
        area.addDock(self.obj_graph['plot2']['dock'],'bottom',self.obj_graph['plot']['dock'])
        self.setCentralWidget(area)


    def create_contenu_dock_plot(self):
        # creation of plot 1 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot']['curve'] = MesCourbes()
        self.obj_graph['plot']['graph'] = self.obj_graph['plot']['curve'].plot_widget 
        self.obj_graph['plot']['dock'].addWidget(self.obj_graph['plot']['graph'])       
        self.obj_graph['plot']['curve'].add_curve('curve acq',(0,250,250),markers_on=True) #acquisition curve
            
        # creation of plot 2 (graph 2)
        # a place for this graph has been defined in create_dock()   
        self.obj_graph['plot2']['curve'] = MesCourbes()
        self.obj_graph['plot2']['graph'] = self.obj_graph['plot2']['curve'].plot_widget
        self.obj_graph['plot2']['dock'].addWidget(self.obj_graph['plot2']['graph']) 
        self.obj_graph['plot2']['curve'].add_curve('curve acq',(0,200,200),markers_on=True)

    def create_contenu_dock_pg(self):
        self.obj_graph['param']['tree'] = ParameterTree()
        self.create_parameter()
        self.obj_graph['param']['dock'].addWidget(self.obj_graph['param']
                                                  ['tree'])

    def create_parameter(self):
        # create a Class with all parameters use in the GUI
        self.parameters = MesParams()
        # build the GUI
        (self.obj_graph['param']
         ['parameter']) = Parameter.create(name='params',
                                           type='group',
                                           children=self.parameters.tree)
        (self.obj_graph['param']
         ['parameter'].sigTreeStateChanged.connect(self.catch_param_change))

        """
        setParameters
        Set the top-level :class:`Parameter <pyqtgraph.parametertree.Parameter>`
        to be displayed in this ParameterTree.

        If *showTop* is False, then the top-level parameter is hidden and only 
        its children will be visible. This is a convenience method equivalent 
        to::
        
            tree.clear()
            tree.addParameters(param, showTop)
        """
        self.obj_graph['param']['tree'].setParameters((self.obj_graph['param']
                                                       ['parameter']),
                                                      showTop=False)
        self.obj_graph['param']['tree'].setWindowTitle('Parameter Tree')


    def catch_param_change(self, _, changes):
        # call in create_parameter()
        for param, _, data in changes:
            path = self.obj_graph['param']['parameter'].childPath(param)
            if path is not None: 
                # print(path) -> ['Input- Zurich', 'time constant']
                child_name = '.'.join(path)
                # print(child_name) -> Input- Zurich.time constant
            else:
                child_name = param.name()
            print('  parameter: %s' % child_name)
            print('  change:    %s' % _)
            print('  data:      %s (%s)' % (str(data),type(data)))

            # who is the name of the parameter tree
            # what the name of the paramater of the parameter tree
            try:
                who, what = child_name.split('.')
            except:
                who = child_name
                what = child_name

            graph_1 = self.obj_graph['plot']['curve']
            graph_2 = self.obj_graph['plot2']['curve']


            ## INSTRUMENT CHANGE
            # CAUTION check it always in first
            if what in ['instrument']:
                # in this case data == name of the instrument
                self.instruments.update(who,data)

            ## CHANGE VALUES ASKED ON THE INSTRUMENT
            if who in ['Input','Generator','Laser Driver']:
                #if what in ['time constant','sensitivity','on','amplitude','frequency','current','temperature','harmonic']:
                # get the nam of instrument asked in the parameter tree 
                instru = self.parameters.give_inst(who)
                self.instruments.set_value(instru,what,data)


            ## START ACQUISITION
            if (who+'.'+what) in 'Generator.start':
                # update old and new acquisition
                self.plot_ghost_curve()
                # create amplitude list
                graph_1.create_amplitude_list(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                graph_2.create_amplitude_list(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                # on lance un timer qui appelera 'timerEvent' toutes les period_timer
                if self.my_timer is None:
                    self.my_timer = self.startTimer(self.period_timer) #c une methode heritée
                # the method timeEvent will be executed each period_timer

            ## AVERAGE ACQUISITION
            if (who+'.'+what) in 'Generator.average':
                # average data
                graph_1.average(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                graph_2.average(
                    curve_id='curve acq',
                    param_tree=self.parameters)  
                # display graphs
                graph_1.display('curve acq','R')
                graph_2.display('curve acq','Phi')


            ## ERASE ACQUISITION
            if (who+'.'+what) in 'Generator.clear all':
                # earase data
                graph_1.clear_data('curve acq')
                graph_1.remove_ghost()
                graph_2.clear_data('curve acq')
                graph_2.remove_ghost()
                # display graphs
                graph_1.display('curve acq','R')
                graph_2.display('curve acq','Phi')  
                # stop acquisition
                self.stop_timer()
                # remove list of curve in the paramter tree
                self.parameters.dico['Graph'].clearChildren()
                self.parameters.dico['Graph'].addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})

            # ## SAVE info resonator cahnge the list of  resonator if the design has been changed
            # if (who+'.'+what) in 'Save.design':
            #     design = self.parameters.give(who='Save',what='design')
            #     self.parameters.dico['Save'].Resonator(design)

            ## SAVE ACQUISITION
            if (who+'.'+what) in 'Save.save':
                # update only the parameters from the section Save and Graph
                graph_1.save_parameters_sauvegarder(curve_id='curve acq',param=self.parameters)
                # save everything
                toolbox.sauvegarder.Save('Amplitude of modulation',self.obj_graph['plot']['curve'].curves)
                

    def plot_ghost_curve(self):
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        # if test_no_restart is False that's mean that the button start has been pressed before the end of the acquisition. In this case no ghost will be ploted.
        test_no_restart = (len(graph_1.curves['curve acq']['data']['R']) == len(graph_1.curves['curve acq']['data']['Amplitude']))
        if (nbr_ghost > 0) and test_no_restart:
            graph_1.copy_curve('curve acq','curve ghost'+str(nbr_ghost))
            graph_1.display('curve ghost'+str(nbr_ghost),'R')
            graph_2.copy_curve('curve acq','curve ghost'+str(nbr_ghost))
            graph_2.display('curve ghost'+str(nbr_ghost),'Phi')
        # earase data               
        graph_1.clear_data('curve acq')
        graph_2.clear_data('curve acq')

    def rajoute_ghost_cuvre(self):
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # put and chack the max of curve
        curves_max = 9
        if nbr_ghost == curves_max:
            self.message_box(text='You have reached the maximum number of curves.')
        # color
        colors = self.color_map(curves_max+1)
        # add curve 
        graph_1.add_curve('curve ghost'+str(nbr_ghost+1),np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        graph_2.add_curve('curve ghost'+str(nbr_ghost+1),np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        # add a line on the parameter tree to give the value of the resonnace frequency
        self.parameters.dico['Graph'].addChild({'name': 'info curve '+str(nbr_ghost+1), 'type': 'str', 'value': '', 'readonly': True})
        self.rajoute_info_ghost()
        # add a line on the parameter tree str(nbr_ghost+2) as a first legend curve has been created in the same time than the parametertree
        self.parameters.dico['Graph'].addChild({'name': 'legend curve '+str(nbr_ghost+2), 'type': 'str', 'value': ''})

    def rajoute_info_ghost(self):
        # add info about the acquisition on the parametre
        graph_1 = self.obj_graph['plot']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        if nbr_ghost > 0:
            vmax = graph_1.estimate_max('curve acq')
            texte = 'max for v = {:.3f} mVrms'.format(vmax*1e3)
            self.parameters.dico['Graph'].param('info curve '+str(nbr_ghost)).setValue(texte)

    def timerEvent(self, _):
        """
        code exécuté toutes les "period_timer"
        """
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # test to stop the timer i.e. the acquisition
        if len(graph_1.curves['curve acq']['data']['R']) == len(graph_1.curves['curve acq']['data']['Amplitude']):
            graph_1.estimate_max('curve acq')
            self.rajoute_ghost_cuvre()
            self.stop_timer()
            # save param on the curve dictionary
            graph_1.save_parameters('curve acq',self.parameters)
            graph_2.save_parameters('curve acq',self.parameters)
            return
        # change current on the instrument to current
        index = len(graph_1.curves['curve acq']['data']['R'])
        #print("index :"+str(index))
        amplitude_i = graph_1.curves['curve acq']['data']['Amplitude'][index]
        instru = self.parameters.give_inst('Generator')
        instru2 = self.parameters.give_inst('Input') ####added 06/05
        self.instruments.set_value(instru,'amplitude',amplitude_i)
        # wait
        time.sleep(self.parameters.waiting_time())
        # get X Y R and Phi from instrument
        try:   # in case poll lenght is not in the parameter tree
            t_poll = self.parameters.give('Input','poll length')
        except:
            t_poll = None
        temp = self.instruments.get_X_Y_R_Phi(instru2,poll_length=t_poll) ####added 06/05
        # update data
        graph_1.update_X_Y_R_Phi('curve acq',temp)
        graph_2.update_X_Y_R_Phi('curve acq',temp)
        #print("all: "+str(graph_2.curves['curve acq']['data']))
        # plot data
        graph_1.display('curve acq','R')
        graph_2.display('curve acq','Phi')

class MesInstrus(toolbox.mother_class.MotherInstru):
    def __init__(self,param_tree):
        self.param_tree = param_tree
        # attribut with all the instrument created
        self.connected = []
        # dico that the code will fill with instru which can be called
        self.use = {}

class MesParams(toolbox.mother_class.MotherParam):
    def __init__(self):
        # dictionary of parameter tree used
        self.dico = {
            'Input':toolbox.parameter_tree.Input(name='Input'),
            'Generator':toolbox.parameter_tree.GeneratorAOM(name='Generator'),
            'Laser Driver':toolbox.parameter_tree.LaserDriver(name='Laser Driver'),
            'Graph':toolbox.parameter_tree.GraphList(name='Graph'),            
            'Save':toolbox.parameter_tree.Save(name='Save')}
        # use the self.dico to build a list required for the GUI
        self.tree = []       
        for k in self.dico:
            self.tree.append(self.dico[k]) 

    def give_all_inst_value(self,who):
        # returns all the parameters necessary for updating the instrument 
        # (amplitude, frequency , etc.)
        A = []
        if who in 'Input':
            for k in ['time constant','sensitivity','harmonic','filter order']: 
                A.append([k,self.dico[who].param(k).value()])
        if who in 'Generator':
            for k in ['amplitude','on','frequency']: 
                A.append([k,self.dico[who].param(k).value()])
        if who in 'Laser Driver':
            for k in ['on','current','temperature']:
                A.append([k,self.dico[who].param(k).value()])
        return A

    # def remaning_time(self):
    #     nbr_p = self.dico['Generator'].param('nbr pts').value()
    #     nbr_s = self.dico['Generator'].param('nbr seqs').value()
    #     wt = self.waiting_time()
    #     rt = (wt)*nbr_p*nbr_s
    #     self.dico['Generator'].remaning_time(rt)  

    def waiting_time(self):
        tc = self.dico['Input'].param('time constant').value()
        wt = self.dico['Generator'].param('waiting time').value()
        return tc*wt


class MesCourbes(toolbox.mother_class.MotherCourbe):
    def __init__(self):
        self.plot_widget = PlotWidget()
        self.plot_widget.showGrid(x=True,y=True)
        # self.plot_widget.getPlotItem().addLegend()
        self.plot_widget.setBackground((0, 0, 0))
        # dictionary with all the curve and their data 
        # "curve acq" is dedicated to the live/acquisition plot
        self.curves = {}

    def add_curve(self, curve_id, curve_color, markers_on=True, linewidht=3):
        curve_name = curve_id
        pen = pg.mkPen(curve_color, width=linewidht)
        symbol = "o"
        symbolPen = pg.mkPen(0,0,0)
        symbolBrush = curve_color
        symbolSize = 8
        # this adds the item to the plot and legend
        if markers_on:
            plot = self.plot_widget.plot(
                name=curve_name, pen=pen, symbol=symbol, symbolPen=symbolPen, 
                symbolBrush=symbolBrush, symbolSize=symbolSize)
        else:
            plot = self.plot_widget.plot(name=curve_name, pen=pen)
        self.curves[curve_id] = {
                    'plot':plot,
                    'data':{'Amplitude':[],'X':[],'Y':[],'R':[],'Phi':[]}
                        }

    def average(self,curve_id,param_tree):
        amplitude_min = param_tree.give('Generator','amplitude min')
        amplitude_max = param_tree.give('Generator','amplitude max')
        nbr_pts = param_tree.give('Generator','nbr pts')
        nbr_seqs = param_tree.give('Generator','nbr seqs')
        amplitude_list = np.linspace(amplitude_min,amplitude_max,nbr_pts)
        self.curves[curve_id]['data']['Amplitude'] = amplitude_list
        for k in ['X','Y','R','Phi']:
            temp = self.curves[curve_id]['data'][k]
            self.curves[curve_id]['data'][k] = toolbox.function.average(temp,nbr_seqs)

    def create_amplitude_list(self,curve_id,param_tree):
        '''
        Create a array with all the amplitude use for the amplitude sweep
        It take into account if many sequences have been asked
        '''
        amplitude_min = param_tree.give('Generator','amplitude min')
        amplitude_max = param_tree.give('Generator','amplitude max')
        nbr_pts = param_tree.give('Generator','nbr pts')
        nbr_seqs = param_tree.give('Generator','nbr seqs')
        amplitude_list = np.linspace(amplitude_min,amplitude_max,nbr_pts)
        if nbr_seqs > 1:
            temp = amplitude_list
            i=0
            while i < int(nbr_seqs)-1:
                i+=1
                amplitude_list = np.append(amplitude_list,temp[::(-1)**i])
        self.curves[curve_id]['data']['Amplitude'] = amplitude_list

    def display(self,curve_id,what,adjust=True):
        X = self.curves[curve_id]['data']['Amplitude']
        Y = self.curves[curve_id]['data'][what]
        # during acquisition current is longer than the others datas
        # so it is useful to reduce it
        if adjust is True:
            if len(X) != len(Y):
                X = self.curves[curve_id]['data']['Amplitude'][0:len(Y)]
        self.set_values(curve_id,X,Y)

    def estimate_max(self,curve_id):
        X = self.curves[curve_id]['data']['Amplitude']
        Y = self.curves[curve_id]['data']['R']
        Ymax = max(Y)
        Xmax = X[toolbox.function.find_nearest_index(Y, Ymax)]
        print('For a modulation of {:.3g}V the singale is maximum and equal to {:.3f}Vrms'.format(Xmax,Ymax))
        self.curves[curve_id]['data']['Amplitude for R max'] = Xmax
        return Xmax

    def update_X_Y_R_Phi(self,curve_id,A):
        self.curves[curve_id]['data']['X'] = np.append(self.curves[curve_id]['data']['X'],A[0])
        self.curves[curve_id]['data']['Y'] = np.append(self.curves[curve_id]['data']['Y'],A[1])
        self.curves[curve_id]['data']['R'] = np.append(self.curves[curve_id]['data']['R'],A[2])
        self.curves[curve_id]['data']['Phi'] = np.append(self.curves[curve_id]['data']['Phi'],A[3]) 

if __name__ == "__main__":
    win = MainWindow()
    win.show()
    pg.exec()
