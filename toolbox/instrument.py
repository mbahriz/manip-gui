# 20201001 - ADD command to generate an AM from the Thorlabs driver laser

import numpy as np

### print all file for the import 
# import os
# current_dir = os.path.dirname(__file__)
# print(current_dir)
# inst_dir = os.path.join(current_dir,"inst")
# print(inst_dir)
# module_name = "toolbox.inst"
# for file in os.listdir(inst_dir):   
#     if file.endswith(".py"):
#         print(file[:-3])

from toolbox.inst import medisoft,ilx_driver,virtual,keithley,arduino_gpib,miniware,stanford,kinjalk,raspberry_pico,thorlabs,arduino_dht,zurich,ametek,tektronix
# from toolbox.inst import picoscope

class Instrument():

    def __init__(self,name=None):
        # name of the instrument
        self.name = name
        # object from the specific class of the instrument
        self.inst_obj = None

        if self.name == "Keithley_2010Multimeter":
            self.inst_obj = keithley.Keithley2010Multimeter()  
        if self.name == "Powermeter_Thorlabs_PM16_401":
            self.inst_obj = thorlabs.PowermeterTorlabsPM16401()
        if self.name == 'Medisoft':    
            self.inst_obj =medisoft.Medisoft()

        # GENERATOR
        if self.name == "Kinjalk_pulse_generator":
            self.inst_obj = kinjalk.Kinjalk_pulse_generator()
        if self.name == "PICO_pulse_generator":
            self.inst_obj = raspberry_pico.PICO_pulse_generator()
        if self.name == 'Picoscope_3207B':
            self.inst_obj = picoscope.Picoscope_3207B()
        if self.name == 'Miniware_CH1':
            self.inst_obj = miniware.Miniware_CH1()
        if self.name == 'Miniware_CH2':
            self.inst_obj = miniware.Miniware_CH2() 
        if self.name == 'Tektronix_CH1':
            self.inst_obj = tektronix.Tektronix_AFG1062_CH1()
        if self.name == 'Tektronix_CH2':
            self.inst_obj = tektronix.Tektronix_AFG1062_CH2() 
                     
        # DRIVER LASER           
        if self.name == 'Thorlabs_ITC400XQCL':
            self.inst_obj = thorlabs.ThorlabsITC400XQCL()    
        if self.name == 'Ilx_driver':
            self.inst_obj = ilx_driver.Ilx_driver()

        # LOCK-IN / GENERATOR               
        if self.name == 'Ametek_7270':
            self.inst_obj = ametek.AmetekLIA()
        if self.name == 'Stanford_SR860':
            self.inst_obj = stanford.Stanford_SR860()
        if self.name == 'Zurich-MFLI_dev4199':
            self.inst_obj = zurich.ZurichMFLI_MD(dev_num='dev4199')
        if self.name == 'Zurich-MFLI_dev4320':
            self.inst_obj = zurich.ZurichMFLI_D(dev_num='dev4320')
        if self.name == 'Zurich-MFLI_dev5255':
            self.inst_obj = zurich.ZurichMFLI_D(dev_num='dev5255')

        # MICROCONTROLER
        if self.name == 'Arduino_DHT':    
            self.inst_obj =arduino_dht.Arduino_DHT()
        if self.name == 'Arduino_GPIB':    
            self.inst_obj =arduino_gpib.Arduino_GPIB()
               
        # VIRTUAL    
        if self.name == 'Virtual':
            self.inst_obj = virtual.Virtual()
    
    def set_value(self,what,value):
        '''
        Give the value 'value' to the parameter 'what' on the instrument
        '''
        self.inst_obj.set_value(what,value)

    def poll(self,poll_length,x=None):
        # poll_length = recording time
        # x is use only with the virtual instrument for simulate a lorentzain curve for example
        return self.inst_obj.poll(poll_length,x=x)

    def poll_continuous(self,temps, graph):
        return self.inst_obj.poll_continuous(total_duration=temps, do_plot=graph)

    def disconnect(self):
        return self.inst_obj.disconnect()

# class Keithley2010Multimeter():
#     def __init__(self):
#         self.daq = None
#         self.connect()

#     def connect(self):
#         rm = pyvisa.ResourceManager()
#         rm.list_resources()
#         self.daq = rm.open_resource('GPIB0::16::INSTR')

#     def set_value(self,what,value):
#         pass
#         #if what in "range":
#         #    return value

#     def poll(self,poll_length,x=None):
#         return [float(self.daq.query(':DATA?')),np.nan,np.nan,np.nan]

# class PowermeterThorlabsPM16401():
#     def __init__(self):
#         self.daq = None
#         self.connect()

#     def connect(self):
#         rm = pyvisa.ResourceManager()
#         rm.list_resources()
#         self.daq = rm.open_resource('USB0::0x1313::0x807C::1909668::INSTR')
        
#     def set_value(self,what,value):
#         inst = self.daq
#         if what in 'wavelength': 
#             inst.write("SENS:CORR:WAV "+str(value))

#     def poll(self,poll_length,x=None):
#          return [float(self.daq.query("Read?")),np.nan,np.nan,np.nan]

# class ThorlabsITC4002QCL():
#     # laser Driver with USB connection
#     def __init__(self):
#         self.daq = self.connect() 

#     def connect(self):
#         #identification de l'instrument
#         rm = pyvisa.ResourceManager()
#         rm.list_resources()
#         inst = rm.open_resource('USB0::0x1313::0x804A::M00560075::INSTR')
#         #print('Device Thorlabs-ITC4002QCL IDN is %s'%(self.IDN()))
#         self.inst = inst
#         return inst

#     def IDN(self):
#         time.sleep(0.1)
#         self.daq.write("*CLS")
#         return self.daq.query("*IDN?")

#     def set_value(self,what,value):
#         inst = self.daq
#         if 'on' in what:
#             if value == True:
#                 inst.write("OUTP ON")
#             if value == False:
#                 inst.write("OUTP OFF")
#         if 'current' in what:
#             value = value/1000 #current given in mA
#             inst.write("SOUR:CURR "+str(value))
#             inst.write("SOUR:CURR "+str(value))
#         if 'temperature' in what:
#             inst.write("SOUR2:TEMP "+str(value)+"C")
#         if 'modulation' in what:
#             if value == True:
#                 inst.write("SOUR:AM 1") #activate modulation of the laser
#             if value == False:
#                 inst.write("SOUR:AM 0")#Desactivate the modulation
#         if 'internal modulation (Square signal)' in what:
#             if value == True:
#                 inst.write("SOUR:AM:SOUR INT") #set internat modulation
#                 inst.write("SOUR:AM:INT:SHAP SQU") #activate a square shape 
#             if value == False:
#                 inst.write("SOUR:AM:SOUR EXT")
#         if 'frequency modulation' in what:
#             value = value*1000 #enter the frequency in kHz
#             inst.write("SOUR:AM:INT:FREQ "+str(value)) #set the frequency modulation
#         if 'amplitude modulation' in what:
#             inst.write("SOUR:AM:INT "+str(value)) #/!\Value here is a pourcetage of the total DC current of the laser


#     def poll(self,poll_length,x=None):
#         inst = self.daq
#         #x is only for the virtual instrument
#         #time.sleep(0.1)
#         a = float(inst.query("SOUR:CURR?"))*1000
#         t = float(inst.query("SOUR2:TEMP?"))
#         mod_freq = float(inst.query("SOUR:AM:INT:FREQ?")) #Mod frequency given in Hz
#         v = float(inst.query("MEAS:VOLT?"))
#         #Return the current in mA and the temperature in degC
#         return [a,t,mod_freq,v]


# class Virtual():
#     def __init__(self):
#         self.daq = self.connect()
#         self.freq = None
#         self.current = None

#     def connect(self):
#         daq = None
#         return daq

#     def set_value(self,what,value):
#         '''
#         Give the value 'value' to the parameter 'what' on the instrument
#         '''
#         print('Virtual instrument : %s has been changed to %s' %(what,str(value)))
#         if what in 'frequency':
#             self.val = value
#         if what in 'current':
#             self.val = value
#         if what in 'amplitude':
#             self.val = value
#         if what in 'sensitivity':  
#             self.val = value
#         if what in 'time constant':
#             self.val = value     

#     def poll(self,poll_length,x=1):
#         # x is used to simulate a Loretzian curve useful only to develop the code and only with the virtual instrument
#         # poll_length = recording time
#         R = (np.random.normal(0,1,1)+5)*1e-4
#         Phi = np.random.normal(-90,90,1)
#         X = R*np.cos(Phi)
#         Y = R*np.sin(Phi)
#         Phi = Phi*180/np.pi # convert phi in degre
#         # to simulate a lorentzian curve
#         #R, Phi = noisy_lorentzian_function(x, amp=10, freq0=32.75e3, Q=10000)
#         return [X[0],Y[0],R[0],Phi[0]]

# class ZurichMFLI():
#     def __init__(self,dev_num):
#         self.daq = None
#         self.dev = dev_num
#         self.connect()
        
#     def connect(self):
#         '''
#         Use Device Discovery to find the ip adress and the port 
#         of the device called dev
#         '''
#         d = zhinst.ziPython.ziDiscovery()
#         d.find(self.dev)
#         devProp = d.get(self.dev)
#         print (devProp)
#         print('Device Zurich-%s adress is %s'%(self.dev,devProp['serveraddress']))
#         print('Device Zurich-%s port is %s'%(self.dev,devProp['serverport']))
#         # Create a connection to the data server
#         apilevel = 6 # allow timestamp support in poll (cf LabOneProgrammingManual page 16)
#         #daq = data acquistion system adress
#         self.daq = zhinst.ziPython.ziDAQServer(devProp['serveraddress'],devProp['serverport'], apilevel)
#         # get ready for data acquisition
#         self.poll_subscribe()


#     def disconnect(self):
#         self.poll_unsubscribe()
#         self.daq.disconnect()

#     def poll_subscribe(self):
#         # Subscribe to the demodulator's sample node path.
#         # If not done the poll function return a empty data
#         # Unsubscribe from all paths.
#         self.daq.unsubscribe('*')
#         demod_index = 0
#         path = '/%s/demods/%d/sample' % (self.dev, demod_index)
#         self.daq.subscribe(path)
#         # ### TEST PART
#         # # Unsubscribe from all paths.
#         # self.daq.unsubscribe('*')
#         # # Asking poll to always return a value for a settings node
#         # self.daq.getAsEvent('/dev4320/sigouts/0/amplitudes/3')
#         # self.daq.subscribe('/dev4320/sigouts/0/amplitudes/3')

#     def poll_unsubscribe(self):
#         self.daq.unsubscribe('*')  

#     def set_value(self,what,value):
#         '''
#         change value on zurich
#         '''
#         if 'frequency' in what:
#             self.daq.setDouble('/%s/oscs/0/freq'% (self.dev), value) 
#         if 'amplitude' in what:
#             self.daq.setDouble('/%s/sigouts/0/amplitudes/1'% (self.dev), value)
#         if 'on' in what:
#             if value == True:
#                 self.daq.setInt('/%s/sigouts/0/enables/1'% (self.dev), 1)
#             if value == False:
#                 self.daq.setInt('/%s/sigouts/0/enables/1'% (self.dev), 0)        
#         if 'sensitivity' in what:  
#             self.daq.setDouble('/%s/sigins/0/range'% (self.dev), value)
#         if 'time constant' in what:
#             self.daq.setDouble('/%s/demods/0/timeconstant'% (self.dev), value)         
#         if 'harmonic' in what:
#             self.daq.setDouble('/%s/demods/0/harmonic'% (self.dev), value)

#     def poll(self,poll_length,x=None):
#         ##############################################################################
#         #-                   Get data  - POLL function                              -#
#         ##############################################################################
#         # POLL Continuously check for value changes (by calling pollEvent) in all
#         # subscribed nodes for the specified duration and return the data. If
#         # no value change occurs in subscribed nodes before duration + timeout,
#         # poll returns no data.
#         # poll_length = 0.1 # recording time in [s]. The function will block during that time
#         # poll_timeout = 500 # Timeout in [ms]. Recommanded 500 ms. The timeout parameter is only
#                 # relevant when communicating in a slow network. In this case it may be
#                 # set to a value larger than the expected round-trip time in the
#                 # network.

#         # don't forget to use the function poll_subscribe() before
#         # if not done the poll function return a empty data
#         demod_index = 0
#         # reduce poll time out for allan deviation
#         poll_timeout = 100
#         # get data
#         data = self.daq.poll(poll_length, poll_timeout, True)
#         x = data[self.dev]['demods'][str(demod_index)]['sample']['x']
#         y = data[self.dev]['demods'][str(demod_index)]['sample']['y']
#         # time_stamp = data[self.dev]['demods'][str(demod_index)]['sample']['timestamp']
#         x = np.mean(x)
#         y = np.mean(y)
#         r = np.abs(x + 1j*y)
#         phi = np.angle(x + 1j*y,deg=True)
#         return [x,y,r,phi]
 

if __name__ == "__main__":
    print('toto')




