import pandas
import matplotlib.pyplot as plt

file_name = '25mVpk_freq_sweep_32kHz_1-300kHz_current_modlation.txt'
# Zurich data
zd_25 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_25['Normalized'] = zd_25['Vrms']/25e-3

file_name = '50mVpk_freq_sweep__1-300kHz_current_modlation.txt'
# Zurich data
zd_50 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_50['Normalized'] = zd_50['Vrms']/50e-3

file_name = '75mVpk_freq_sweep__1-300kHz_current_modlation.txt'
# Zurich data
zd_75 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_75['Normalized'] = zd_75['Vrms']/75e-3

file_name = '100mVpk_freq_sweep__1-300kHz_current_modlation.txt'
# Zurich data
zd_100 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_100['Normalized'] = zd_100['Vrms']/100e-3

fig, _ = plt.subplots()
gridsize = (1, 2)
ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
ax1.plot(zd_25['Freq']*1e-3,zd_25['Vrms'],label='Zurich output 25mVpk')
ax1.plot(zd_50['Freq']*1e-3,zd_50['Vrms'],label='Zurich output 50mVpk')
ax1.plot(zd_75['Freq']*1e-3,zd_75['Vrms'],label='Zurich output 75mVpk')
ax1.plot(zd_100['Freq']*1e-3,zd_100['Vrms'],label='Zurich output 100mVpk')
ax1.set_xlabel('frequency [kHz]', labelpad=0)
ax1.set_ylabel(r'Bias on 10 $\Omega$ resistor [Vrms]', labelpad=0)
ax1.legend(loc=0)

ax2 = plt.subplot2grid(gridsize, (0, 1), colspan=1, rowspan=1)
ax2.plot(zd_25['Freq']*1e-3,zd_25['Normalized'],label='Zurich output 25mVpk')
ax2.plot(zd_50['Freq']*1e-3,zd_50['Normalized'],label='Zurich output 50mVpk')
ax2.plot(zd_75['Freq']*1e-3,zd_75['Normalized'],label='Zurich output 75mVpk')
ax2.plot(zd_100['Freq']*1e-3,zd_100['Normalized'],label='Zurich output 100mVpk')
ax2.set_xlabel('frequency [kHz]', labelpad=0)
ax2.set_ylabel(r'Bias on 10 $\Omega$ resistor [Vrms] / Zurich output [Vpk]', labelpad=0)
ax2.legend(loc=0)

fig.savefig('mod_ampl_calibration_Thorlabs_Zurich.png')

plt.show()