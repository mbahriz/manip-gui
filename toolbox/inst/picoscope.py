import ctypes
from picosdk.ps3000a import ps3000a as ps
import picosdk.discover  as discover
import time
from picosdk.functions import assert_pico_ok



class Picoscope_3207B():
   
    def __init__(self):
        # self.daq = None
        self.startFrequency=10e3#hz
        self.pkToPk=int(30e-3*2*1000000)#Vpp
        
        self.connect() 
        self.flag = True
        
        
    def connect(self):  
        
        self.flag = True  # Flag to control clearing on start
        # Gives the device a handle
        self.status = {}
        self.chandle = ctypes.c_int16()
        
        # Opens the device/s
        self.status["openunit"] = ps.ps3000aOpenUnit(ctypes.byref(self.chandle), None)
        self.status["openunit"]=286
        try:
            assert_pico_ok(self.status["openunit"])
        except:
        
            # powerstate becomes the status number of openunit
            powerstate = self.status["openunit"]
        
            # If powerstate is the same as 282 then it will run this if statement
            if powerstate == 282:
                # Changes the power input to "PICO_POWER_SUPPLY_NOT_CONNECTED"
                self.status["ChangePowerSource"] = ps.ps3000aChangePowerSource(self.chandle, 282)
            # If the powerstate is the same as 286 then it will run this if statement
            elif powerstate == 286:
                # Changes the power input to "PICO_USB3_0_DEVICE_NON_USB3_0_PORT"
                self.status["ChangePowerSource"] = ps.ps3000aChangePowerSource(self.chandle, 286)
            else:
                raise
        
            assert_pico_ok(self.status["ChangePowerSource"])
           
            # self.daq = self.chandle
            

    def set_value(self,what,value):
       
        # print(self.chandle)
        
        offsetVoltage = 0
        # ampli_Vpp=0.12 #vpp
        # pkToPk=int(ampli_Vpp*1000000) #conversion
        wavetype = ctypes.c_int16(0) # PS3000A_SINE
        # startFrequency = 50000 # Hz
        stopFrequency = 500000
        increment = 0
        dwellTime = 1
        sweepType =ctypes.c_int32(0) #ctypes.c_int16(1) = PS3000A_UP
        operation = 0
        shots = 0
        sweeps = 0
        triggertype = ctypes.c_int32(0)# triggerType = ctypes.c_int16(0) = PS3000A_SIGGEN_RISING
        triggerSource = ctypes.c_int32(0)# triggerSource = ctypes.c_int16(0) = P3000A_SIGGEN_NONE
        extInThreshold = 1
        
        if 'frequency' in what:
            self.startFrequency=value #set the frequency modulation
            if self.flag == True:
                self.status["SetSigGenBuiltIn"] = ps.ps3000aSetSigGenBuiltIn(self.chandle, offsetVoltage,self.pkToPk, wavetype, self.startFrequency, stopFrequency, increment, dwellTime , sweepType,operation, shots, sweeps, triggertype, triggerSource, extInThreshold)
                # print(self.status)
                # print(self.pkToPk/1000000)
                # print(self.startFrequency)
                assert_pico_ok(self.status["SetSigGenBuiltIn"])
            
            # print(value)
        if 'amplitude' in what:
            ampli_Vpk=value
            ampli_Vpp=ampli_Vpk*2 #vpp
            self.pkToPk=int(ampli_Vpp*1000000) #conversionvalue))
            if self.flag == True:
                self.status["SetSigGenBuiltIn"] = ps.ps3000aSetSigGenBuiltIn(self.chandle, offsetVoltage,self.pkToPk, wavetype, self.startFrequency, stopFrequency, increment, dwellTime , sweepType,operation, shots, sweeps, triggertype, triggerSource, extInThreshold)
                # print(self.status)
                # print(self.pkToPk/1000000)
                # print(self.startFrequency)
                assert_pico_ok(self.status["SetSigGenBuiltIn"])
          
      
        if 'on' in what:
            if type(value) is not int: # 24/05/2022 replace in by is to avoid pb with 'harmonic'
                if value == True:
                    self.flag = True
                    self.status["SetSigGenBuiltIn"] = ps.ps3000aSetSigGenBuiltIn(self.chandle, offsetVoltage,self.pkToPk, wavetype, self.startFrequency, stopFrequency, increment, dwellTime , sweepType,operation, shots, sweeps, triggertype, triggerSource, extInThreshold)
                    # print(self.status)
                    # print(self.pkToPk/1000000)
                    # print(self.startFrequency)
                    # print(value)
                    assert_pico_ok(self.status["SetSigGenBuiltIn"])
                  
                if value == False: 
                    self.status["SetSigGenBuiltIn"] = ps.ps3000aSetSigGenBuiltIn(self.chandle, offsetVoltage,0, wavetype, 0.1, stopFrequency, increment, dwellTime , sweepType,operation, shots, sweeps, triggertype, triggerSource, extInThreshold)
                    # print(self.status)
                    # print(self.pkToPk/1000000)
                    # print(self.startFrequency)
                    # print(value)
                    assert_pico_ok(self.status["SetSigGenBuiltIn"])
                    self.flag = False

        

    def disconnect(self): 
        self.status["close"] = ps.ps3000aCloseUnit(self.chandle)
        print(self.status)
        assert_pico_ok(self.status["close"])
        
        
        
    def poll(self,poll_length,x=None):
        pass
    
    
    
####( version with on as connected button)


#### note that in parameter tree 'on':True
                
                
# import ctypes
# from picosdk.ps3000a import ps3000a as ps
# import picosdk.discover  as discover
# import time
# from picosdk.functions import assert_pico_ok



# class Picoscope_3207B():
   
#     def __init__(self):
#         # self.daq = None
#         self.startFrequency=10e3#hz
#         self.pkToPk=int(30e-3*1000000)
        
#         self.connect() 
        
        
#         self.flag = True 
      
        
#     def connect(self):  
        
#         self.flag = True  # Flag to control clearing on start
#         # Gives the device a handle
#         self.status = {}
#         self.chandle = ctypes.c_int16()
        
#         # Opens the device/s
#         self.status["openunit"] = ps.ps3000aOpenUnit(ctypes.byref(self.chandle), None)
#         self.status["openunit"]=286
#         try:
#             assert_pico_ok(self.status["openunit"])
#         except:
        
#             # powerstate becomes the status number of openunit
#             powerstate = self.status["openunit"]
        
#             # If powerstate is the same as 282 then it will run this if statement
#             if powerstate == 282:
#                 # Changes the power input to "PICO_POWER_SUPPLY_NOT_CONNECTED"
#                 self.status["ChangePowerSource"] = ps.ps3000aChangePowerSource(self.chandle, 282)
#             # If the powerstate is the same as 286 then it will run this if statement
#             elif powerstate == 286:
#                 # Changes the power input to "PICO_USB3_0_DEVICE_NON_USB3_0_PORT"
#                 self.status["ChangePowerSource"] = ps.ps3000aChangePowerSource(self.chandle, 286)
#             else:
#                 raise
        
#             assert_pico_ok(self.status["ChangePowerSource"])
           
#             # self.daq = self.chandle
            

#     def set_value(self,what,value):
       
#         # print(self.chandle)
        
#         offsetVoltage = 0
#         # ampli_Vpp=0.12 #vpp
#         # pkToPk=int(ampli_Vpp*1000000) #conversion
#         wavetype = ctypes.c_int16(0) # PS3000A_SINE
#         # startFrequency = 50000 # Hz
#         stopFrequency = 90000
#         increment = 0
#         dwellTime = 1
#         sweepType =ctypes.c_int32(0) #ctypes.c_int16(1) = PS3000A_UP
#         operation = 0
#         shots = 0
#         sweeps = 0
#         triggertype = ctypes.c_int32(0)# triggerType = ctypes.c_int16(0) = PS3000A_SIGGEN_RISING
#         triggerSource = ctypes.c_int32(0)# triggerSource = ctypes.c_int16(0) = P3000A_SIGGEN_NONE
#         extInThreshold = 1
        
#         if 'frequency' in what:
#             self.startFrequency=value #set the frequency modulation
#             if self.flag == True:
#                 self.status["SetSigGenBuiltIn"] = ps.ps3000aSetSigGenBuiltIn(self.chandle, offsetVoltage,self.pkToPk, wavetype, self.startFrequency, stopFrequency, increment, dwellTime , sweepType,operation, shots, sweeps, triggertype, triggerSource, extInThreshold)
#                 print(self.status)
#                 print(self.pkToPk/1000000)
#                 print(self.startFrequency)
#                 assert_pico_ok(self.status["SetSigGenBuiltIn"])
            
#             # print(value)
#         if 'amplitude' in what:
#             ampli_Vpk=value
#             ampli_Vpp=ampli_Vpk*2 #vpp
#             self.pkToPk=int(ampli_Vpp*1000000) #conversionvalue))
#             if self.flag == True:
#                 self.status["SetSigGenBuiltIn"] = ps.ps3000aSetSigGenBuiltIn(self.chandle, offsetVoltage,self.pkToPk, wavetype, self.startFrequency, stopFrequency, increment, dwellTime , sweepType,operation, shots, sweeps, triggertype, triggerSource, extInThreshold)
#                 print(self.status)
#                 print(self.pkToPk/1000000)
#                 print(self.startFrequency)
#                 assert_pico_ok(self.status["SetSigGenBuiltIn"])
          
      
#         if 'on' in what:
#             if type(value) is not int: # 24/05/2022 replace in by is to avoid pb with 'harmonic'
#                 if value == True:
#                     if self.flag == False:
#                         self.connect()
#                     self.status["SetSigGenBuiltIn"] = ps.ps3000aSetSigGenBuiltIn(self.chandle, offsetVoltage,self.pkToPk, wavetype, self.startFrequency, stopFrequency, increment, dwellTime , sweepType,operation, shots, sweeps, triggertype, triggerSource, extInThreshold)
#                     # print(self.status)
#                     # print(self.pkToPk/1000000)
#                     # print(self.startFrequency)
#                     assert_pico_ok(self.status["SetSigGenBuiltIn"])
                  
#                 if value == False:
                    
#                     self.disconnect() 

#         if 'Lost' in what:
#             pass

          
                    


#     def disconnect(self): 
#         self.flag = False
#         self.status["close"] = ps.ps3000aCloseUnit(self.chandle)
#         print(self.status)
#         assert_pico_ok(self.status["close"])
        
        
        
#     def poll(self,poll_length,x=None):
#         pass

if __name__ == "__main__":

    
    pico = Picoscope_3207B()

    pico.set_value('frequency',32000)
    pico.set_value('amplitude',0.10)
    pico.set_value('on',False)
    time.sleep(5)
    pico.set_value('on',True)
    time.sleep(5)
    pico.set_value('on',False)
    pico.set_value('frequency',20000)
    pico.set_value('amplitude',0.2)
    pico.set_value('on',True)
  
    time.sleep(10)
    # pico.set_value('on',True)
    # time.sleep(5)
    # pico.set_value('on',False)
    # pico.set_value('connection',False)
    # pico.set_value('on',True)
    # pico.set_value('frequency',35000)
    # pico.set_value('amplitude',0.2)
    # time.sleep(10)
    # pico.set_value('on',False)
    # pico.disconnect()
    # pico.set_value('frequency',32000)
    # pico.set_value('amplitude',0.70)
    # time.sleep(10)
    # pico.set_value('frequency',50000)
    # pico.set_value('amplitude',0.30)
    # pico.set_value('on',True)
    # time.sleep(10)
    # pico.set_value('on',off)
    # pico.set_value('amplitude',0.30)
    # pico.disconnect()
    # # time.sleep(10)
    # # pico.set_value('frequency',900)
    # time.sleep(30)
    pico.disconnect()

