# -*- coding: utf-8 -*-
"""
Created on Wed Jul 10 10:42:14 2024

@author: rimed
"""

# -*- coding: utf-8 -*-
"""
Created on Fri May 31 15:10:23 2024

@author: user1
"""

import sys
import os
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QLineEdit, QVBoxLayout, QWidget, QFileDialog, QHBoxLayout, QComboBox, QCheckBox, QSpinBox
import numpy as np
import pyvisa
import time
import csv
from pyqtgraph.exporters import ImageExporter
import pyqtgraph as pg

class Stanford_SR860:
    
    def __init__(self):
        
        self.instrument = self.connect() 


    def connect(self):
        
        rm = pyvisa.ResourceManager('C:/Windows/System32/visa32.dll')
        rm.list_resources()
        
        try:
                inst = rm.open_resource('USB0::0xB506::0x2000::005323::INSTR') #USB
        except:
                inst = rm.open_resource('GPIB0::4::INSTR')  #gpib
               
        return inst
        
        
        

    def set_value(self, what, value):
        """Set the value for the given parameter on the SR860"""
        try:
            if 'frequency' in what:
                value = max(1e-6, min(500e3, value))
                self.instrument.write(f"FREQ {value:.6f}")
            elif 'amplitude' in what:
                value = max(1e-9, min(2.0, value))
                self.instrument.write(f"SLVL {value:.3f}")
            elif 'sensitivity' in what:
                sensitivity_indices = {
                   1.0: 0, 0.5: 1, 0.2: 2, 0.1: 3, 0.05: 4, 0.02: 5, 0.01: 6, 0.005: 7, 0.002: 8, 0.001: 9,
                   0.0005: 10, 0.0002: 11, 0.0001: 12, 0.00005: 13, 0.00002: 14, 0.00001: 15, 0.000005: 16, 0.000002: 17, 0.000001: 18,
                   0.0000005: 19, 0.0000002: 20, 0.0000001: 21, 0.00000005: 22, 0.00000002: 23, 0.00000001: 24, 0.000000005: 25, 0.000000002: 26, 0.000000001: 27
               }
                sensitivity_index = sensitivity_indices.get(value)
    
                self.instrument.write(f"SCAL {sensitivity_index}")
                
            elif 'time constant' in what:
                time_constant_indices = {
                   0.000001: 0, 0.000003: 1, 0.00001: 2, 0.00003: 3, 0.0001: 4, 0.0003: 5, 0.001: 6, 0.003: 7, 0.01: 8, 0.03: 9,
                   0.1: 10, 0.3: 11, 1.0: 12, 3.0: 13, 10.0: 14, 30.0: 15, 100.0: 16, 300.0: 17, 1000.0: 18,
                   3000.0: 19, 10000: 20, 30000: 21
               }
                time_constant_index = time_constant_indices.get(value)
        
                self.instrument.write(f"OFLT {time_constant_index}")
            elif 'harmonic' in what:
                value = int(value)
                if 1 <= value <= 99:
                    self.instrument.write(f"HARM {value}")
                else:
                    raise ValueError("Harmonic value out of range")
            elif 'filter order' in what:
                filter_indices = {
                   6.0: 0, 12.0: 1, 18.0: 2, 24.0: 3
               }
                filter_index = filter_indices.get(value)
                self.instrument.write(f"OFSL {filter_index}")
            else:
                raise ValueError(f"Unknown parameter: {what}")
        except Exception as e:
            print(f"Failed to set {what} to {value}: {e}")

    def poll(self,poll_length,x=None):
        poll_timeout = 0
        x_value = float(self.instrument.query("OUTP? 0"))
        y_value = float(self.instrument.query("OUTP? 1"))
        r_value = float(self.instrument.query("OUTP? 2"))
        Theta_value = float(self.instrument.query("OUTP? 3"))
        
        x = np.mean(x_value)
        y = np.mean(y_value)
        r =np.mean(r_value)
        phi =np.mean(Theta_value)
        # r = np.abs(x + 1j*y)
        # phi = np.angle(x + 1j*y,deg=True)
        return [x,y,r,phi]
