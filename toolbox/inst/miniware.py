from mhs5200 import MHS5200
# pip install git+https://github.com/dapperfu/python_mhs5200.git#egg=mhs5200
import mhs5200.enums as WAVE
import time

class Miniware:
    daq = None
    
    def __init__(self):
        # self.port="COM3"
        # self.port="COM8"
        self.port="COM13"
        if not Miniware.daq:
            Miniware.daq = self.connect()
            self.flag = True
            
            
            self.daq.chan1 = self.daq.channels[0]
            self.daq.chan1.offset = 0  # V
            self.daq.chan1.wave = WAVE.SINE
            self.daq.chan1.frequency = 10e3#hz
            self.daq.chan1.amplitude = 2*10*30e-3#Vpp facteur 10 necessaire 
            
            self.daq.chan2 = self.daq.channels[1]
            self.daq.chan2.offset = 0  # V
            self.daq.chan2.wave = WAVE.SINE
            self.daq.chan2.frequency = 10e3#hz
            self.daq.chan2.amplitude = 2*10*30e-3#Vpp facteur 10 necessaire 
    def connect(self):
        signal_gen = MHS5200(self.port)
        self.flag = True
        return signal_gen

    def disconnect(self):
        if self.flag == True:
            Miniware.daq.off()
        Miniware.daq.serial.close()
        print("miniware is closed")
        Miniware.daq =False
        
class Miniware_CH1(Miniware):

    def set_value(self, what, value):

        if 'frequency' in what:
            self.daq.chan1.frequency = value  # set the frequency modulation
           

        if 'amplitude' in what:
            ampli_Vpk = value
            ampli_Vpp = ampli_Vpk*10 * 2  #Vpp facteur 10 necessaire 
            self.daq.chan1.amplitude = ampli_Vpp
            
            
        if 'on' in what:
    
            if type(value) is not int: 
                if value == True:
                    self.daq.on()
                    self.daq.chan1.offset = 0  # V
                    self.daq.chan1.wave = WAVE.SINE   
                    self.flag = True
                if value == False: 
                    self.daq.off()
                    self.flag = False
        
 
    def disconnect(self):
        Miniware.disconnect(self)


class Miniware_CH2(Miniware):
  
    def set_value(self, what, value):
        
        if 'frequency' in what:
            self.daq.chan2.frequency = value  # set the frequency modulation
            
        if 'amplitude' in what:
            ampli_Vpk = value
            ampli_Vpp = ampli_Vpk*10 * 2  #Vpp facteur 10 necessaire 
            self.daq.chan2.amplitude = ampli_Vpp
            

    def disconnect(self):
        pass

# Usage example:
if __name__ == "__main__":
   
    mini_ch1 = Miniware_CH1()
    
#     mini_ch1.set_value('frequency', 3000)
#     mini_ch1.set_value('amplitude', 0.2)
#     mini_ch1.set_value('on', True)
#     time.sleep(10)
#     mini_ch1.set_value('on', False)
#     time.sleep(10)
    
    
#     mini_ch2 = Miniware_CH2()
#     mini_ch2.set_value('frequency', 32000)
#     mini_ch2.set_value('amplitude', 0.1)
#     time.sleep(10)
    
    
#     mini_ch1.disconnect()
#     time.sleep(10)
#     mini_ch1 = Miniware_CH1()
#     mini_ch1.set_value('frequency', 4000)
#     mini_ch1.set_value('amplitude', 0.3)
#     mini_ch1.set_value('on', True)
    # mini_ch1.set_value('on', True)
    # time.sleep(10)
    # mini_ch1.disconnect()

    # mini_ch1.set_value('on', True)
    # time.sleep(10)
    # mini_ch1.set_value('frequency', 5000)
    # mini_ch1.set_value('amplitude', 0.3)
    # mini_ch1.set_value('on', False)
    
    # mini_ch1.set_value('frequency', 1000)
    # mini_ch1.set_value('amplitude', 0.8)
    # time.sleep(10)
    # mini_ch1.set_value('disconnect', False)
    
    
    
    # mini_ch1.set_value('disconnect', True)
    
    # mini_ch2 = Miniware_CH2()
    # mini_ch2.set_value('frequency', 25000)
    # mini_ch2.set_value('amplitude', 0.01)
    
    # time.sleep(5)
    
    
    
    # mini_ch1 = Miniware_CH1()
    # mini_ch1.set_value('frequency', 3000)
    # mini_ch1.set_value('amplitude', 0.8)
    # time.sleep(5)
    # mini_ch1.set_value('disconnect', False)
    
    # mini_ch1 = Miniware_CH1()
    # # mini_ch1.set_value('frequency', 3000)
    # # mini_ch1.set_value('amplitude', 0.2)
    # mini_ch1.set_value('on', True)
    # time.sleep(30)
    # mini_ch1.set_value('on', False)
    # # mini_ch2 = Miniware_CH2()
    # # mini_ch2.set_value('frequency', 1000)
    # # mini_ch2.set_value('amplitude', 0.4)
    # time.sleep(30)
    # mini_ch1.disconnect()
