import numpy as np
import time
import pyvisa

class AmetekLIA():
    def __init__(self):
        self.daq = None
        self.connect()

    def connect(self):
        rm = pyvisa.ResourceManager()
        print('Visa resources (code AmetekLIA) : ',rm.list_resources('?*'))
        self.daq = rm.open_resource('USB0::0x0A2D::0x001B::20211118::RAW')

    def disconnect(self):
        print('Ametek LIA close connection')
        self.daq.before_close()
        return_status = self.daq.close()
        return return_status

    def poll(self,poll_length,x=None):
        _ = poll_length #not use here
        _ = x #not use here
        x = float(self.query('X.')) * np.sqrt(2) #with convertion in Vrms
        y = float(self.query('Y.')) * np.sqrt(2) #with convertion in Vrms
        r = np.abs(x + 1j*y)
        phi = np.angle(x + 1j*y,deg=True)
        return [x,y,r,phi]

    def query(self,cmd):
        def Inst_Query_Command_USB(sCmd):
            inst = self.daq
            print('Ametek LIA code send query command: ' + sCmd)
            inst.write_raw(sCmd)
            sResponse = inst.read()
            # read the status and overload bytes
            nStb = bytes((sResponse[len(sResponse)-2:len(sResponse)-1:]),'utf-8')
            nOvb = bytes((sResponse[len(sResponse)-1:len(sResponse):]),'utf-8')
            nStatusByte = int(nStb[0])
            # mask out bits 4, 5 & 6 which are not consistent across all instruments
            nStatusByte = nStatusByte & 143
            nOverloadByte = int(nOvb[0])
            # strip the returned response of the line feed, status & overload bytes, and 
            # the null terminator
            sResponse = sResponse[0:len(sResponse)-4:]
            # return the response from the instrument, the status byte, and the overload byte
            return sResponse, nStatusByte, nOverloadByte

        def Print_Status_Byte(nStatusByte):
            if (nStatusByte & 1 == 1):
                print('Command Done')
            if (nStatusByte & 2 == 2):
                print('Invalid command')
            if (nStatusByte & 4 == 4):
                print('Command parameter error')
            if (nStatusByte & 8 == 8):
                print('Reference unlock')
            # bits 4, 5 and 6 are instrument model number dependent so are
            # not decoded here
            if (nStatusByte & 128 == 128):
                print('Data Available')

        def Print_72XXOverload_Byte(nOverloadByte):
            if (nOverloadByte & 1 == 1):
                print('X(1) output overload')
            if (nOverloadByte & 2 == 2):
                print('Y(1) output overload')
            if (nOverloadByte & 4 == 4):
                print('X2 output overload')
            if (nOverloadByte & 8 == 8):
                print('Y2 output overload')
            if (nOverloadByte & 16 == 16):
                print('CH1 output overload')
            if (nOverloadByte & 32 == 32):
                print('CH2 output overload')
            if (nOverloadByte & 64 == 64):
                print('CH3 output overload')
            if (nOverloadByte & 128 == 128):
                print('CH4 output overload')

        tReturn = Inst_Query_Command_USB(cmd)
        # decode and print the meaning of the status byte
        Print_Status_Byte(tReturn[1])
        # decode and print the meaning of the overload byte
        Print_72XXOverload_Byte(tReturn[2])
        # if response was generated print it
        if (tReturn[0] != ''):
            print('Command response: ' + tReturn[0])
        return tReturn[0]

    def set_value(self,what,value):
        '''
        change value on Ametek
        '''
        # ATTENTION list should be the same in the parameter tree
        list_sensitivity_LIA = [2e-9,5e-9,10e-9,20e-9,50e-9,100e-9,200e-9,500e-9,
                1e-6,2e-6,5e-6,10e-6,20e-6,50e-6,100e-6,200e-6,500e-6,
                1e-3,2e-3,5e-3,10e-3,20e-3,50e-3,100e-3,200e-3,500e-3,1]
        list_time_constant_LIA = [10e-6,20e-6,50e-6,100e-6,200e-6,500e-6,
                1e-3,2e-3,5e-3,10e-3,20e-3,50e-3,100e-3,200e-3,500e-3,
                1,2,5]
        if 'frequency' in what:
            self.query('IE 0') # reference channel internal=0 ext_rearpanel=1 ext_front=2
            time.sleep(10e-3)
            self.query('OF. '+'{:.4f}'.format(value)) # ATTENTION si trop de decimal AMETEK ne fonctionne pas
        if 'amplitude' in what:
            # value should be in Volts
            amp = int(value*1e6)
            self.query('OA '+str(amp))
        if 'on' in what:
            if type(value) is not int:
                if value == True:
                    print("Ametek ON function doesn't exist yet !")
                if value == False:
                    print("Ametek ON function doesn't exist yet !")    
        if 'sensitivity' in what: 
            X = list_sensitivity_LIA.index(value)+1 
            self.query('IMODE. 0') # 0 for tension 1 for current 2 for current too
            self.query('SEN '+str(X))
        if 'time constant' in what:
            X = list_time_constant_LIA.index(value)
            self.query('NOISEMODE 0') # if noise mode is on TC lies in the range 500us 10ms  
            time.sleep(100e-3)
            self.query('TC '+str(X))     
        if 'harmonic' in what:
            self.query('REFN '+str(value))
        if 'filter order' in what:
            print("Ametek FILTER ORDER function doesn't exist yet !")


if __name__ == "__main__":
    #Call of the classes
    mon_ametek = AmetekLIA()
    #mon_ametek.query('IMODE. 0')
    #æmon_ametek.query('TC 2')
    #mon_ametek.query('NOISEMODE 0')

    mon_ametek.set_value(what='frequency',value=18.1245465396646)
    #mon_ametek.set_value(what='sensitivity',value=1)
    #mon_ametek.set_value(what='harmonic',value=1)
    #mon_ametek.set_value(what='time constant',value=100e-3)
    #mon_ametek.set_value(what='amplitude', value=0.5)

    res = mon_ametek.poll(poll_length=None,x=None)
    print(res)

    mon_ametek.disconnect()


