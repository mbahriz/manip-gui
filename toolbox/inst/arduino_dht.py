
import serial
import time
import serial.tools.list_ports
import numpy as np
import toolbox.RH as H2O

'''code arduino_dht


Le capteur est connecté à la broche 2 de l'Arduino et est défini comme un DHT22
permet de mesurer et de surveiller en temps réel la température et l'humidité 

#include <DHT.h>

#define DHTPIN 2
#define DHTTYPE DHT22  // Change this to DHT11 if you are using DHT11

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  delay(10);  // Delay to wait for the sensor to stabilize

  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  if (!isnan(humidity) && !isnan(temperature)) {
    Serial.print("arduino_dht ");
    Serial.print("H:");
    Serial.print(humidity, 1);  // 1 decimal place
    Serial.print(", T:");
    Serial.println(temperature, 1);  // 1 decimal place
  }
}

'''


class Arduino_DHT:
    TERMINATOR = '\r'.encode('UTF8')  # '\r' is Carriage Return

    def __init__(self):
        self.ser = None
        self.humidity_value = 40
        self.temperature_value = 30
        self.water_conc_value = 0
        self.flag = True

        self.connect()

    def connect(self):
        """Establish connection with the Arduino device."""
        arduino_port = self.find_arduino_port()
        if arduino_port:
            try:
                # Establish the serial connection
                self.ser = serial.Serial(arduino_port, 9600, timeout=1)
                print(f"Connected to Arduino on {arduino_port}")
                time.sleep(2)  # Give Arduino some time to start sending data
            except serial.SerialException as e:
                print(f"Error opening serial port {arduino_port}: {e}")
        else:
            print("Arduino not found.")
    
    def disconnect(self):
        """Close the serial connection."""
        if self.ser:
            self.ser.close()
            self.ser = None
            print("Arduino connection closed.")

    def find_arduino_port(self):
        """Find the correct Arduino serial port based on device description."""
        arduino_ports = [
            p.device
            for p in serial.tools.list_ports.comports()
            if 'Arduino' in p.description or 'VID:PID' in p.hwid
        ]
        return arduino_ports[0] if arduino_ports else None

    def inst_init_values(self):
        """Initialize instrument values (if needed)."""
        pass
    
    

    def poll(self, poll_length=0.003, x=None):
        """Poll data from the Arduino sensor."""
        if not self.ser:
            return None
        
        self.ser.timeout = poll_length  # Set timeout for reading
        try:
            raw_data = self.ser.readline()  # Read data from serial port
            decoded_data = raw_data.decode('utf-8', errors='replace')
            self.data = decoded_data.rstrip()  # Clean data
            
            # Reset timeout to default
            self.ser.timeout = None
            
            if self.data.startswith('arduino_dht') and 'H:' in self.data and 'T:' in self.data:
                # Parse humidity and temperature from the data
                humidity_str, temperature_str = self.data.split(',')
                
                try:
                    check_humidity_value = round(float(humidity_str.split('H:')[1]), 1)
                    check_temperature_value = round(float(temperature_str.split('T:')[1]), 1)
                    
                    # Calculate water concentration using RH toolbox
                    CONC = H2O.Waterconc(check_humidity_value, check_temperature_value)
                    check_water_conc_value = int(CONC.CH2O())
                    
                    # Apply validity checks
                    if check_humidity_value > 100 or check_temperature_value < 10 or check_temperature_value > 100:
                        return None
                    
                    # Only update values if there's a significant change or if it's the first read
                    if self.flag:
                        self.humidity_value = check_humidity_value
                        self.temperature_value = check_temperature_value
                        self.water_conc_value = check_water_conc_value
                        self.flag = False
                        return self.humidity_value, self.temperature_value, self.water_conc_value

                    # Return new values if changes are within acceptable limits
                    if abs(check_humidity_value - self.humidity_value) > 10 or abs(check_temperature_value - self.temperature_value) > 3:
                        return None
                    else:
                        self.humidity_value = check_humidity_value
                        self.temperature_value = check_temperature_value
                        self.water_conc_value = check_water_conc_value
                        return self.humidity_value, self.temperature_value, self.water_conc_value
                except ValueError:
                    pass

        except Exception as e:
            # print(f"Error reading data: {e}")
            return None

    def set_value(self, what, value):
        
        pass


if __name__ == "__main__":
    # Create an instance of the Arduino_DHT class
    mon_PICO = Arduino_DHT()
    R = []

    try:
        while True:
            # Continuously receive data
            R = mon_PICO.poll()
            if R is not None:
                print(R[0], R[1], R[2])  # Print humidity, temperature, and water concentration
            time.sleep(1)  # Add a delay for polling

    except KeyboardInterrupt:
        # Gracefully handle keyboard interrupt
        mon_PICO.disconnect()
