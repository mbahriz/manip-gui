import pyvisa
import time
import numpy as np

'''
GBF
'''

class Tektronix_AFG1062:
    # laser Driver with USB connection
    def __init__(self):
       
        # self.dev = dev_num
 
        self.connect() 

    def connect(self):
        #identification de l'instrument
        rm = pyvisa.ResourceManager('C:/Windows/System32/visa32.dll')
        rm.list_resources()
        # print(rm.list_resources())

        self.daq = rm.open_resource('USB0::0x0699::0x0353::2011056::INSTR')

    def disconnect(self):
        self.daq.close()
    
    
    
class Tektronix_AFG1062_CH1(Tektronix_AFG1062):    

    def set_value(self,what,value):
        inst = self.daq
        inst.write("SOUR1:FUNC SIN")
        if 'on' in what:
            if value == True:
                inst.write("OUTP1:STAT ON")
            if value == False:
                inst.write("OUTP1:STAT OFF")
        if 'amplitude' in what:
            value = value*2 #current given in mA
            inst.write(f"SOUR1:VOLT {value}")
        if 'frequency' in what:
            inst.write(f"SOUR1:FREQ {value}")
            
       
    def disconnect(self):
        Tektronix_AFG1062.disconnect(self)

class Tektronix_AFG1062_CH2(Tektronix_AFG1062):    

    def set_value(self,what,value):
        inst = self.daq
        inst.write("SOUR2:FUNC SIN")
        if 'on' in what:
            if value == True:
                inst.write("OUTP2:STAT ON")
            if value == False:
                inst.write("OUTP2:STAT OFF")
        if 'amplitude' in what:
            value = value*2 #current given in mA
            inst.write(f"SOUR2:VOLT {value}")
        if 'frequency' in what:
            inst.write(f"SOUR2:FREQ {value}")
            
       
    def disconnect(self):
        Tektronix_AFG1062.disconnect(self)

        
if __name__ == "__main__":
    
    tek_ch1 = Tektronix_AFG1062_CH1()
    
    tek_ch1.set_value('frequency', 3000)
    tek_ch1.set_value('amplitude', 0.2)
    tek_ch1.set_value('on', True)
    
    tek_ch2 = Tektronix_AFG1062_CH2()
    
    tek_ch2.set_value('frequency', 5000)
    tek_ch2.set_value('amplitude', 0.4)
    tek_ch2.set_value('on', True)

