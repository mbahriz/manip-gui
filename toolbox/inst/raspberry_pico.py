import serial
import sys
path = '/Users/michaelbahriz/Recherche/Manips/Codes/manip-gui'
sys.path.append(path) 
import toolbox.parameter_tree

##############################################################################
##### Instrument Class
##############################################################################
class PICO_pulse_generator():
    TERMINATOR = '\r'.encode('UTF8')  # '\r' is Carriage Return

    def __init__(self):
        self.daq = None
        self.inst_init_values()
        self.connect()

    def connect(self):
        portx=self.find_PICO()
        bps=9600
        timex=1   # timeout setting, None： waiting for operation forever, 0 to return the result of the request immediately, the other values are waiting timeouts ( in seconds ）
        ser=serial.Serial(portx,bps,timeout=timex)
        self.daq=ser
        print('PICO_pulse_generator is CONNECTED port={} baudrate={}'.format(ser.port,ser.baudrate))

    def disconnect(self):
        self.daq.close()# close the serial port 

    def find_PICO(self):
        def serial_ports_list():
            import serial.tools.list_ports
            port_list = list(serial.tools.list_ports.comports())
            print(port_list)
            if len(port_list) == 0:
                print(' no available serial port ')
            else:
                for i in range(0,len(port_list)):
                    print(port_list[i])
            return port_list
        port_list = serial_ports_list()
        for port, desc, hwid in sorted(port_list):
            print("{}: {} [{}]".format(port, desc, hwid))
            if 'VID:PID=2E8A:0005' in hwid:
                port_PICO = port
            else:
                port_PICO = None
        print('PICO port is : ',port_PICO)
        return port_PICO	

    def inst_init_values(self):
        # should be init here as we need 2 parameters in the same time frequency and duty cycle
        dico = toolbox.parameter_tree.instrument_initials_values('Pulse module')
        self.carrier = dico['carrier frequency']
        self.duty = dico['duty cycle']

    def poll(self,poll_length,x=None):
        pass

    def receive(self) -> str:
        line = self.daq.read_until(self.TERMINATOR)
        return line.decode('UTF8').strip()

    def send(self, text: str) -> bool: # text est du type string et la fonction renvoie un boolean
        line = '%s\r\f' % text
        self.daq.write(line.encode('UTF8'))
        # the line should be echoed.
        # If it isn't, something is wrong.
        return text == self.receive()

    def set_value(self,what,value):
        carrier_modified = False
        if 'carrier' in what: # in Hz
            self.carrier = value
            carrier_modified = True
        if 'duty' in what: # in %
            self.duty = value
            carrier_modified = True
        if carrier_modified:
            print('PICO carrier has CHANGED {} {}'.format(what,str(value)))
            f = str(self.carrier)
            d = str(self.duty)
            echo = self.send('pulse(frequency={},duty_cycle={})'.format(f,d))
            print('PICO pulse() echo : ', echo)
            receive = self.receive()
            print('PICO pulse() receive: ', receive)
            carrier_modified = False

if __name__ == "__main__":
    #Call of the classes
    mon_PICO = PICO_pulse_generator()
    mon_PICO.set_value('carrier', 10)
    mon_PICO.set_value('duty', 35)

'''
# CODE TO COPY ON THE PICO 

# main.py 

# as its name is main.py
# it will automaticaly run on the PICO when it is plugged

from machine import Pin
import utime

# POUR QUE CA MARCHE THONNY DOIT ETRE FERME
# ET AVOIR DEBRANCHE ET REBRANCHE LE PICO

# use onboard LED which is controlled by Pin 25
LED = machine.PWM(machine.Pin(25))
OUT = machine.PWM(machine.Pin(15))

# Turn the LED on
def pulse(frequency, duty_cycle): # feq in Hz duty in %
    convertion = 65535/100
    for gpio in [LED, OUT]:
        gpio.freq(int(frequency))
        gpio.duty_u16(int(duty_cycle*convertion))



'''