import numpy as np

###############################################
#--   RH  humidité relative -> ppmv         --#
###############################################
class Waterconc:
    def __init__(self,RH,T):
        self.RH = RH# taux d'humitité relative indique le pourcentage de vapeur d’eau rapporté au maximum possible[%]
        self.T = T
        
        
    def CH2O(self):  
        self.T = self.T+273.15 #[K]
        P = 101325 #[Pa]
        
        R = 8.3144621 #[J/mol/K]
        Vm = R*self.T/P #volume molaire [m3/mol]
        #print('Volume molaire de  l\'eau {:.4f} m3/mol'.format(Vm))
        
        Mm_H2O = 18.015 # Masse molaire de l'eau [g/mol]
        
        Psat = P*np.exp(13.7-5120/self.T) # pression saturante de l'eau [Pa] REF https://fr.wikipedia.org/wiki/Pression_de_vapeur_saturante
        #print('Psat pression de saturation de l\'eau {:.4f} Pa (modèle 1)'.format(Psat))
        # Psat = 288.68*(1.098+(T-273.15)/100)**8.02 # pression saturante de l'eau [Pa] REF https://enbau-online.ch/bauphysik/fr/3-3-humidite-de-lair-et-pression-de-vapeur-condensation/
        # print('Psat pression de saturation de l\'eau {:.4f} Pa (modèle 2)'.format(Psat))
        
        rho_sat = Mm_H2O * Psat /R /self.T #masse volumique de l'eau à la pression de saturation [g/m3]
        #print('rho_sat masse volumique de l\'eau à la pression de saturation {:.4f} g/m3'.format(rho_sat))
        
        Pvap = Psat*self.RH/100 # pression partielle de l'eau dans l'air [Pa]
        #print('Pvap pression partielle de l\'eau dans l\'air {:.4f} Pa'.format(Pvap))
        
        rho_vap = rho_sat*self.RH/100 #masse volumique de l\'eau à la pression partielle de l\'eau dans l\'air [g/m3]
        #print('rho_vap masse volumique de l\'eau à la pression partielle de l\'eau dans l\'air {:.4f} g/m3'.format(rho_vap))
        
        rho_v = Mm_H2O/Vm # masse volumique de l'eau [g/m3]
        #print('rho_v masse volumique de l\'eau {:.4f} g/m3'.format(rho_v))
        
        C_H2O = rho_vap/rho_v*1e6 # concetration en volume d'eau dans l'air [ppmv]
        #print('C_H2O concetration en volume d\'eau dans l\'air {:.4f} ppmv'.format(C_H2O))
        
        # rho_v = Mm_H2O * Pvap /R /T #masse volumique de l'eau [g/m3]
        # print(rho_v)
        
        # print(rho_v/(Mm_H2O/Vm)*100)
        return C_H2O
        '''
        https://fr.wikipedia.org/wiki/Humidit%C3%A9_relative
        Pour avoir un ordre d’idée, dans la zone de confort (soit environ 20 °C et 50 % d’humidité), une augmentation de 1 °C va provoquer une baisse de 2 à 3 % du taux d’humidité relative et inversement. Ce n’est qu’un ordre d’idée car les relations dans les diagrammes d’humidité sont faites de courbes. Un air à 20 °C et 50 % d'humidité relative contient 8,65 g d'eau par mètre cube ; à la même température mais à 100 % d'humidité relative, il en contient 17,3 g/m3, c'est-à-dire deux fois plus2. 
        
        https://fr.wikipedia.org/wiki/Eau
        
        https://fr.wikipedia.org/wiki/Pression_de_vapeur_saturante
        
        https://fr.wikipedia.org/wiki/Volume_molaire
        
        https://fr.wikipedia.org/wiki/Pression_de_vapeur_saturante_de_l%27eau
        '''