import numpy as np

def noisy_lorentzian_function(freq, amp, freq0, Q):
    '''
    Allow to draw calculted a Lorentzian profil. 
    Return the amplitude of dispacement of a forced excitation,
    and the phase between the resonator displacement and actuation force  
    And add noise on the frequency
    
    Prarameters
    -----------
    freq : Pulsation of the excitation
    amp : Amplitude = Force/m_eff/2Pi     
    freq0 : Resonance frequency
    Q : quality factor      

    Return
    ------
    Return the amplitude of dispacement of a forced excitation,
    and the phase between the resonator displacement and actuation force       

    '''
    # add noise on the frequency
    freq = freq*(1+np.random.normal(0,1/Q/100,1))
    # add noise on the amplitue amp = Force/m_eff/2Pi
    amp = np.random.normal(0,1,1)+10
    # complex lorentzian function
    L = amp/((freq0**2-freq**2)+1J*freq0*freq/Q)
    # Calculate amplitude and phase
    R = np.abs(L)
    Phi = np.angle(L,deg=True)
    return R, Phi

def noisy_lorentzian_function_Capa(freq, amp, freq0, Q, Capa):
    '''
    Allow to draw calculted a Lorentzian profil. 
    Return the amplitude of dispacement of a forced excitation,
    and the phase between the resonator displacement and actuation force  
    And add noise on the frequency
    Add a capacitor Capa in parallel
    
    Prarameters
    -----------
    freq : Pulsation of the excitation
    amp : Amplitude = Force/m_eff/2Pi     
    freq0 : Resonance frequency
    Q : quality factor
    Capa : Capacitor in parallel  (resonator capacitance when it's not moving)    

    Return
    ------
    Return the amplitude of dispacement of a forced excitation,
    and the phase between the resonator displacement and actuation force       

    '''
    # add noise on the frequency
    freq = freq*(1+np.random.normal(0,1/Q/100,1))
    # add noise on the amplitue amp = Force/m_eff/2Pi
    amp = amp + np.random.normal(0,1,1)*amp/10
    # complex lorentzian function
    # Lorentzian expression from Aoust's thesis Amp = F/Meff
    L = amp/((freq0**2-freq**2)+1J*freq0*freq/Q)+Capa*freq*2*np.pi
    # /!\ +Capa*freq*2*np.pi et non pas +1J*Capa*freq*2*np.pi depahsage entre la partie elec et meca demonstraiot navec le modele entiere elec (cf these Roman)
    # Calculate amplitude and phase
    R = np.abs(L)
    Phi = np.angle(L,deg=True)
    return R, Phi

class Virtual():
    def __init__(self):
        self.daq = self.connect()
        self.freq = None
        self.current = None

    def connect(self):
        daq = None
        return daq

    def disconnect(self):
        pass

    def set_value(self,what,value):
        '''
        Give the value 'value' to the parameter 'what' on the instrument
        '''
        print('Virtual instrument : %s has been changed to %s' %(what,str(value)))
        if what in 'frequency':
            self.val = value
        if what in 'current':
            self.val = value
        if what in 'amplitude':
            self.val = value
        if what in 'sensitivity':  
            self.val = value
        if what in 'time constant':
            self.val = value     

    def poll(self,poll_length,x=1):
        # x is used to simulate a Loretzian curve useful only to develop the code and only with the virtual instrument
        # poll_length = recording time
        R = (np.random.normal(0,1,1)+5)*1e-4
        Phi = np.random.normal(-90,90,1)
        X = R*np.cos(Phi)
        Y = R*np.sin(Phi)
        Phi = Phi*180/np.pi # convert phi in degre
        # to simulate a lorentzian curve
        # R, Phi = noisy_lorentzian_function(x, amp=10, freq0=15e3, Q=10000)
        # to simulate a lorentzian curve with a capcitor
        R, Phi = noisy_lorentzian_function_Capa(x, amp=1, freq0=15e3, Q=1000, Capa=5e-12)
        return [X[0],Y[0],R[0],Phi[0]]

    def poll_continuous(self,total_duration,do_plot):
        # x is used to simulate a Loretzian curve useful only to develop the code and only with the virtual instrument
        # poll_length = recording time
        time = np.linspace(0,total_duration,20000)
        R = (np.random.randn(len(time))+5)*1e-4
        my_data = {'Time_R':time,'R':R}
        return my_data