import pyvisa
import time

import serial
import serial.tools.list_ports
import toolbox.RH as H2O
# import RH as H2O

"""code arduino_gpib

Le code présente deux parties distinctes : 
1-un script Arduino pour lire une valeur analogique à partir d'un capteur arduino d'humidité connecté à la broche A0 
2-un autre qui interagit avec un instrument GPIB (appareil de mesure pour la température) en utilisant la commande 'T?', puis lit la température (self.T).

il effectuer des calculs basés sur les données reçues. les détails de calculs sont décrites dans la la fonction poll


script Arduino:


void setup() {
  // Initialize Serial communication
  Serial.begin(9600);
}

void loop() {
  // Read analog value from pin A0
  Serial.print("arduino_gpib ");
  int sensorValue = analogRead(A0);

  // Print the analog value to the Serial Monitor
  Serial.print("Analog Value: ");
  Serial.println(sensorValue);

  // Delay for a short time to make the output readable
  delay(50);
}

"""

import pyvisa
import time
import serial
import serial.tools.list_ports
# import RH as H2O

class Arduino_GPIB:
    TERMINATOR = '\r'  # Carriage Return for serial communication

    def __init__(self, resource_address='GPIB0::8::INSTR'):
        self.daq = None
        self.ser = None
        self.resource_address = resource_address
        self.RH = 0
        self.T = 0
        self.water_conc_value = 0
        self.data = 0
        self.flag = True
        self.connect()

    def connect(self):
        """Establish connection with the Arduino and GPIB device."""
        # Connect to Arduino via serial port
        arduino_port = self.find_arduino_port()
        if arduino_port:
            try:
                self.ser = serial.Serial(arduino_port, 9600, timeout=1)
                print(f"Connected to Arduino on {arduino_port}")
                time.sleep(2)  # Give Arduino time to start sending data
            except serial.SerialException as e:
                print(f"Error opening serial port {arduino_port}: {e}")
        else:
            print("Arduino not found.")
        
        # Connect to GPIB device
        try:
            rm = pyvisa.ResourceManager('C:/Windows/System32/visa32.dll')
            print("Connecting to GPIB device...")
            self.daq = rm.open_resource(self.resource_address)
            print(f"Connected to GPIB device at {self.resource_address}")
        except pyvisa.Error as e:
            print(f"Error connecting to GPIB device: {e}")

    def disconnect(self):
        """Close the serial and GPIB connections."""
        if self.daq:
            self.daq.close()
            print("GPIB connection closed.")
        if self.ser:
            self.ser.close()
            print("Arduino connection closed.")

    def find_arduino_port(self):
        """Find the correct Arduino serial port based on device description."""
        arduino_ports = [
            p.device
            for p in serial.tools.list_ports.comports()
            if 'Arduino' in p.description or 'VID:PID' in p.hwid
        ]
        return arduino_ports[0] if arduino_ports else None

    def poll(self, poll_length=0.003, x=None):
        """Poll data from the Arduino and GPIB sensor."""


        try:
            # Query temperature from GPIB device
            command = 'T?'
            self.data = self.daq.query(command)
            self.T = round(float(self.data.strip()), 2)
            # print(f"Temperature from GPIB device: {self.T} °C")
            
            # Read analog data from Arduino
            self.ser.timeout = poll_length  # Set timeout for reading
            raw_data = self.ser.readline()  # Read data from serial port
            decoded_data = raw_data.decode('utf-8', errors='replace')
            self.data = decoded_data.rstrip()  # Clean data
            
            # Reset timeout to default
            self.ser.timeout = None

            if self.data.startswith('arduino_gpib') and 'Analog Value:' in self.data:
                value_str = self.data.split('Analog Value:')[1].strip()
                try:
                    analog_value = int(value_str)
                    # print(f"Analog Value from Arduino: {analog_value}")

                    # Calculate voltage and relative humidity
                    vout = (analog_value / 1023) * 5
                    RH_p1 = ((vout / 5) - 0.16) / 0.0062
                    RH_p2 = (1.0546 - (self.T * 0.00216))

                    self.RH = round(RH_p1 / RH_p2, 2)

                    # Calculate water concentration
                    self.water_conc_value = self.calculate_water_concentration(self.RH, self.T)

                    # Validity checks
                    if self.RH > 100 or self.RH <= 0 or self.T < 10 or self.T > 100:
                        # print("Invalid data received.")
                        return None

                    # Only update values if there's a significant change or it's the first read
                    if self.flag:
                        self.flag = False
                        return self.RH, self.T, self.water_conc_value

                    # Return new values if changes are within acceptable limits
                    if abs(self.RH - self.RH) > 10 or abs(self.T - self.T) > 3:
                        return None
                    else:
                        return self.RH, self.T, self.water_conc_value

                except ValueError as e:
                   pass

        except Exception as e:
            pass

    def calculate_water_concentration(self, RH, T):
        """Calculate water concentration using relative humidity and temperature."""
        return (RH * T) / 100

    def set_value(self, what, value):
        """Set value for serial port or other parameters."""
        pass

if __name__ == "__main__":
    # Create an instance of the Arduino_GPIB class
    mon_PICO = Arduino_GPIB()
    R = []

    try:
        while True:
            # Continuously receive data from Arduino and GPIB
            R = mon_PICO.poll()
            if R is not None:
                print(f"RH: {R[0]}%, Temp: {R[1]}C, Water Conc: {R[2]} ppm")
            time.sleep(1)  # Add a delay for polling to avoid overloading

    except KeyboardInterrupt:
        # Gracefully handle keyboard interrupt
        mon_PICO.disconnect()
