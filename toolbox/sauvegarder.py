import time
import os
import pandas
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import toolbox.function
import toolbox.wiki
from scipy import interpolate

def load_mpl_params():
    # MATPLOTLIB PARAMETERS
    mpl.rcParams.update(mpl.rcParamsDefault)
    # mpl.rcParamsDefault.keys --> display all parameters available
    params = {'legend.fontsize': 10, #ATTENTION usually 16
            'figure.figsize': (10, 8),
            'axes.labelsize': 18,
            'axes.titlesize': 18,
            'xtick.labelsize': 14,#ATTENTION usually 18
            'ytick.labelsize': 14,#ATTENTION usually 18
            'figure.subplot.left': 0.15,
            'figure.subplot.right': 0.85,
            'figure.subplot.bottom': 0.15,
            'figure.subplot.top': 0.85,
            'xtick.direction': 'in',
            'ytick.direction': 'in',
            'xtick.major.size': 5,
            'xtick.major.width': 1.3,
            'xtick.minor.size': 3,
            'xtick.minor.width': 1,
            'xtick.major.pad': 8,
            'ytick.major.pad': 8,
            'lines.linewidth': 1,
            'axes.grid': True,
            'axes.grid.axis': 'both',
            'axes.grid.which': 'both',
            'grid.alpha': 0.5,
            'grid.color': '111111',
            'grid.linestyle': '--',
            'grid.linewidth': 0.8,
            'savefig.dpi': 300, }
    mpl.rcParams.update(params)  

class Save():
    def __init__(self,what,curve1,curve2=None):
        self.origin = what
        self.courbe1 = curve1
        self.courbe2 = curve2
        self.create_curve_list() # create attribut courbe1_list/courbe1_list/courbe1_prefix
        # sauvegarde
        self.file_name = []
        self.path = self.fabricate_path()
        self.create_path()
        self.update_legend_for_all_curve()
        for num_curve in range(0,len(self.courbe1_list)):
            self.file_name.append(self.fabricate_file_name(num_curve))
            self.save_param(num_curve)
            self.save_data(num_curve)
        self.save_graph()
        self.dokuwiki()

    def clean_up_curve_list(self,curve_list):
        # remove fit curve
        for k in curve_list:
            if 'fit' in k:
                curve_list.remove(k)
        # remove the last ghost which is empty
        curve_list = curve_list[:-1] 
        # move 'curve acq' at the end
        if 'Continous Acquisition' not in self.origin:  
                # move 'curve acq' at the end
                curve_list.remove('curve acq')
        curve_list.append('curve acq')
        return curve_list

    def create_curve_list(self):
        curve_list = list(self.courbe1.keys())
        self.courbe1_list = self.clean_up_curve_list(curve_list)
        if self.courbe2 != None:
            curve_list = list(self.courbe2.keys())
            self.courbe2_list = self.clean_up_curve_list(curve_list)
        self.create_curve_prefix()            

    def create_curve_prefix(self):
        myList = self.courbe1_list
        A = np.linspace(1,len(myList)+1,len(myList)+1)
        if 'Allan deviation' in self.origin:
            self.courbe1_prefix = ['AD{:0>2}'.format(int(k)) for k in A]
        if 'Amplitude of modulation' in self.origin:
            self.courbe1_prefix = ['AOM{:0>2}'.format(int(k)) for k in A]
        if 'Continous Acquisition' in self.origin:
            self.courbe1_prefix = ['CA{:0>2}'.format(int(k)) for k in A]            
        if 'Electrical characterization' in self.origin:
            self.courbe1_prefix = ['EC{:0>2}'.format(int(k)) for k in A]
        if 'Frequency sweep' in self.origin:
            self.courbe1_prefix = ['FS{:0>2}'.format(int(k)) for k in A]
        if 'Laser PV2I' in self.origin:
            self.courbe1_prefix = ['PV2I{:0>2}'.format(int(k)) for k in A]
        if 'Photoacoustic' in self.origin:
            self.courbe1_prefix = ['PA{:0>2}'.format(int(k)) for k in A]

    def create_path(self):
        # if don't exist create the directory
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def dokuwiki(self):
        curve_name = 'curve acq'
        parameters = self.courbe1[curve_name]['data']['parameters']
        print('path : ',self.path)
        file_name = self.fabricate_title_name().replace(self.path,'')
        print('file_name : ',file_name)
        test = parameters['Save']['dokuwiki'] # readl value (True or False) of dokuwiki checkbox
        if test:
            print('** DoKuWiKi \n')
            adresse, titre = self.dokuwiki_manip_param()
            super_dico = []
            if 'Allan deviation' in self.origin:
                # GRAPH
                link1 = toolbox.wiki.upload_media(adresse, self.path, file_name+'_ALLAN.png')
                link2 = toolbox.wiki.upload_media(adresse, self.path, file_name+'_TIME.png')
                link = link1 + link2 
                # RESULTS
                for num_curve in range(0,len(self.courbe1_list)):
                    super_dico.append(self.dokuwiki_info_allan(num_curve))
            if 'Amplitude of modulation' in self.origin:
                # GRAPH
                link = toolbox.wiki.upload_media(adresse, self.path, file_name+'_MAG.png') 
                # RESULTS
                for num_curve in range(0,len(self.courbe1_list)):
                    super_dico.append(self.dokuwiki_info_amp_mod(num_curve)) 
                    
                
            if 'Electrical characterization' in self.origin:
                # GRAPH
                link = toolbox.wiki.upload_media(adresse, self.path, file_name+'_ADM.png')
                # RESULTS
                for num_curve in range(0,len(self.courbe1_list)):
                    super_dico.append(self.dokuwiki_info_electrical_characterization(num_curve))
            if 'Frequency sweep' in self.origin:
                # GRAPH
                link1 = toolbox.wiki.upload_media(adresse, self.path, file_name+'_MAG.png')
                #link2 = toolbox.wiki.upload_media(adresse, self.path, file_name+'_PHASE.png')
                link = link1 #+ link2 
                # RESULTS
                for num_curve in range(0,len(self.courbe1_list)):
                    super_dico.append(self.dokuwiki_info_frequency_sweep(num_curve))
            if 'Photoacoustic' in self.origin:
                # GRAPH
                link = toolbox.wiki.upload_media(adresse, self.path, file_name+'_MAG.png') 
                # RESULTS
                for num_curve in range(0,len(self.courbe1_list)):
                    super_dico.append(self.dokuwiki_info_photoacoustic(num_curve))
            # FILE
            for num_curve in range(0,len(self.courbe1_list)):
                name = self.file_name[num_curve]
                print('name :', name)
                prefixe = self.courbe1_prefix[num_curve]
                link = link + toolbox.wiki.upload_file(adresse, self.path, name+'.csv',prefixe)
                link = link + toolbox.wiki.upload_file(adresse, self.path, name+'.txt',prefixe)
            # final text
            wiki_table = toolbox.wiki.superdico_to_dokuwiki_table(super_dico)
            texte = titre + link + '\n' + wiki_table + '\n  * **conclusion : ** \n'
            # update wiki
            toolbox.wiki.update_wiki_text(adresse,texte)
    
    def dokuwiki_manip_param(self):
        curve_name = 'curve acq'
        parameters = self.courbe1[curve_name]['data']['parameters']
        path_info = parameters['Save']['path info'] + '\n'
        text_dscpt = parameters['Save']['setup description'] + '\n'
        design = parameters['Save']['design']
        resonator = parameters['Save']['resonator']
        manip_param = {'laser':parameters['Save']['laser'],
                    'amplifier':parameters['Save']['amplifier'],
                    'gas':parameters['Save']['gas'],
                    'concentration':parameters['Save']['concentration'],
                    'acoustic cavities':parameters['Save']['acoustic cavities']}
        # remove key if the value is 'Unknow'
        manip_param = toolbox.function.remove_dico_entry(manip_param, 'Unknow')
        # dico to dokuwiki table
        wiki_param = toolbox.wiki.dataframe_to_dokuwiki_table(pandas.DataFrame(manip_param, index=[0])) + '\n'            
        try:
            device = parameters['Save']['device']
        except:
            device = ''
        adress = ':'.join((design,resonator))
        today = time.strftime("%Y%m%d (%H:%M:%S)")
        path_info = '  * **objectif :** ' + path_info
        text_dscpt  = '  * **comments : **' + text_dscpt + '\n'    
        title = '==== ' + ' - '.join((today,self.origin,device)) + ' ====\n'+ path_info + text_dscpt + wiki_param + '\n'
        return adress, title 

    def dokuwiki_info_allan(self,num):
        curve_name = self.courbe1_list[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        # dico
        file_info = {}
        # context
        file_info['prefix'] = self.courbe1_prefix[num]
        file_info['legend'] = parameters['Graph']['legend curve '+str(num+1)]
        # param manip
        file_info['harmonic'] = '{:.0f}f'.format(parameters['Input']['harmonic'])
        file_info['acq. time'] = '{:.0f} min'.format(parameters['Allan Deviation']['acq. time'])
        # results manip
        file_info['SNR'] = '{:.0f}'.format(self.courbe1[curve_name]['data']['SNR'])
        file_info['mean'] = '{:.0f}uVrms'.format(self.courbe1[curve_name]['data']['Mean']*1e6)
        file_info['LOD 1s'] = '{:.0f}ppmv'.format(self.courbe1[curve_name]['data']['LOD 1s'])
        file_info['LOD 10s'] = '{:.0f}ppmv'.format(self.courbe1[curve_name]['data']['LOD 10s'])
        return file_info

    def dokuwiki_info_amp_mod(self,num):
        curve_name = self.courbe1_list[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        # dico
        file_info = {}
        # context
        file_info['prefix'] = self.courbe1_prefix[num]
        file_info['legend'] = parameters['Graph']['legend curve '+str(num+1)]
        # param manip
        file_info['harmonic'] = '{:.0f}f'.format(parameters['Input']['harmonic'])
        return file_info

    def dokuwiki_info_electrical_characterization(self,num):
        curve_name = self.courbe1_list[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        # dico
        file_info = {}
        # context
        file_info['prefix'] = self.courbe1_prefix[num]
        file_info['legend'] = parameters['Graph']['legend curve '+str(num+1)]
        # param manip
        file_info['harmonic'] = '{:.0f}f'.format(parameters['Input']['harmonic'])
        R_fit = self.courbe1[curve_name+' fit']['data']['Admittance']
        if len(R_fit) > 1:
            file_info['f0_fit (kHz)'] = '{:.3f}'.format(self.courbe1[curve_name+' fit']['data']['f0']*1e-3)
            file_info['Q_fit'] = '{:.0f}'.format(self.courbe1[curve_name+' fit']['data']['Q'])
            file_info['C0_fit (pF)'] = '{:.3f}'.format(self.courbe1[curve_name+' fit']['data']['C0']*1e12)
        else:
            file_info['f0_fit (kHz)'] = 'NaN'
            file_info['Q_fit'] = 'NaN'
            file_info['C0_fit (pF)'] = 'NaN'         
        return file_info
           
    def dokuwiki_info_frequency_sweep(self,num):
        curve_name = self.courbe1_list[num]   
        parameters = self.courbe1[curve_name]['data']['parameters']
        # dico
        file_info = {}
        # context
        file_info['prefix'] = self.courbe1_prefix[num]
        file_info['legend'] = parameters['Graph']['legend curve '+str(num+1)]
        # param manip
        file_info['harmonic'] = '{:.0f}f'.format(parameters['Input']['harmonic'])
        file_info['f0 (kHz)'] = '{:.3f}'.format(self.courbe1[curve_name]['data']['f0']*1e-3)
        #file_info['freq'] = '%.0f-%.0fkHz'%(parameters['Acquisition']['freq min']/1e3,parameters['Acquisition']['freq max']/1e3)
        # fit
        R_fit = self.courbe1[curve_name+' fit']['data']['R']
        if len(R_fit) > 1:
            file_info['f0_fit (kHz)'] = '{:.3f}'.format(self.courbe1[curve_name+' fit']['data']['f0']*1e-3)
            #file_info['f0_sigma_fit'] = self.courbe1[curve_name+' fit']['data']['f0 sigma']
            file_info['Q_fit'] = '{:.0f}'.format(self.courbe1[curve_name+' fit']['data']['Q'])
            #file_info['Q_sigma_fit'] = self.courbe1[curve_name+' fit']['data']['Q sigma']
            #file_info['C0_fit'] = self.courbe1[curve_name+' fit']['data']['C0']
        else:
            file_info['f0_fit (kHz)'] = 'NaN'
            file_info['Q_fit'] = 'NaN'
        return file_info    

    def dokuwiki_info_photoacoustic(self,num):
        curve_name = self.courbe1_list[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        # dico
        file_info = {}
        # context
        file_info['prefix'] = self.courbe1_prefix[num]
        file_info['legend'] = parameters['Graph']['legend curve '+str(num+1)]
        # param manip
        file_info['harmonic'] = '{:.0f}f'.format(parameters['Input']['harmonic'])
        return file_info

    def fabricate_file_name(self,num):
        curve_name = self.courbe1_list[num]
        prefix = self.courbe1_prefix[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        legend = parameters['Graph']['legend curve '+str(num+1)]
        try:
            device = parameters['Save']['device']
        except:
            device = ''
        if legend != '':
            legend = '_'+legend
        if 'Allan deviation' in self.origin:
            acq_time = '%.0fmin'%(parameters['Allan Deviation']['acq. time'])
            concentration = '%.0fppmv'%(parameters['Allan Deviation']['concentration'])
            time_cst = '%.0fms'%(parameters['Input']['time constant']*1e3)
            temperature = '{:.0f}C'.format(parameters['Laser Driver']['temperature'])
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            try:
                amplitude = '{:.0f}mV'.format(parameters['Generator']['amplitude']*1e3)
            except:
                amplitude=''  
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3) 
            current = '{:.0f}mA'.format(parameters['Laser Driver']['current'])   
            #Mean = '{:.0f}mVrms'.format(self.courbe1[curve_name]['data']['Mean']*1e3)
            signal_to_noise = 'SNR{:.0f}'.format(self.courbe1[curve_name]['data']['SNR'])            
            file_name = self.path + prefix + legend + '_'  + '_'.join([device,harmonic,signal_to_noise,acq_time,concentration,
                                time_cst,current,amplitude,frequency,temperature])
        if 'Amplitude of modulation' in self.origin:
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            amp = '%.0f-%.0fmVpk'%(parameters['Generator']['amplitude min']*1e3,parameters['Generator']['amplitude max']*1e3) 
            temperature = '{:.0f}C'.format(parameters['Laser Driver']['temperature']) 
            current = '{:.0f}mA'.format(parameters['Laser Driver']['current']) 
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3)     
            file_name = self.path + prefix + legend + '_'  + '_'.join([device,frequency,harmonic,amp,current,temperature]) 
            
        if 'Continous Acquisition' in self.origin:
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3)
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            time_cst = '%.0fms'%(parameters['Input']['time constant']*1e3)
            file_name = self.path + prefix + legend + '_' +'_'.join([harmonic,frequency,time_cst])     

        if 'Electrical characterization' in self.origin:
            ampli = '{}'.format(parameters['Amplifier']['amplifier'])
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            freq = '%.0f-%.0fkHz'%(parameters['Acquisition']['freq min']/1e3,parameters['Acquisition']['freq max']/1e3)   
            file_name = self.path + prefix + legend + '_' +'_'.join([device,harmonic,freq,ampli])
        if 'Frequency sweep' in self.origin:
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            freq0 = '{:.0f}Hz'.format(self.courbe1[curve_name]['data']['f0'])
            freq = '%.0f-%.0fkHz'%(parameters['Acquisition']['freq min']/1e3,parameters['Acquisition']['freq max']/1e3)
            file_name = self.path + prefix + legend + '_' +'_'.join([device,harmonic,freq0,freq])
        if 'Laser PV2I' in self.origin:
            curr = '%.0f-%.0fmA'%(parameters['Laser Driver']['current min'],parameters['Laser Driver']['current max'])
            temp = 'T{:.0f}C'.format(parameters['Laser Driver']['temperature'])   
            file_name = self.path + prefix + legend + '_'  + '_'.join([curr,temp]) 
        if 'Photoacoustic' in self.origin:
            curr = '%.0f-%.0fmA'%(parameters['Laser Driver']['current min'],parameters['Laser Driver']['current max'])
            temperature = '{:.0f}C'.format(parameters['Laser Driver']['temperature'])
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            try:
                amplitude = '{:.0f}mV'.format(parameters['Generator']['amplitude']*1e3) 
            except:
                amplitude = '' 
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3)   
            file_name = self.path + prefix + legend + '_'  + '_'.join([device,harmonic,amplitude,temperature,curr,frequency]) 
        return file_name   

    def fabricate_title_name(self):
        curve_name = self.courbe1_list[0]
        parameters = self.courbe1[curve_name]['data']['parameters']
        path_info = parameters['Save']['path info']
        prefix = self.courbe1_prefix[0]
        today = time.strftime("%Y%m%d_%HH%Mmin")
        try:
            device = parameters['Save']['device']
        except:
            device = ''
        if path_info == '':
            file_name = '_'.join([prefix.replace('01',''),device,today])
        else: 
            file_name = '_'.join([prefix.replace('01',''),device,today,path_info])
        return self.path+file_name       

    def fabricate_path(self):
        curve_name = 'curve acq'
        parameters = self.courbe1[curve_name]['data']['parameters']
        today = time.strftime("%Y%m%d")
        path_laser = parameters['Save']['laser']
        path_design = parameters['Save']['design']
        if 'Unknow' in path_design:
            path_resonator = 'Unknow'
        else:
            path_resonator = parameters['Save']['resonator']
        path_info = parameters['Save']['path info']
        if path_info == '':
            path_info = today
        else:
            path_info = today+'_'+path_info
        if 'C:' in parameters['Save']['path']:
            slash = '\\'
        else:
            slash = '/'
        if 'device' in parameters['Save'].keys():
            path_device = parameters['Save']['device']
            path = parameters['Save']['path'] + slash.join([path_design,path_resonator,path_device]) + slash
        else:
            # path = parameters['Save']['path'] + slash.join([path_design,path_resonator]) + slash
            # print('device False',path)
            if 'acoustic_sensor' in parameters['Save'].keys():
                path_acoustic_sensor = parameters['Save']['acoustic_sensor']
                path = parameters['Save']['path'] + slash.join([path_design,path_resonator,path_acoustic_sensor]) + slash
            else:
                path = parameters['Save']['path'] + slash.join([path_design,path_resonator]) + slash
        if 'Allan deviation' in self.origin:
            path = path + slash.join(['allan_deviation',path_info]) + slash
        if 'Amplitude of modulation' in self.origin:
            path = path + slash.join(['amplitude_of_modulation',path_info]) + slash
            
        if 'Continous Acquisition' in self.origin:
            path = path + slash.join(['Continous Acquisition',path_info]) + slash
            
        if 'Electrical characterization' in self.origin:
            path = path + slash.join(['electrical_characterization',path_info]) + slash            
        if 'Frequency sweep' in self.origin:
            path = path + slash.join(['frequency_sweep',path_info]) + slash
        if 'Laser PV2I' in self.origin:
            path = parameters['Save']['path'] + slash.join(['Laser',path_laser]) + slash
            path = path + slash.join(['pv2i',path_info]) + slash
        if 'Photoacoustic' in self.origin:
            path = path + slash.join(['photoacoustic',path_info]) + slash
        return path      

    def update_legend_for_all_curve(self):
        # juste before savinng the last legend has been saved on 'curve acq'
        # so we update all the other curve
        for k in ['Graph','Save']:
            dico = self.courbe1['curve acq']['data']['parameters'][k]
            for curve_name in self.courbe1_list:
                self.courbe1[curve_name]['data']['parameters'][k] = dico

    def save_param(self,num):
        curve_name = self.courbe1_list[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        # save a txt file with all the parameter
        info = open(self.file_name[num]+'.txt','w')
        info.write(time.strftime("%Y-%m-%d")+' '+time.strftime("%H:%M:%S")+ '\n')
        info.write('\n')   
        for k in parameters:
            info.write(k+'\n')
            for j in parameters[k]:
                texte = '  * '+str(j)+' : '+str(parameters[k][j])+'\n'
                info.write(texte)      
        info.close()

    def save_data(self,num):
        # save csv file with all the data
        curve_name = self.courbe1_list[num]
        if 'Allan deviation' in self.origin:
            data_1 = pandas.DataFrame({'time (s)': self.courbe1[curve_name]['data']['Time'],
                        'time wanted (s)': self.courbe1[curve_name]['data']['Time wanted'],
                        #'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                        #'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                        'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                        #'phase (deg)': self.courbe1[curve_name]['data']['Phi'],
                        })
            data_1.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8') 
            
            data_2 = pandas.DataFrame({'Tau-adev (s)': self.courbe1[curve_name]['data']['Tau-adev'],
                        'Deviation-adev (Vrms)': self.courbe1[curve_name]['data']['Deviation-adev'],
                        'Concetration-adev (ppmv)': self.courbe1[curve_name]['data']['Concentration-adev']})
            data_2.to_csv(self.file_name[num] +'_ADEV.csv', sep=';', encoding='utf-8') 

            data_3 = pandas.DataFrame({'Tau-oadev (s)': self.courbe1[curve_name]['data']['Tau-oadev'],
                        'Deviation-oadev (Vrms)': self.courbe1[curve_name]['data']['Deviation-oadev'],
                        'Concetration-oadev (ppmv)': self.courbe1[curve_name]['data']['Concentration-oadev'],
                        })
            data_3.to_csv(self.file_name[num] +'_OADEV.csv', sep=';', encoding='utf-8') 
        if 'Amplitude of modulation' in self.origin:
            # save csv file with all the data
            data = pandas.DataFrame({'amplitude of modualtion (Vpk)': self.courbe1[curve_name]['data']['Amplitude'],
                        'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                        'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                        'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                        'phase (deg)': self.courbe1[curve_name]['data']['Phi']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')

            
        if 'Continous Acquisition' in self.origin:
            data = pandas.DataFrame({'Time (s)': self.courbe1[curve_name]['data']['tim'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi'],
                'RH (%)': self.courbe1[curve_name]['data']['RH'],
                'T (degC)': self.courbe1[curve_name]['data']['T'],
                'H2O (ppm)': self.courbe1[curve_name]['data']['H2O']
                
                
                })
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8') 
            
            
        if 'Electrical characterization' in self.origin:
            data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name]['data']['Freq'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi'],
                'Vexcitation (Vrms)': self.courbe1[curve_name]['data']['Vexc'],
                'Gain': self.courbe1[curve_name]['data']['Gain'],
                'Vout/Vexc': self.courbe1[curve_name]['data']['Vout/Vexc'],
                'Admittance (1/Ohm)': self.courbe1[curve_name]['data']['Admittance']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8') 
            if len(self.courbe1[curve_name+' fit']['data']['Freq']) > 1:
                data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name+' fit']['data']['Freq'],
                            'Admittance (1/Ohm)': self.courbe1[curve_name+' fit']['data']['Admittance']})
                data.to_csv(self.file_name[num] +'_FIT_ADM.csv', sep=';', encoding='utf-8')
        if 'Frequency sweep' in self.origin:
            data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name]['data']['Freq'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8') 
            if len(self.courbe1[curve_name+' fit']['data']['Freq']) > 1:
                data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name+' fit']['data']['Freq'],
                            'r (Vrms)': self.courbe1[curve_name+' fit']['data']['R']})
                data.to_csv(self.file_name[num] +'_FIT_MAG.csv', sep=';', encoding='utf-8')
            if len(self.courbe2[curve_name+' fit']['data']['Freq']) > 1:
                data = pandas.DataFrame({'Frequency (Hz)': self.courbe2[curve_name+' fit']['data']['Freq'],
                            'phase (deg)': self.courbe2[curve_name+' fit']['data']['Phi']})
                data.to_csv(self.file_name[num] +'_FIT_PHASE.csv', sep=';', encoding='utf-8')
        if 'Laser PV2I' in self.origin:
            data = pandas.DataFrame({'Current (mA)': self.courbe1[curve_name]['data']['Current'],
                'voltage (V)': self.courbe1[curve_name]['data']['Voltage'],
                'power (mW)': self.courbe1[curve_name]['data']['Power']
                })
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')
        if 'Photoacoustic' in self.origin:
            #in case that arrays do not have the same length           
            data_dict = {'current (mA)': self.courbe1[curve_name]['data']['Current'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi'],
                'RH (percent)': self.courbe1[curve_name]['data']['RH'],
                'T (degC)': self.courbe1[curve_name]['data']['T'],
                'H2O (ppm)': self.courbe1[curve_name]['data']['H2O']}
            # Find the maximum length among all arrays
            max_length = max(map(len, data_dict.values()))
            
            # Create a list of dictionaries
            data_list = []
            for i in range(max_length):
                row = {   
                    'current (mA)': data_dict['current (mA)'][i] if i < len(data_dict['current (mA)']) else None,
                    'x (Vrms)': data_dict['x (Vrms)'][i] if i < len(data_dict['x (Vrms)']) else None,
                    'y (Vrms)': data_dict['y (Vrms)'][i] if i < len(data_dict['y (Vrms)']) else None,
                    'r (Vrms)': data_dict['r (Vrms)'][i] if i < len(data_dict['r (Vrms)']) else None,
                    'phase (deg)': data_dict['phase (deg)'][i] if i < len(data_dict['phase (deg)']) else None,
                    'RH (percent)': data_dict['RH (percent)'][i] if i < len(data_dict['RH (percent)']) else None,
                    'T (degC)': data_dict['T (degC)'][i] if i < len(data_dict['T (degC)']) else None,
                    'H2O (ppm)': data_dict['H2O (ppm)'][i] if i < len(data_dict['H2O (ppm)']) else None}
                data_list.append(row)
        
            # Create a DataFrame from the list of dictionaries
            data = pandas.DataFrame(data_list)
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')  
        

    def save_graph(self):
        # color and graph param
        load_mpl_params()
        if 'Allan deviation' in self.origin:
            self.save_graph_Allan()
        if 'Amplitude of modulation' in self.origin:
            self.save_graph_Amplitude_Of_Modulation()
            
        if 'Continous Acquisition' in self.origin:
            self.save_graph_Continous_Acquisition()
            
        if 'Electrical characterization' in self.origin:
            self.save_graph_Electrical_Characterization()
        if 'Frequency sweep' in self.origin:
            self.save_graph_Frequency_Sweep()
        if 'Laser PV2I' in self.origin:
            self.save_graph_Laser_PV2I()
        if 'Photoacoustic' in self.origin:
            self.save_graph_PhotoAcoustic()

    def save_graph_Allan(self):
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            curve_name = self.courbe1_list[num]
            R = self.courbe1[curve_name]['data']['R']
            Time = self.courbe1[curve_name]['data']['Time']
            Mean = self.courbe1[curve_name]['data']['Mean']
            signal_to_noise = self.courbe1[curve_name]['data']['SNR']
            step_time = self.courbe1[curve_name]['data']['Step time']
            step_time_dev = self.courbe1[curve_name]['data']['Step time dev']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            legend = legend +' Mean={:.3f}mVrms SNR={:.1f} step={:.1f}+/-{:.3f}ms'.format(Mean*1e3,signal_to_noise,step_time*1e3,step_time_dev*1e3)
            ax1.plot(Time, R, color=colors[2*num],linewidth=2,marker='.',label=legend)

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)        
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_TIME.png')
        plt.close(fig)

        # ALLAN DEVIATION
        def add_plot_allan(num):
            curve_name = self.courbe1_list[num]
            Tadev = self.courbe1[curve_name]['data']['Tau-adev']
            Cadev = self.courbe1[curve_name]['data']['Concentration-adev']
            Toadev = self.courbe1[curve_name]['data']['Tau-oadev']
            Coadev = self.courbe1[curve_name]['data']['Concentration-oadev']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            ax1.plot(Tadev, Cadev, color=colors[2*num],linewidth=2,marker='.',label=legend+' (adev)')
            ax1.plot(Toadev, Coadev, color=colors[2*num+1],linewidth=2,marker='.',label=legend+' (oadev)')
            # add value on the curve
            # find and plot the LOD @ 1s
            C1s = self.courbe1[curve_name]['data']['LOD 1s']
            print('LOD at 1s is {:.3f} ppmv'.format(C1s))
            ax1.scatter(1, C1s, color='r', alpha=0.5)
            ax1.text(1, C1s, '{:.0f}ppmv @ 1s'.format(C1s), rotation=15,color=colors[2*num])
            # find and plot the LOD @ 10s
            if max(Toadev) > 10:
                C10s = self.courbe1[curve_name]['data']['LOD 10s']
                print('LOD at 10s is {:.3f} ppmv'.format(C10s))
                ax1.scatter(10, C10s, color='r', alpha=0.5)
                ax1.text(10, C10s, '{:.0f}ppmv @ 10s'.format(C10s), rotation=15,color=colors[2*num])
            # find and plot the lowest LOD
            Tau_LOD, Con_LOD = self.courbe1[curve_name]['data']['LOD']
            print('LOD at {:.3f}s is {:.3f} ppmv'.format(Tau_LOD,Con_LOD))
            ax1.scatter(Tau_LOD, Con_LOD, color='r', alpha=0.5)
            ax1.text(Tau_LOD, Con_LOD, '{:.0f}ppmv @ {:.1f}s '.format(Con_LOD,Tau_LOD), rotation=15,color=colors[2*num])

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_allan(num_curve) 
        ax1.set_ylabel('concentration [ppmv]', labelpad=4)
        ax1.set_xlabel('time constant [s]', labelpad=4)
        ax1.set_xscale('log')
        ax1.set_yscale('log')
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_ALLAN.png')
        plt.close(fig)

        ## TIME STEP
        def add_plot_time_step(num):
            curve_name = self.courbe1_list[num]
            Time = self.courbe1[curve_name]['data']['Time']
            Mean = self.courbe1[curve_name]['data']['Mean']
            signal_to_noise = self.courbe1[curve_name]['data']['SNR']
            step_time = self.courbe1[curve_name]['data']['Step time']
            step_time_dev = self.courbe1[curve_name]['data']['Step time dev']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            legend = legend +' Mean={:.3f}mVrms SNR={:.1f} step={:.1f}+/-{:.3f}ms'.format(Mean*1e3,signal_to_noise,step_time*1e3,step_time_dev*1e3)
            if len(Time) > 5000:
                divisor = 1000
                step = int(len(Time)/divisor)
            else:
                step = 1
            legend = legend + '\nThe number of points has been divided by {}'.format(step)    
            ax1.plot(Time[0:-1:step], np.diff(Time)[::step]*1e3, color=colors[2*num],linewidth=1,label=legend)
            

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_time_step(num_curve)        
        ax1.set_ylabel('step time [ms]', labelpad=4)
        ax1.set_xlabel('time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_TIME_STEP.png')
        plt.close(fig)


    def save_graph_Amplitude_Of_Modulation(self):
        number_color = 10
        cmap = plt.get_cmap('tab10')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        def add_plot_mag(num):
            curve_name = self.courbe1_list[num]
            Amp = self.courbe1[curve_name]['data']['Amplitude']*1e3
            R = self.courbe1[curve_name]['data']['R']
            Amp_at_Rmax = self.courbe1[curve_name]['data']['Amplitude for R max']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            ax1.plot(Amp, R, color=colors[num],linewidth=2,marker='.',label=legend+' max for x={:.3f}mV'.format(Amp_at_Rmax*1e3))

        # magnitude
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)         
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('amplitude of modulation [mVpk]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)
        
    def save_graph_Continous_Acquisition(self):
        
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]
            title=self.courbe1[curve_name]['data']['title1']
            Tim = self.courbe1[curve_name]['data']['tim']
            R = self.courbe1[curve_name]['data']['R']
            ax1.plot(Tim, R, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            

        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)
        
        
        def add_plot_RH(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title2']
            Tim = self.courbe1[curve_name]['data']['tim']
            RH = self.courbe1[curve_name]['data']['RH']
            ax1.plot(Tim, RH, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_RH(num_curve)
        ax1.set_ylabel('RH [%]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_RH.png')
        plt.close(fig) 
        
        def add_plot_T(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title3']
            Tim = self.courbe1[curve_name]['data']['tim']
            T = self.courbe1[curve_name]['data']['T']
            ax1.plot(Tim, T, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_T(num_curve)
        ax1.set_ylabel('T [degC]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_T.png')
        plt.close(fig) 

        def add_plot_H2O(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title4']
            Tim = self.courbe1[curve_name]['data']['tim']
            H2O = self.courbe1[curve_name]['data']['H2O']
            ax1.plot(Tim, H2O, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_H2O(num_curve)
        ax1.set_ylabel('H2O [ppm]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_water.png')
        plt.close(fig)  


    def save_graph_Electrical_Characterization(self):
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # ADMITTANCE
        def add_plot_adm(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            R = self.courbe1[curve_name]['data']['Admittance']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            R_fit = self.courbe1[curve_name+' fit']['data']['Admittance']
            ax1.plot(Freq*1e-3, R, color=colors[2*num],linewidth=2,label=legend,marker='.')
            if len(R_fit) > 1:
                Q_mag = self.courbe1[curve_name+' fit']['data']['Q']
                f0_mag = self.courbe1[curve_name+' fit']['data']['f0']
                C0_mag = self.courbe1[curve_name+' fit']['data']['C0']
                ax1.plot(Freq_fit*1e-3, R_fit, color=colors[2*num+1],
                linewidth=1,label='Q={:.0f} f0={:.3f}kHz \n C0={:.4f}pF'.format(Q_mag,f0_mag/1e3,C0_mag*1e12))

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_adm(num_curve)
        ax1.set_ylabel(r'admittance [1/$\Omega$]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_ADM.png')
        plt.close(fig)

        # MAGNITUDE
        def add_plot_mag(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            R = self.courbe1[curve_name]['data']['R']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            R_fit = self.courbe1[curve_name+' fit']['data']['R']
            ax1.plot(Freq*1e-3, R, color=colors[2*num],linewidth=2,label=legend,marker='.')
            if len(R_fit) > 1:
                Q_mag = self.courbe1[curve_name+' fit']['data']['Q']
                f0_mag = self.courbe1[curve_name+' fit']['data']['f0']
                C0_mag = self.courbe1[curve_name+' fit']['data']['C0']
                ax1.plot(Freq_fit*1e-3, R_fit, color=colors[2*num+1],
                linewidth=1,label='Q={:.0f} f0={:.3f}kHz \n C0={:.2g}pF'.format(Q_mag,f0_mag/1e3,C0_mag*1e12))

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)

        # PHASE
        def add_plot_phase(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            Phi = self.courbe1[curve_name]['data']['Phi']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            Phi_fit = self.courbe1[curve_name+' fit']['data']['Phi']
            ax1.plot(Freq*1e-3, Phi, color=colors[2*num],
            linewidth=2,label=legend,marker='.')

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_phase(num_curve)
        ax1.set_ylabel('phase [degrees]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_PHASE.png')
        plt.close(fig)

    def save_graph_Frequency_Sweep(self):
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            R = self.courbe1[curve_name]['data']['R']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            R_fit = self.courbe1[curve_name+' fit']['data']['R']
            ax1.plot(Freq*1e-3, R, color=colors[2*num],linewidth=2,label=legend,marker='.')
            if len(R_fit) > 1:
                Q_mag = self.courbe1[curve_name+' fit']['data']['Q']
                Q_sigma = self.courbe1[curve_name+' fit']['data']['Q sigma']
                f0_mag = self.courbe1[curve_name+' fit']['data']['f0']
                f0_sigma = self.courbe1[curve_name+' fit']['data']['f0 sigma']
                C0_mag = self.courbe1[curve_name+' fit']['data']['C0']
                label_text = 'f0={:.3f}kHz(+/-){:.3f}Hz \nQ={:.0f}(+/-){:.1f} \nC0={:.2g}pF'.format(f0_mag/1e3, f0_sigma, Q_mag, Q_sigma, C0_mag*1e12)
                ax1.plot(Freq_fit*1e-3, R_fit, color=colors[2*num+1],linewidth=1,label=label_text)

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)

        # PHASE
        def add_plot_phase(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            Phi = self.courbe1[curve_name]['data']['Phi']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            Phi_fit = self.courbe2[curve_name+' fit']['data']['Phi']
            ax1.plot(Freq*1e-3, Phi, color=colors[2*num],
            linewidth=2,label=legend,marker='.')
            if len(Phi_fit) > 1:
                Q_phase = self.courbe2[curve_name+' fit']['data']['Q']
                Q_sigma = self.courbe2[curve_name+' fit']['data']['Q sigma']
                f0_phase = self.courbe2[curve_name+' fit']['data']['f0']
                f0_sigma = self.courbe2[curve_name+' fit']['data']['f0 sigma']
                C0_phase = self.courbe2[curve_name+' fit']['data']['C0']
                label_text = 'f0={:.3f}kHz(+/-){:.3f}Hz \nQ={:.0f}(+/-){:.1f} \nC0={:.2g}pF'.format(f0_phase/1e3, f0_sigma, Q_phase, Q_sigma, C0_phase*1e12)
                ax1.plot(Freq_fit*1e-3, Phi_fit, color=colors[2*num+1],
                linewidth=1,label=label_text)

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_phase(num_curve)
        ax1.set_ylabel('phase [degrees]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_PHASE.png')
        plt.close(fig)

    def save_graph_Laser_PV2I(self):
        number_color = 20
        cmap = plt.get_cmap('tab20b')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        def add_plot_pv2i(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            temperature = ' T{:.0f}C'.format(parameters['Laser Driver']['temperature']) 
            I = self.courbe1[curve_name]['data']['Current']
            V = self.courbe1[curve_name]['data']['Voltage']
            P = self.courbe1[curve_name]['data']['Power']
            ax1.plot(I, V, color=colors[num],linewidth=2,marker='.',label=legend+temperature)
            ax2.plot(I, P, color=colors[12+num],linewidth=2,marker='.')

        # graph
        fig, ax1 = plt.subplots()

        
        ax1.set_ylabel('voltage [V]', labelpad=4, color=colors[0])
        ax1.tick_params(axis='y', labelcolor=colors[0])
        ax1.set_xlabel('current [mA]', labelpad=4)
        #ax1.set_title(temperature.replace('T','Temp=').replace('C','°C'))
        ax2 = ax1.twinx()
        ax2.set_ylabel('power [mW]', labelpad=0, color=colors[12])
        ax2.tick_params(axis='y', labelcolor=colors[12])
        ax2.grid(b=False)
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_pv2i(num_curve)
        ax1.legend(loc=1)
        #ax2.legend(loc=3)
        fig.savefig(self.fabricate_title_name()+'.png')
        plt.close(fig)

    def save_graph_PhotoAcoustic(self):
        number_color = 10
        cmap = plt.get_cmap('tab10')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            I = self.courbe1[curve_name]['data']['Current']
            R = self.courbe1[curve_name]['data']['R']
            ax1.plot(I, R, color=colors[num],linewidth=2,marker='.',label=legend)

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('current [mA]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)

        # PHASE
        def add_plot_phase(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            I = self.courbe1[curve_name]['data']['Current']
            Phi = self.courbe1[curve_name]['data']['Phi']
            ax1.plot(I, Phi, color=colors[num],linewidth=2,marker='.',label=legend)

        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_phase(num_curve)        
        ax1.set_ylabel('phase [degrees]', labelpad=4)
        ax1.set_xlabel('current [mA]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_PHASE.png')
        plt.close(fig)


