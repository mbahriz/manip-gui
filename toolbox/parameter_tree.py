#   20201001 add command to generate AM from driver laser need to connect th analog output BNC on an input entrance of the MFLI and set the ref oscillator as external to trigger on the laser driver mod (setup for Thorlabs FP laser)
# 21 septembre 2023 Update for pyqtgraph 0.13.1
'''
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

app = QtGui.QApplication([])
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree, ParameterItem, registerParameterType
'''
import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets

app = pg.mkQApp()
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

import pandas as pd
import numpy as np

import os
# The section below will allow you to import lists from CSV files
# Get the current directory
current_directory = os.getcwd()
print(f"The current directory is: {current_directory}")
# Join the list directory to the current one
list_directory = os.path.join(current_directory, "list")
print(f"The list directory is: {list_directory}")

import toolbox.inst.medisoft as medi

##############################################################################
##### GLOBAL 
##############################################################################
path_WINDOWS = 'C:\\Users\\QEPAS\\Documents\\DATA\\'
path_WINDOWS2 = 'C:\\Users\\User1\\QEPAS\\Documents\\DATA\\'
path_WINDOWS3 = 'C:\\Users\\Nanomir\\Documents\\DATA\\'
path_WINDOWS4 = 'C:\\Users\\user1\\Documents\\Data_QEPAS\\' # Precision 3551 -> Pc Tarek CHU
path_WINDOWS5 = 'C:\\Users\\qepas2\\Documents\\Data_GUI' # ancien Roman
path_MACOS = '/Users/michaelbahriz/Recherche/Instrumentations/Codes/manip-gui/data/'
path_LIST = [path_WINDOWS,path_WINDOWS2,path_WINDOWS3,path_WINDOWS4,path_WINDOWS5,path_MACOS]
path = path_WINDOWS2
amp_max = 250e-3 # amplitude of modulation max
amp_min_miniware = 5e-3
amp_max_miniware = 200e-3 # amplitude of modulation max
freq_min_acq, freq_max_acq = 10e3, 20e3
freq_max_ametek = 250e3
freq_max_zurich = 500e3
keithley_time_cst_min = 10e-3

##############################################################################
##### INSTRUMENT INIT 
##############################################################################

generator_init = {'frequency' : 10e3,
                'amplitude' : 30e-3,
                'on' : False }
input_init = {'time constant' : 100e-3,
                'filter order' : 2,
                'poll length' : 0.01,
                'sensitivity' : 1,
                'harmonic' : 1 } 
arduino_init = {}# Needed to avoid errors 
laser_driver_init = {'on' : False,
                'current' : 1, #in mA
                'temperature' : 25} 
pulse_module_init = {'pulse width':40, # in ns
                'carrier frequency':500e3,
                'frequency':1000, # frequency = modulation frequency
                'duty cycle':10}   # in %
medisoft_init = {'Expair_filename':medi.Medisoft.connect('Medisoft')}                
all_instru_init = {'Generator' :generator_init,
                'Input' : input_init,
                'Arduino' : arduino_init,
                'Laser Driver' : laser_driver_init,
                'Pulse module' : pulse_module_init,
                'Medisoft':medisoft_init}


def instrument_initials_values(who):
    return all_instru_init[who]

##############################################################################
##### LIST RESONATOR, LASER ....
##############################################################################
def import_list (name_file = "list_Hsquare_75um_V5"):
    # import lists from a python file
    list_file = os.path.join(list_directory, name_file+".csv")
    df = pd.read_csv(list_file)
    df_as_list = df[name_file].tolist()
    print("Import:", name_file)
    print(df_as_list)
    return df_as_list

list_laser_driver = ['Virtual','Thorlabs_ITC400XQCL','ILx_driver']

list_amplifier = ['FEMTO-100K-50M','FEMTO-40K-100M','TIA-B','TIA-C','TIA-1','TIA-2','TIA-3','TIA-4','Without','Unknow']
list_laser = ['QCL_Zayneb_10.9um_C2H4','Norca4_2.4um_CH4','Eblana_1.3um_H2O','Elena_2.3um_CH4_V346','Isoprene_11um_C5H8','CO_4.6um','Aceton_8.2um_C3H6O','Laser_CO2','Benzene_1.8um_C6H6','NO_5.2um','Without','Unknow']
list_gas = ['air','vacuum','acetone','isoprene','CO','NO','ch4','c2h4','h2co','c6h6','Unknow']

# df = pd.DataFrame(list_laser, columns=['list_laser'])
# df.to_csv('list_laser.csv')

list_design = ['QTF','H_75um','H_75um_V2','Hsquare_75um','Hsquare_75um_V3','Hsquare_75um_V4','Hsquare_75um_V5','Hsquare_100um','Centipede_75um','Centipede_75um_v2','Acoustic_cavities','Acoustic_cavities_Cent','Manip_QEPAS','Menbrane','Cantilever','Unknow']
#list_design = ['QTF','H-Res_75um','H-Res_75um_V2','Hsquare-Res_75um','Hsquare-Res_75um_V3','Hsquare-Res_75um_V4','Hsquare-Res_100um','Acoustic_resonator','Menbrane','Cantilever','Unknow']

list_microphone = ['microphone_3V']
list_others = ['SOI_substrate_75um']
list_menbrane = ['Q7BM_2p5mm_eptot13um_q300','Q7BM_4mm_eptot13um_q300','Q7BM_3mm_eptot13um_q300','Q4MG1_2mm_eptot11um_q600nm','Q4MG_2mm_eptot12um_q600nm','Menbrane_2mm','Menbrane_1mm_10um','Q10_1_5mm_2um','Q9_2mm_8um','M_SiN500nm_1','M_SiN500nm_4','M_SiN500nm_5','M_SiN500nm_6','Unknow']
# list_Acoustic_cavities = ['None','Bare','L050F_1','L050F_2','L060F_1','L060F_2','L066_1','L067_1','L068_1','L070F_1','L070F_2','L080F_1','L080F_2','L084_1','L090F_1','L090F_2','L097_1','L100F_1','L100F_2','L105','L110F_1','L110F_2','L112','L115','L116_1','L120_1','L122','L121_1','L121_2','L121_3','L121_4','L121_5','L130F','L133','L136_1','L140F','L141F_1','L141F_2','L141F_3','L141F_4','L141F_5','L141F_6','L141F_7','L141F_8','L141F_9','L145','L150F','L155F','L157_1','L157_2','L157_3','L157_4','L160F','L170F','L173','L180F','L185','L190F','L200_1','L200F','L210F_1','L210F_2','L220F_1','L220F_2','L215_1','L245_1','L245_2','L245_3','L230','L230_2','L230_3','L230_4','L230_5','L230_6','L230_7','L230F_8','L230F_9','L240F_1','L240F_2','L240F_3','L250F_1','L250F_2','L260','L260_2','Unknow']
list_Acoustic_cavities = import_list(name_file="list_Acoustic_cavities")
list_Acoustic_cavities_Cent = import_list(name_file="list_Acoustic_cavities_Cent")
list_Manip_QEPAS = import_list(name_file="list_Manip_QEPAS")
list_Cantilever = ['CHS100A41N7_1','CHS100A41N7_5','CHS100A41N7_3','C_L580_e75um','C_L690_e75um','C_L1000_l1500_e75um','C_L1000_l1000_e75um','C_L1522_l990_e157um','C_L1330_l984_e157um','C_L1625_l988_e157um','C_L1428_l985_e157um','Unknow']
list_Acoustic_sensor = list_Cantilever + list_menbrane + list_microphone + list_others

list_QTF = ['QTF-210330','QTF-210330-A9','Unknow']
list_HRes_75um = ['H75R45F4.1','H75R45F4.2','H75R45F4.3','H75R45F4.4','H75R45F4.5','H75R15M500.1','H75R15M500.2','H75R15M500.3','H75R15M500.4','H75R65M500.1','H75R65M500.5','H75R65F4.1','H75R15F4.1','H75R15F4.5','Unknow']
list_HRes_75um_V2 = ['H75F4N5.2','H75F4N8C.1','H75F4N8.2','H75M500N8C.1','Unknow']
list_HRes_75um_device =['Unknow','10arms','9arms','8arms','7arms','6arms','5arms','4arms','3arms']
list_HRes_75um_V2_device =['Unknow','10arms','9arms','8arms','7arms','6arms','5arms','4arms','3arms']
list_Hsquare_75um=['Unknow','HS75R15M350o1.1','HS75R15M350o4.1','HS75R15L350o2.1','HS75R15L430o1.1','HS75R15M200o1.1','HS75R15S700o1.1']
list_Hsquare_75um_V3=['Unknow','HS75R15ls200.1','HS75R10ls100.1','HS75R10ls200.1','HS75R5ls200.1','HS75R20ls200.1']
list_Hsquare_75um_V4=['Unknow','HS75R15d150a10.1','HS75R20d150a12.1','HS75R20d150a10.1','HS75R15d125.1']
#list_Hsquare_75um_V5=['Unknow','HS75V5_A10.1','HS75V5_A50.1','HS75V5_B20.1','HS75V5_B50.1','HS75V5_C30.1','HS75V5_C50.1','HS75V5_A50.2','HS75V5_B50.2','HS75V5_C50.2','HS75V5_A40.1','HS75V5_B40.1','HS75V5_C40.1','HS75V5_A50.3','HS75V5_B40.2','HS75V5_A40.2','HS75V5_C40.4']
list_Hsquare_75um_V5=import_list(name_file="list_Hsquare_75um_V5")
list_Hsquare_75um_device = ['Unknow','Top','Bottom left','Bottom right']
list_HSRes_100um = ['HS100A455N7C6.1','HS100A41N7C6.1','HS100A41N7C12.1','Unknow']
list_HSRes_100um_device = ['Unknow','Top','Bottom left','Bottom right']
list_Centipede_75um = ['Cent_W1','Cent_W2','Cent_WTest','Cent_T1','Cent_T2','Cent_TTest','Cent_H1','Cent_H2','Unknow']
#list_Centipede_75um_v2 = ['Cent75v2_W1','Cent75v2_W2','Cent75v2_T1','Cent75v2_T2','Unknow']
list_Centipede_75um_v2=import_list(name_file="list_Centipede_75um_v2")
list_Centipede_75um_device = ['All','Top','Middle','Bottom','Unknow']

# ATTENTION list should be the same in the ametek.py
list_sensitivity_LIA_Ametek = [2e-9,5e-9,10e-9,20e-9,50e-9,100e-9,200e-9,500e-9,
    1e-6,2e-6,5e-6,10e-6,20e-6,50e-6,100e-6,200e-6,500e-6,
    1e-3,2e-3,5e-3,10e-3,20e-3,50e-3,100e-3,200e-3,500e-3,1] # Ametek LIA works with list :-(
list_sensitivity_LIA_Zurich = [1e-3, 3e-3, 10e-3, 30e-3, 100e-3, 300e-3,1 ,3] 
list_time_constant_LIA = [10e-6,20e-6,50e-6,100e-6,200e-6,500e-6,
    1e-3,2e-3,5e-3,10e-3,20e-3,50e-3,100e-3,200e-3,500e-3,
    1,2,5]
list_sensitivity_LIA_stanford =[1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.001,
    0.0005, 0.0002, 0.0001, 0.00005, 0.00002, 0.00001, 0.000005, 0.000002, 0.000001,
    0.0000005, 0.0000002, 0.0000001, 0.00000005, 0.00000002, 0.00000001, 0.000000005, 0.000000002, 0.000000001]
list_time_constant_LIA_stanford =[ 0.000001, 0.000003, 0.00001, 0.00003, 0.0001, 0.0003, 0.001, 0.003, 0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30, 100, 300, 1000, 3000, 10000 ]
list_filter_LIA_stanford =[6.0, 12.0, 18.0, 24.0]



##############################################################################
##### CLASS
##############################################################################
class MonParametre():
    # mother class use to transfert method to other class

    def child_list(self):
        # return a list with the name and value of all the children
        A = ''
        for i in self.children():
            A = A+i.name()+' : '+str(i.value())+'\n'
        return A

class Acquisition(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'freq min', 'type': 'float','value': freq_min_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'freq max', 'type': 'float','value': freq_max_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'nbr pts', 'type': 'int','value': 100, 'step': 100, 'limits': (10, 100e3),'siPrefix': False, 'tip':'number of points use for the frequecy sweep'})
        self.addChild({'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 1, 'tip':'number of sequences, number of time frequecy sweep is running'})
        self.addChild({'name': 'waiting time', 'type': 'list','values':[0,1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor. Indeed, the Zurick don\'t send only one value but a list of value acquierd durint the waiting time'})
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'average', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'})

    def remaning_time(self,value):
        self.param('remaning time').setValue(value)


class AllPlotScalableGroup(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'group'
        opts['addText'] = "Add"
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'path', 'type': 'str', 'value': path})

    def addNew(self):
        new = OneCurve(name="Curve_%d" % (len(self.childs)))
        new.param('file name').setValue("Curve %d.csv" % (len(self.childs)))
        self.addChild(new,dict(removable=True))
        self.addChild({'name': 'sinusoidal fit', 'type': 'bool', 'value': False})

class Amplifier(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'amplifier', 'type': 'list', 'values': list_amplifier, 'value': 'Unknow'})
        self.addChild({'name': 'gain', 'type': 'float','value': 10e6, 'step': 10e6,'siPrefix': True, 'suffix': 'V/A','decimals':3})

        self.amplifier = self.param('amplifier')
        self.gain = self.param('gain')
        self.amplifier.sigValueChanged.connect(self.amplifier_changed)

    def amplifier_changed(self):
        # ['TIA-B','TIA-C','FEMTO-100K-50M','FEMTO-40K-100M','Unknow']
        if self.amplifier.value() in 'FEMTO-100K-50M':
            self.gain.setValue(50e6)
        if self.amplifier.value() in 'FEMTO-40K-100M':
            self.gain.setValue(100e6)
        if self.amplifier.value() in 'TIA-B':
            pass
        if self.amplifier.value() in 'TIA-C':
            pass

class AllanDeviation(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'acq. time', 'type': 'float','value': 30, 'step': 10, 'limits': (0.1, 3600),'siPrefix': True, 'suffix': 'min','decimals':6,'tip':'total acquisition time'})  
        self.addChild({'name': 'concentration', 'type': 'float','value': 0, 'step': 10, 'limits': (1, 1e6),'siPrefix': True, 'suffix': 'ppmv','decimals':6,'tip':'concentration'})
        self.addChild({'name': 'type', 'type': 'list', 'values': ['adev','oadev'], 'value': 'adev', 'tip':'oadev = overlapping and adev = standarad allan deviation'})  
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'})


class Arduino(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Arduino_DHT','Arduino_GPIB'], 'value': 'Virtual'})
        self.instru = self.param('instrument')

        self.instru.sigValueChanged.connect(self.instru_changed)
        # self.j=0 #Flag
    def instru_changed(self):
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])
        # if  'Arduino_DHT'  in self.instru.value():  #and self.j==0:
        #     self.addChild({'name': 'COM', 'type': 'list', 'values': [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 'value': arduino_init['COM']})
                    
        # if  'Arduino_GPIB' in self.instru.value():  #and self.j==0:
        #     self.addChild({'name': 'COM', 'type': 'list', 'values': [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 'value': arduino_init['COM']})
               

class Generator(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320','Zurich-MFLI_dev5255','Ametek_7270','Stanford_SR860','Kinjalk_pulse_generator','Picoscope_3207B',"Tektronix_CH1","Tektronix_CH2",'Miniware_CH1','Miniware_CH2'], 'value': 'Virtual'})
        self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)

    def instru_changed(self):
        child_list = self.children()
        for i in range(2, len(child_list)):
            self.removeChild(child_list[i])        
        if  'Zurich' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if  'Ametek' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (1, freq_max_ametek),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            #self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if 'Kinjalk' in self.instru.value():
            self.addChild({'name': 'info', 'type': 'str', 'value': ''})
            self.addChild({'name': 'real frequency', 'type': 'str', 'value': ''})
            self.real_freq = self.param('real frequency')
            self.freq = self.param('frequency')
            self.freq.setValue(pulse_module_init['frequency'])
            self.info = self.param('info')
            self.ChangeDuty()
            self.freq.sigValueChanged.connect(self.ChangeDuty)
        if  'Picoscope_3207B' in self.instru.value():  #and self.j==0:
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 1e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})                        
        if  'Tektronix_CH1' in self.instru.value():  #and self.j==0:
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 1e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})           
        if  'Tektronix_CH2' in self.instru.value():  #and self.j==0:
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 1e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})        
        if  'Miniware_CH1' in self.instru.value():
            # self.addChild({'name': 'connection', 'type': 'bool', 'value': miniware_CH1['connection'], 'tip': "ON when it is checked"})
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 1e-3, 'limits': (amp_min_miniware, amp_max_miniware),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if  'Miniware_CH2' in self.instru.value():
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 1e-3, 'limits': (amp_min_miniware, amp_max_miniware),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if  'Stanford_SR860' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
         
    def ChangeDuty(self):
        X, Y = toolbox.inst.kinjalk.XnY_modulator_frequency(frequency=self.freq.value(), duty=0.5)
        x, x_round, x_err = X
        y, y_round, y_err = Y 
        true_state, false_state = toolbox.inst.kinjalk.logic_state(x=x_round, y=y_round) 
        text_info = 'x={:.0f}(err{:.3f}) y={:.0f}(err{:.3f}) 1->{:.0f}cycles 0->{:.0f}cycles'.format(x, x_err, y, y_err, true_state, false_state)
        self.info.setValue(text_info)
        text_real_freq = '{:.6f} kHz'.format(toolbox.inst.kinjalk.give_real_frequency(x_round,y_round)/1e3)
        self.real_freq.setValue(text_real_freq)

# Generator for electrical charaterisation for lower excitation tension
class GeneratorEC(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320','Zurich-MFLI_dev5255'], 'value': 'Virtual'})
        self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
        self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 1e-6, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
        self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})

# AOF For the code AmplitudeOfModulation
class GeneratorAOM(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320','Zurich-MFLI_dev5255'], 'value': 'Virtual'})
        self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
        self.addChild({'name': 'amplitude', 'type': 'float','value': generator_init['amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
        self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        self.addChild({'name': 'amplitude min', 'type': 'float','value': 1e-3, 'step': 1, 'limits': (0, 1),'siPrefix': True, 'suffix': 'V','decimals':6})
        self.addChild({'name': 'amplitude max', 'type': 'float','value': 5e-3, 'step': 1, 'limits': (0, 1),'siPrefix': True, 'suffix': 'V','decimals':6})
        self.addChild({'name': 'nbr pts', 'type': 'int','value': 100, 'step': 100, 'limits': (5, 100e3),'siPrefix': False, 'tip':'number of points use for the frequecy sweep'})
        self.addChild({'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 1, 'tip':'number of sequences, number of time frequecy sweep is running'})
        self.addChild({'name': 'waiting time', 'type': 'list','values':[0,1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'})        
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'average', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'}) 

class GraphList(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})

class FitEC(pTypes.GroupParameter,MonParametre):
    # fir for electrical characterization
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'f0', 'type': 'float','value': 32e3, 'step': 10, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})     
        self.addChild({'name': 'Q', 'type': 'float','value': 10000, 'step': 10, 'limits': (1, 1e6),'siPrefix': False})
        self.addChild({'name': 'max res', 'type': 'float','value': 10000, 'step': 10,'siPrefix': False})
        self.addChild({'name': 'C0', 'type': 'float','value': 1e-12, 'step': 100, 'limits': (0, 1e-3), 'suffix': 'F','siPrefix': True,'tip':'parasitic capacitor'})
        self.addChild({'name': 'freq min', 'type': 'float','value': freq_min_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'freq max', 'type': 'float','value': freq_max_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'guess', 'type': 'action'})
        self.addChild({'name': 'fit', 'type': 'action'})
        self.addChild({'name': 'plot', 'type': 'action'})

class FitFS(pTypes.GroupParameter,MonParametre):
    # fir for frequency sweep
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        # self.addChild({'name': 'resonance frequency', 'type': 'float','value': 32e3, 'step': 10, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'initial guess for amp', 'type': 'float','value': 1e-3, 'step': 1,'siPrefix': True, 'suffix': 'Vrms'})    
        self.addChild({'name': 'initial guess for f0', 'type': 'float','value': 10000, 'step': 10, 'limits': (1, 1e6),'siPrefix': True, 'suffix': 'Hz'}) 
        self.addChild({'name': 'initial guess for Q', 'type': 'float','value': 300, 'step': 10, 'limits': (1, 1e6),'siPrefix': False})
        self.addChild({'name': 'value of C0', 'type': 'float','value': 1e-12, 'step': 0.1e-12, 'limits': (0, 1e-6), 'suffix': 'F','siPrefix': True,'tip':'parasitic capacitor'})
        self.addChild({'name': 'freq min', 'type': 'float','value': freq_min_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'freq max', 'type': 'float','value': freq_max_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'full width', 'type': 'action'})
        self.addChild({'name': 'fit MAG', 'type': 'action'})
        self.addChild({'name': 'fit PHASE', 'type': 'action'})

class Input(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        #time_cst = 50e-3 #100e-3 # time cst use for the acquisition
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Ametek_7270','Keithley_2010Multimeter','Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320','Zurich-MFLI_dev5255','Stanford_SR860'], 'value': 'None'})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)        
    def instru_changed(self):
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])     
        if  'Ametek' in self.instru.value():
            self.addChild({'name': 'time constant', 'type': 'list','value': input_init['time constant'], 'values':list_time_constant_LIA,'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': 'sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Ametek, 'value': input_init['sensitivity'], 'tip':'in [V]'})
            self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
        if 'Keithley' in self.instru.value():
            self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'], 'step': 100e-3, 'limits': (keithley_time_cst_min, 10),'siPrefix': True, 'suffix': 's'})
            self.addChild({'name': 'range Melles Griot', 'type': 'list', 'values': [3,10,30,100,300,1000,3000], 'value': 3,'tip':'connect the powermeter on the sense ohm of the Keithley. Range setting has to be done manually. ATTENTION Range 10 correspond to the value 3, then range 10 correspond to value 30 on the gear...'})
        if 'Virtual' in self.instru.value(): # harmonic is needed for sauvegarder.py
            self.addChild({'name': 'time constant', 'type': 'float','value': 5e-3,'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})   
            self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
        if  'Zurich' in self.instru.value():
            self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': 'filter order','type':'list','values':[1,2,3,4,5,6,7,8], 'value':input_init['filter order'],'tip':'low pass filter order'})
            # NB : Poll length is not used in Allan code
            self.addChild({'name': 'poll length', 'type': 'list','values': [0.0005,0.001,0.005,0.01,0.05,0.1,0.5,1],'siPrefix': True, 'suffix': 's','value':input_init['poll length'],'tip':'data acquisition time, then averaged, the Zurick don t send one value but a list of value. Not useful for Allan'})
            self.addChild({'name': 'sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Zurich, 'value': input_init['sensitivity'], 'tip':'in [V]'})
            self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
        if  'Stanford_SR860' in self.instru.value():
            # self.addChild({'name': 'filter order','type':'list','value':12.0,'values':list_filter_LIA_stanford, 'suffix': 'dB','tip':'low pass filter order'})
            self.addChild({'name': 'time constant', 'type': 'list','value': input_init['time constant'], 'values':list_time_constant_LIA_stanford,'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            # self.addChild({'name': 'poll length', 'type': 'list','values': [0.0005,0.001,0.005,0.01,0.05,0.1,0.5,1],'siPrefix': True, 'suffix': 's','value':input_init['poll length'],'tip':'data acquisition time, then averaged, the Zurick don t send one value but a list of value. Not useful for Allan'})
            self.addChild({'name': 'sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_stanford, 'value': input_init['sensitivity'], 'tip':'in [V]'})
            self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            

# # Input Allan the same that input without poll length
# class InputAllan(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320'], 'value': 'Virtual'})
#         self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'], 'step': 100e-3, 'limits': (time_cst_min, 10),'siPrefix': True, 'suffix': 's','tip':'For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
#         self.addChild({'name': 'filter order','type':'list','values':[1,2,3,4,5,6,7,8], 'value':input_init['filter order'],'tip':'low pass filter order'})
#         self.addChild({'name': 'sensitivity', 'type': 'list', 'values': list_sensitivity_LIA, 'value': input_init['sensitivity']})
#         self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})

# class InputPV2I(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         #time_cst = 50e-3 #ATTENTION don't use value below 10e-3 the recording time # time cst use for the acquisition
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Keithley_2010Multimeter'], 'value': 'Virtual'})
#         self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'], 'step': 100e-3, 'limits': (time_cst_min, 10),'siPrefix': True, 'suffix': 's'})
#         self.addChild({'name': 'range Melles Griot', 'type': 'list', 'values': [3,10,30,100,300,1000,3000], 'value': 3,'tip':'connect the powermeter on the sense ohm of the Keithley. Range setting has to be done manually. ATTENTION Range 10 correspond to the value 3, then range 10 correspond to value 30 on the gear...'})

class LaserDopplerVibrometer(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'decoder', 'type': 'list','values':['Unknow','VD06'],'value': 'Unknow'})
        self.addChild({'name': 'sensitivity', 'type': 'list','values':['Unknow','1mm/s/V (20kHz)',
                        '2mm/s/V LP (100kHz)','2mm/s/V (350kHz)',
                        '10mm/s/V LP (100kHz)','10mm/s/V (350kHz)',
                        '50mm/s/V LP (100kHz)','50mm/s/V (350kHz)'],
                        'value': 'Unknow'})

class LaserDriver(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': list_laser_driver, 'value': 'Virtual'})
        self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
        self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 750),'siPrefix': True, 'suffix': 'mA','decimals':6})
        self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30,30),'siPrefix': True, 'suffix': '°C','decimals':3})
        # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})

# PA For photoacoustic
class LaserDriverPA(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': list_laser_driver, 'value': 'Virtual'})
        self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
        self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 350),'siPrefix': True, 'suffix': 'mA','decimals':6})
        self.addChild({'name': 'current min', 'type': 'float','value': 1, 'step': 1, 'limits': (1e-1, 700),'siPrefix': True, 'suffix': 'mA','decimals':6})
        self.addChild({'name': 'current max', 'type': 'float','value': 5, 'step': 1, 'limits': (1e-1, 750),'siPrefix': True, 'suffix': 'mA','decimals':6})
        self.addChild({'name': 'nbr pts', 'type': 'int','value': 5, 'step': 100, 'limits': (5, 100e3),'siPrefix': False, 'tip':'number of points use for the frequecy sweep'})
        self.addChild({'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 1, 'tip':'number of sequences, number of time frequecy sweep is running'})
        self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-35, 35),'siPrefix': True, 'suffix': '°C','decimals':3})
        # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})
        # self.addChild({'name': 'internal modulation (Square signal)', 'type': 'bool','value': False, 'tip': "ON to deliver a square signal modulation on Thorlabs laser"})
        # self.addChild({'name': 'internal frequency modulation', 'type': 'float','value': 100, 'step': 0.001, 'limits': (0.05,100),'siPrefix': True, 'suffix': 'kHz','decimals':6})
        # self.addChild({'name': 'internal amplitude modulation', 'type': 'float','value': 1, 'step': 1, 'limits': (1, 100),'siPrefix': True, 'suffix': '%','decimals':6})  
        self.addChild({'name': 'waiting time', 'type': 'list','values':[0,1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'})
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'average', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'})

# # AOF For the code AmplitudeOfModulation
# class LaserDriverAOM(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Thorlabs_ITC400XQCL'], 'value': 'Virtual'})
#         self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
#         self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 400),'siPrefix': True, 'suffix': 'mA','decimals':6})
#         self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30, 30),'siPrefix': True, 'suffix': '°C','decimals':3})
#         # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})
#         # self.addChild({'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'})

# # For AllanDeviation
# class LaserDriverAD(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Thorlabs_ITC400XQCL'], 'value': 'Virtual'})
#         self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
#         self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 400),'siPrefix': True, 'suffix': 'mA','decimals':6})
#         self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30, 30),'siPrefix': True, 'suffix': '°C','decimals':3})
#         # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})


# # FS For frequency sweep
# class LaserDriverFS(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Thorlabs_ITC400XQCL'], 'value': 'Virtual'})
#         self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
#         self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 400),'siPrefix': True, 'suffix': 'mA','decimals':6})
#         self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30,30),'siPrefix': True, 'suffix': '°C','decimals':3})
#         # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})

class Medisoft_expair(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        path_info = ''
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Medisoft'], 'value': 'Virtual'})
        self.instru = self.param('instrument')      
        self.instru.sigValueChanged.connect(self.instru_changed)
               
    def instru_changed(self):
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])
        if  'Medisoft' in self.instru.value():
            self.addChild({'name': 'Expair_filename', 'type': 'str','value':medisoft_init['Expair_filename']})

     

class OneCurve(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        opts['removable'] = True
        opts['renamable'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'file name', 'type': 'str', 'value': 'curve 1'})
        self.addChild({'name': 'color', 'type': 'color', 'value': "FF0", 'tip': "This is a color button"})

class Plotter(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        
        self.addChild({'name': 'waiting time', 'type': 'list','values':[0,1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor. Indeed, the Zurick don\'t send only one value but a list of value acquierd durint the waiting time'})
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'stop', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'})
    def remaning_time(self,value):
        self.param('remaning time').setValue(value)

class PulseModule(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Kinjalk_pulse_generator','PICO_pulse_generator'], 'value': 'None'})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)        

    def instru_changed(self):
        #carrier_freq = [1/(nbr_cycle_per_periode*i*clock_cycle) for i in [4,2,1]]
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])        
        if 'Kinjalk' in self.instru.value():
            one_cycle =  toolbox.inst.kinjalk.give_clock_cycle()
            self.addChild({'name': 'pulse width', 'type': 'list','values': [(3*x-1)*one_cycle*1e9 for x in range(2,24,2) ] ,'siPrefix': False, 'suffix': 'ns', 'tip' : 'in ns'}) 
            self.addChild({'name': 'carrier frequency', 'type': 'float','value': pulse_module_init['carrier frequency'], 'limits': (10e3, 5e6) , 'step':5e3, 'siPrefix': True, 'suffix': 'Hz'}) 
            self.addChild({'name': 'duty cycle', 'type': 'str', 'value': 'nan'}) 
            #self.addChild({'name': 'duty cycle', 'type': 'list','values': [50, 1, 0.1, 0.01, 1e-3, 1e-4] ,'siPrefix': False, 'suffix': '%', 'tip':'in %'})         
            #self.addChild({'name': 'carrier frequency', 'type': 'str', 'value': '0 MHz'})
            self.addChild({'name': 'info', 'type': 'str', 'value': ''})
            self.pulse = self.param('pulse width')
            self.duty = self.param('duty cycle')
            self.carrier = self.param('carrier frequency')
            self.info = self.param('info')
            self.ChangeDuty()
            #self.modulation = self.param('modulation frequency')
            self.pulse.sigValueChanged.connect(self.ChangeDuty)
            self.carrier.sigValueChanged.connect(self.ChangeDuty)
        if 'PICO' in self.instru.value():
            self.addChild({'name': 'carrier frequency', 'type': 'float','value': pulse_module_init['carrier frequency'], 'limits': (1, 5e6) , 'step':5e3, 'siPrefix': True, 'suffix': 'Hz'}) 
            self.addChild({'name': 'duty cycle', 'type': 'float','value': pulse_module_init['duty cycle'], 'limits': (0, 100) , 'step':1, 'siPrefix': True, 'suffix': '%'}) 
            self.addChild({'name': 'pulse width', 'type': 'str', 'value': 'nan'}) 
            self.pulse = self.param('pulse width')
            self.duty = self.param('duty cycle')
            self.carrier = self.param('carrier frequency')
            self.ChangePulse()
            self.duty.sigValueChanged.connect(self.ChangePulse)
            self.carrier.sigValueChanged.connect(self.ChangePulse)

    def ChangeDuty(self):
        pulse = self.pulse.value()
        freq = self.carrier.value()
        duty = pulse*1e-9*freq
        text_duty = '{:.3g} %'.format(duty*100)
        X, Y = toolbox.inst.kinjalk.XnY_carrier_frequency(pulse_width=pulse, duty_cycle=duty)
        x, _, _ = X
        y, y_round, y_err = Y  
        true_state, false_state = toolbox.inst.kinjalk.logic_state(x=x, y=y_round) 
        text_info = 'x={:.0f} y={:.0f}(err{:.3f}) 1->{:.0f}cycles 0->{:.0f}cycles'.format(x, y, y_err,true_state, false_state)
        self.duty.setValue(text_duty)
        self.info.setValue(text_info)

    def ChangePulse(self):
        duty= self.duty.value()
        freq = self.carrier.value()
        pulse = 1/freq*duty/100
        text_pulse = '{:.3g} ns'.format(pulse*1e9)
        self.pulse.setValue(text_pulse)

class Save(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        path_info = ''
        self.addChild({'name': 'setup description', 'type': 'text', 'value': 'Some text...'})
        self.addChild({'name': 'save', 'type': 'action'})
        self.addChild({'name': 'dokuwiki', 'type': 'bool', 'value': False})
        self.addChild({'name': 'path', 'type': 'list', 'values': path_LIST, 'value': path})
        self.addChild({'name': 'path info', 'type': 'str', 'value': path_info})
        self.addChild({'name': 'laser', 'type': 'list', 'values': list_laser, 'value': 'Unknow'})
        self.addChild({'name': 'amplifier', 'type': 'list', 'values': list_amplifier, 'value': 'Unknow'})
        self.addChild({'name': 'gas', 'type': 'list', 'values': list_gas, 'value': 'Unknow'})
        self.addChild({'name': 'concentration', 'type': 'str', 'value': 'Unknow'})
        self.addChild({'name': 'acoustic cavities', 'type': 'list', 'values': list_Acoustic_cavities + list_Acoustic_cavities_Cent , 'value': 'None'})            
        self.addChild({'name': 'design', 'type': 'list', 'values': list_design, 'value': 'Unknow'})
        # self.addChild({'name': 'resonator', 'type': 'list', 'values': list_QTF, 'value': 'Unknow'})
        self.design = self.param('design')
        self.design.sigValueChanged.connect(self.design_changed)    

    def design_changed(self):
        child_list = self.children()
        for i in range(11, len(child_list)):
            self.removeChild(child_list[i])  
        if self.design.value() in 'Manip_QEPAS':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Manip_QEPAS, 'value': 'Unknow'})            
        if self.design.value() in 'QTF':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_QTF, 'value': 'Unknow'})
        if self.design.value() == 'Centipede_75um':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Centipede_75um, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_Centipede_75um_device, 'value': 'Unknow'})
        if self.design.value() == 'Centipede_75um_v2':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Centipede_75um_v2, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_Centipede_75um_device, 'value': 'Unknow'})
        if self.design.value() == 'H-Res_75um':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_HRes_75um, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_HRes_75um_device, 'value': 'Unknow'})
        if self.design.value() == 'H-Res_75um_V2':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_HRes_75um_V2, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_HRes_75um_V2_device, 'value': 'Unknow'})            
        if self.design.value() in 'Hsquare_75um':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Hsquare_75um, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_Hsquare_75um_device, 'value': 'Unknow'})
        if self.design.value() in 'Hsquare_75um_V3':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Hsquare_75um_V3, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_Hsquare_75um_device, 'value': 'Unknow'})   
        if self.design.value() in 'Hsquare_75um_V4':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Hsquare_75um_V4, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_Hsquare_75um_device, 'value': 'Unknow'})    
        if self.design.value() in 'Hsquare_75um_V5':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Hsquare_75um_V5, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_Hsquare_75um_device, 'value': 'Unknow'})    
        if self.design.value() in 'Hsquare_100um':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_HSRes_100um, 'value': 'Unknow'})
            self.addChild({'name': 'device', 'type': 'list', 'values': list_HSRes_100um_device, 'value': 'Unknow'})
        if self.design.value() in 'Acoustic_cavities':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Acoustic_cavities, 'value': 'Unknow'})
            self.addChild({'name': 'acoustic_sensor', 'type': 'list', 'values': list_Acoustic_sensor, 'value': 'Unknow'})
        if self.design.value() in 'Acoustic_cavities_Cent':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Acoustic_cavities_Cent, 'value': 'Unknow'})
            self.addChild({'name': 'acoustic_sensor', 'type': 'list', 'values': list_Acoustic_sensor, 'value': 'Unknow'})
        if self.design.value() in 'Menbrane':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_menbrane, 'value': 'Unknow'})            
        if self.design.value() in 'Cantilever':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Cantilever, 'value': 'Unknow'}) 
        if self.design.value() in 'Cantilever':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_others, 'value': 'Unknow'}) 

##############################################################################
##############################################################################
## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    ## Create tree of Parameter objects
    params = [Input(name='Input'),
              Generator(name='Generator'),
              AllPlotScalableGroup(name='Graph'),
              PulseModule(name='Pulse module')]

    p = Parameter.create(name='params', type='group', children=params)

    ## Create two ParameterTree widgets, both accessing the same data
    t = ParameterTree()
    t.setParameters(p, showTop=False)
    t.setWindowTitle('pyqtgraph example: Parameter Tree')


    win = QtWidgets.QWidget()
    layout = QtWidgets.QGridLayout()
    win.setLayout(layout)
    layout.addWidget(t, 1, 0, 1, 1)  #widget added at row 0, column 0, with a row span of 1 and a column span of 2:
    win.show()
    win.resize(800,800)

    pg.exec()

    # import sys
    # if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    #     QtGui.QApplication.instance().exec_()

##############################################################################
##############################################################################


#### OLD VERSION #########

# def build_dico(dico):

#   '''
#   This dictionary define all the parametre tree use for the GUI 
#   (graphic utilisator interface)
#   The dico here is use for the inital value
#   '''


#   p_global = [{'name': 'Input', 'type': 'group',
#                'children': [{'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199'], 'value': dico['Input']['instrument']},
#                             {'name': 'time constant', 'type': 'float',
#                              'value': dico['Input']['time constant'], 'step': 100e-3, 'limits': (400e-9, 10),
#                              'siPrefix': True, 'suffix': 's'},
#                              {'name': 'recording time', 'type': 'float',
#                              'value': dico['Input']['recording time'], 'step': 10e-3, 'limits': (10e-3, dico['Input']['time constant']),
#                              'siPrefix': True, 'suffix': 's','tip':'data acquisition time, then averaged'},
#                              {'name': 'sensitivity', 'type': 'list', 'values': [1e-3,3e-3,1e-2,3e-2,1e-1,3e-1,1,3], 'value': dico['Input']['sensitivity']},
#                             ]},
#               {'name': 'Frequency sweep', 'type': 'group',
#                'children': [{'name': 'generator', 'type': 'group', 'children': [
#                             {'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199'], 'value': dico['Frequency sweep']['generator']['instrument']},
#                             {'name': 'amplitude', 'type': 'float','value': dico['Frequency sweep']['generator']['amplitude'], 'step': 50e-3, 'limits': (0, 1),'siPrefix': True, 'suffix': 'V'},
#                             {'name': 'on', 'type': 'bool', 'value': dico['Frequency sweep']['generator']['on'], 'tip': "ON when it is checked"},
#                             ]},
#                             {'name': 'acquisition', 'type': 'group', 'children': [
#                             {'name': 'freq min', 'type': 'float',
#                              'value': dico['Frequency sweep']['acquisition']['freq min'], 'step': 10e3, 'limits': (1, 500e3),
#                              'siPrefix': True, 'suffix': 'Hz','decimals':6},
#                              {'name': 'freq max', 'type': 'float',
#                              'value': dico['Frequency sweep']['acquisition']['freq max'], 'step': 10e3, 'limits': (1, 500e3),
#                              'siPrefix': True, 'suffix': 'Hz','decimals':6},
#                              {'name': 'nbr pts', 'type': 'int',
#                              'value': dico['Frequency sweep']['acquisition']['nbr pts'], 'step': 100, 'limits': (10, 100e3),
#                              'siPrefix': False, 'tip':'number of points use for the frequecy sweep'},
#                              {'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],
#                              'value': dico['Frequency sweep']['acquisition']['nbr seqs'], 'tip':'number of sequences, number of time frequecy sweep is running'},
#                              {'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],
#                              'value': dico['Frequency sweep']['acquisition']['waiting time'], 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'},
#                              {'name': 'start', 'type': 'action'},
#                              {'name': 'average', 'type': 'action'},
#                              {'name': 'clear all', 'type': 'action'}
#                              ]}
#                           ]},
#               {'name': 'Save', 'type': 'group',
#                'children': [{'name': 'path', 'type': 'str', 'value': dico['Save']['path']},
#                             {'name': 'path info', 'type': 'str', 'value': dico['Save']['path info']},
#                             {'name': 'sample name', 'type': 'str', 'value': dico['Save']['sample name']},
#                             {'name': 'sample info', 'type': 'str', 'value': dico['Save']['sample info']},
#                             {'name': 'save', 'type': 'action'}                 
#                           ]}
#               ]
#   return p_global
