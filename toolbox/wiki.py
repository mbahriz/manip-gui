import dokuwiki 
import pandas as pd

'''
I - Get It Working

    1) You need at least the 2008-03-31 release of DokuWiki.
    2) Enable the XML-RPC interface in the “Authentication Settings” section
    3) Set the remoteuser option (see section II)
    4) For security reasons it's safer to allow access to the XML-RPC over HTTPS only. DokuWiki's .htaccess.dist contains some rewrite rules to do that.

II - Configuration Setting: remoteuser

With this option you can restrict the access to the XML-RPC backend. You can list multiple user or group names1) separated by commas, or leave it blank to allow all users (including guests). The default !!not set!! will never allow anyone to access the backend.

This option limits access to the API backend (except the login method). ACLs are still checked when the called methods run. Note that if useacl is not enabled and this value is not the default !!not set!!, it will always allow anyone regardless of the value.

    Type: String
    Default: !!not set!! (not accessible by anyone)


'''

def superdico_to_dokuwiki_table(superdico):
    # convert a list of dictionary in dokuwiki table
    dico = {}
    # key creation using the first graph    
    for k, v in superdico[0].items(): # k for key and v for value
        dico[k] = [v]
    # append the other values    
    for i in range(1,len(superdico)): 
        for k, v in superdico[i].items(): # k for key and v for value
            dico[k].append(v)
    # convert dico in wiki table        
    df = pd.DataFrame(dico)  
    wiki_table = dataframe_to_dokuwiki_table(df) + '\n'
    return wiki_table

def dataframe_to_dokuwiki_table(df):
    # convert a dataframe in wiki table
    # Get the column names as a list
    columns = list(df.columns)

    # Create the header row
    header_row = "^ " + " ^ ".join(columns) + " ^"

    # Create the data rows
    data_rows = "\n".join(["| " + " | ".join(map(str, row)) + " |" for row in df.values])

    # Combine all parts to form the DokuWiki table
    dokuwiki_table = f"{header_row}\n{data_rows}"

    return dokuwiki_table

def wiki_connect():
    URL = 'https://wikinano.ies.umontpellier.fr/ures'
    USER = 'python'
    PASSWORD = 'nanomir2010'

    try:
        wiki = dokuwiki.DokuWiki(URL, USER, PASSWORD)
    except (dokuwiki.DokuWikiError, Exception) as err:
        print('unable to connect: %s' % err)
    return wiki

def update_wiki_text(page_adress,text):
    wiki = wiki_connect()
    print('wiki adress : \n',page_adress)
    print('wiki text   : \n',text)
    wiki.pages.append(page_adress,'\n' + text)

def upload_media(adress,path,file):
    wiki = wiki_connect()
    media = adress +':'+ file
    wiki.medias.add(media, path+file, overwrite=True)
    link_media = '{{:' + media + '?direct&100|}}'
    return link_media

def upload_file(adress, path, file,num):
    wiki = wiki_connect()
    media = adress +':'+ file.replace(path,'')
    wiki.medias.add(media, file, overwrite=True)
    link_file = '{{:' + media + '|'+num+'}}'
    return link_file

# print(wiki.pages.list(namespace='test1'))
# print(wiki.pages.get('test1'))
# print(wiki.pages.get('test1:test1a'))
# wiki.pages.append('test1:test1a','\n  *  append ce texte')
# print(wiki.pages.get('test1:test1a'))

