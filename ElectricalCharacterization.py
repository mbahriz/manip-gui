
# 21 septembre 2023 Update for pyqtgraph 0.13.1
# -*- coding: utf-8 -*-
"""
GUI for uRes photoacoustic
"""

import os
from collections import namedtuple

from pyqtgraph.dockarea import DockArea
from pyqtgraph.dockarea import Dock
from pyqtgraph import PlotWidget

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets, QtCore
from pyqtgraph.parametertree import interact, ParameterTree, Parameter

import numpy as np
from numpy.polynomial.polynomial import polyfit
import toolbox.instrument 
import toolbox.parameter_tree
#import toolbox.function
import toolbox.sauvegarder
import toolbox.mother_class
import scipy.optimize
import time


# only if you want a white background for your graph
# pg.setConfigOption('background', 'w')



class MainWindow(QtWidgets.QMainWindow,toolbox.mother_class.MotherMainWindow): 
    def __init__(self):
        super().__init__()
        # the tuple version of a dictionary, use to store width and height of the screen 
        Screen = namedtuple('Screen', 'width height')
        # get screen's information (height and width)
        # CAUTION this attribut can be modified by other method like .show
        # it can put all the values at 0, even they were welle defined
        self.screen = Screen(width=1500,height=800) 
        # dictionary with all the graphic elements 
        self.obj_graph = {'plot': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                  },
                          'plot2': {'dock': None,
                                   'graph': None,
                                   'curve': None
                                   },         
                          'param': {'dock': None,
                                    'tree': None,
                                    'parameter': None}}                          

        self.on_init()       
        # change the size of the graphic interface
        self.resize(self.screen.width, self.screen.height)
        # my timer use for acquisition in real time
        self.my_timer = None
        self.period_timer = 50
        # create an attribute with all my instruments 
        self.instruments = MesInstrus(param_tree=self.parameters)
        # display the GUI
        self.show()

    def on_init(self):
        # for debug it is useful to separate the create         
        self.create_dock()
        self.create_contenu_dock_pg()
        self.create_contenu_dock_plot()

    def create_dock(self):
        area = DockArea()
        self.obj_graph['param']['dock'] = Dock("Electrical characterization")
        self.obj_graph['plot']['dock'] = Dock("Admittance Vout/Vexc/Gain")
        #self.obj_graph['plot2']['dock'] = Dock("Admittance Vout/Vexc/Gain")
        area.addDock(self.obj_graph['param']['dock'], 'left')
        area.addDock(self.obj_graph['plot']['dock'],'right')
        #area.addDock(self.obj_graph['plot2']['dock'],'bottom',self.obj_graph['plot']['dock'])
        self.setCentralWidget(area)

    def create_contenu_dock_plot(self):
        # creation of plot 1 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot']['curve'] = MesCourbes()
        self.obj_graph['plot']['graph'] = self.obj_graph['plot']['curve'].plot_widget 
        self.obj_graph['plot']['dock'].addWidget(self.obj_graph['plot']['graph'])
        self.obj_graph['plot']['curve'].add_curve('curve acq',(0,250,250),markers_on=True)
        self.obj_graph['plot']['curve'].add_curve('curve acq fit',(200,200,0),markers_on=False, linewidht=2)  
               
        # # creation of plot 2 (graph 2)
        # # a place for this graph has been defined in create_dock()   
        # self.obj_graph['plot2']['curve'] = MesCourbes()
        # self.obj_graph['plot2']['graph'] = self.obj_graph['plot2']['curve'].plot_widget
        # self.obj_graph['plot2']['dock'].addWidget(self.obj_graph['plot2']['graph']) 
        # self.obj_graph['plot2']['curve'].add_curve('curve acq',(0,200,200),markers_on=True)
        # self.obj_graph['plot2']['curve'].add_curve('curve acq fit',(200,200,0),markers_on=False, linewidht=2)  

    def create_contenu_dock_pg(self):
        self.obj_graph['param']['tree'] = ParameterTree()
        self.create_parameter()
        self.obj_graph['param']['dock'].addWidget(self.obj_graph['param']
                                                  ['tree'])
    def create_parameter(self):
        # create a Class with all parameters use in the GUI
        self.parameters = MesParams()
        # build the GUI
        (self.obj_graph['param']
         ['parameter']) = Parameter.create(name='params',
                                           type='group',
                                           children=self.parameters.tree)
        (self.obj_graph['param']
         ['parameter'].sigTreeStateChanged.connect(self.catch_param_change))

        """
        setParameters
        Set the top-level :class:`Parameter <pyqtgraph.parametertree.Parameter>`
        to be displayed in this ParameterTree.

        If *showTop* is False, then the top-level parameter is hidden and only 
        its children will be visible. This is a convenience method equivalent 
        to::
        
            tree.clear()
            tree.addParameters(param, showTop)
        """
        self.obj_graph['param']['tree'].setParameters((self.obj_graph['param']
                                                       ['parameter']),
                                                      showTop=False)
        self.obj_graph['param']['tree'].setWindowTitle('Parameter Tree')


    def catch_param_change(self, _, changes):
        # call in create_parameter()
        for param, _, data in changes:
            path = self.obj_graph['param']['parameter'].childPath(param)
            if path is not None: 
                # print(path) -> ['Input- Zurich', 'time constant']
                child_name = '.'.join(path)
                # print(child_name) -> Input- Zurich.time constant
            else:
                child_name = param.name()
            print('  parameter: %s' % child_name)
            print('  change:    %s' % _)
            print('  data:      %s (%s)' % (str(data),type(data)))

            # who is the name of the parameter tree
            # what the name of the paramater of the parameter tree
            try:
                who, what = child_name.split('.')
            except:
                who = child_name
                what = child_name

            graph_1 = self.obj_graph['plot']['curve']

            ## INSTRUMENT CHANGE
            # CAUTION check it always in first
            if what in ['instrument']:
                # in this case data == name of the instrument
                self.instruments.update(who,data)

            ## CHANGE VALUES ASKED ON THE INSTRUMENT
            if who in ['Input','Generator','Laser Driver']:
                #if what in ['time constant','sensitivity','amplitude','on','frequency','external reference','current','temperature','internal modulation (Square signal)','frequency modulation','amplitude modulation','harmonic']:
                # get the name of instrument asked in the parameter tree 
                instru = self.parameters.give_inst(who)
                self.instruments.set_value(instru,what,data)

            # CHANGE AMPLIFIER
            # if amplifier change in Amplfier paramtree -> update Ampfifier in Save paramtree
            if who in ['Amplifier']:
                #self.parameters.dico['Fit'].param('freq max').Value(f_acq_max)
                if what in ['amplifier']:
                    ampli = self.parameters.dico['Amplifier'].param('amplifier').value()
                    self.parameters.dico['Save'].param('amplifier').setValue(ampli)

            ## START ACQUISITION
            if (who+'.'+what) in 'Acquisition.start':
                # update old and new acquisition
                self.plot_ghost_curve()                
                # create frequency list
                graph_1.create_frequency_list(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                # on lance un timer qui appelera 'timerEvent' toutes les period_timer
                if self.my_timer is None:
                    self.my_timer = self.startTimer(self.period_timer) #c une methode heritée
                # the method timeEvent will be executed each period_timer

            ## AVERAGE ACQUISITION
            if (who+'.'+what) in 'Acquisition.average':
                # average data
                graph_1.average(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                # display graphs
                graph_1.display('curve acq','Admittance')


            ## ERASE ACQUISITION
            if (who+'.'+what) in 'Acquisition.clear all':
                graph_1.remove_ghost()
                for k in ['acq','acq fit']:
                    # earase data
                    graph_1.clear_data('curve '+k)
                    # display graphs
                    graph_1.display('curve '+k,'Admittance')
                # stop acquisition
                self.stop_timer()
                # remove list of curve in the paramter tree and a new one
                self.parameters.dico['Graph'].clearChildren()
                self.parameters.dico['Graph'].addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})

            ## FIT 
            # for MAG -> GRAPH 1
            if (who+'.'+what) in 'Fit.guess':
                self.guess_initial_value()    
            if (who+'.'+what) in 'Fit.fit':
                self.lorentzian_fit()               
            if (who+'.'+what) in 'Fit.plot':
                self.plot_fit()     

            ## SAVE ACQUISITION
            if (who+'.'+what) in 'Save.save':
                # update only the parameters from the section Save and Graph
                graph_1.save_parameters_sauvegarder(curve_id='curve acq',param=self.parameters)
                # save everything                
                toolbox.sauvegarder.Save('Electrical characterization',self.obj_graph['plot']['curve'].curves)#,self.obj_graph['plot2']['curve'].curves)


    def freq_limit(self):
        # adjuste frequency min and max used for the fit to avoid bug
        f_acq_min = self.parameters.dico['Acquisition'].param('freq min').value()
        f_acq_max = self.parameters.dico['Acquisition'].param('freq max').value()
        f_fit_min = self.parameters.dico['Fit'].param('freq min').value()
        f_fit_max = self.parameters.dico['Fit'].param('freq max').value()
        # f_fit_min > f_fit_max
        if f_fit_min > f_fit_max:
            self.parameters.dico['Fit'].param('freq min').setValue(f_fit_max)
            self.parameters.dico['Fit'].param('freq max').setValue(f_fit_min)
            f_fit_min = self.parameters.dico['Fit'].param('freq min').value()
            f_fit_max = self.parameters.dico['Fit'].param('freq max').value()
        # fit_min < fit_max < acq_min < acq_max             
        if (f_fit_min <= f_acq_min) and (f_fit_max <= f_acq_min):
            self.parameters.dico['Fit'].param('freq min').setValue(f_acq_min)
            self.parameters.dico['Fit'].param('freq max').setValue(f_acq_max)
        # fit_min < acq_min < fit_max < acq_max 
        if (f_fit_min <= f_acq_min) and (f_fit_max >= f_acq_min) and (f_fit_max <= f_acq_max):
            self.parameters.dico['Fit'].param('freq min').setValue(f_acq_min)
        # acq_min < fit_min < acq_max < fit_max
        if (f_fit_min >= f_acq_min) and (f_fit_min <= f_acq_max) and (f_fit_max >= f_acq_max):
            self.parameters.dico['Fit'].param('freq max').setValue(f_acq_max)
        # acq_min < acq_max < fit_min < fit_max
        if (f_fit_min >= f_acq_max) and (f_fit_max >= f_acq_max):
            self.parameters.dico['Fit'].param('freq min').setValue(f_acq_min)
            self.parameters.dico['Fit'].param('freq max').setValue(f_acq_max)

    def freq_range(self):
        # adjust fequency
        self.freq_limit()
        f_min = self.parameters.give('Fit','freq min')
        f_max = self.parameters.give('Fit','freq max')
        return f_min, f_max

    def guess_initial_value(self):
        graph = self.obj_graph['plot']['curve']
        f_min, f_max = self.freq_range()
        graph.guess_initial_value('curve acq',f_min,f_max,self.parameters)

    def lorentzian_fit(self):
        graph = self.obj_graph['plot']['curve']
        f_min, f_max = self.freq_range()
        graph.lorentzian_fit('curve acq',f_min,f_max,self.parameters)

    def plot_fit(self):
        f_min, f_max = self.freq_range()
        # earase data
        graph = self.obj_graph['plot']['curve']
        graph.clear_data('curve acq fit')
        # display graphs
        graph.update_data_fit(f_min,f_max,self.parameters) 
        graph.display('curve acq fit','Admittance') 
        self.rajoute_info_curve()

    def plot_ghost_curve(self):
        graph_1 = self.obj_graph['plot']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        # if test_no_restart is False that's mean that the button start has been pressed before the end of the acquisition. In this case no ghost will be ploted.
        test_no_restart = (len(graph_1.curves['curve acq']['data']['R']) == len(graph_1.curves['curve acq']['data']['Freq']))
        if (nbr_ghost > 0) and test_no_restart:
            graph_1.copy_curve('curve acq','curve ghost'+str(nbr_ghost))
            graph_1.display('curve ghost'+str(nbr_ghost),'Admittance')
            graph_1.copy_curve('curve acq fit','curve ghost'+str(nbr_ghost)+' fit')
            graph_1.display('curve ghost'+str(nbr_ghost)+' fit','Admittance')
        # earase data               
        graph_1.clear_data('curve acq')
        graph_1.clear_data('curve acq fit')

    def rajoute_ghost_cuvre(self):
        graph_1 = self.obj_graph['plot']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # put and check the max of curve
        curves_max = 9
        if nbr_ghost == curves_max:
            self.message_box(text='You have reached the maximum number of curves.')
        # color
        colors = self.color_map(curves_max+1) 
        # add curve
        graph_1.add_curve('curve ghost'+str(nbr_ghost+1),np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        graph_1.add_curve('curve ghost'+str(nbr_ghost+1)+' fit',np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        # add a line on the parameter tree to give the value of the resonnace frequency
        self.parameters.dico['Graph'].addChild({'name': 'info fit '+str(nbr_ghost+1), 'type': 'str', 'value': '', 'readonly': True})
        # add a line on the parameter tree str(nbr_ghost+2) as a first legend curve has been created in the same time than the parametertree
        self.parameters.dico['Graph'].addChild({'name': 'legend curve '+str(nbr_ghost+2), 'type': 'str', 'value': ''})      

    def rajoute_info_curve(self):
        # add info about the acquisition on the parametre
        graph_1 = self.obj_graph['plot']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        if nbr_ghost > 0:
            capa = self.parameters.dico['Fit'].param('C0').value()
            f0 = self.parameters.dico['Fit'].param('f0').value()
            Q = self.parameters.dico['Fit'].param('Q').value()
            texte = 'f0={:.4f}kHz Q={:.0f} C={:.3f}pF'.format(f0*1e-3,Q,capa*1e12)
            self.parameters.dico['Graph'].param('info fit '+str(nbr_ghost)).setValue(texte)   

    def timerEvent(self, _):
        """
        code exécuté toutes les "period_timer"
        """
        graph_1 = self.obj_graph['plot']['curve']
        # test to stop the timer i.e. the acquisition
        if len(graph_1.curves['curve acq']['data']['R']) == len(graph_1.curves['curve acq']['data']['Freq']):
            self.stop_timer()
            self.rajoute_ghost_cuvre()
            self.guess_initial_value()
            self.lorentzian_fit()
            self.plot_fit()
            # save param on the curve dictionary
            graph_1.save_parameters('curve acq',self.parameters)
            return
        # change frequency on the instrument to freq
        index = len(graph_1.curves['curve acq']['data']['R'])
        freq_i = graph_1.curves['curve acq']['data']['Freq'][index]
        instru = self.parameters.give_inst('Generator') #######A verifier
        instru2 = self.parameters.give_inst('Input')
        self.instruments.set_value(instru,'frequency',freq_i) #changement freq de modulation
        self.parameters.dico['Generator'].param('frequency').setValue(freq_i)  #displays the current frequency
        # wait
        time.sleep(self.parameters.waiting_time())
        # get X Y R and Phi from instrument 
        try:  # test as in virtual instrument there is no "poll length"
            t_poll = self.parameters.give('Input','poll length')
        except:
            t_poll = None 
        temp = self.instruments.get_X_Y_R_Phi(instru2,poll_length=t_poll,freq=freq_i)
        # update data
        graph_1.update_X_Y_R_Phi('curve acq',temp)
        graph_1.update_admittance('curve acq',self.parameters,temp)
        # plot data
        graph_1.display('curve acq','Admittance')


class MesInstrus(toolbox.mother_class.MotherInstru):
    def __init__(self,param_tree):
        self.param_tree = param_tree
        # attribut with all the instrument created
        self.connected = []
        # dico that the code will fill with instru which can be called
        self.use = {}
      

class MesParams(toolbox.mother_class.MotherParam):
    def __init__(self):
        # dictionary of parameter tree used
        self.dico = {
            'Amplifier':toolbox.parameter_tree.Amplifier(name='Amplifier'),
            'Input':toolbox.parameter_tree.Input(name='Input'),
            'Generator':toolbox.parameter_tree.GeneratorEC(name='Generator'),
            'Acquisition':toolbox.parameter_tree.Acquisition(name='Acquisition'),
            'Fit':toolbox.parameter_tree.FitEC(name='Fit'),
            'Graph':toolbox.parameter_tree.GraphList(name='Graph'),
            'Save':toolbox.parameter_tree.Save(name='Save')}
        # use the self.dico to build a list required for the GUI
        self.tree = []       
        for k in self.dico:
            self.tree.append(self.dico[k]) 

    # def give_all_inst_value(self,who):
    #     # returns all the parameters necessary for updating the instrument 
    #     # (amplitude, frequency , etc.)
    #     A = []
    #     if who in 'Input':
    #         for k in ['time constant','sensitivity','harmonic','filter order']: 
    #             A.append([k,self.dico[who].param(k).value()])
    #     if who in 'Generator':
    #         for k in ['amplitude','on','frequency']: 
    #             A.append([k,self.dico[who].param(k).value()])
    #     if who in 'Laser Driver':
    #         for k in ['on','current','temperature']:
    #             A.append([k,self.dico[who].param(k).value()])
    #     return A

    def waiting_time(self):
        tc = self.dico['Input'].param('time constant').value()
        wt = self.dico['Acquisition'].param('waiting time').value()
        return tc*wt

class MesCourbes(toolbox.mother_class.MotherCourbe):
    def __init__(self):
        self.plot_widget = PlotWidget()
        self.plot_widget.showGrid(x=True,y=True)
        # self.plot_widget.getPlotItem().addLegend()
        self.plot_widget.setBackground((0, 0, 0))
        #  dictionary with all the curve and their data 
        # curve 0 is dedicated to the live/acquisition plot
        self.curves = {}

    def add_curve(self, curve_id, curve_color, markers_on=False, linewidht=3):
        curve_name = curve_id
        pen = pg.mkPen(curve_color, width=linewidht)
        symbol = "o"
        symbolPen = pg.mkPen(0,0,0)
        symbolBrush = curve_color
        symbolSize = 8
        # this adds the item to the plot and legend
        if markers_on:
            plot = self.plot_widget.plot(
                name=curve_name, pen=pen, symbol=symbol, symbolPen=symbolPen, 
                symbolBrush=symbolBrush, symbolSize=symbolSize
            )
        else:
            plot = self.plot_widget.plot(name=curve_name, pen=pen)
        self.curves[curve_id] = {
                    'plot':plot,
                    'data':{'Freq':[],'X':[],'Y':[],'R':[],'Phi':[],'Gain':[],'Vexc':[],'Vout/Vexc':[],'Admittance':[]}
                        }

    def average(self,curve_id,param_tree):
        nbr_seqs = param_tree.give('Acquisition','nbr seqs')
        for k in ['X','Y','R','Phi','Freq','Vexc','Vout/Vexc','Admittance']:
            temp = self.curves[curve_id]['data'][k]
            self.curves[curve_id]['data'][k] = toolbox.function.average(temp,nbr_seqs)

    def create_frequency_list(self,curve_id,param_tree):
        '''
        Create a array with all the frequency use for the frequency sweep
        It take into account if many sequences have been asked
        '''
        freq_min = param_tree.give('Acquisition','freq min')
        freq_max = param_tree.give('Acquisition','freq max')
        nbr_pts = param_tree.give('Acquisition','nbr pts')
        nbr_seqs = param_tree.give('Acquisition','nbr seqs')
        freq_list = np.linspace(freq_min,freq_max,nbr_pts)
        if nbr_seqs > 1:
            temp = freq_list
            i=0
            while i < int(nbr_seqs)-1:
                i+=1
                freq_list = np.append(freq_list,temp[::(-1)**i])
        self.curves[curve_id]['data']['Freq'] = freq_list

    def display(self,curve_id,what,adjust=True):
        X = self.curves[curve_id]['data']['Freq']
        Y = self.curves[curve_id]['data'][what]
        # during acquisition freq is longer than the others datas
        # so it is useful to reduce it
        if adjust is True:
            if len(X) != len(Y):
                X = self.curves[curve_id]['data']['Freq'][0:len(Y)]
        self.set_values(curve_id,X,Y)

    def guess_initial_value(self,curve_data,freq_min,freq_max,param_tree):
        Freq = self.curves[curve_data]['data']['Freq'].copy()
        Yadm = self.curves[curve_data]['data']['Admittance'].copy()
        Freq, Yadm = toolbox.function.reduce_data_range(Freq,Yadm,freq_min,freq_max)
        try:
            # Fit linear
            a, b = polyfit(Freq, Yadm, deg=1) # Linear fit of the admittance
            Ynormalized = Yadm - (a+b*Freq) # Substracting the linear fit
            self.curves[curve_data]['data']['Admittance without C0'] = Ynormalized
            # guess C0
            C0 = b/(2*np.pi)
            print('Guess give : C0={:.6f}pF'.format(C0*1e12))
            # guess f0 and Q
            f0 = Freq[np.argmax(Ynormalized)]
            max_res = max(Ynormalized)    
            print('Guess give : f0={:.6f}kHz'.format(f0*1e-3))
            delta_f = 2*(f0-Freq[toolbox.function.find_nearest_index(Ynormalized,np.max(Ynormalized)/2)])
            Q = f0/delta_f
            print('Guess give : Q={:.6f}'.format(Q))
            param_tree.dico['Fit'].param('C0').setValue(C0)
            param_tree.dico['Fit'].param('f0').setValue(f0)
            param_tree.dico['Fit'].param('Q').setValue(Q)
            param_tree.dico['Fit'].param('max res').setValue(max_res)
        except:
            print('Can not fit the linear curve, pas possible :-(')
            param_tree.dico['Fit'].param('C0').setValue(1e-12)
            param_tree.dico['Fit'].param('f0').setValue(Freq[np.argmax(Yadm)])
            param_tree.dico['Fit'].param('Q').setValue(1)
            param_tree.dico['Fit'].param('max res').setValue(max(Yadm) )


    def lorentzian_fit(self,curve_data,freq_min,freq_max,param_tree):
        Freq = self.curves[curve_data]['data']['Freq'].copy()
        #Yadm = self.curves[curve_data]['data']['Admittance without C0'].copy()
        Yadm = self.curves[curve_data]['data']['Admittance'].copy()
        Freq, Yadm = toolbox.function.reduce_data_range(Freq,Yadm,freq_min,freq_max)
        # read initial value
        C0 = param_tree.dico['Fit'].param('C0').value()
        f0 = param_tree.dico['Fit'].param('f0').value()
        Q = param_tree.dico['Fit'].param('Q').value()
        max_res = param_tree.dico['Fit'].param('max res').value()
        amp = max_res*f0**2/Q
        # lorentzain function
        def lorentzian(freq, A, freq0, Qfactor,C):
            L = A/((freq0**2-freq**2)+1J*freq0*freq/Qfactor)+C*2*np.pi*freq
            return np.abs(L)     
        # fit 
        try:
            popt, pcov = scipy.optimize.curve_fit(
                                        lorentzian, 
                                        Freq,
                                        Yadm,
                                        p0=(amp,f0,Q,C0))
                                        #bounds=(0,float('Inf')))
            amp_fit, f0_fit, Q_fit, C0_fit = popt
            perr = np.sqrt(np.diag(pcov))
            amp_fit_sigma, f0_fit_sigma, Q_fit_sigma, C0_fit_sigma = perr
            print('The Lorentzian fit gives : f0={:.3f}kHz+/-{:.3f}Hz Q={:.1f}+/-{:.1f} C0={:.1f}+/-{:.1f}pF'.format(
                f0_fit/1e3, f0_fit_sigma, Q_fit, Q_fit_sigma, C0_fit*1e12, C0_fit_sigma*1e12))
            # uptdate paramtree
            param_tree.dico['Fit'].param('C0').setValue(C0_fit)
            param_tree.dico['Fit'].param('f0').setValue(f0_fit)
            param_tree.dico['Fit'].param('Q').setValue(Q_fit)
            param_tree.dico['Fit'].param('max res').setValue(amp_fit*Q/f0**2)
        except:
            print('Can not fit the curve, pas possible :-(')

    def update_data_fit(self,freq_min,freq_max,param_tree):
        # data
        X = self.curves['curve acq']['data']['Freq'].copy()
        Y = self.curves['curve acq']['data']['Admittance'].copy()
        X, Y = toolbox.function.reduce_data_range(X,Y,freq_min,freq_max)
        # value from praratree
        capa_fit = param_tree.dico['Fit'].param('C0').value()
        f0_fit = param_tree.dico['Fit'].param('f0').value()
        Q_fit = param_tree.dico['Fit'].param('Q').value()
        max_res = param_tree.dico['Fit'].param('max res').value()
        amp_fit = max_res *f0_fit**2/Q_fit 
        # data
        X_fit = np.linspace(int(min(X)),int(max(X)),1000)
        Y_fit = toolbox.function.lorentzianMAG(X_fit, amp_fit, f0_fit, Q_fit, capa_fit)        
        self.curves['curve acq fit']['data']['Freq'] = X_fit.copy() 
        self.curves['curve acq fit']['data']['Admittance'] = Y_fit.copy()
        self.curves['curve acq fit']['data']['Q'] = Q_fit
        self.curves['curve acq fit']['data']['f0'] = f0_fit
        self.curves['curve acq fit']['data']['C0'] = capa_fit

    def update_X_Y_R_Phi(self,curve_id,A):
        self.curves[curve_id]['data']['X'] = np.append(self.curves[curve_id]['data']['X'],A[0])
        self.curves[curve_id]['data']['Y'] = np.append(self.curves[curve_id]['data']['Y'],A[1])
        self.curves[curve_id]['data']['R'] = np.append(self.curves[curve_id]['data']['R'],A[2])
        self.curves[curve_id]['data']['Phi'] = np.append(self.curves[curve_id]['data']['Phi'],A[3])

    def update_admittance(self,curve_id,param_tree,A):
        # Add value for Vexc Vout .... juste after the end of the acquisition
        gain = param_tree.dico['Amplifier'].param('gain').value()
        self.curves[curve_id]['data']['Gain'] = np.append(self.curves[curve_id]['data']['Gain'],gain)
        Vexc = param_tree.dico['Generator'].param('amplitude').value()
        self.curves[curve_id]['data']['Vexc'] = np.append(self.curves[curve_id]['data']['Vexc'],Vexc/np.sqrt(2)) # square root 2 for pk to rms conversion
        transfertfunction = A[2]/Vexc*np.sqrt(2)
        ### ATTENTION all measurements before 11 may 2023 are false (cf manip 230511)
        '''
        self.curves[curve_id]['data']['Vexc'] = np.append(self.curves[curve_id]['data']['Vexc'],Vexc*np.sqrt(2)) # square root 2 for pk to rms conversion
        replace by
        self.curves[curve_id]['data']['Vexc'] = np.append(self.curves[curve_id]['data']['Vexc'],Vexc/np.sqrt(2)) # square root 2 for pk to rms conversion

        transfertfunction = A[2]/Vexc/np.sqrt(2)
        replace by
        transfertfunction = A[2]/Vexc*np.sqrt(2)

        '''
        self.curves[curve_id]['data']['Vout/Vexc'] = np.append(self.curves[curve_id]['data']['Vout/Vexc'],transfertfunction)
        admittance = transfertfunction/gain
        self.curves[curve_id]['data']['Admittance'] = np.append(self.curves[curve_id]['data']['Admittance'],admittance)

if __name__ == "__main__":
    win = MainWindow()
    win.show()
    pg.exec()