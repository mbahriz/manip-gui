import sys
import csv  # Import the csv module


import os
from collections import namedtuple

import pyqtgraph as pg
from pyqtgraph import PlotWidget
from pyqtgraph.Qt import QtWidgets, QtCore
from pyqtgraph.parametertree import interact, ParameterTree, Parameter
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QFileDialog,QLabel,QSlider,QDoubleSpinBox
from PyQt5.QtCore import QTimer


from pyqtgraph.dockarea import DockArea, Dock

import numpy as np
import toolbox.instrument 
import toolbox.parameter_tree
import toolbox.sauvegarder
import toolbox.mother_class
import scipy.optimize
import time


class MainWindow(QMainWindow):
    
    def __init__(self):
        super().__init__()
        # the tuple version of a dictionary, use to store width and height of the screen 
        Screen = namedtuple('Screen', 'width height')
        # get screen's information (height and width)
        # CAUTION this attribut can be modified by other method like .show
        # it can put all the values at 0, even they were welle defined
        self.screen = Screen(width=1500,height=800) 
        # dictionary with all the graphic elements 
        self.obj_graph = {'plot1': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'MAG'
                                  },
                          'plot2': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'RH'
                                   },
                          'plot3': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'T'
                                   }, 
                          'plot4': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'H2O'
                                   }, 
       
                          'param': {'dock': None,
                                    'tree': None,
                                    'parameter': None}}                          

        self.on_init()       
        # change the size of the graphic interface
        self.resize(self.screen.width, self.screen.height)
        # my timer use for acquisition in real time
        # self.my_timer = None
        # self.period_timer = 1
        # create an attribute with all my instruments 
        self.instruments = MesInstrus(param_tree=self.parameters)
        # display the GUI
        self.show()
        
        
        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.update_plot)
        self.time_values = []
        self.X_values = []
        self.Y_values = []
        self.R_values = []
        self.Phi_values = []
        
        
        #arduino
        self.temperature_values = []
        self.humidity_values = []
        self.water_conc_values = []
        
        
        
        self.time_interval = 0.1
        self.current_time = 0
        self.clear_on_start = True  # Flag to control clearing on start
      
    def start_plotting(self):
        #disable x-axis zoom buttom
        self.fixed_x_range = False
        self.apply_range_button.setEnabled(False)
        self.apply_range_button_2.setEnabled(False)
      
        
        
       
        #dynamic zoom auto
        self.plot_widget1.enableAutoRange('x')
        self.plot_widget2.enableAutoRange('x')
        self.plot_widget3.enableAutoRange('x')
        self.plot_widget4.enableAutoRange('x')
        self.plot_widget1.enableAutoRange('y')
        self.plot_widget2.enableAutoRange('y')
        self.plot_widget3.enableAutoRange('y')
        self.plot_widget4.enableAutoRange('y')

        #dynamic zoom slider enable 
        self.zoom_slider.setEnabled(True)
        
        
        
        if self.clear_on_start:
            self.clear_plotting()  # Clear the graphs only if clear_on_start is True
            self.timer = pg.QtCore.QTimer()
            self.timer.timeout.connect(self.update_plot)
        self.running = True
        
        
        
        self.time_interval = int(20+self.parameters.waiting_time())
        self.timer.start(self.time_interval)  #time loop 
        self.start_time = time.time()
        
    def stop_plotting(self):
        self.running = False
        self.timer.stop() 
        self.clear_on_start = True  # Set flag to False when stopped
        
        self.min_spinbox.setEnabled(True)
        self.max_spinbox.setEnabled(True)
        self.apply_range_button.setEnabled(True)
        self.apply_range_button_2.setEnabled(True)
        self.zoom_slider.setEnabled(False)
        
        
        self.plot_data1.setData(self.time_values, self.R_values)
        
        instru3 = self.parameters.give_inst('Arduino')

        if instru3=='Arduino_DHT' or instru3=='Arduino_GPIB':

                
            self.plot_data2.setData(self.time_values, self.humidity_values)
            self.plot_data3.setData(self.time_values, self.temperature_values)
            self.plot_data4.setData(self.time_values, self.water_conc_values)
        
        
    #     #for a dynamic zoom purpose
    #     self.timer = pg.QtCore.QTimer()
    #     self.time_interval = self.parameters.poll_length()
    #     self.timer.start(int(self.parameters.poll_length()*10e3))  # Update every 100 milliseconds
    #     self.timer.timeout.connect(self.update_stop)
        
    # def update_stop(self):
    #     if self.running == False:
    #         self.plot_data1.setData(self.time_values[self.zoom_slider.value():], self.R_values[self.zoom_slider.value():])
    #         self.plot_data2.setData(self.time_values[self.zoom_slider.value():], self.humidity_values[self.zoom_slider.value():])
    #         self.plot_data3.setData(self.time_values[self.zoom_slider.value():], self.temperature_values[self.zoom_slider.value():])
    #         self.plot_data4.setData(self.time_values[self.zoom_slider.value():], self.water_conc_values[self.zoom_slider.value():])
        
        
        
    def clear_plotting(self):
        self.time_values = []
        self.X_values = []
        self.Y_values = []
        self.R_values = []
        self.Phi_values = []
        
        
        
        #arduino
        self.temperature_values = []
        self.humidity_values = []
        self.water_conc_values = []

        # Clear the plot data and reset the time for both plots
        self.plot_data1.setData([], [])
        self.plot_data2.setData([], [])
        self.plot_data3.setData([], [])
        self.plot_data4.setData([], [])
        
        self.current_time = 0  # Reset time
        self.running = False  # Stop updating the plots
        self.timer.stop()



    def run(self):
        self.show()
        sys.exit(QApplication.instance().exec_())
      
        
    def on_init(self):
        # for debug it is useful to separate the create         
        self.create_dock()
        self.create_contenu_dock_pg()
        self.create_contenu_dock_plot()

        
    def create_dock(self):
        area = DockArea()
        self.obj_graph['param']['dock'] = Dock("CA Continous Aqcuisition setting parameters")
        
        
        #mfli
        self.obj_graph['plot1']['dock'] = Dock("R (Vrms) function of time (s)")
        self.obj_graph['plot2']['dock'] = Dock("RH (%) function of time (s)")
        self.obj_graph['plot3']['dock'] = Dock("T (°C) function of time (s)")
        self.obj_graph['plot4']['dock'] = Dock("H2O (ppm) function of time (s)")
        
        
        
        
        area.addDock(self.obj_graph['param']['dock'],'left')
        
        
        area.addDock(self.obj_graph['plot1']['dock'],'right')
        area.addDock(self.obj_graph['plot2']['dock'],'bottom',self.obj_graph['plot1']['dock'])
        area.addDock(self.obj_graph['plot3']['dock'],'bottom',self.obj_graph['plot2']['dock'])
        area.addDock(self.obj_graph['plot4']['dock'],'bottom',self.obj_graph['plot3']['dock'])
        
        
        
        self.dockArea = DockArea()
        self.setCentralWidget(self.dockArea)
        
        dock2 = Dock("Zoom", size=(0.1, 0.1))
        self.dockArea.addDock(dock2, 'top',self.obj_graph['param']['dock'] )
        self.zoom_slider = QSlider(orientation=1)  # 1 for horizontal orientation
        self.min_slide=-200
        self.zoom_slider.setMinimum(self.min_slide)
        self.zoom_slider.setMaximum(-2)
        
        
        self.zoom_slider.setValue(-100)#valeur initial nbre de points affichier
        self.zoom_slider.setSingleStep(1)  # Set the step to 1
      

        
        # self.slider_value_label = QLabel("<center>Display last {} data points </center>".format(abs(self.zoom_slider.value())))
        
       
        
        dock2.addWidget(self.zoom_slider)


        ##############################################################
        
        # dock3=Dock("X-axis", size=(0.8, 0.8))
        # self.dockArea.addDock(dock3, 'top',dock2)
        

        # self.min_max_layout = QHBoxLayout()
        self.min_label = QLabel("Min X:")
        self.min_spinbox = QDoubleSpinBox()
        
        self.min_spinbox.setFixedSize(100, 25)
        self.min_spinbox.setMaximum(200000000000000000000000)
        self.min_spinbox.setValue(-10)  # Set a default minimum value

        self.max_label = QLabel("Max X:")
        self.max_spinbox = QDoubleSpinBox()
        self.max_spinbox.setFixedSize(100, 25)
        self.max_spinbox.setMaximum(200000000000000000000000) 
        self.max_spinbox.setValue(200)  # Set a default maximum valuee
        
       

        self.apply_range_button = QPushButton("Apply X-Axis Range")
        self.apply_range_button.clicked.connect(self.apply_x_axis_range)
        
        self.apply_range_button_2 = QPushButton("Auto")
        self.apply_range_button_2.clicked.connect(self.apply_auto_range)
        
        
        dock2_layout = QHBoxLayout()
        # Add widgets to the horizontal layout
        dock2_layout.addWidget(self.min_label)
        dock2_layout.addWidget(self.min_spinbox)
        dock2_layout.addWidget(self.max_label)
        dock2_layout.addWidget(self.max_spinbox)
        dock2_layout.addWidget(self.apply_range_button)
        dock2_layout.addWidget(self.apply_range_button_2)
        

        # Create a widget for dock3 and set the layout
        dock2_widget = QWidget()
        dock2_widget.setLayout(dock2_layout)
        
        
        
        dock2.addWidget(dock2_widget)


        
        
        
        self.fixed_x_range = False#flag to activate xaxis range zoom
        self.apply_range_button.setEnabled(False)  # Disable the button initially
        self.apply_range_button_2.setEnabled(False)  # Disable the button initially
        
        self.setCentralWidget(area)
        
    def create_contenu_dock_plot(self):
        # creation of plot 1 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot1']['curve'] = MesCourbes()
        self.obj_graph['plot1']['graph'] = self.obj_graph['plot1']['curve'].plot_widget 
        
        self.plot_widget1=self.obj_graph['plot1']['graph']
        self.obj_graph['plot1']['dock'].addWidget(self.obj_graph['plot1']['graph'])       
        self.obj_graph['plot1']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data1=self.obj_graph['plot1']['graph'].plot(pen='g', symbol='o',symbolBrush='g')
        
        # creation of plot 2 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot2']['curve'] = MesCourbes()
        self.obj_graph['plot2']['graph'] = self.obj_graph['plot2']['curve'].plot_widget 
        self.plot_widget2=self.obj_graph['plot2']['graph']
        self.obj_graph['plot2']['dock'].addWidget(self.obj_graph['plot2']['graph'])       
        self.obj_graph['plot2']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data2=self.obj_graph['plot2']['graph'].plot(pen='c', symbol='o',symbolBrush='c')
        
        # creation of plot 3 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot3']['curve'] = MesCourbes()
        self.obj_graph['plot3']['graph'] = self.obj_graph['plot3']['curve'].plot_widget 
        self.plot_widget3=self.obj_graph['plot3']['graph']
        self.obj_graph['plot3']['dock'].addWidget(self.obj_graph['plot3']['graph'])       
        self.obj_graph['plot3']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data3=self.obj_graph['plot3']['graph'].plot(pen='r', symbol='o',symbolBrush='r')
        
        # creation of plot 4 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot4']['curve'] = MesCourbes()
        self.obj_graph['plot4']['graph'] = self.obj_graph['plot4']['curve'].plot_widget 
        self.plot_widget4=self.obj_graph['plot4']['graph']
        self.obj_graph['plot4']['dock'].addWidget(self.obj_graph['plot4']['graph'])       
        self.obj_graph['plot4']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data4=self.obj_graph['plot4']['graph'].plot(pen='w', symbol='o',symbolBrush='w')
        
        # plot1_width = 1500  # Set your desired width for plot1
        # plot1_height = 500  # Set your desired height for plot1
    
        # self.obj_graph['plot1']['graph'].setFixedWidth(plot1_width)
        # self.obj_graph['plot1']['graph'].setFixedHeight(plot1_height)

        
    def apply_x_axis_range(self):
        self.fixed_x_range = True
        self.plot_widget1.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        self.plot_widget2.setXRange(self.min_spinbox.value(), self.max_spinbox.value())    
        self.plot_widget3.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        self.plot_widget4.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        

        
        
    def apply_auto_range(self):
         self.fixed_x_range = True
         self.plot_widget1.enableAutoRange('x')
         self.plot_widget2.enableAutoRange('x')
         self.plot_widget3.enableAutoRange('x')
         self.plot_widget4.enableAutoRange('x')
         
         
         self.plot_widget1.enableAutoRange('y')
         self.plot_widget2.enableAutoRange('y')
         self.plot_widget3.enableAutoRange('y')
         self.plot_widget4.enableAutoRange('y')
         
   
      
    def create_contenu_dock_pg(self):
        self.obj_graph['param']['tree'] = ParameterTree()
        self.create_parameter()
        self.obj_graph['param']['dock'].addWidget(self.obj_graph['param']
                                                  ['tree'])
        
    def create_parameter(self):
        # create a Class with all parameters use in the GUI
        self.parameters = MesParams()
        # build the GUI
        (self.obj_graph['param']
         ['parameter']) = Parameter.create(name='params',
                                           type='group',
                                           children=self.parameters.tree)
        (self.obj_graph['param']
         ['parameter'].sigTreeStateChanged.connect(self.catch_param_change))
    
        """
        setParameters
        Set the top-level :class:`Parameter <pyqtgraph.parametertree.Parameter>`
        to be displayed in this ParameterTree.
    
        If *showTop* is False, then the top-level parameter is hidden and only 
        its children will be visible. This is a convenience method equivalent 
        to::
        
            tree.clear()
            tree.addParameters(param, showTop)
        """
        self.obj_graph['param']['tree'].setParameters((self.obj_graph['param']
                                                       ['parameter']),
                                                      showTop=False)
        self.obj_graph['param']['tree'].setWindowTitle('Parameter Tree')    

    def catch_param_change(self, _, changes):
        
        # call in create_parameter()
        for param, _, data in changes:
            path = self.obj_graph['param']['parameter'].childPath(param)
            if path is not None: 
                # print(path) -> ['Input- Zurich', 'time constant']
                child_name = '.'.join(path)
                # print(child_name) -> Input- Zurich.time constant
            else:
                child_name = param.name()
            print('  parameter: %s' % child_name)
            print('  change:    %s' % _)
            print('  data:      %s (%s)' % (str(data),type(data)))
    
            # who is the name of the parameter tree
            # what the name of the paramater of the parameter tree
            try:
                who, what = child_name.split('.')
            except:
                who = child_name
                what = child_name 
                
            graph_1 = self.obj_graph['plot1']['curve']
            graph_2 = self.obj_graph['plot2']['curve']
            graph_3 = self.obj_graph['plot3']['curve']
            graph_4 = self.obj_graph['plot4']['curve']
    
            ## INSTRUMENT CHANGE
            # CAUTION check it always in first
            if what in ['instrument']:
                # in this case data == name of the instrument
                print(who,data)
                self.instruments.update(who,data)
    
            ## CHANGE VALUES ASKED ON THE INSTRUMENT
            if who in ['Input','Generator','Gen_ext_1','Laser Driver','Pulse module']:
                #if what in ['time constant','sensitivity','amplitude','on','frequency','external reference','current','temperature','internal modulation (Square signal)','frequency modulation','amplitude modulation','harmonic']:
                    # get the name of instrument asked in the parameter tree 
                instru = self.parameters.give_inst(who)
                self.instruments.set_value(instru,what,data)
                # if who == what:
                #     self.instruments.update(who,data)    
                   
            ## START ACQUISITION
            if (who+'.'+what) in 'Plotter.start':
                
                
                # clear first then start
                for k in ['acq']:
                    # earase data
                    graph_1.clear_data('curve '+k)
                    graph_2.clear_data('curve '+k)
                    graph_3.clear_data('curve '+k)
                    graph_4.clear_data('curve '+k)
                    
                

                
                self.start_plotting()
                
                # update old and new acquisition
                # on lance un timer qui appelera 'timerEvent' toutes les period_timer
                # if self.my_timer is None:
                #     self.my_timer = self.startTimer(self.period_timer) #c une methode heritée
                # the method timeEvent will be executed each period_timer
            
                          
            if (who+'.'+what) in 'Plotter.stop': 
                self.stop_plotting()
            
            ## ERASE ACQUISITION
            if (who+'.'+what) in 'Plotter.clear all':
                
                self.clear_plotting()
                
                for k in ['acq']:
                    # earase data
                    graph_1.clear_data('curve '+k)
                
                
                # remove list of curve in the paramter tree and a new one
                self.parameters.dico['Graph'].clearChildren()
                self.parameters.dico['Graph'].addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})
    
    
            ## SAVE ACQUISITION
            if (who+'.'+what) in 'Save.save':
                

                # update only the parameters from the section Save and Graph
                graph_1.save_parameters_sauvegarder(curve_id='curve acq',param=self.parameters)
                # save everything                
                # toolbox.sauvegarder.Save('Continous Acquisition',self.obj_graph['plot']['curve'].curves,self.obj_graph['plot2']['curve'].curves)
                toolbox.sauvegarder.Save('Continous Acquisition',self.obj_graph['plot1']['curve'].curves)
                # options = QFileDialog.Options()
                # options |= QFileDialog.DontUseNativeDialog
                # file_name, _ = QFileDialog.getSaveFileName(self, "Save to CSV", "", "CSV Files (*.csv)", options=options)
                # if file_name:
                #     if not file_name.endswith('.csv'):
                #         file_name += '.csv'  # Append .csv if not already included
            
                #     with open(file_name, 'w', newline='') as csvfile:
                #         csvwriter = csv.writer(csvfile, delimiter=';')
                #         csvwriter.writerow(['Time', 'R(Vrms)', 'Phi(Deg)'])
                #         csvwriter.writerows(zip(self.time_values, self.R_values, self.Phi_values))
                
                
    
    # def timerEvent(self, _):
        
    #     instru2 = self.parameters.give_inst('Input')
    #     # time.sleep(self.parameters.waiting_time())
        
    #     try:
    #         t_poll = self.parameters.give('Input', 'poll length')
    #     except:
    #         t_poll = None
    
    #     temp = self.instruments.get_X_Y_R_Phi(instru2, poll_length=t_poll)
        
    #     # update data
     
    #     # during acquisition freq is longer than the others datas
    #     # so it is useful to reduce it

            
        
    #     self.X_value=temp[0]
    #     self.Y_value=temp[1]
    #     self.R_value=temp[2]
    #     self.Phi_value=temp[3]
        
    def update_plot(self):
        instru2 = self.parameters.give_inst('Input')
        instru3 = self.parameters.give_inst('Arduino')
        if self.running:
            try:
                t_poll = self.parameters.give('Input', 'poll length')
            except:
                t_poll = None
        
            temp = self.instruments.get_X_Y_R_Phi(instru2, poll_length=t_poll)
            
            self.X_value=temp[0]
            self.Y_value=temp[1]
            self.R_value=temp[2]
            self.Phi_value=temp[3]
            
            if instru3=='Arduino_DHT' or instru3=='Arduino_GPIB' :
                 temp_arduino = self.instruments.get_data(instru3,poll_length=t_poll)
                 if temp_arduino is not None:
                     self.humidity_value = temp_arduino[0]
                     self.temperature_value = temp_arduino[1]
                     self.water_conc_value = temp_arduino[2]
            
            self.current_time = time.time() - self.start_time
            self.time_values.append(round(self.current_time,3))
            
            self.X_values.append(self.X_value)
            self.Y_values.append(self.Y_value)
            self.R_values.append(self.R_value)
            self.Phi_values.append(self.Phi_value)
            
            # self.slider_value_label.setText("<center>Display last {} data points </center>".format(abs(self.zoom_slider.value())))

            self.plot_data1.setData(self.time_values[self.zoom_slider.value():], self.R_values[self.zoom_slider.value():])
            
            self.obj_graph['plot1']['curve'].plot_widget.setTitle(self.obj_graph['plot1']['title'])
            self.obj_graph['plot2']['curve'].plot_widget.setTitle(self.obj_graph['plot2']['title'])
            self.obj_graph['plot3']['curve'].plot_widget.setTitle(self.obj_graph['plot3']['title'])
            self.obj_graph['plot4']['curve'].plot_widget.setTitle(self.obj_graph['plot4']['title'])

            
            graph_1 = self.obj_graph['plot1']['curve']
            graph_1.curves['curve acq']['data']['title1']=self.obj_graph['plot1']['title']
            graph_1.curves['curve acq']['data']['title2']=self.obj_graph['plot2']['title']
            graph_1.curves['curve acq']['data']['title3']=self.obj_graph['plot3']['title']
            graph_1.curves['curve acq']['data']['title4']=self.obj_graph['plot4']['title']
            
            graph_1.curves['curve acq']['data']['tim'] = np.append(graph_1.curves['curve acq']['data']['tim'],self.current_time)
            
            
            graph_1.curves['curve acq']['data']['R'] = np.append(graph_1.curves['curve acq']['data']['R'],self.R_value)
            graph_1.curves['curve acq']['data']['X'] = np.append(graph_1.curves['curve acq']['data']['X'],self.X_value) 
            graph_1.curves['curve acq']['data']['Y'] = np.append(graph_1.curves['curve acq']['data']['Y'],self.Y_value)
            graph_1.curves['curve acq']['data']['Phi'] = np.append(graph_1.curves['curve acq']['data']['Phi'],self.Phi_value)
            
           
            
            
            if instru3=='Arduino_DHT' or instru3=='Arduino_GPIB':
                self.humidity_values.append(self.humidity_value)
                self.temperature_values.append(self.temperature_value)
                self.water_conc_values.append(self.water_conc_value)
                
                self.plot_data2.setData(self.time_values[self.zoom_slider.value():], self.humidity_values[self.zoom_slider.value():])
                self.plot_data3.setData(self.time_values[self.zoom_slider.value():], self.temperature_values[self.zoom_slider.value():])
                self.plot_data4.setData(self.time_values[self.zoom_slider.value():], self.water_conc_values[self.zoom_slider.value():])
                                               
                
            
                graph_1.curves['curve acq']['data']['RH'] = np.append(graph_1.curves['curve acq']['data']['RH'],self.humidity_value)
                graph_1.curves['curve acq']['data']['T'] = np.append(graph_1.curves['curve acq']['data']['T'],self.temperature_value)
                graph_1.curves['curve acq']['data']['H2O'] = np.append(graph_1.curves['curve acq']['data']['H2O'],self.water_conc_value)
            
           
            else:
                self.humidity_value=0
                self.temperature_value=0
                self.water_conc_value=0
                graph_1.curves['curve acq']['data']['RH'] = np.append(graph_1.curves['curve acq']['data']['RH'],self.humidity_value)
                graph_1.curves['curve acq']['data']['T'] = np.append(graph_1.curves['curve acq']['data']['T'],self.temperature_value)
                graph_1.curves['curve acq']['data']['H2O'] = np.append(graph_1.curves['curve acq']['data']['H2O'],self.water_conc_value)
                
            graph_1.save_parameters('curve acq',self.parameters)
            
            # self.current_time += self.time_interval
            if self.min_slide>-2147483648:
                self.min_slide-=1
                self.zoom_slider.setMinimum(self.min_slide)
                
     
            
    def closeEvent(self, event):
        result = QtWidgets.QMessageBox.question(self,
                                                "Confirm Exit...",
                                                "Do you want to exit ?",
                                                (QtWidgets.QMessageBox.Yes |
                                                  QtWidgets.QMessageBox.No))
        if result == QtWidgets.QMessageBox.Yes:
            # permet d'ajouter du code pour fermer proprement
            # close all instrument
           
            self.instruments.close_all_inst()
            self.timer.stop() 
            
            # print("Serial connection closed.")
            event.accept()
        else:
            event.ignore()
        pass    
            

        

class MesInstrus(toolbox.mother_class.MotherInstru):
    def __init__(self,param_tree):
        self.param_tree = param_tree
        # attribut with all the instrument created
        self.connected = []
        # dico that the code will fill with instru which can be called
        self.use = {} 
        
class MesParams(toolbox.mother_class.MotherParam):
    def __init__(self):
        # dictionary of parameter tree used
        self.dico = {
            
            'Laser Doppler Vibrometer':toolbox.parameter_tree.LaserDopplerVibrometer(name='Laser Doppler Vibrometer'),
            'Arduino':toolbox.parameter_tree.Arduino(name='Arduino'),
            'Input':toolbox.parameter_tree.Input(name='Input'),
            'Generator':toolbox.parameter_tree.Generator(name='Generator'),
            'Pulse module':toolbox.parameter_tree.PulseModule(name='Pulse module'),
            'Laser Driver':toolbox.parameter_tree.LaserDriver(name='Laser Driver'),
            'Plotter':toolbox.parameter_tree.Plotter(name='Plotter'),
            'Graph':toolbox.parameter_tree.GraphList(name='Graph'),
            'Save':toolbox.parameter_tree.Save(name='Save')}
        # use the self.dico to build a list required for the GUI
        self.tree = []       
        for k in self.dico:
            self.tree.append(self.dico[k]) 
    def waiting_time(self):
        tc = self.dico['Input'].param('time constant').value()
        wt = self.dico['Plotter'].param('waiting time').value()
        return tc*wt
    
    def poll_length(self):
        return self.dico['Input'].param('poll length').value()
    
class MesCourbes(toolbox.mother_class.MotherCourbe):
    def __init__(self):
        self.plot_widget = PlotWidget()
        self.plot_widget.showGrid(x=True,y=True)
        # self.plot_widget.getPlotItem().addLegend()
        self.plot_widget.setBackground((0, 0, 0))
        #  dictionary with all the curve and their data 
        # curve 0 is dedicated to the live/acquisition plot
        self.curves = {}

    def add_curve(self, curve_id, curve_color, markers_on=False, linewidht=3):
        curve_name = curve_id
        pen = pg.mkPen(curve_color, width=linewidht)
        symbol = "o"
        symbolPen = pg.mkPen(0,0,0)
        symbolBrush = curve_color
        symbolSize = 8
        # this adds the item to the plot and legend
        if markers_on:
            plot = self.plot_widget.plot(
                name=curve_name, pen=pen, symbol=symbol, symbolPen=symbolPen, 
                symbolBrush=symbolBrush, symbolSize=symbolSize
            )
        else:
            plot = self.plot_widget.plot(name=curve_name, pen=pen)
        self.curves[curve_id] = {
                    'plot':plot,
                    'data':{'tim':[],'X':[],'Y':[],'R':[],'Phi':[],'RH':[],'T':[],'H2O':[],
                    'title1':[],'title2':[],'title3':[],'title4':[]
                            
                            }
                        }   
        


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()
    main.run() 
    pg.exec()       

       