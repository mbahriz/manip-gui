#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 22 15:06:50 2022

@author: michaelbahriz

to estimate the displacement from LDV measurement 
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import toolbox.function
import os

# =============================================================================
# INPUT + READ CSV
# =============================================================================

path = os.path.dirname(os.path.abspath(__file__))+'\\' # get the path of the file
print('path :',path)
file = 'YYY.csv'
sample_name = 'XXX'

data = pd.read_csv(path+file, sep=';', skiprows=[1])
print(data.head())

freq = data['Frequency (Hz)']
Vrms = data['r (Vrms)']

ldv_sensitivity = 2e-3 # in [m/s/v]

# =============================================================================
# CONNVERTION
# =============================================================================

V = np.sqrt(2)*Vrms
speed = V*ldv_sensitivity
disp = speed/(2*np.pi*freq)


# =============================================================================
# GRAPH 
# =============================================================================

toolbox.function.load_mpl_params()
plt.plot(freq/1e3, disp*1e9, 'bo', label=sample_name)
plt.legend(loc=0)
plt.xlabel('frequency (kHz)')
plt.ylabel('displacement (nm)')
plt.savefig(path+file.replace('.csv','')+'_disp.png')
plt.show()

