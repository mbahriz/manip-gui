import numpy as np

# A = [1, 2, 3]
# B = A[::-1]
# print('A',A)
# print('B',B)

# i=0
# while i < 4:
#     i+=1
#     for k in A[::(-1)**i]:
#         A.append(k)
#     print(i)
#     print('A',A)

C = np.linspace(1,4,4)
print(C)
temp = C
i=0
ns = 5
while i < ns-1:
    i+=1
    C = np.append(C,temp[::(-1)**i])    
    print('i:',i,' C:',C)   

i=0
nv = len(C)
print('nv',nv)
print('nv/ns',nv/ns)
D=[]
while i <=ns-1:
    i+=1
    TEMP = C[int(nv/ns)*(i-1):int(nv/ns)*i]
    TEMP = TEMP[::(-1)**(i+1)]
    D.append(TEMP)
print('D',D)     
E = np.mean(D,axis=0)
print('E',E)

myList=["1ab","2ac","3ab","4b","5ac","6ac","7ac","8ab","8b","10ac","11ac"]
print(myList.count('a'))
print(myList)
print(myList[:-1])
print(myList[::-1])
print(myList[:0:-1])
myList = myList[:0:-1]
print(myList)

A = np.linspace(1,len(myList)+1,len(myList)+1)
B = ['{:0>2}'.format(int(k)) for k in A]
print(A)
print(B)

A = [1,2,3,4]
print(A,len(A),np.diff(A))