'''
code on the PICO
as its name is mian.py
it will automaticaly run on the PICO when it is plugged
'''


from machine import Pin
import utime

# POUR QUE CA MARCHE THONNY DOIT ETRE FERME
# ET AVOIR DEBRANCHE ET REBRANCHE LE PICO

# use onboard LED which is controlled by Pin 25
LED = Pin(25, Pin.OUT)

# Turn the LED on
def on():
    LED.value(1)

# Turn the LED off
def off():
    LED.value(0)

def blink(t,dt):
    total = int(t/dt/2)
    print(total)
    for i in range (0,total):
        on()
        utime.sleep(dt)
        off()
        utime.sleep(dt)

