'''
code on the HOST computer
'''


import serial
import time

def find_PICO():
    port_list = serial_ports_list()
    for port, desc, hwid in sorted(port_list):
        print("{}: {} [{}]".format(port, desc, hwid))
        if 'VID:PID=2E8A:0005' in hwid:
            port_PICO = port
        else:
            port_PICO = None
    print('PICO port is : ',port_PICO)
    return port_PICO	

def serial_ports_list():
    import serial.tools.list_ports
    port_list = list(serial.tools.list_ports.comports())
    print(port_list)
    if len(port_list) == 0:
        print(' no available serial port ')
    else:
        for i in range(0,len(port_list)):
            print(port_list[i])
    return port_list

class Sender:
    TERMINATOR = '\r'.encode('UTF8')  # '\r' is Carriage Return

    def __init__(self, device='/dev/ttyACM0', baud=9600, timeout=1):
        self.serial = serial.Serial(device, baud, timeout=timeout)

    def receive(self) -> str:
        line = self.serial.read_until(self.TERMINATOR)
        return line.decode('UTF8').strip()

    def send(self, text: str) -> bool: # text est du type string et la fonction renvoie un boolean
        line = '%s\r\f' % text
        self.serial.write(line.encode('UTF8'))

        # the line should be echoed.
        # If it isn't, something is wrong.
        return text == self.receive()

    def close(self):
        self.serial.close()

if __name__ == "__main__":
    portx = find_PICO()
    PICO = Sender(device=portx)

    echo = PICO.send('2 + 2')
    print('2 + 2 echo : ', echo)
    receive = PICO.receive()
    print('2 + 2 receive: ', receive)
    time.sleep(0.5)

    echo = PICO.send('on()')
    print('on() echo : ', echo)
    receive = PICO.receive()
    print('on() receive: ', receive)
    time.sleep(1)

    echo = PICO.send('off()')
    print('off() echo : ', echo)
    receive = PICO.receive()
    print('off() receive: ', receive)
    time.sleep(1)

    echo = PICO.send('blink(t=5,dt=0.1)')
    print('blink() echo : ', echo)
    receive = PICO.receive()
    print('blink() receive: ', receive)

    PICO.close()


'''
 Controlling a Raspberry Pi Pico remotely using PySerial

You can use a Raspberry Pi Pico as a powerful peripheral to a host - a Raspberry Pi, a Jetson Nano, a laptop or workstation. In this article you'll see how to interact with a Pico running MicroPython or CircuitPython by writing some Python code that runs on the host.

The software is easy to use. It enables you to send a Python statement to the Pico and read the results. The statement can be any valid MicroPython code.
Setting up the host and the Pico

For this article I've used a Raspberry Pi as the host, but any computer running Windows, Linux or Mac OS will do so long as it has Python 3.5 or later installed.

You can use this technique to connect a Raspberry Pi Pico to a Jetson Nano or any other member of the Jetson family.

You'll need to install MicroPython on the Pico. You'll find instructions for MicroPython installation in the official Getting Started Guide which I reviewed recently.

 The £10 Guide is worth buying, but if you can't wait for your copy to arrive you can download a free pdf.

The guide will also tell you how to use the Thonny editor to install the necessary code on your Pico.
Connect the host and the Pico

First, connect the host to the Pico using a USB data lead. Some USB leads only supply power. They will not work.
Install software on the Pico

Next, install the blinker script.

The blinker.py script runs on the Pico, but you should rename it to main.py. Here's the code:

from machine import Pin

#use onboard LED which is controlled by Pin 25
led = Pin(25, Pin.OUT)


# Turn the LED on
def on():
    led.value(1)

# Turn the LED off
def off():
    led.value(0)

    Install it on the Pico using the Thonny editor.
        Open the gist on GitHub.
        Copy the code to your clipboard.
        Open Thonny and make sure it has connected to the Pico.
        Paste the code into the Thonny editor window.
        Save it on the Pico as main.py.
        Close the Thonny editor.

If you leave the Thonny editor open it will keep the serial port open on the host, and the serial program below will not work!

Since you saved the program as main.py it will run on the Pico automatically.

The sender.py script runs on the host. It uses PySerial to send commands from the host to the Raspberry Pi Pico and read the result.

Here's the sender code:

import serial


class Sender:
    TERMINATOR = '\r'.encode('UTF8')


    def __init__(self, device='/dev/ttyACM0', baud=9600, timeout=1):
        self.serial = serial.Serial(device, baud, timeout=timeout)

    def receive(self) -> str:
        line = self.serial.read_until(self.TERMINATOR)
        return line.decode('UTF8').strip()

    def send(self, text: str) -> bool:
        line = '%s\r\f' % text
        self.serial.write(line.encode('UTF8'))

        # the line should be echoed.
        # If it isn't, something is wrong.
        return text == self.receive()

    def close(self):
        self.serial.close()


On the host

    Run pip3 install pyserial.
    Copy sender.py from this github gist into an editor and save it in a directory of your choice.
    In that directory, run python3 to start an interactive session.
    Type from sended import Sender
    Type s = Sender(). If you are running on Windows, you will need to type s = Sender('COM6') replacing COM6 by whatever port the Pico is visible on.
    Type s.send('2 + 2')
    Type s.receive(). If all is well, this will print the result 4.
    Type s.send('on()'. The on-board LED on the Pico should turn on.
    Type s.send('off()'. The on-board LED on the Pico should turn off.
    When you have finished, type s.close().

Of course in a real application you'd normally create the sender, send and receive data using a Python script. In a future post I'll show how to do that to create a simple weather station with the Pico and plot the temperature and light level on the host. Before that I'll explore other ways of connecting and controlling the Pico from a Host.
To keep up to date with this series follow @rareblog on twitter.
'''