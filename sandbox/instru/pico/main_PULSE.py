'''
code on the PICO
as its name is main.py
it will automaticaly run on the PICO when it is plugged
'''


from machine import Pin
import utime

# POUR QUE CA MARCHE THONNY DOIT ETRE FERME
# ET AVOIR DEBRANCHE ET REBRANCHE LE PICO

# use onboard LED which is controlled by Pin 25
LED = machine.PWM(machine.Pin(25))
OUT = machine.PWM(machine.Pin(15))

# Turn the LED on
def pulse(frequency, duty_cycle): # feq in Hz duty in %
    convertion = 65535/100
    for gpio in [LED, OUT]:
        gpio.freq(int(frequency))
        gpio.duty_u16(int(duty_cycle*convertion))


