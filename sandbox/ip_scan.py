import socket

for ping in range(1,254):
    address = "162.38.208." + str(ping)
    socket.setdefaulttimeout(10)
    try:
        hostname, alias, addresslist = socket.gethostbyaddr(address)

    except socket.herror:
        hostname = None
        alias = None
        addresslist = address

    if hostname is not None:
        print(addresslist, '=>', hostname)

# import subprocess
  
# for ping in range(1,10):
#     address = "162.38.208." + str(ping)
#     res = subprocess.call(['ping', '-c', '3', address])
#     if res == 0:
#         print( "ping to", address, "OK")
#     elif res == 2:
#         print("no response from", address)
#     else:
#         print("ping to", address, "failed!")