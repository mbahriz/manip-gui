import toolbox.instrument
import time
import numpy as np

MyZurich = toolbox.instrument.Instrument(name='Zurich-MFLI_dev4199')



MyZurich.set_value('on', True)

MyZurich.set_value('sensitivity', 500e-2)
# Set the filter order
MyZurich.set_value('harmonic', 1)
MyZurich.set_value('time constant', 0.2)


MyZurich.inst_obj.daq.unsubscribe('*')
demod_index = 0
path = path = '/%s/demods/%d/sample' % (MyZurich.inst_obj.dev, demod_index) 
MyZurich.inst_obj.daq.subscribe(path)


def get_noise_spectral_density():	
	poll_length=0.1
	poll_timeout = 100
	data = MyZurich.inst_obj.daq.poll(poll_length, poll_timeout, True)
	dev = MyZurich.inst_obj.dev
	xdata = data[dev]['demods'][str(demod_index)]['sample']['x']
	ydata = data[dev]['demods'][str(demod_index)]['sample']['y']
	time_stamp = data[dev]['demods'][str(demod_index)]['sample']['timestamp']
	x_mean = np.mean(xdata)
	x_std = np.std(xdata) 
	y_mean = np.mean(ydata)
	y_std = np.std(ydata)
	rdata = np.abs(xdata + 1j*ydata)
	r_mean = np.mean(rdata)
	r_std = np.std(rdata)
	
	



print(xdata)
print('x_mean = {}'.format(x_mean))
print('x_std = {}'.format(x_std))

""" data = MyZurich.poll(poll_length=0.1)
x,y,r,phi = data
print(data) """


"""
freq_start = 32750
freq_stop = 32765
freq_step = 1
frequencies = np.arange(freq_start, freq_stop, freq_step)

for frequency in frequencies:
	MyZurich.set_value('frequency', frequency)
	time.sleep('')

"""


MyZurich.disconnect()