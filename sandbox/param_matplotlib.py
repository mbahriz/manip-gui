import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

print(mpl.rcParamsDefault.keys)

number_color = 5
cmap = plt.get_cmap('tab20b')
colors = [cmap(i) for i in np.linspace(0, 1, number_color)] 
print(colors)