import pandas as pd

def dataframe_to_dokuwiki_table(df):
    # Get the column names as a list
    columns = list(df.columns)

    # Create the header row
    header_row = "^ " + " ^ ".join(columns) + " "

    # Create the data rows
    data_rows = "\n".join(["| " + " | ".join(map(str, row)) + " |" for row in df.values])

    # Combine all parts to form the DokuWiki table
    dokuwiki_table = f"{header_row}\n{data_rows}"

    return dokuwiki_table

# Example usage:
# Create a sample DataFrame
data = {'Name': ['Alice', 'Bob', 'Charlie'],
        'Age': [25, 30, 35],
        'City': ['New York', 'San Francisco', 'Los Angeles']}

df = pd.DataFrame(data)

# Convert DataFrame to DokuWiki table
dokuwiki_table = dataframe_to_dokuwiki_table(df)

# Print or use the DokuWiki table as needed
print(dokuwiki_table)
