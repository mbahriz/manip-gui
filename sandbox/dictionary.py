pv = {'output':{'frequency':100,'amplitude':100e-3,'output':False},
        'input':{'time constant':100e-3,'sensitivity':3},
        'freq sweep':{'freq min':1e3,'freq max':100e3,'nbr pt':500,'start':False}
        }
print(pv['output']['frequency'])
pv['output']['frequency'] = 50
print(pv['output'])
print(pv)

for k in pv:
    print(k)
    print(type(k))

inst = {'used':{'Input':1,'Frequency sweep':2} ,
    'created':{'Virtual':3,'Zurich-MFLI_dev4199':4}}    

if 3 in inst['created']:
    print('True')

print(inst['created'])

print('**',pv.keys())
if 'output' in pv.keys():
    print('YES')


dico = {'fats': 10, 'proteins': 10, 'carbohydrates': 80}
keys = []
for k in dico.keys():
    if dico[k] < 50:
        keys.append(k)
for k in keys:
    del dico[k]        
print(dico)