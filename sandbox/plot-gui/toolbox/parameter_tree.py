#   20201001 add command to generate AM from driver laser need to connect th analog output BNC on an input entrance of the MFLI and set the ref oscillator as external to trigger on the laser driver mod (setup for Thorlabs FP laser)

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

app = QtGui.QApplication([])
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree, ParameterItem, registerParameterType


##############################################################################
##### GLOBAL 
##############################################################################
path_WINDOWS = 'C:\\Users\\QEPAS\\Documents\\DATA\\'
path_MACOS = '/Users/michaelbahriz/Recherche/Manips/Codes/manip-gui/data/'
path = path_WINDOWS

class MonParametre():
    # mother class use to transfert method to other class

    def child_list(self):
        # return a list with the name and value of all the children
        A = ''
        for i in self.children():
            A = A+i.name()+' : '+str(i.value())+'\n'
        return A

class AllPlotScalableGroup(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'group'
        opts['addText'] = "Add"
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'plot', 'type': 'action'}) 
        self.addChild({'name': 'path', 'type': 'str', 'value': path})

    def addNew(self):
        num_curve = len(self.childs)-1
        new = OneCurve(name="Curve_%d" % (num_curve))
        new.param('file name').setValue("Curve %d.csv" % (num_curve))
        self.addChild(new,dict(removable=True))

class OneCurve(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        opts['removable'] = True
        opts['renamable'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'file name', 'type': 'str', 'value': 'curve 1'})
        self.addChild({'name': 'color', 'type': 'color', 'value': "FF0", 'tip': "This is a color button"})

class Save(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        path_info = 'test python code'
        sample_name = 'QTF'
        sample_info = 'vacuum'
        self.addChild({'name': 'path', 'type': 'str', 'value': path})
        self.addChild({'name': 'path info', 'type': 'str', 'value': path_info})
        self.addChild({'name': 'sample name', 'type': 'str', 'value': sample_name})
        self.addChild({'name': 'sample info', 'type': 'str', 'value': sample_info})
        self.addChild({'name': 'other information', 'type': 'text', 'value': 'Some text...'})
        self.addChild({'name': 'save', 'type': 'action'})    

##############################################################################
##############################################################################
## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    ## Create tree of Parameter objects
    params = [Input(name='Input'),
              Generator(name='Generator'),
              AllPlotScalableGroup(name='Graph')]

    p = Parameter.create(name='params', type='group', children=params)

    ## Create two ParameterTree widgets, both accessing the same data
    t = ParameterTree()
    t.setParameters(p, showTop=False)
    t.setWindowTitle('pyqtgraph example: Parameter Tree')

    win = QtGui.QWidget()
    layout = QtGui.QGridLayout()
    win.setLayout(layout)
    layout.addWidget(t, 1, 0, 1, 1)
    win.show()
    win.resize(800,800)

    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

##############################################################################
##############################################################################


#### OLD VERSION #########

# def build_dico(dico):

#   '''
#   This dictionary define all the parametre tree use for the GUI 
#   (graphic utilisator interface)
#   The dico here is use for the inital value
#   '''


#   p_global = [{'name': 'Input', 'type': 'group',
#                'children': [{'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199'], 'value': dico['Input']['instrument']},
#                             {'name': 'time constant', 'type': 'float',
#                              'value': dico['Input']['time constant'], 'step': 100e-3, 'limits': (400e-9, 10),
#                              'siPrefix': True, 'suffix': 's'},
#                              {'name': 'recording time', 'type': 'float',
#                              'value': dico['Input']['recording time'], 'step': 10e-3, 'limits': (10e-3, dico['Input']['time constant']),
#                              'siPrefix': True, 'suffix': 's','tip':'data acquisition time, then averaged'},
#                              {'name': 'sensitivity', 'type': 'list', 'values': [1e-3,3e-3,1e-2,3e-2,1e-1,3e-1,1,3], 'value': dico['Input']['sensitivity']},
#                             ]},
#               {'name': 'Frequency sweep', 'type': 'group',
#                'children': [{'name': 'generator', 'type': 'group', 'children': [
#                             {'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199'], 'value': dico['Frequency sweep']['generator']['instrument']},
#                             {'name': 'amplitude', 'type': 'float','value': dico['Frequency sweep']['generator']['amplitude'], 'step': 50e-3, 'limits': (0, 1),'siPrefix': True, 'suffix': 'V'},
#                             {'name': 'on', 'type': 'bool', 'value': dico['Frequency sweep']['generator']['on'], 'tip': "ON when it is checked"},
#                             ]},
#                             {'name': 'acquisition', 'type': 'group', 'children': [
#                             {'name': 'freq min', 'type': 'float',
#                              'value': dico['Frequency sweep']['acquisition']['freq min'], 'step': 10e3, 'limits': (1, 500e3),
#                              'siPrefix': True, 'suffix': 'Hz','decimals':6},
#                              {'name': 'freq max', 'type': 'float',
#                              'value': dico['Frequency sweep']['acquisition']['freq max'], 'step': 10e3, 'limits': (1, 500e3),
#                              'siPrefix': True, 'suffix': 'Hz','decimals':6},
#                              {'name': 'nbr pts', 'type': 'int',
#                              'value': dico['Frequency sweep']['acquisition']['nbr pts'], 'step': 100, 'limits': (10, 100e3),
#                              'siPrefix': False, 'tip':'number of points use for the frequecy sweep'},
#                              {'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],
#                              'value': dico['Frequency sweep']['acquisition']['nbr seqs'], 'tip':'number of sequences, number of time frequecy sweep is running'},
#                              {'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],
#                              'value': dico['Frequency sweep']['acquisition']['waiting time'], 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'},
#                              {'name': 'start', 'type': 'action'},
#                              {'name': 'average', 'type': 'action'},
#                              {'name': 'clear all', 'type': 'action'}
#                              ]}
#                           ]},
#               {'name': 'Save', 'type': 'group',
#                'children': [{'name': 'path', 'type': 'str', 'value': dico['Save']['path']},
#                             {'name': 'path info', 'type': 'str', 'value': dico['Save']['path info']},
#                             {'name': 'sample name', 'type': 'str', 'value': dico['Save']['sample name']},
#                             {'name': 'sample info', 'type': 'str', 'value': dico['Save']['sample info']},
#                             {'name': 'save', 'type': 'action'}                 
#                           ]}
#               ]
#   return p_global
