#!C:\\Users\\QEPAS\\.conda\\envs\\Interfero_Py37\\python.exe'
# -*- coding: utf-8 -*-
"""
GUI for uRes photoacoustic
"""

import os
from collections import namedtuple
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore
from pyqtgraph.Qt import QtGui
from pyqtgraph.Qt import QtWidgets
from pyqtgraph.dockarea import DockArea
from pyqtgraph.dockarea import Dock
from pyqtgraph.parametertree import Parameter
from pyqtgraph.parametertree import ParameterTree
from pyqtgraph import GraphicsWindow
from pyqtgraph import PlotWidget
import numpy as np
import toolbox.parameter_tree
import toolbox.function

import time


class MainWindow(QtWidgets.QMainWindow): 
    def __init__(self):
        super().__init__()
        # the tuple version of a dictionary, use to store width and height of the screen 
        Screen = namedtuple('Screen', 'width height')
        # get screen's information (height and width)
        # CAUTION this attribut can be modified by other method like .show
        # it can put all the values at 0, even they were welle defined
        self.screen = Screen(width=1500,height=800) 
        # dictionary with all the graphic elements 
        self.obj_graph = {'plot': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                  },    
                          'param': {'dock': None,
                                    'tree': None,
                                    'parameter': None}}                          

        self.on_init()       
        # change the size of the graphic interface
        self.resize(self.screen.width, self.screen.height)
        # display the GUI
        self.show()

    def on_init(self):
        # for debug it is useful to separate the create         
        self.create_dock()
        self.create_contenu_dock_pg()
        self.create_contenu_dock_plot()

    def create_dock(self):
        area = DockArea()
        self.obj_graph['param']['dock'] = Dock("PA (Photo-Acoustic) setting parameters")
        self.obj_graph['plot']['dock'] = Dock("R (V) function of current (mA)")
        area.addDock(self.obj_graph['param']['dock'], 'right')
        area.addDock(self.obj_graph['plot']['dock'],'left')
        self.setCentralWidget(area)


    def create_contenu_dock_plot(self):
        # creation of plot 1 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot']['curve'] = MesCourbes()
        self.obj_graph['plot']['graph'] = self.obj_graph['plot']['curve'].plot_widget 
        self.obj_graph['plot']['dock'].addWidget(self.obj_graph['plot']['graph'])       
        self.obj_graph['plot']['curve'].add_curve('curve acq',(0,200,200),markers_on=True) #acquisition curve
        self.obj_graph['plot']['curve'].add_curve('curve ghost',(128,128,128),markers_on=False, linewidht=1) #previous acquisition        
               

    def create_contenu_dock_pg(self):
        self.obj_graph['param']['tree'] = ParameterTree()
        self.create_parameter()
        self.obj_graph['param']['dock'].addWidget(self.obj_graph['param']
                                                  ['tree'])

    def create_parameter(self):
        # create a Class with all parameters use in the GUI
        self.parameters = MesParams()
        # build the GUI
        (self.obj_graph['param']
         ['parameter']) = Parameter.create(name='params',
                                           type='group',
                                           children=self.parameters.tree)
        (self.obj_graph['param']
         ['parameter'].sigTreeStateChanged.connect(self.catch_param_change))

        """
        setParameters
        Set the top-level :class:`Parameter <pyqtgraph.parametertree.Parameter>`
        to be displayed in this ParameterTree.

        If *showTop* is False, then the top-level parameter is hidden and only 
        its children will be visible. This is a convenience method equivalent 
        to::
        
            tree.clear()
            tree.addParameters(param, showTop)
        """
        self.obj_graph['param']['tree'].setParameters((self.obj_graph['param']
                                                       ['parameter']),
                                                      showTop=False)
        self.obj_graph['param']['tree'].setWindowTitle('Parameter Tree')


    def catch_param_change(self, _, changes):
        # call in create_parameter()
        for param, _, data in changes:
            path = self.obj_graph['param']['parameter'].childPath(param)
            if path is not None: 
                # print(path) -> ['Input- Zurich', 'time constant']
                child_name = '.'.join(path)
                # print(child_name) -> Input- Zurich.time constant
            else:
                child_name = param.name()
            print('  parameter: %s' % child_name)
            print('  change:    %s' % _)
            print('  data:      %s (%s)' % (str(data),type(data)))

            # who is the name of the parameter tree
            # what the name of the paramater of the parameter tree
            try:
                who, what = child_name.split('.')
            except:
                who = child_name
                what = child_name

            graph1 = self.obj_graph['plot']['curve']
            params = self.parameters


            ## PLOT
            if (who+'.'+what) in 'Graph.plot':
                print('ok')
                x = params.dico['Graph'].children()[2]
                y = x.children()
                print(y)
                name = y[0]
                color = y[1]
                print(name.value())
                print(color.value())
                # ATTENTION pour eviter un crash rajouter des courbes avant d'appuyer sur plot



                # # display ghost curve
                # graph_1.copy_curve('curve acq','curve ghost')
                # graph_1.display('curve ghost','R',adjust=False)
                # graph_2.copy_curve('curve acq','curve ghost')
                # graph_2.display('curve ghost','Phi',adjust=False)
                # # earase data               
                # graph_1.clear_data('curve acq')
                # graph_2.clear_data('curve acq')
                # # create frequency list
                # graph_1.create_current_list(
                #     curve_id='curve acq',
                #     param_tree=self.parameters)
                # graph_2.create_current_list(
                #     curve_id='curve acq',
                #     param_tree=self.parameters)
                # # on lance un timer qui appelera 'timerEvent' toutes les period_timer
                # if self.my_timer is None:
                #     self.my_timer = self.startTimer(self.period_timer) #c une methode heritée
                # # the method timeEvent will be executed each period_timer

            ## AVERAGE ACQUISITION
            if (who+'.'+what) in 'Laser Driver.average':
                # average data
                graph_1.average(
                    curve_id='curve acq',
                    param_tree=self.parameters)
                graph_2.average(
                    curve_id='curve acq',
                    param_tree=self.parameters)  
                # display graphs
                graph_1.display('curve acq','R')
                graph_2.display('curve acq','Phi')


            ## ERASE ACQUISITION
            if (who+'.'+what) in 'Laser Driver.clear all':
                # earase data
                graph_1.clear_data('curve acq')
                graph_2.clear_data('curve acq')
                # display graphs
                graph_1.display('curve acq','R')
                graph_2.display('curve acq','Phi')  

            ## SAVE ACQUISITION
            if (who+'.'+what) in 'Save.save':
                toolbox.function.save_PA(
                    self.parameters,
                    self.obj_graph['plot']['curve'])



class MesParams():
    def __init__(self):
        # dictionary of parameter tree used
        self.dico = {
            'Save':toolbox.parameter_tree.Save(name='Save'),
            'Graph':toolbox.parameter_tree.AllPlotScalableGroup(name='Graph')}
        # use the self.dico to build a list required for the GUI
        self.tree = []       
        for k in self.dico:
            self.tree.append(self.dico[k]) 

    def give(self,who,what):
        # return the value of 'what' from 'who' in the parameter tree
        return self.dico[who].param(what).value()



class MesCourbes():
    def __init__(self):
        self.plot_widget = PlotWidget()
        self.plot_widget.showGrid(x=True,y=True)
        # self.plot_widget.getPlotItem().addLegend()
        self.plot_widget.setBackground((255, 255, 255))
        # dictionary with all the curve and their data 
        # "curve acq" is dedicated to the live/acquisition plot
        self.curves = {}

    def add_curve(self, curve_id, curve_color, markers_on=True, linewidht=3):
        curve_name = curve_id
        pen = pg.mkPen(curve_color, width=linewidht)
        symbol = "o"
        symbolPen = pg.mkPen(0,0,0)
        symbolBrush = curve_color
        symbolSize = 8
        # this adds the item to the plot and legend
        if markers_on:
            plot = self.plot_widget.plot(
                name=curve_name, pen=pen, symbol=symbol, symbolPen=symbolPen, 
                symbolBrush=symbolBrush, symbolSize=symbolSize)
        else:
            plot = self.plot_widget.plot(name=curve_name, pen=pen)
        self.curves[curve_id] = {
                    'plot':plot,
                    'data':{'Current':[],'X':[],'Y':[],'R':[],'Phi':[]}
                        }

    def clear_data(self,curve_id):
        for k in ['Current','X','Y','R','Phi']:
            self.curves[curve_id]['data'][k] = []       

    def copy_curve(self,mother,daughter):
        # mother = curve_id of the mother
        # daughter = curve_id of the daughter 
        # create a daughter curve which a copy of the mother curve
        # You cannot copy a dictionary simply by typing dict2 = dict1, because: dict2 will only be a reference to dict1, and changes made in dict1 will automatically also be made in dict2.
        # There are ways to make a copy, one way is to use the built-in Dictionary method copy().
        self.curves[daughter]['data'] = self.curves[mother]['data'].copy()

    def display(self,curve_id,what,adjust=True):
        X = self.curves[curve_id]['data']['Current']
        Y = self.curves[curve_id]['data'][what]
        # during acquisition current is longer than the others datas
        # so it is useful to reduce it
        if adjust is True:
            if len(X) != len(Y):
                X = self.curves[curve_id]['data']['Current'][0:len(Y)]
        self.set_values(curve_id,X,Y)

    def remove_curve(self, curve_id):
        curve_id = str(curve_id)
        if curve_id in self.curves:
            self.plot_widget.removeItem(self.curves[curve_id]['plot'])
            del self.curves[curve_id]

    def set_values(self, curve_id, data_x, data_y):
        curve = self.curves[curve_id]['plot']
        curve.setData(data_x, data_y)

if __name__ == '__main__': # permet d'executer que avec le run 
    import sys
    APP = pg.mkQApp() # fabriquer fenetre, ligne obligatoire
    WIN = MainWindow() # cree instance à ma class
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'): # fait moi tourner cette fenetre tant 
       QtWidgets.QApplication.instance().exec_()
    # le IF fait tourner la fenetre tant qu'elle n'est pas fermée ou que la fonction close est apellé

