import dokuwiki 

'''
I - Get It Working

    1) You need at least the 2008-03-31 release of DokuWiki.
    2) Enable the XML-RPC interface in the “Authentication Settings” section
    3) Set the remoteuser option (see section II)
    4) For security reasons it's safer to allow access to the XML-RPC over HTTPS only. DokuWiki's .htaccess.dist contains some rewrite rules to do that.

II - Configuration Setting: remoteuser

With this option you can restrict the access to the XML-RPC backend. You can list multiple user or group names1) separated by commas, or leave it blank to allow all users (including guests). The default !!not set!! will never allow anyone to access the backend.

This option limits access to the API backend (except the login method). ACLs are still checked when the called methods run. Note that if useacl is not enabled and this value is not the default !!not set!!, it will always allow anyone regardless of the value.

    Type: String
    Default: !!not set!! (not accessible by anyone)


'''

URL = 'https://wikinano.ies.umontpellier.fr/ures'
USER = 'python'
PASSWORD = 'nanomir2010'


try:
    wiki = dokuwiki.DokuWiki(URL, USER, PASSWORD)
except (dokuwiki.DokuWikiError, Exception) as err:
    print('unable to connect: %s' % err)


# print(wiki.pages.list(namespace='test1'))
# print(wiki.pages.get('test1'))
# print(wiki.pages.get('test1:test1a'))
wiki.pages.append('qtf:qtf-210330','\n  *  append ce texte')
# print(wiki.pages.get('test1:test1a'))

path =  '/Users/michaelbahriz/Recherche/Instrumentations/Codes/manip-gui/data/QTF/QTF-210330/frequency_sweep/20231213/'
file_name =  'FS_20231213_MAG.png'

print(wiki.medias.list())

media = 'qtf:qtf-210330:test_image.png'
wiki.medias.add(media, path+file_name, overwrite=True)
print(wiki.medias.info(media))
link_media = '{{:' + media + '?direct&100|}}'
wiki.pages.append('qtf:qtf-210330',link_media)
