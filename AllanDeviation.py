#!C:\\Users\\QEPAS\\.conda\\envs\\Interfero_Py37\\python.exe'
# -*- coding: utf-8 -*-
# 21 septembre 2023 Update for pyqtgraph 0.13.1
"""
GUI for uRes AllaDeviation measurement
"""

import os
from collections import namedtuple

from pyqtgraph.dockarea import DockArea
from pyqtgraph.dockarea import Dock
from pyqtgraph import PlotWidget

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets, QtCore
from pyqtgraph.parametertree import interact, ParameterTree, Parameter

import numpy as np
import toolbox.instrument 
import toolbox.parameter_tree
#import toolbox.function
import toolbox.sauvegarder
import toolbox.mother_class
import allantools as at
import time


class MainWindow(QtWidgets.QMainWindow,toolbox.mother_class.MotherMainWindow): 
    def __init__(self):
        super().__init__()
        # the tuple version of a dictionary, use to store width and height of the screen 
        Screen = namedtuple('Screen', 'width height')
        # get screen's information (height and width)
        # CAUTION this attribut can be modified by other method like .show
        # it can put all the values at 0, even they were welle defined
        self.screen = Screen(width=1500,height=800) 
        # dictionary with all the graphic elements 
        self.obj_graph = {'plot': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                  },
                          'plot2': {'dock': None,
                                   'graph': None,
                                   'curve': None
                                   },         
                          'param': {'dock': None,
                                    'tree': None,
                                    'parameter': None}}                          

        self.on_init()       
        # change the size of the graphic interface
        self.resize(self.screen.width, self.screen.height)
        # my timer use for acquisition in real time
        self.my_timer = None
        self.period_timer = 50
        # create an attribute with all my instruments 
        self.instruments = MesInstrus(param_tree=self.parameters)
        # display the GUI
        self.show()

    def on_init(self):
        # for debug it is useful to separate the create         
        self.create_dock()
        self.create_contenu_dock_pg()
        self.create_contenu_dock_plot()

    def create_dock(self):
        area = DockArea()
        self.obj_graph['param']['dock'] = Dock("AD (Allan Deviation) setting parameters")
        self.obj_graph['plot']['dock'] = Dock("R (Vrms) function of time (s)")
        self.obj_graph['plot2']['dock'] = Dock("Concentration (ppmv) function of tau (s)")
        area.addDock(self.obj_graph['param']['dock'], 'left')
        area.addDock(self.obj_graph['plot']['dock'],'right')
        area.addDock(self.obj_graph['plot2']['dock'],'bottom',self.obj_graph['plot']['dock'])
        self.setCentralWidget(area)

    def create_contenu_dock_plot(self):
        # creation of plot 1 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot']['curve'] = MesCourbes()
        self.obj_graph['plot']['graph'] = self.obj_graph['plot']['curve'].plot_widget 
        self.obj_graph['plot']['dock'].addWidget(self.obj_graph['plot']['graph'])       
        self.obj_graph['plot']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
             
        # creation of plot 2 (graph 2)
        # a place for this graph has been defined in create_dock()   
        self.obj_graph['plot2']['curve'] = MesCourbes(log=True)
        self.obj_graph['plot2']['graph'] = self.obj_graph['plot2']['curve'].plot_widget
        self.obj_graph['plot2']['dock'].addWidget(self.obj_graph['plot2']['graph']) 
        self.obj_graph['plot2']['curve'].add_curve('curve acq',(0,250,250),markers_on=False)
 
    def create_contenu_dock_pg(self):
        self.obj_graph['param']['tree'] = ParameterTree()
        self.create_parameter()
        self.obj_graph['param']['dock'].addWidget(self.obj_graph['param']
                                                  ['tree'])

    def create_parameter(self):
        # create a Class with all parameters use in the GUI
        self.parameters = MesParams()
        # build the GUI
        (self.obj_graph['param']
         ['parameter']) = Parameter.create(name='params',
                                           type='group',
                                           children=self.parameters.tree)
        (self.obj_graph['param']
         ['parameter'].sigTreeStateChanged.connect(self.catch_param_change))

        """
        setParameters
        Set the top-level :class:`Parameter <pyqtgraph.parametertree.Parameter>`
        to be displayed in this ParameterTree.

        If *showTop* is False, then the top-level parameter is hidden and only 
        its children will be visible. This is a convenience method equivalent 
        to::
        
            tree.clear()
            tree.addParameters(param, showTop)
        """
        self.obj_graph['param']['tree'].setParameters((self.obj_graph['param']
                                                       ['parameter']),
                                                      showTop=False)
        self.obj_graph['param']['tree'].setWindowTitle('Parameter Tree')

    def catch_param_change(self, _, changes):
        # call in create_parameter()
        for param, _, data in changes:
            path = self.obj_graph['param']['parameter'].childPath(param)
            if path is not None: 
                # print(path) -> ['Input- Zurich', 'time constant']
                child_name = '.'.join(path)
                # print(child_name) -> Input- Zurich.time constant
            else:
                child_name = param.name()
            print('  parameter: %s' % child_name)
            print('  change:    %s' % _)
            print('  data:      %s (%s)' % (str(data),type(data)))

            # who is the name of the parameter tree
            # what the name of the paramater of the parameter tree
            try:
                who, what = child_name.split('.')
            except:
                who = child_name
                what = child_name
            graph_1 = self.obj_graph['plot']['curve']
            graph_2 = self.obj_graph['plot2']['curve']


            ## INSTRUMENT CHANGE
            # CAUTION check it always in first
            if what in ['instrument']:
                # in this case data == name of the instrument
                self.instruments.update(who,data)

            ## CHANGE VALUES ASKED ON THE INSTRUMENT
            if who in ['Input','Generator','Laser Driver']:
                #if what in ['time constant','sensitivity','on','amplitude','frequency','current','temperature','modulation','frequency modulation','amplitude modulation','harmonic']:
                # get the nam of instrument asked in the parameter tree 
                instru = self.parameters.give_inst(who)
                self.instruments.set_value(instru,what,data)

            ## START ACQUISITION
            if (who+'.'+what) in 'Allan Deviation.start':
                # update old and new acquisition
                self.plot_ghost_curve()
                # start acquisition
                self.start_acquisition()

            ## ALLAN TYPE
            if (who+'.'+what) in 'Allan Deviation.type':
                self.trace_allan()

            ## ERASE ACQUISITION
            if (who+'.'+what) in 'Allan Deviation.clear all':
                # earase data
                graph_1.clear_data('curve acq')
                graph_1.remove_ghost()
                graph_2.clear_data('curve acq')
                graph_2.remove_ghost()
                # display graphs
                graph_1.display('curve acq','Time','R')
                type_allan = self.parameters.give('Allan Deviation','type')
                graph_2.display('curve acq','Tau-'+type_allan,'Concentration-'+type_allan)
                #stop acquisition
                self.stop_timer()  
                # remove list of curve in the paramter tree
                self.parameters.dico['Graph'].clearChildren()
                self.parameters.dico['Graph'].addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})

            # ## SAVE info resonator cahnge the list of  resonator if the design has been changed
            # if (who+'.'+what) in 'Save.design':
            #     design = self.parameters.give(who='Save',what='design')
            #     self.parameters.dico['Save'].Resonator(design)

            ## SAVE ACQUISITION
            if (who+'.'+what) in 'Save.save':
                # update only the parameters from the section Save and Graph
                graph_1.save_parameters_sauvegarder(curve_id='curve acq',param=self.parameters)
                # save everything                
                toolbox.sauvegarder.Save('Allan deviation',self.obj_graph['plot']['curve'].curves)

            # CONCENTRATION
            # if concentration change in Allan Deviation paramtree -> update concentration in Save paramtree
            if who in ['Allan Deviation']:
                #self.parameters.dico['Fit'].param('freq max').Value(f_acq_max)
                if what in ['concentration']:
                    conc = str(self.parameters.dico['Allan Deviation'].param('concentration').value()) + ' ppmv'
                    self.parameters.dico['Save'].param('concentration').setValue(conc)

    def plot_ghost_curve(self):
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        # if test_no_restart is False that's mean that the button start has been pressed before the end of the acquisition. In this case no ghost will be ploted.
        test_no_restart = (len(graph_1.curves['curve acq']['data']['R']) == len(graph_1.curves['curve acq']['data']['Time wanted']))
        if (nbr_ghost > 0) and test_no_restart:
            graph_1.copy_curve('curve acq','curve ghost'+str(nbr_ghost))
            graph_1.display('curve ghost'+str(nbr_ghost),'Time','R')
            graph_2.copy_curve('curve acq','curve ghost'+str(nbr_ghost))
            type_allan = self.parameters.give('Allan Deviation','type')
            graph_2.display('curve ghost'+str(nbr_ghost),'Tau-'+type_allan,'Concentration-'+type_allan)
        # earase data               
        graph_1.clear_data('curve acq')
        graph_2.clear_data('curve acq')

    def rajoute_info_ghost(self):
        # add info about the acquisition on the parametre
        graph_1 = self.obj_graph['plot']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        if nbr_ghost > 0:
            snr, mean = graph_1.SNR('curve acq')
            step_time = graph_1.curves['curve acq']['data']['Step time']
            step_time_dev = graph_1.curves['curve acq']['data']['Step time dev']
            texte = 'SNR={:.0f}  mean={:.3f}mVrms  setp time={:.2f}+/-{:.2g}ms'.format(snr,mean*1e3,step_time*1e3,step_time_dev*1e3)
            self.parameters.dico['Graph'].param('info curve '+str(nbr_ghost)).setValue(texte)

    def rajoute_info_LOD(self):
        # add info about the acquisition on the parametre
        graph_1 = self.obj_graph['plot']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # display ghost curve
        if nbr_ghost > 0:
            C1s = graph_1.curves['curve acq']['data']['LOD 1s']
            C10s = graph_1.curves['curve acq']['data']['LOD 10s']
            T, C = graph_1.curves['curve acq']['data']['LOD']
            texte = 'LOD with oadev {:.0f}ppmv@1s {:.0f}ppmv@10s {:.0f}ppmv@{:.1f}s'.format(C1s,C10s,C,T)
            self.parameters.dico['Graph'].param('info allan '+str(nbr_ghost)).setValue(texte)

    def rajoute_ghost_cuvre(self):
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        # count number of ghost
        nbr_ghost = self.count_the_number_of_ghost_curves()
        # put and chack the max of curve
        curves_max = 9
        if nbr_ghost == curves_max:
            self.message_box(text='You have reached the maximum number of curves.')
        # color
        colors = self.color_map(curves_max+1)
        # add curve 
        graph_1.add_curve('curve ghost'+str(nbr_ghost+1),np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        graph_2.add_curve('curve ghost'+str(nbr_ghost+1),np.array(colors[nbr_ghost])*255,markers_on=False, linewidht=1)
        # update the parameter tree
        param = self.parameters.dico['Graph']
        # add a line on the parameter tree to give the value of the resonnace frequency
        param.addChild({'name': 'info curve '+str(nbr_ghost+1), 'type': 'str', 'value': '', 'readonly': True})
        param.addChild({'name': 'info allan '+str(nbr_ghost+1), 'type': 'str', 'value': '', 'readonly': True})
        # add a line on the parameter tree str(nbr_ghost+2) as a first legend curve has been created in the same time than the parametertree
        param.addChild({'name': 'legend curve '+str(nbr_ghost+2), 'type': 'str', 'value': ''})        

    @toolbox.function.ressources_decorator
    def start_acquisition(self):
        """
        lance l'acquisition pour Allan
        """
        total_duration = self.parameters.give('Allan Deviation','acq. time')*60
        graph_1 = self.obj_graph['plot']['curve']
        # lance acquisition Allan
        instru = self.parameters.give_inst('Input')
        all_data = self.instruments.get_Allan(inst=instru,acq_time=total_duration)
        # ne garde que les valeurs acquise avec le pas step_time
        step_time = 100e-3
        target_time = step_time 
        data = {'Time':[],'Time wanted':[],'R':[]}
        # while target_time < total_duration:
        #     idx = toolbox.function.find_nearest_index(all_data['Time_R'], target_time)
        #     T = all_data['Time_R'][idx]
        #     R = all_data['R'][idx]
        #     data['Time'].append(T)
        #     data['Time wanted'].append(target_time)
        #     data['R'].append(R)
        #     target_time += step_time
        #     print('Compute time : {:.3f} s'.format(target_time))
        idx_last = 0
        idx_first = toolbox.function.find_nearest_index(all_data['Time_R'], target_time)
        while target_time < total_duration:
            idx = toolbox.function.find_nearest_index(all_data['Time_R'][idx_last:2*idx_first+idx_last], target_time)
            T = all_data['Time_R'][idx_last:2*idx_first+idx_last][idx]
            R = all_data['R'][idx_last:2*idx_first+idx_last][idx]
            data['Time'].append(T)
            data['Time wanted'].append(target_time)
            data['R'].append(R)
            target_time += step_time
            idx_last = idx_last + idx
            # print('Compute time : {:.3f} s index : [{}:{}]'.format(target_time,idx_last,2*idx_first+idx_last))
            
        # update graph
        graph_1.curves['curve acq']['data']['Time'] = data['Time']
        graph_1.curves['curve acq']['data']['Time wanted'] = data['Time wanted']
        graph_1.curves['curve acq']['data']['R'] = data['R']
        # plot graph
        graph_1.display('curve acq','Time','R')
        self.trace_allan()
        self.rajoute_ghost_cuvre()
        self.rajoute_info_ghost()
        self.rajoute_info_LOD()
        # save param on the curve dictionary
        graph_1.save_parameters('curve acq',self.parameters)


    def trace_allan(self):
        # compute Allan deviation in concentration
        graph_1 = self.obj_graph['plot']['curve']
        graph_2 = self.obj_graph['plot2']['curve']
        concentration = self.parameters.give('Allan Deviation','concentration')
        graph_1.allandeviation('curve acq',C=concentration,dev='adev')
        graph_1.allandeviation('curve acq',C=concentration,dev='oadev')
        # print SNR
        graph_1.SNR('curve acq')
        # copy graph1 on graph2 to display AllanCurve
        graph_2.curves['curve acq']['data'] = graph_1.curves['curve acq']['data'].copy()
        type_allan = self.parameters.give('Allan Deviation','type')
        #print('graph_2 : ',graph_2.curves['curve acq']['data'])
        graph_2.display('curve acq','Tau-'+type_allan,'Concentration-'+type_allan)
       

class MesInstrus(toolbox.mother_class.MotherInstru):
    def __init__(self,param_tree):
        self.param_tree = param_tree
        # attribut with all the instrument created
        self.connected = []
        # dico that the code will fill with instru which can be called
        self.use = {}

    def get_Allan(self,inst,acq_time):
        self.connect_inst(inst)
        return self.use[inst].poll_continuous(temps=acq_time,graph=True)  

class MesParams(toolbox.mother_class.MotherParam):
    def __init__(self):
        # dictionary of parameter tree used
        self.dico = {
            'Input':toolbox.parameter_tree.Input(name='Input'),
            'Generator':toolbox.parameter_tree.Generator(name='Generator'),
            'Laser Driver':toolbox.parameter_tree.LaserDriver(name='Laser Driver'),
            'Allan Deviation':toolbox.parameter_tree.AllanDeviation(name='Allan Deviation'),
            'Graph':toolbox.parameter_tree.GraphList(name='Graph'),
            'Save':toolbox.parameter_tree.Save(name='Save')}
        # use the self.dico to build a list required for the GUI
        self.tree = []       
        for k in self.dico:
            self.tree.append(self.dico[k]) 

    # def give_all_inst_value(self,who):
    #     # returns all the parameters necessary for updating the instrument 
    #     # (amplitude, frequency , etc.)
    #     A = []
    #     if who in 'Input':
    #         for k in ['time constant','sensitivity','harmonic','filter order']: 
    #             A.append([k,self.dico[who].param(k).value()])
    #     if who in 'Generator':
    #         for k in ['amplitude','on']: 
    #             A.append([k,self.dico[who].param(k).value()])
    #     if who in 'Laser Driver':
    #         for k in ['on','current','temperature']:
    #             A.append([k,self.dico[who].param(k).value()])
    #     return A

    # def waiting_time(self):
    #     tc = self.dico['Input'].param('time constant').value()
    #     wt = self.dico['Allan Deviation'].param('waiting time').value()
    #     return tc*wt

class MesCourbes(toolbox.mother_class.MotherCourbe):
    def __init__(self,log=False):
        self.plot_widget = PlotWidget()
        self.plot_widget.showGrid(x=True,y=True)
        # self.plot_widget.getPlotItem().addLegend()
        self.plot_widget.setBackground((0, 0, 0))
        # dictionary with all the curve and their data 
        # "curve acq" is dedicated to the live/acquisition plot
        self.curves = {}
        if log is True:
            self.plot_widget.setLogMode(True,True)

    def add_curve(self, curve_id, curve_color, markers_on=True, linewidht=3):
        curve_name = curve_id
        pen = pg.mkPen(curve_color, width=linewidht)
        symbol = "o"
        symbolPen = pg.mkPen(0,0,0)
        symbolBrush = curve_color
        symbolSize = 8
        # this adds the item to the plot and legend
        if markers_on:
            plot = self.plot_widget.plot(
                name=curve_name, pen=pen, symbol=symbol, symbolPen=symbolPen, 
                symbolBrush=symbolBrush, symbolSize=symbolSize)
        else:
            plot = self.plot_widget.plot(name=curve_name, pen=pen)
        self.curves[curve_id] = {
                    'plot':plot,
                    'data':{'Time':[],'Time wanted':[],'R':[],'Tau-adev':[],'Tau-oadev':[],'Deviation-adev':[],'Concentration-adev':[],'Deviation-oadev':[],'Concentration-oadev':[]}
                        }

    def allandeviation(self, curve_id, C, dev='adev'):
        '''
        compute AllanDeviation
        dev : 'oadev' or 'adev' (overlapping ou standarad allan deviation)
        C   : concentration in ppmv
        '''
        time = self.curves[curve_id]['data']['Time']
        signal = self.curves[curve_id]['data']['R']
        # Sampling frequency [Hz] fs and its deviation
        t_step = np.mean(np.diff(time))
        t_step_std = np.std(np.diff(time))
        self.curves[curve_id]['data']['Step time'] = t_step
        self.curves[curve_id]['data']['Step time dev'] = t_step_std
        ## Allan Deviation ##
        a = at.Dataset(data=signal,rate=1/t_step,data_type='freq',taus="all") 
        a.compute(dev) # Compute modified Allan deviation
        self.curves[curve_id]['data']['Tau-'+dev] = a.out['taus']
        self.curves[curve_id]['data']['Deviation-'+dev] = a.out['stat']
        self.put_allan_in_concentration(curve_id, C, dev)
        if 'oadev' in dev:
            self.limit_of_detection(curve_id)

    # def create_time_list(self,curve_id,param_tree):
    #     '''
    #     Create a array with all the  time use for the  time sweep
    #     the time between two step is the time cosntant on the locking x the waiting time factor
    #     '''
    #     step_time = param_tree.waiting_time()
    #     time_min = step_time
    #     time_max = param_tree.give('Allan Deviation','acq. time')
    #     time_list = np.arange(time_min,time_max,step_time)
    #     self.curves[curve_id]['data']['Time wanted'] = time_list

    def display(self,curve_id,what_X,what_Y):
        X = self.curves[curve_id]['data'][what_X]
        Y = self.curves[curve_id]['data'][what_Y]
        # step is used to reduce the number of point to display
        if len(X) > 5000:
            step = int(len(X)/500)
        else:
            step = 1
        print('Graph step = ',step)
        self.set_values(curve_id,X[::step],Y[::step])

    def limit_of_detection(self,curve_id):
        X, Y, f = self.interpolate(curve_id,'Tau-oadev','Concentration-oadev')
        # LOD at 1s
        C1s = f(1)
        print('LOD at 1s is {:.3f} ppmv'.format(C1s))
        self.curves[curve_id]['data']['LOD 1s'] = C1s
        # LOD at 10s
        if max(X) > 10:
            C10s = f(10)
            print('LOD at 10s is {:.3f} ppmv'.format(C10s))
            self.curves[curve_id]['data']['LOD 10s'] = C10s
        else:
            self.curves[curve_id]['data']['LOD 10s'] = float('nan')
        # LOD the lowest
        Con_LOD = min(Y)
        idx = toolbox.function.find_nearest_index(Y,Con_LOD)
        Tau_LOD = X[idx]
        print('LOD at {:.3f}s is {:.3f} ppmv'.format(Tau_LOD,Con_LOD))
        self.curves[curve_id]['data']['LOD'] = [Tau_LOD,Con_LOD]

    def put_allan_in_concentration(self, curve_id,C,dev):
        # C concentration in ppmv
        mean = np.mean(self.curves[curve_id]['data']['R'])
        deviation = self.curves[curve_id]['data']['Deviation-'+dev]
        self.curves[curve_id]['data']['Concentration-'+dev] = deviation*C/mean

    def SNR(self, curve_id):
        signal = self.curves[curve_id]['data']['R']
        mean = np.mean(signal)
        self.curves[curve_id]['data']['Mean'] = mean
        '''
        The standard deviation is the square root of the average 
        of the squared deviations from the mean, 
        i.e., std = sqrt(mean(x)), where x = abs(a - a.mean())**2.
        '''
        dev = np.std(signal)
        signal_to_noise = mean/dev
        self.curves[curve_id]['data']['SNR'] = signal_to_noise
        print('SNR = ',signal_to_noise)
        return signal_to_noise, mean


if __name__ == "__main__":
    win = MainWindow()
    win.show()
    pg.exec()
