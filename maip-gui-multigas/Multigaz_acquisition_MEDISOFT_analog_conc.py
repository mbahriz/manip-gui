#!C:\\Users\\QEPAS\\.conda\\envs\\Interfero_Py37\\python.exe'
# -*- coding: utf-8 -*-
# 14 novembre 2023 Update for pyqtgraph 0.13.1


import sys



import os
from collections import namedtuple
import serial
import pyqtgraph as pg
from pyqtgraph import PlotWidget
# from pyqtgraph.Qt import QtWidgets, QtCore
from pyqtgraph.parametertree import interact, ParameterTree, Parameter
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QFileDialog,QLabel,QSlider,QDoubleSpinBox
from PyQt5.QtCore import QTimer
from PyQt5.QtCore import QTimer, QPropertyAnimation, QRect
from PyQt5.QtCore import Qt  # Import Qt for alignment constants
from PyQt5.QtGui import QTransform
from PyQt5.QtWidgets import QCheckBox

from PyQt5 import QtWidgets, QtGui, QtCore



from pyqtgraph.dockarea import DockArea, Dock

import numpy as np
import toolbox.instrument 
import toolbox.parameter_tree
import toolbox.sauvegarder
import toolbox.mother_class
import scipy.optimize
import time
import toolbox.RH as H2O
import toolbox.medisoft_complet as medis
import toolbox.inst.medisoft as medi

class MainWindow(QMainWindow): 
    def __init__(self):
        super().__init__()
        super(MainWindow, self).__init__()
        # the tuple version of a dictionary, use to store width and height of the screen 
        Screen = namedtuple('Screen', 'width height')
        # get screen's information (height and width)
        # CAUTION this attribut can be modified by other method like .show
        # it can put all the values at 0, even they were welle defined
        self.screen = Screen(width=1500,height=800) 
        
       
        
        # dictionary with all the graphic elements 
        
        self.obj_graph = {'plot1': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'gaz1'
                                  },
                          'plot2': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'gaz2'
                                   },
                          'plot3': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'gaz3'
                                   }, 
                          'plot4': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'gaz4'
                                   }, 
                          
                          'plot5': {
                                   'title':'water'
                                   }, 
                          
                          'plot6': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'Flow(l/s)'
                                   }, 
                          'plot7': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'CO2 (%)'
                                   }, 
                          'plot8': {'dock': None,
                                   'graph': None,
                                   'curve': None,
                                   'title':'Volume (l)'
                                   }, 
                          
                          'param': {'dock': None,
                                    'tree': None,
                                    'parameter': None
                                    }} 
        
        
        
        #pente calib gaz (mv/ppm)
        self.SLOPE_gaz1=0.0295
        self.SLOPE_gaz2=0.0012
        self.SLOPE_gaz3=0.0034
        self.SLOPE_gaz4=0.2819
        
        
        #current limit (mA)
        self.current_gaz1=100
        self.current_gaz2=100
        self.current_gaz3=100
        self.current_gaz4=100
            
        self.offset_volume=0
        
        self.plotter_stop_flag=False
        
              

        self.on_init()       
        # change the size of the graphic interface
        self.resize(self.screen.width, self.screen.height)
        # my timer use for acquisition in real time
        # self.my_timer = None
        # self.period_timer =1    #50
        # create an attribute with all my instruments 
        self.instruments = MesInstrus(param_tree=self.parameters)
        # display the GUI
        self.show()


     
        
        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.update_plot)
        # self.timer.timeout.connect(self.update_plot_2)
        self.time_values = []
        
        
        
        #conc
        self.R1_conc_list=[]
        self.R2_conc_list=[]
        self.R3_conc_list=[]
        self.R4_conc_list=[]
        
        # self.R1_values = []
        # self.R2_values = []
        # self.R3_values = []
        # self.R4_values = []
        
        #arduino
        # self.temperature_values = []
        # self.humidity_values = []
        # self.water_conc_values = []
        
        #Medisoft
        self.time_stamp_values=[]
        # self.index_values=[]
        self.flow_values = []
        self.volume_values=[]
        self.CO2_values = []
        
        
        self.time_interval = 0.1
        self.current_time = 0
        self.clear_on_start = True  # Flag to control clearing on start
        
        # Define variables to store selected points
        self.selected_points = []
        
        
        
        
        
        # Connect the mouse press event to a custom method
        self.plot_widget8.scene().mousePressEvent = self.on_mouse_press_event

    # Define a function to update the markers
    def update_markers(self):
        # Clear previous markers
        for item in self.plot_widget8.items():
            if isinstance(item, pg.ScatterPlotItem):
                self.plot_widget8.removeItem(item)
        # Plot the latest two markers
        for point in self.selected_points[-2:]:
            x_val, y_val = point
            marker = pg.ScatterPlotItem([x_val], [y_val], symbol='o', pen='r', brush='r', size=10)
            self.plot_widget8.addItem(marker)
                
            
    # Custom method to handle mouse press event
    def on_mouse_press_event(self, event):
        pos = event.scenePos()
        # Convert mouse position to data coordinates
        x_val = self.plot_widget8.plotItem.vb.mapSceneToView(pos).x()
        y_val = self.plot_widget8.plotItem.vb.mapSceneToView(pos).y()
        # Add the clicked point to the list of selected points
        self.selected_points.append((x_val, y_val))
        # Update the markers
        self.update_markers()
        
        # If more than two points are selected, remove the oldest point
        if len(self.selected_points) > 2:
            self.selected_points.pop(0)
            self.update_markers()
        
        # If two points are selected, calculate the difference
        if len(self.selected_points) == 2:
            point1, point2 = self.selected_points
            self.difference = abs(point2[1] - point1[1])
            # print(f"Difference between points: {self.difference:.2f}")
            self.label_1.setText(f"CV: {self.difference:.2f} L") 

        
        
        
        
      
    def start_plotting(self):
        if self.clear_on_start:
            self.clear_plotting()  # Clear the graphs only if clear_on_start is True
            self.timer = pg.QtCore.QTimer()
            self.timer.timeout.connect(self.update_plot)
            
        #disable x-axis zoom buttom
        self.fixed_x_range = False
        self.apply_range_button.setEnabled(True)
        # self.apply_range_button_2.setEnabled(True)
        

        
        #dynamic zoom auto
        self.plot_widget1.enableAutoRange('x')
        self.plot_widget2.enableAutoRange('x')
        self.plot_widget3.enableAutoRange('x')
        self.plot_widget4.enableAutoRange('x')
        self.plot_widget6.enableAutoRange('x')
        self.plot_widget7.enableAutoRange('x')
        self.plot_widget8.enableAutoRange('x')
        
        self.plot_widget1.enableAutoRange('y')
        self.plot_widget2.enableAutoRange('y')
        self.plot_widget3.enableAutoRange('y')
        self.plot_widget4.enableAutoRange('y')
        self.plot_widget6.enableAutoRange('y')
        self.plot_widget7.enableAutoRange('y')
        self.plot_widget8.enableAutoRange('y')
        #dynamic zoom slider enable 
        self.zoom_slider.setEnabled(True)
        
        
        instru4=self.parameters.give_inst('Medisoft')
        
        
        
        if instru4=='Medisoft':
            
            #update medisoft file
            update_medi = self.parameters.dico['Medisoft'].param('Expair_filename')
            update_medi.setValue(medi.Medisoft.connect('Medisoft'))
        
        
        self.running = True
        
        ###

        self.time_interval =self.parameters.poll_length()#0.01 #self.parameters.waiting_time()
        self.timer.start(int(self.parameters.poll_length()*10e3))#self.parameters.waiting_time()*1e3)  # Update every 100 milliseconds
        self.start_time = time.time()
        
    def stop_plotting(self):
        self.running = False
        self.timer.stop()  
        self.clear_on_start = True  # Set flag to False when stopped
        
        self.min_spinbox.setEnabled(True)
        self.max_spinbox.setEnabled(True)
        self.apply_range_button.setEnabled(True)
        # self.apply_range_button_2.setEnabled(True)
        self.zoom_slider.setEnabled(True)
        
        
        
        # self.plot_data1.setData(self.time_values, self.R1_values)# Display the last 100 values
        # self.plot_data2.setData(self.time_values, self.R2_values)
        # self.plot_data3.setData(self.time_values, self.R3_values)
        # self.plot_data4.setData(self.time_values, self.R4_values)
        
        
        
        
        
        # self.plot_widget1.setTitle(self.obj_graph['plot1']['title']+" brut (Vrms)")
        # self.plot_widget2.setTitle(self.obj_graph['plot2']['title']+" brut (Vrms)")
        # self.plot_widget3.setTitle(self.obj_graph['plot3']['title']+" brut (Vrms)")
        # self.plot_widget4.setTitle(self.obj_graph['plot4']['title']+" brut (Vrms)")
        
        self.obj_graph['plot6']['curve'].plot_widget.setTitle(self.obj_graph['plot6']['title'])
        self.obj_graph['plot7']['curve'].plot_widget.setTitle(self.obj_graph['plot7']['title'])
        
        
        instru4=self.parameters.give_inst('Medisoft')
        #forsaving pupose
        graph_1 = self.obj_graph['plot1']['curve']
        
        c1_list=[]#iso
        c2_list=[]#no
        c3_list=[]#ace
        c4_list=[]#co
        
        
        
        if instru4=='Medisoft':
            
            
            

            # medi_file=medis.Medisoft_complet(None,None)
            # info_patient=medi_file.get_info_patient()
            # save_setup_description_param =self.parameters.dico['Save'].param('setup description')
                
            # for _ in info_patient:
            #      # print('\n'.join(item))
                
            #     save_setup_description_param.setValue(''.join(info_patient))
            #     updated_value = save_setup_description_param.value()
            

            #load all points medisoft file
            #example:
            #medi=medis.Medisoft_complet('11:20:51.773','11:28:06.010')
            print(self.time_stamp_values[0],"toooooooooooooooooooo",self.time_stamp_values[-1])
            medi=medis.Medisoft_complet(self.time_stamp_values[0],self.time_stamp_values[-1])
           
            #description
            info_patient=medi.get_info_patient()
            save_setup_description_param =self.parameters.dico['Save'].param('setup description')
            #description info patient   
            current_value = save_setup_description_param.value()
            for _ in info_patient:
                  # print('\n'.join(item))
                
                save_setup_description_param.setValue(current_value +'\n'+''.join(info_patient))
                
                # save_setup_description_param.setValue(''.join(info_patient))
                updated_value = save_setup_description_param.value()
            
            #data
            t_creat_list,t_creat_h_list,times,indices,flows,volume_values,CO2_values,R1_list,R2_list,R3_list,R4_list=medi.get_lists()
            
            
            self.x_col=t_creat_list
            self.y_col=flows
            
            
            g1,g2,g3,g4=self.parameters.gain()
              
            # print(g1,g2,g3,g4)
            if g1 is not None:
                c1_list=[((x1/g1)*1000)/self.SLOPE_gaz1 for x1 in R1_list]
            if g1 is None:
                c1_list=[x1*0 for x1 in R1_list]
                
            if g2 is not None:
                c2_list=[((x2/g2)*1000)/self.SLOPE_gaz2 for x2 in R2_list]
            if g2 is None:
                c2_list=[x2*0 for x2 in R2_list]
                
            if g3 is not None:
                c3_list=[((x3/(g3*10))*1000)/self.SLOPE_gaz3 for x3 in R3_list]
            if g3 is None:
                c3_list=[x3*0 for x3 in R3_list]
                
            if g4 is not None:
                c4_list=[((x4/(g4*10))*1000)/self.SLOPE_gaz4 for x4 in R4_list]
            if g4 is None:
                c4_list=[x4*0 for x4 in R4_list]
            

            self.plot_data1.setData(t_creat_list, c1_list)# Display the last 100 values
            self.plot_data2.setData(t_creat_list, c2_list)
            self.plot_data3.setData(t_creat_list, c3_list)
            self.plot_data4.setData(t_creat_list, c4_list)
            
            self.plot_data6.setData(t_creat_list, flows)
            self.plot_data7.setData(t_creat_list, CO2_values)
            self.plot_data8.setData(t_creat_list, volume_values)
            
            graph_1.curves['curve acq']['data']['time_increm_medisoft_complet'] = t_creat_list
            graph_1.curves['curve acq']['data']['time_clock_medisoft_complet'] = t_creat_h_list
            
            
            graph_1.curves['curve acq']['data']['time_stamp_medisoft_complet'] = times
            graph_1.curves['curve acq']['data']['index_medisoft_complet'] = indices
            graph_1.curves['curve acq']['data']['volume_medisoft_complet'] = volume_values
            graph_1.curves['curve acq']['data']['flow_medisoft_complet'] = flows
            graph_1.curves['curve acq']['data']['CO2_medisoft_complet'] = CO2_values
            graph_1.curves['curve acq']['data']['R1_medisoft_complet'] = R1_list
            graph_1.curves['curve acq']['data']['R2_medisoft_complet'] = R2_list
            graph_1.curves['curve acq']['data']['R3_medisoft_complet'] = R3_list
            graph_1.curves['curve acq']['data']['R4_medisoft_complet'] = R4_list
            
            graph_1.curves['curve acq']['data']['Conc1_medisoft_complet'] = c1_list
            graph_1.curves['curve acq']['data']['Conc2_medisoft_complet'] = c2_list
            graph_1.curves['curve acq']['data']['Conc3_medisoft_complet'] = c3_list
            graph_1.curves['curve acq']['data']['Conc4_medisoft_complet'] = c4_list
            
           
            
           
            
           
            
            
            
            
        else:  
            
            #in this case self.time_stamp_values will be equal to zeros
            graph_1.curves['curve acq']['data']['time_medisoft_complet'] = self.time_stamp_values
            
            graph_1.curves['curve acq']['data']['time_stamp_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['index_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['volume_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['flow_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['CO2_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['Conc1_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['Conc2_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['Conc3_medisoft_complet'] = self.time_stamp_values
            graph_1.curves['curve acq']['data']['Conc4_medisoft_complet'] =self.time_stamp_values
            
    #     self.timer = pg.QtCore.QTimer()
    #     self.time_interval = self.parameters.poll_length()
    #     self.timer.start(int(self.parameters.poll_length()*10e3))  # Update every 100 milliseconds
    #     self.timer.timeout.connect(self.update_stop)
                
    # def update_stop(self):
        
    #     if self.running == False:
    #         if self.auto_range_checkbox.isChecked():
    #             # Enable region selection
                
    #             self.plot_widget6.enableAutoRange('y')
    #         else:
                
                
    #             # self.plot_widget6.enableAutoRange('x')
    #             self.plot_widget6.enableAutoRange('y') 
                    


    def apply_cv(self):
        cv=round(self.difference,3)
        save_setup_description_param =self.parameters.dico['Save'].param('setup description')
        #description info patient   
        current_value = save_setup_description_param.value()
        
        if cv<0:     
            cv=abs(cv)
            save_setup_description_param.setValue(current_value +'\n'+'CV='+str(cv)+'L')
        else:
            save_setup_description_param.setValue(current_value +'\n'+'CV='+str(cv)+'L')
            
        # save_setup_description_param.setValue(''.join(info_patient))
        updated_value = save_setup_description_param.value()
        
    def remove_mark(self):
    
        self.selected_points = []
        self.update_markers()
        
    def zero_offset_volume(self):
        
            self.offset_volume = self.volume_value
            
    def toggle_region_selection(self):
        if self.auto_range_checkbox.isChecked():
            self.plot_widget6.setYRange(-0.1,0.5)
             
        else:
            self.plot_widget6.enableAutoRange('x')
            self.plot_widget6.enableAutoRange('y')
           

  
    
                
        
    def clear_plotting(self):
       
        
        self.time_values = []
        #MFLI
        self.R1_values = []
        self.R2_values = []
        self.R3_values = []
        self.R4_values = []
        
        self.R1_conc_list=[]
        self.R2_conc_list=[]
        self.R3_conc_list=[]
        self.R4_conc_list=[]
        
        #arduino
        # self.temperature_values = []
        # self.humidity_values = []
        # self.water_conc_values = []
        
        #Medisoft
        # print("before_clearing",self.time_stamp_values)
        self.time_stamp_values=[]
        # print("after_clearing",self.time_stamp_values)
        self.index_values=[]
        self.flow_values = []
        self.volume_values=[]
        self.CO2_values = []
        
       
        
        
        instru4=self.parameters.give_inst('Medisoft')
        
        
        
        if instru4=='Medisoft':
            #setup description
            enter=self.instruments.set_value(instru4,0,0)
            save_setup_description_param =self.parameters.dico['Save'].param('setup description')
            save_setup_description_param.setValue('')
            updated_value = save_setup_description_param.value()
        

        # Clear the plot data and reset the time for both plots
        self.plot_data1.setData([], [])
        self.plot_data2.setData([], [])
        self.plot_data3.setData([], [])
        self.plot_data4.setData([], [])
        self.plot_data6.setData([], [])
        self.plot_data7.setData([], [])
        self.plot_data8.setData([], [])
        self.time_values_label.setText("<center> <b style='font-size: 12pt;'>   Time: 0.00s | RH: 0% | T: 0°C | H<sub>2</sub>O: 0ppm </b> </center>")
        self.current_time = 0  # Reset time
        self.running = False  # Stop updating the plots
        self.timer.stop()




    def run(self):
        self.show()
        sys.exit(QApplication.instance().exec_())
        
        

    def on_init(self):
        # for debug it is useful to separate the create         
        self.create_dock()
        self.create_contenu_dock_pg()
        self.create_contenu_dock_plot()

    def create_dock(self):
        area = DockArea()
        self.obj_graph['param']['dock'] = Dock("Continuous measurements_Multi")
        
       
        
        
        #mfli
        self.obj_graph['plot1']['dock'] = Dock("gaz1 (ppm)")
        self.obj_graph['plot2']['dock'] = Dock("gaz2 (ppm)")
        self.obj_graph['plot3']['dock'] = Dock("gaz3 (ppm)")
        self.obj_graph['plot4']['dock'] = Dock("gaz4 (ppm)")
        #medisoft
        self.obj_graph['plot6']['dock'] = Dock("Flow (ATP l/s)")
        self.obj_graph['plot7']['dock'] = Dock("CO2 (%)")
        self.obj_graph['plot8']['dock'] = Dock("Volume (l)")
        
       ###########################self.obj_graph['plot2']['dock'] = Dock("Concentration (ppmv) function of tau (s)")
        area.addDock(self.obj_graph['param']['dock'],'left')
        
        
       
        
        
        area.addDock(self.obj_graph['plot6']['dock'],'right')
        area.addDock(self.obj_graph['plot7']['dock'],'bottom',self.obj_graph['plot6']['dock'])
        
        area.addDock(self.obj_graph['plot1']['dock'],'right',self.obj_graph['plot6']['dock'])
        area.addDock(self.obj_graph['plot2']['dock'],'bottom',self.obj_graph['plot1']['dock'])
        area.addDock(self.obj_graph['plot3']['dock'],'right',self.obj_graph['plot7']['dock'])
        area.addDock(self.obj_graph['plot4']['dock'],'right',self.obj_graph['plot3']['dock'])
        
        
        
        self.dockArea = DockArea()
        self.setCentralWidget(self.dockArea)
        
        
       
        # Create a dock and add it to the DockArea at the top
        dock1 = Dock("Humidity", size=(0.8, 0.8))
        self.dockArea.addDock(dock1, 'bottom',self.obj_graph['plot6']['dock'])

        # Add a QLabel with a red message to the dock
        self.time_values_label = QLabel("<center> <b style='font-size: 12pt;'>   Time: 0.00s | RH: 0% | T: 0°C | H<sub>2</sub>O: 0ppm </b> </center>")
        
        self.time_values_label.setStyleSheet("color: black")
        dock1.addWidget(self.time_values_label)
        
        ##############################################################
        dock2 = Dock("Zoom", size=(0.1, 0.1))
        self.dockArea.addDock(dock2, 'top',self.obj_graph['param']['dock'] )
        self.zoom_slider = QSlider(orientation=1)  # 1 for horizontal orientation
        self.min_slide=-200
        self.zoom_slider.setMinimum(self.min_slide)
        self.zoom_slider.setMaximum(-2)
        
        
        self.zoom_slider.setValue(-100)#valeur initial nbre de points affichier
        self.zoom_slider.setSingleStep(1)  # Set the step to 1
 
        # self.slider_value_label = QLabel("<center>Display last {} data points </center>".format(abs(self.zoom_slider.value())))

        dock2.addWidget(self.zoom_slider)
        
        # dock2.addWidget(self.slider_value_label)

        
        
        # dock3=Dock("X-axis", size=(0.8, 0.8))
        # self.dockArea.addDock(dock3, 'top',dock2)
        

        # self.min_max_layout = QHBoxLayout()
        self.min_label = QLabel("Min X:")
        self.min_spinbox = QDoubleSpinBox()
        
        self.min_spinbox.setFixedSize(100, 25)
        self.min_spinbox.setMaximum(200000000000000000000000)
        self.min_spinbox.setValue(-10)  # Set a default minimum value

        self.max_label = QLabel("Max X:")
        self.max_spinbox = QDoubleSpinBox()
        self.max_spinbox.setFixedSize(100, 25)
        self.max_spinbox.setMaximum(200000000000000000000000) 
        self.max_spinbox.setValue(200)  # Set a default maximum value
        
       

        self.apply_range_button = QPushButton("Apply X-Axis Range")
        self.apply_range_button.clicked.connect(self.apply_x_axis_range)
        
        self.apply_range_button_2 = QPushButton("Auto")
        self.apply_range_button_2.clicked.connect(self.apply_auto_range)
        
        
        
        self.auto_range_checkbox = QCheckBox("control Flow (L/s)")
        self.auto_range_checkbox.stateChanged.connect(self.toggle_region_selection)
        
        
      
        
        dock2_layout = QVBoxLayout()  # Use a QVBoxLayout to contain multiple QHBoxLayouts
        # Add widgets to the first horizontal layout
        dock2_layout_1 = QHBoxLayout()
        dock2_layout_1.addWidget(self.min_label)
        dock2_layout_1.addWidget(self.min_spinbox)
        dock2_layout_1.addWidget(self.max_label)
        dock2_layout_1.addWidget(self.max_spinbox)
        dock2_layout_1.addWidget(self.apply_range_button)
        dock2_layout_1.addWidget(self.apply_range_button_2)
        
        # Add widgets to the second horizontal layout
        dock2_layout_2 = QHBoxLayout()
        dock2_layout_2.addWidget(self.auto_range_checkbox)
        dock2_layout_2.setAlignment(self.auto_range_checkbox, Qt.AlignCenter)
        
        
        # Add both horizontal layouts to the vertical layout
        dock2_layout.addLayout(dock2_layout_2)
        dock2_layout.addLayout(dock2_layout_1)
        
        
        # Create a widget for dock3 and set the layout
        dock2_widget = QWidget()
        dock2_widget.setLayout(dock2_layout)
        
        dock2.addWidget(dock2_widget)


        area.addDock(self.obj_graph['plot8']['dock'],'top',dock2)
        
        ##############################################################
        
        
        

        
        
        
        
        
        self.setCentralWidget(area)
        
        
        self.fixed_x_range = False#flag to activate xaxis range zoom
        self.apply_range_button.setEnabled(True)  # enable the button initially
        self.apply_range_button_2.setEnabled(True)  # enable the button initially
        
        

        


    def create_contenu_dock_plot(self):
        # creation of plot 1 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot1']['curve'] = MesCourbes()
        self.obj_graph['plot1']['graph'] = self.obj_graph['plot1']['curve'].plot_widget
        self.plot_widget1=self.obj_graph['plot1']['graph']
        self.obj_graph['plot1']['dock'].addWidget(self.plot_widget1)       
        self.obj_graph['plot1']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data1=self.obj_graph['plot1']['graph'].plot(pen='g', symbol='o',symbolBrush='g')
        
        # creation of plot 2 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot2']['curve'] = MesCourbes()
        self.obj_graph['plot2']['graph'] = self.obj_graph['plot2']['curve'].plot_widget 
        self.plot_widget2=self.obj_graph['plot2']['graph']
        self.obj_graph['plot2']['dock'].addWidget(self.plot_widget2)  
        self.obj_graph['plot2']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data2=self.obj_graph['plot2']['graph'].plot(pen='c', symbol='o',symbolBrush='c')
        
        # creation of plot 3 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot3']['curve'] = MesCourbes()
        self.obj_graph['plot3']['graph'] = self.obj_graph['plot3']['curve'].plot_widget 
        self.plot_widget3=self.obj_graph['plot3']['graph']
        self.obj_graph['plot3']['dock'].addWidget(self.plot_widget3)       
        self.obj_graph['plot3']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data3=self.obj_graph['plot3']['graph'].plot(pen='r', symbol='o',symbolBrush='r')
        
        # creation of plot 4 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot4']['curve'] = MesCourbes()
        self.obj_graph['plot4']['graph'] = self.obj_graph['plot4']['curve'].plot_widget 
        self.plot_widget4=self.obj_graph['plot4']['graph']
        self.obj_graph['plot4']['dock'].addWidget(self.plot_widget4)   
        self.obj_graph['plot4']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data4=self.obj_graph['plot4']['graph'].plot(pen='w', symbol='o',symbolBrush='w')
        
        #medisoft
        # creation of plot 6 (graph 1)
        # a place for this graph has been defined in create_dock()
        self.obj_graph['plot6']['curve'] = MesCourbes()
        self.obj_graph['plot6']['graph'] = self.obj_graph['plot6']['curve'].plot_widget 
        self.plot_widget6=self.obj_graph['plot6']['graph']
        self.obj_graph['plot6']['dock'].addWidget(self.plot_widget6)        
        self.obj_graph['plot6']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data6=self.obj_graph['plot6']['graph'].plot(pen='y', symbol='o',symbolBrush='y')
        
        
        self.plot_widget6.setMinimumSize(760, 600)
        
        #calculate area
        # Enable region selection
        
        
        
        
        
        self.obj_graph['plot7']['curve'] = MesCourbes()
        self.obj_graph['plot7']['graph'] = self.obj_graph['plot7']['curve'].plot_widget 
        self.plot_widget7=self.obj_graph['plot7']['graph']
        self.obj_graph['plot7']['dock'].addWidget(self.plot_widget7)       
        self.obj_graph['plot7']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data7=self.obj_graph['plot7']['graph'].plot(pen='m', symbol='o',symbolBrush='m')
        
        
        
        
        self.obj_graph['plot8']['curve'] = MesCourbes()
        self.obj_graph['plot8']['graph'] = self.obj_graph['plot8']['curve'].plot_widget 
        self.plot_widget8=self.obj_graph['plot8']['graph']
        self.obj_graph['plot8']['dock'].addWidget(self.plot_widget8)       
        self.obj_graph['plot8']['curve'].add_curve('curve acq',(0,200,200),markers_on=False) #acquisition curve
        self.plot_data8=self.obj_graph['plot8']['graph'].plot(pen='y', symbol='o', symbolBrush='orange', symbolPen='yellow')
        
        
        self.plot_widget8.setMinimumSize(760, 600)
        
        self.add_cv = QPushButton("Add CV")  
        self.add_cv.clicked.connect(self.apply_cv)
        
        self.remove_cv = QPushButton("Remove Markers")  
        self.remove_cv.clicked.connect(self.remove_mark)
        
        self.zero_volume = QPushButton("Adjust offset volume at rest")  
        self.zero_volume.clicked.connect(self.zero_offset_volume)
        
        
        self.label_1 = QLabel("CV: L")
        

        
        layout_cv = QHBoxLayout()
        
        layout_cv.addWidget(self.label_1)
        layout_cv.addWidget(self.add_cv)
        layout_cv.addWidget(self.remove_cv)
        # layout_cv.addWidget(self.zero_volume)
        
        
        dock_widget = QWidget()
        dock_widget.setLayout(layout_cv)
        
        self.obj_graph['plot8']['dock'].addWidget(dock_widget)
        
        # Enable vertical region selection on y-axis
        self.region_item = pg.LinearRegionItem(orientation='horizontal',brush=pg.mkBrush(0, 255, 0, 50), pen=pg.mkPen(0, 255, 0))
        self.region_item.setZValue(-10)  # Ensure region is drawn beneath the curve
        self.region_item.setRegion([0.1, 0.3])  # Set your desired range here
        self.plot_widget6.addItem(self.region_item)
        # Connect the region change signal to enforce constraints
        # Connect the region change signal to enforce constraints
        self.region_item.sigRegionChanged.connect(self.enforce_region_constraints)
   
    
   
    def enforce_region_constraints(self):
        min_val, max_val = self.region_item.getRegion()
        constrained_min = max(0.1,0.1)
        constrained_max = min(0.3, 0.3)
        self.region_item.setRegion([constrained_min, constrained_max])

 
    def create_contenu_dock_pg(self):
        self.obj_graph['param']['tree'] = ParameterTree()
        self.create_parameter()
        
        
        self.obj_graph['param']['dock'].addWidget(self.obj_graph['param']
                                                  ['tree'])
        
        
    def apply_x_axis_range(self):
        self.fixed_x_range = True
        self.plot_widget1.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        self.plot_widget2.setXRange(self.min_spinbox.value(), self.max_spinbox.value())    
        self.plot_widget3.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        self.plot_widget4.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        self.plot_widget6.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        self.plot_widget7.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        self.plot_widget8.setXRange(self.min_spinbox.value(), self.max_spinbox.value())
        
        
    def apply_auto_range(self):
        self.fixed_x_range = True
        self.plot_widget1.enableAutoRange('x')
        self.plot_widget2.enableAutoRange('x')
        self.plot_widget3.enableAutoRange('x')
        self.plot_widget4.enableAutoRange('x')
        self.plot_widget6.enableAutoRange('x')
        self.plot_widget7.enableAutoRange('x')
        self.plot_widget8.enableAutoRange('x')
        
        self.plot_widget1.enableAutoRange('y')
        self.plot_widget2.enableAutoRange('y')
        self.plot_widget3.enableAutoRange('y')
        self.plot_widget4.enableAutoRange('y')
        self.plot_widget6.enableAutoRange('y')
        self.plot_widget7.enableAutoRange('y')
        self.plot_widget8.enableAutoRange('y')
        self.auto_range_checkbox.setChecked(False)
        
        
        
        
    def create_parameter(self):
        # create a Class with all parameters use in the GUI
        self.parameters = MesParams()

        
        # build the GUI
        (self.obj_graph['param']
         ['parameter']) = Parameter.create(name='params',
                                           type='group',
                                           children=self.parameters.tree)
        (self.obj_graph['param']
         ['parameter'].sigTreeStateChanged.connect(self.catch_param_change))
        
        
        
        
        

        """
        setParameters
        Set the top-level :class:`Parameter <pyqtgraph.parametertree.Parameter>`
        to be displayed in this ParameterTree.

        If *showTop* is False, then the top-level parameter is hidden and only 
        its children will be visible. This is a convenience method equivalent 
        to::
        
            tree.clear()
            tree.addParameters(param, showTop)
        """
        self.obj_graph['param']['tree'].setParameters((self.obj_graph['param']
                                                       ['parameter']),
                                                      showTop=False)
        self.obj_graph['param']['tree'].setWindowTitle('Parameter Tree')
        
        # Apply the delegate
        delegate = toolbox.parameter_tree.RedNameDelegate(self.obj_graph['param']['tree'])
        self.obj_graph['param']['tree'].setItemDelegateForColumn(0, delegate)

    def catch_param_change(self, _, changes):
        # call in create_parameter()
        for param, _, data in changes:
            
            
            
            path = self.obj_graph['param']['parameter'].childPath(param)
            
           
            
            if path is not None: 
                # print(path) -> ['Input- Zurich', 'time constant']
                child_name = '.'.join(path)
                # print(child_name) -> Input- Zurich.time constant
            else:
                child_name = param.name()
            print('  parameter: %s' % child_name)
            print('  change:    %s' % _)
            print('  data:      %s (%s)' % (str(data),type(data)))

            # who is the name of the parameter tree
            # what the name of the paramater of the parameter tree
            try:
                who, what = child_name.split('.')
            except:
                who = child_name
                what = child_name
                
                
            graph_1 = self.obj_graph['plot1']['curve']
            graph_2 = self.obj_graph['plot2']['curve']
            graph_3 = self.obj_graph['plot3']['curve']
            graph_4 = self.obj_graph['plot4']['curve']
            graph_6 = self.obj_graph['plot6']['curve']
            graph_7 = self.obj_graph['plot7']['curve']
            graph_8 = self.obj_graph['plot8']['curve']


            ## INSTRUMENT CHANGE
            # CAUTION check it always in first
            if what in ['instrument']:
                # in this case data == name of the instrument
                print(who,data)
                self.instruments.update(who,data)
    
            ## CHANGE VALUES ASKED ON THE INSTRUMENT
            if who in ['Input','Medisoft','MFLI ch1','Gen_ext_1','MFLI ch2','MFLI ch3','MFLI ch4','Gen_ext_2','Gen_ext_3','Gen_ext_4','Laser Driver_1','Laser Driver_2','Laser Driver_3','Laser Driver_4']:
                #if what in ['time constant','sensitivity','on','amplitude','frequency','current','temperature','modulation','frequency modulation','amplitude modulation','harmonic']:
                # get the nam of instrument asked in the parameter tree 
                instru = self.parameters.give_inst(who)
                self.instruments.set_value(instru,what,data)  
                
                
                
            def adjust_laser_current(self, laser_driver, direction):
               instru_las = self.parameters.give_inst(laser_driver)
               if instru_las is None:
                   print(f"Instrument for {laser_driver} doesn't exist. Skipping adjustment.")
                   return
               if direction =='up':
                   if instru_las=='Thorlabs_ITC4002QCL_M00660255':
                       Ilaser =self.current_gaz1
                       Iparam = 0.001  # Assuming this is the desired current
                   if instru_las=='Thorlabs_LDC4005_M00440716':
                       Ilaser =self.current_gaz2
                       Iparam = 0.001  # Assuming this is the desired current
                   if instru_las=='Thorlabs_ITC4002QCL_M00560075':
                       Ilaser =self.current_gaz3
                       Iparam = 0.001  # Assuming this is the desired current
                   if instru_las=='Thorlabs_ITC4005QCL_M00603635':
                        Ilaser =self.current_gaz4
                        Iparam = 0.001  # Assuming this is the desired current
                   num_steps = int((Ilaser - Iparam) / 0.2)  # Adjust this proportionally as needed
                   Ilist = np.linspace(Iparam, Ilaser, num_steps)
                   for current_i in Ilist:
                     self.instruments.set_value(instru_las, 'current', current_i)
                     self.parameters.dico[f'{laser_driver}'].param('current').setValue(current_i)#displays the current current
                     time.sleep(0.00005)
                     print('Current set to {:.3f}mA'.format(current_i))
                     
                     
               if direction =='down':
                    if instru_las=='Thorlabs_ITC4002QCL_M00660255':
                        Ilaser =self.instruments.get_data(instru_las, poll_length=None)[0]
                        Iparam = 0.001  # Assuming this is the desired current
                    if instru_las=='Thorlabs_LDC4005_M00440716':
                        Ilaser =self.instruments.get_data(instru_las, poll_length=None)[0]
                        Iparam = 0.001  # Assuming this is the desired current
                    if instru_las=='Thorlabs_ITC4002QCL_M00560075':
                        Ilaser =self.instruments.get_data(instru_las, poll_length=None)[0]
                        Iparam = 0.001  # Assuming this is the desired current
                    if instru_las=='Thorlabs_ITC4005QCL_M00603635':
                         Ilaser =self.instruments.get_data(instru_las, poll_length=None)[0]
                         Iparam = 0.001  # Assuming this is the desired current
                    num_steps = int((Ilaser - Iparam) / 0.2)  # Adjust this proportionally as needed
                    Ilist = np.linspace(Ilaser, Iparam, num_steps)
                    for current_i in Ilist:
                      self.instruments.set_value(instru_las, 'current', current_i)
                      self.parameters.dico[f'{laser_driver}'].param('current').setValue(current_i)#displays the current current
                      time.sleep(0.00005)
                      print('Current set to {:.3f}mA'.format(current_i))
                      
                
               
               
            if (who+'.'+what) in 'Laser Driver_1.go up Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_1.go up Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_1.go up Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_1.go up Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_1', 'up')
            if (who+'.'+what) in 'Laser Driver_2.go up Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_2.go up Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_2.go up Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_2.go up Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_2', 'up')
            if (who+'.'+what) in 'Laser Driver_3.go up Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_3.go up Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_3.go up Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_3.go up Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_3', 'up') 
            if (who+'.'+what) in 'Laser Driver_4.go up Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_4.go up Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_4.go up Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_4.go up Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_4', 'up') 
                
            if (who+'.'+what) in 'Laser Driver_1.go down Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_1.go down Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_1.go down Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_1.go down Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_1', 'down')
            if (who+'.'+what) in 'Laser Driver_2.go down Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_2.go down Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_2.go down Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_2.go down Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_2', 'down')
            if (who+'.'+what) in 'Laser Driver_3.go down Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_3.go down Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_3.go down Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_3.go down Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_3', 'down') 
            if (who+'.'+what) in 'Laser Driver_4.go down Ilaser 4002QCL_M00660255' or (who+'.'+what) in 'Laser Driver_4.go down Ilaser LDC4005_M00440716' or (who+'.'+what) in 'Laser Driver_4.go down Ilaser 4002QCL_M00560075' or (who+'.'+what) in 'Laser Driver_4.go down Ilaser 4005QCL_M00603635': 
                adjust_laser_current(self, 'Laser Driver_4', 'down') 
            
                   
            ## START ACQUISITION
            if (who+'.'+what) in 'Plotter.start':
                #flag to stop before saving
                self.plotter_stop_flag=False
                # clear first then start
                for k in ['acq']:
                    # earase data
                    graph_1.clear_data('curve '+k)
                    graph_2.clear_data('curve '+k)
                    graph_3.clear_data('curve '+k)
                    graph_4.clear_data('curve '+k)
                    graph_6.clear_data('curve '+k)
                    graph_7.clear_data('curve '+k)
                    graph_8.clear_data('curve '+k)
                
                
                
                self.start_plotting()
                
                
                          
            if (who+'.'+what) in 'Plotter.stop': 
                self.plotter_stop_flag=True
                self.stop_plotting()
                
                
                
                
            ## ERASE ACQUISITION
            if (who+'.'+what) in 'Plotter.clear all':
                
                self.clear_plotting()

               
                
                for k in ['acq']:
                    # earase data
                    graph_1.clear_data('curve '+k)
                    graph_2.clear_data('curve '+k)
                    graph_3.clear_data('curve '+k)
                    graph_4.clear_data('curve '+k)
                    graph_6.clear_data('curve '+k)
                    graph_7.clear_data('curve '+k)
                    graph_8.clear_data('curve '+k)
                
                # remove list of curve in the paramter tree and a new one
                self.parameters.dico['Graph'].clearChildren()
                self.parameters.dico['Graph'].addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})
           
            
               
                
            
                
    
            ## SAVE ACQUISITION
            if (who+'.'+what) in 'Save.save':
                
                if self.plotter_stop_flag==False:
                    msg_box = QtWidgets.QMessageBox()
                    msg_box.setIcon(QtWidgets.QMessageBox.Warning)
                    msg_box.setInformativeText("<center> <font color='red' size='5'> <b> NOT SAVED </font> <br> <br> <b> Click on stop then save <\b> <\center>")
                    #
                    msg_box.exec_()
                    
                    pass
                
                else:
                   
                

                    # update only the parameters from the section Save and Graph
                    graph_1.save_parameters_sauvegarder(curve_id='curve acq',param=self.parameters)
                    
                    # save everything                
                    # toolbox.sauvegarder.Save('Continous Acquisition',self.obj_graph['plot']['curve'].curves,self.obj_graph['plot2']['curve'].curves)
                    toolbox.sauvegarder.Save('Conc_Gas_MD',self.obj_graph['plot1']['curve'].curves)
                    
                    
                    #message to confirm saving well
                    
                    msg_box = QtWidgets.QMessageBox()
                    msg_box.setIcon(QtWidgets.QMessageBox.Information)
                    msg_box.setInformativeText(
                        "<center> <font color='green' size='6'> <b> Data SAVED successfully </b> </font></center> <br> <center> Check the file <a href='file:///C://Users//user1//Documents//Data_QEPAS//QTF//Conc_Gas_MD'>here</a></center>"
                    )
                    msg_box.setText("Success")
                    msg_box.setStandardButtons(QtWidgets.QMessageBox.Ok)
                    msg_box.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
                    msg_box.exec_()
                    
       
        
    def update_plot(self):

            
        instru3 = self.parameters.give_inst('Arduino')
        instru4=self.parameters.give_inst('Medisoft')

        
        if self.running:
            
            

            ###
            self.current_time = time.time() - self.start_time
            
            self.time_values.append(round(self.current_time,3))
            
            # self.slider_value_label.setText("<center>Display last {} data points </center>".format(abs(self.zoom_slider.value())))


        
            # self.st_time=time.time()
            
       
 
            if instru3=='Arduino_DHT' or instru3=='Arduino_GPIB':
                temp_arduino = self.instruments.receive(instru3)
                if temp_arduino is not None:
                    self.humidity_value = temp_arduino[0]
                    self.temperature_value = temp_arduino[1]
                    self.water_conc_value = temp_arduino[2]
                
                
                # self.humidity_values.append(self.humidity_value)
                # self.temperature_values.append(self.temperature_value)
                # self.water_conc_values.append(self.water_conc_value)
                                               
                self.time_values_label.setText(f"<center> <b style='font-size: 12pt;'> Time: {self.current_time:.2f}s | RH: {self.humidity_value}% | T: {self.temperature_value}°C | H<sub>2</sub>O: {self.water_conc_value:,.0f}ppm </b></center>")
           

            else:
                self.humidity_value=30
                self.temperature_value=25
                self.water_conc_value=9300
                
               
            if instru4=='Medisoft':
                  temp_medisoft = self.instruments.receive(instru4)
                  if temp_medisoft is not None:
                      self.time_stamp, self.index, self.flow_value, self.volume_value,self.CO2_value,self.R1_value,self.R2_value,self.R3_value,self.R4_value= temp_medisoft
                      # self.time_stamp, self.index, self.flow_value, self.CO2_value, self.step = temp_medisoft
                      # self.time_stamp = temp_medisoft[0]
                      # self.index = temp_medisoft[1]
                      # self.flow_value = temp_medisoft[2]
                      # self.CO2_value = temp_medisoft[3]
                      # print(self.time_stamp)
                      # print(self.time_stamp,self.index,self.flow_value)
                      
                  
                      
                  g1,g2,g3,g4=self.parameters.gain()
                  
                  # print(type(g1),g2,g3,g4)
                  if g1 is not None:
                      R1_conc=((self.R1_value/g1)*1000)/self.SLOPE_gaz1
                      self.R1_conc_list.append(R1_conc)
                  if g2 is not None:
                      R2_conc=((self.R2_value/g2)*1000)/self.SLOPE_gaz2
                      self.R2_conc_list.append(R2_conc)
                  if g3 is not None:
                      R3_conc=((self.R3_value/(g3*10))*1000)/self.SLOPE_gaz3 #ampli TA pris en compte calib+ ne pas oublier ampli gain 10
                      self.R3_conc_list.append(R3_conc)
                  if g4 is not None:
                      R4_conc=((self.R4_value/(g4*10))*1000)/self.SLOPE_gaz4 #ampli TA pris en compte calib+ ne pas oublier ampli gain 10
                      self.R4_conc_list.append(R4_conc)
                      
                      
                  self.volume_value_1=self.volume_value-self.offset_volume
                  self.time_stamp_values.append(self.time_stamp)
                  # self.index_values.append(self.index)
                  self.flow_values.append(self.flow_value)
                  self.volume_values.append(self.volume_value_1)
                  self.CO2_values.append(self.CO2_value)
                  
                  
                  # self.R1_values.append(self.R1_value)
                  # self.R2_values.append(self.R2_value)
                  # self.R3_values.append(self.R3_value)
                  # self.R4_values.append(self.R4_value)
                  
                  
                  #mfli_medisoft

                      
                  self.plot_data6.setData(self.time_values[self.zoom_slider.value():], self.flow_values[self.zoom_slider.value():])
                
                
                  self.plot_data8.setData(self.time_values[self.zoom_slider.value():], self.volume_values[self.zoom_slider.value():])
                     

                  self.plot_data7.setData(self.time_values[self.zoom_slider.value():], self.CO2_values[self.zoom_slider.value():])
                  
   
                  
                  # print(self.time_values,self.R1_values)

                  self.plot_data1.setData(self.time_values[self.zoom_slider.value():], self.R1_conc_list[self.zoom_slider.value():])# Display the last 100 values
                  self.plot_data2.setData(self.time_values[self.zoom_slider.value():], self.R2_conc_list[self.zoom_slider.value():])
                  self.plot_data3.setData(self.time_values[self.zoom_slider.value():], self.R3_conc_list[self.zoom_slider.value():])
                  self.plot_data4.setData(self.time_values[self.zoom_slider.value():], self.R4_conc_list[self.zoom_slider.value():])

                 
            else:
                  self.time_stamp=0
                  self.index=0
                  self.flow_value=0
                  self.volume_value=0
                  self.CO2_value=0
                  self.R1_value = 0
                  self.R2_value = 0
                  self.R3_value = 0
                  self.R4_value = 0
                  # self.step=0 
                  
            graph_1 = self.obj_graph['plot1']['curve']
            
            graph_1.curves['curve acq']['data']['title1']=self.obj_graph['plot1']['title']
            graph_1.curves['curve acq']['data']['title2']=self.obj_graph['plot2']['title']
            graph_1.curves['curve acq']['data']['title3']=self.obj_graph['plot3']['title']
            graph_1.curves['curve acq']['data']['title4']=self.obj_graph['plot4']['title']
            graph_1.curves['curve acq']['data']['title5']=self.obj_graph['plot5']['title']
            
            graph_1.curves['curve acq']['data']['title6']=self.obj_graph['plot6']['title']
            graph_1.curves['curve acq']['data']['title7']=self.obj_graph['plot7']['title']
            graph_1.curves['curve acq']['data']['title8']=self.obj_graph['plot8']['title']
            
            self.obj_graph['plot1']['curve'].plot_widget.setTitle(self.obj_graph['plot1']['title']+' (ppm)')
            self.obj_graph['plot2']['curve'].plot_widget.setTitle(self.obj_graph['plot2']['title']+' (ppm)')
            self.obj_graph['plot3']['curve'].plot_widget.setTitle(self.obj_graph['plot3']['title']+' (ppm)')
            self.obj_graph['plot4']['curve'].plot_widget.setTitle(self.obj_graph['plot4']['title']+' (ppm)')
            
            self.obj_graph['plot6']['curve'].plot_widget.setTitle(self.obj_graph['plot6']['title'])
            self.obj_graph['plot7']['curve'].plot_widget.setTitle(self.obj_graph['plot7']['title'])
            self.obj_graph['plot8']['curve'].plot_widget.setTitle(self.obj_graph['plot8']['title'])

            graph_1.curves['curve acq']['data']['tim'] = np.append(graph_1.curves['curve acq']['data']['tim'],self.current_time)
            graph_1.curves['curve acq']['data']['R1'] = np.append(graph_1.curves['curve acq']['data']['R1'],self.R1_value)
            graph_1.curves['curve acq']['data']['R2'] = np.append(graph_1.curves['curve acq']['data']['R2'],self.R2_value)
            graph_1.curves['curve acq']['data']['R3'] = np.append(graph_1.curves['curve acq']['data']['R3'],self.R3_value)
            graph_1.curves['curve acq']['data']['R4'] = np.append(graph_1.curves['curve acq']['data']['R4'],self.R4_value)
                        
                       
                
            graph_1.curves['curve acq']['data']['RH'] = np.append(graph_1.curves['curve acq']['data']['RH'],self.humidity_value)
            graph_1.curves['curve acq']['data']['T'] = np.append(graph_1.curves['curve acq']['data']['T'],self.temperature_value)
            graph_1.curves['curve acq']['data']['H2O'] = np.append(graph_1.curves['curve acq']['data']['H2O'],self.water_conc_value)
                         
              
            graph_1.curves['curve acq']['data']['time_stamp_medisoft'] = np.append(graph_1.curves['curve acq']['data']['time_stamp_medisoft'],self.time_stamp)
            graph_1.curves['curve acq']['data']['index_medisoft'] = np.append(graph_1.curves['curve acq']['data']['index_medisoft'],self.index)
            graph_1.curves['curve acq']['data']['volume_medisoft'] = np.append(graph_1.curves['curve acq']['data']['volume_medisoft'],self.volume_value)
            graph_1.curves['curve acq']['data']['flow_medisoft'] = np.append(graph_1.curves['curve acq']['data']['flow_medisoft'],self.flow_value)
            graph_1.curves['curve acq']['data']['CO2_medisoft'] = np.append(graph_1.curves['curve acq']['data']['CO2_medisoft'],self.CO2_value)
                
                
           
            
            
            graph_1.save_parameters('curve acq',self.parameters)
            
            
            
            
            if self.min_slide>-2147483648:
                self.min_slide-=1
                self.zoom_slider.setMinimum(self.min_slide)
            
            
                
            # self.current_time += self.time_interval 
            # print(time.time() - self.st_time)
        

        
    def closeEvent(self, event):
        result = QtWidgets.QMessageBox.question(self,
                                                "Confirm Exit...",
                                                "<center> <font color='red' size='6'> Do you want to exit ? </font> <br> N.B This action safley reduces laser currents down to ZEROs.<\center>",
                                                (QtWidgets.QMessageBox.Yes |
                                                  QtWidgets.QMessageBox.No))
        if result == QtWidgets.QMessageBox.Yes:
            # permet d'ajouter du code pour fermer proprement
            # close all instrument
           
            
            self.running = False
            self.timer.stop() 
            
            
                    
                    # Loop through each laser driver
            # Loop through each laser driver
            for driver_num in range(1, 5):  # Assuming there are 4 laser drivers
                instru = self.parameters.give_inst(f'Laser Driver_{driver_num}')
                if instru is None:
                    print(f"Instrument for Laser Driver_{driver_num} doesn't exist. Skipping adjustment.")
                    continue
                
                Ilaser = self.instruments.get_data(instru, poll_length=None)[0]
                Iparam = 0.001  # Assuming this is the desired current
            
                
                if Ilaser > Iparam:
                    # Calculate the number of steps dynamically based on Ilaser
                    num_steps = int((Ilaser - Iparam) / 0.2)  # Adjust this proportionally as needed
                    Ilist = np.linspace(Ilaser, Iparam, num_steps)
                    for current_i in Ilist:
                        self.instruments.set_value(instru, 'current', current_i)
                        self.parameters.dico[f'Laser Driver_{driver_num}'].param('current').setValue(current_i)#displays the current current
                        print('Current set to {:.3f}mA'.format(current_i))
                        time.sleep(0.00005)
                    
                    
                    
                    
            self.instruments.close_all_inst()
            # print("Serial connection closed.")
            event.accept()
        else:
            event.ignore()
        pass
    

class MesInstrus(toolbox.mother_class.MotherInstru):

    def __init__(self,param_tree):
        self.param_tree = param_tree
        # attribut with all the instrument created
        self.connected = []
        # dico that the code will fill with instru which can be called
        self.use = {}
        
    


class MesParams(toolbox.mother_class.MotherParam):
    def __init__(self):
        # dictionary of parameter tree used
        self.dico = {
            
            
            'Arduino':toolbox.parameter_tree.Arduino(name='Arduino'),
            
            'Medisoft':toolbox.parameter_tree.Medisoft_expair(name='Medisoft'),
            
            'Input':toolbox.parameter_tree.Input(name='Input'),
            
            
            
            'QEPAS_1': toolbox.parameter_tree.QEPAS(name='QEPAS 1'),
           
            
            'MFLI ch1':toolbox.parameter_tree.Generator1(name='MFLI ch1'),
            
            'Gen_ext_1':toolbox.parameter_tree.Generator_extern(name='Gen_ext_1'),
            
            'Laser Driver_1':toolbox.parameter_tree.LaserDriver(name='Laser Driver_1'),
            
            'QEPAS_2:':toolbox.parameter_tree.QEPAS(name='QEPAS 2'),
            
            'MFLI ch2':toolbox.parameter_tree.Generator2(name='MFLI ch2'),
            
            'Gen_ext_2':toolbox.parameter_tree.Generator_extern(name='Gen_ext_2'),
            
            'Laser Driver_2':toolbox.parameter_tree.LaserDriver(name='Laser Driver_2'),
            
            'QEPAS_3:':toolbox.parameter_tree.QEPAS(name='QEPAS 3'),
            'MFLI ch3':toolbox.parameter_tree.Generator3(name='MFLI ch3'),
            
            'Gen_ext_3':toolbox.parameter_tree.Generator_extern(name='Gen_ext_3'),
            
            'Laser Driver_3':toolbox.parameter_tree.LaserDriver(name='Laser Driver_3'),
            
            'QEPAS_4:':toolbox.parameter_tree.QEPAS(name='QEPAS 4'),
            'MFLI ch4':toolbox.parameter_tree.Generator4(name='MFLI ch4'),  
            
            'Gen_ext_4':toolbox.parameter_tree.Generator_extern(name='Gen_ext_4'),
            
            'Laser Driver_4':toolbox.parameter_tree.LaserDriver(name='Laser Driver_4'),
            
          
          
                  
            'Plotter':toolbox.parameter_tree.Plotter(name='Plotter'),
            'Graph':toolbox.parameter_tree.GraphList(name='Graph'),
            'Save':toolbox.parameter_tree.Save(name='Save')}
        
        
        
        
        
        # use the self.dico to build a list required for the GUI
        self.tree = []       
        for k in self.dico:
            self.tree.append(self.dico[k]) 
            
        
        
            
            
    def waiting_time(self):
        tc = self.dico['Input'].param('time constant').value()
        wt = self.dico['Plotter'].param('waiting time').value()
        return tc*wt
    def poll_length(self):
        return self.dico['Input'].param('poll length').value()
    
    def describ(self):
        return self.dico['Save'].param('setup description').value()
    
    def gain(self):
          try:
              value_1 = self.dico['MFLI ch1'].param('1_gain').value()
          except KeyError:
              value_1 = None
          
          try:
              value_2 = self.dico['MFLI ch2'].param('2_gain').value()
          except KeyError:
              value_2 = None
          
          try:
              value_3 = self.dico['MFLI ch3'].param('3_gain').value()
          except KeyError:
              value_3 = None
          
          try:
              value_4 = self.dico['MFLI ch4'].param('4_gain').value()
          except KeyError:
              value_4 = None
          
          return value_1, value_2, value_3, value_4
    
    
    
class MesCourbes(toolbox.mother_class.MotherCourbe):
    def __init__(self):
        self.plot_widget = PlotWidget()
        self.plot_widget.showGrid(x=True,y=True)
        # self.plot_widget.getPlotItem().addLegend()
        self.plot_widget.setBackground((0, 0, 0))
        #  dictionary with all the curve and their data 
        # curve 0 is dedicated to the live/acquisition plot
        self.curves = {}

    def add_curve(self, curve_id, curve_color, markers_on=False, linewidht=3):
        curve_name = curve_id
        pen = pg.mkPen(curve_color, width=linewidht)
        symbol = "o"
        symbolPen = pg.mkPen(0,0,0)
        symbolBrush = curve_color
        symbolSize = 8
        # this adds the item to the plot and legend
        if markers_on:
            plot = self.plot_widget.plot(
                name=curve_name, pen=pen, symbol=symbol, symbolPen=symbolPen, 
                symbolBrush=symbolBrush, symbolSize=symbolSize
            )
        else:
            plot = self.plot_widget.plot(name=curve_name, pen=pen)
        self.curves[curve_id] = {
                    'plot':plot, 
                    'data':{'tim':[],'R1':[],'R2':[],'R3':[],'R4':[],'RH':[],'T':[],'H2O':[],'flow_medisoft':[],'volume_medisoft':[],'CO2_medisoft':[],'time_stamp_medisoft':[],'index_medisoft':[],
                            
                            
                            'time_increm_medisoft_complet':[],'time_clock_medisoft_complet':[],'flow_medisoft_complet':[],'CO2_medisoft_complet':[],'time_stamp_medisoft_complet':[],'index_medisoft_complet':[],'volume_medisoft_complet':[],
                            'R1_medisoft_complet':[],'R2_medisoft_complet':[],'R3_medisoft_complet':[],'R4_medisoft_complet':[],
                            'Conc1_medisoft_complet':[],'Conc2_medisoft_complet':[],'Conc3_medisoft_complet':[],'Conc4_medisoft_complet':[],
                            'title1':[],'title2':[],'title3':[],'title4':[],'title5':[],'title6':[],'title7':[],'title8':[]
                            }
                        }   
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = MainWindow()
    main.run()
    app.exec_()
       
