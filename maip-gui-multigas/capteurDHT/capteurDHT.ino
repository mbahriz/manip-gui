#include <DHT.h>

#define DHTPIN 2
#define DHTTYPE DHT22  // Change this to DHT11 if you are using DHT11

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  delay(10);  // Delay to wait for the sensor to stabilize

  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  if (!isnan(humidity) && !isnan(temperature)) {
    Serial.print("arduino_dht ");
    Serial.print("H:");
    Serial.print(humidity, 1);  // 1 decimal place
    Serial.print(", T:");
    Serial.println(temperature, 1);  // 1 decimal place
  }
}


