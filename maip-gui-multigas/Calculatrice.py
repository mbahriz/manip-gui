
# -*- coding: utf-8 -*-
# 21 septembre 2023 Update for pyqtgraph 0.13.1

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets

app = pg.mkQApp("Parameter Tree Example")
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

import toolbox.modulation
import toolbox.RH as H2O
## test subclassing parameters
## This parameter automatically generates two child parameters which are always reciprocals of each other
class Modulation(pTypes.GroupParameter):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'Laser driver', 'type': 'list', 'values': ['Thorlabs_ITC4001QCL','Thorlabs_ITC4002QCL','Thorlabs_ITC4005QCL'], 'value': 'Thorlabs_ITC4002QCL'})        
        self.addChild({'name': 'Frequency', 'type': 'float', 'value': 50e3, 'suffix': 'Hz', 'siPrefix': True,'decimals':6, 'step': 1000})
        self.addChild({'name': 'Tension', 'type': 'float', 'value': 25e-3, 'suffix': 'Vpk', 'siPrefix': True,'decimals':6, 'step': 1e-3})
        self.addChild({'name': 'Current', 'type': 'float', 'value': 7.59725e-3, 'suffix': 'App', 'siPrefix': True,'decimals':6, 'step': 1e-3})
        self.addChild({'name': 'TF', 'type': 'float', 'value':7.59725e-3/25e-3  , 'suffix': 'App/Vpp', 'siPrefix': True,'decimals':6, 'step': 1e-3})

        self.f = self.param('Frequency')
        self.u = self.param('Tension')
        self.i = self.param('Current')
        self.tf = self.param('TF')
        self.ref = self.param('Laser driver')
       
        self.convert = toolbox.modulation.Convertion(ref_Thorlabs=self.ref.value())

        self.f.sigValueChanged.connect(self.fChanged)
        self.u.sigValueChanged.connect(self.uChanged)
        self.i.sigValueChanged.connect(self.iChanged)
        self.tf.sigValueChanged.connect(self.tfChanged)
        self.ref.sigValueChanged.connect(self.refChanged)
               
    def fChanged(self):
        F = self.f.value()
        I = self.i.value()
        U = self.convert.give_U(frequency=F, current=I)
        TF=I/(U*2)
        self.u.setValue(U, blockSignal=self.uChanged)
        self.tf.setValue(TF, blockSignal=self.tfChanged)

    def uChanged(self):
        F = self.f.value()
        U = self.u.value()
        I = self.convert.give_I(frequency=F, tension=U)
        TF=I/(U*2)
        self.i.setValue(I, blockSignal=self.iChanged)
        self.tf.setValue(TF, blockSignal=self.tfChanged)

    def iChanged(self):
        F = self.f.value()
        I = self.i.value()
        U = self.convert.give_U(frequency=F, current=I)
        TF=I/(U*2)
        self.u.setValue(U, blockSignal=self.uChanged)
        self.tf.setValue(TF, blockSignal=self.tfChanged)
   
    def tfChanged(self):
        I = self.i.value()
        U = self.u.value()
        TF=I/(U*2)
        self.tf.setValue(TF, blockSignal=self.tfChanged)
        
    def refChanged(self):
        self.convert = toolbox.modulation.Convertion(ref_Thorlabs=self.ref.value())
        self.fChanged()

          
class Voltageconversion(pTypes.GroupParameter):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'Volt_pk', 'type': 'float', 'value': 1, 'suffix': 'VpK', 'siPrefix': True,'decimals':6, 'step': 1e-3})
        self.addChild({'name': 'Volt_pp', 'type': 'float', 'value':2, 'suffix': 'Vpp', 'siPrefix': True,'decimals':6, 'step': 1e-3})
        self.addChild({'name': 'Volt_rms', 'type': 'float', 'value':707e-3, 'suffix': 'Vrms', 'siPrefix': True,'decimals':6, 'step': 1e-3})        
        
        self.Vpk = self.param('Volt_pk')
        self.Vpp = self.param('Volt_pp')
        self.Vrms = self.param('Volt_rms')
        
        self.Vpk.sigValueChanged.connect(self.vpkChanged)
        self.Vpp.sigValueChanged.connect(self.vppChanged)
        self.Vrms.sigValueChanged.connect(self.vrmsChanged) 
        
        
    def vpkChanged(self):
        VPK = self.Vpk.value()
        VPP = 2*self.Vpk.value()
        VRMS = self.Vpk.value()/ (2 ** 0.5)
        self.Vpp.setValue(VPP, blockSignal=self.vppChanged)
        self.Vrms.setValue(VRMS, blockSignal=self.vrmsChanged) 

    def vppChanged(self):
        VPP = self.Vpp.value()
        VPK = self.Vpp.value()/2
        VRMS = self.Vpp.value()/ (2*(2 ** 0.5))
        self.Vpk.setValue(VPK, blockSignal=self.vpkChanged)
        self.Vrms.setValue(VRMS, blockSignal=self.vrmsChanged)    


    def vrmsChanged(self):
        VRMS = self.Vrms.value()
        VPK = VRMS*(2 ** 0.5)
        VPP = VRMS*2*(2 ** 0.5)
        self.Vpk.setValue(VPK, blockSignal=self.vpkChanged)
        self.Vpp.setValue(VPP, blockSignal=self.vppChanged) 
        
class WaterC(pTypes.GroupParameter):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)


        self.addChild({'name': 'RH', 'type': 'float', 'value':25, 'suffix': 'percent', 'siPrefix': True,'decimals':6, 'step': 1e-3})
        self.addChild({'name': 'T', 'type': 'float', 'value':40, 'suffix': 'degC', 'siPrefix': True,'decimals':6, 'step': 1e-3})
        self.addChild({'name': 'Conc H2O', 'type': 'float', 'value':12400, 'suffix': 'ppm', 'siPrefix': True,'decimals':6, 'step': 1e-3})        
        self.RH = self.param('RH')
        self.T = self.param('T')
        self.Conc_H2O = self.param('Conc H2O')
        
        self.RH.sigValueChanged.connect(self.RHChanged)
        self.T.sigValueChanged.connect(self.TChanged)
        self.Conc_H2O.sigValueChanged.connect(self.RHTChanged)
        
    def RHChanged(self):
        RH = self.RH.value()
        T=self.T.value()
        CONC=H2O.Waterconc(RH,T)
        CONC_H2O = CONC.CH2O()
        self.Conc_H2O.setValue(CONC_H2O, blockSignal=self.RHTChanged)
        self.T.setValue(T, blockSignal=self.TChanged)
        
    def TChanged(self):
        RH = self.RH.value()
        T=self.T.value()
        CONC=H2O.Waterconc(RH,T)
        CONC_H2O = CONC.CH2O()
        self.Conc_H2O.setValue(CONC_H2O, blockSignal=self.RHTChanged)       
        self.RH.setValue(RH, blockSignal=self.RHChanged)
        
    def RHTChanged(self):        
        RH = self.RH.value()
        T=self.T.value()
        CONC=H2O.Waterconc(RH,T)
        CONC_H2O = CONC.CH2O()
        self.Conc_H2O.setValue(CONC_H2O, blockSignal=self.RHTChanged)
        

class NNEA(pTypes.GroupParameter):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'Conc LOD @ 1s', 'type': 'float', 'value':15, 'suffix': 'ppmv', 'siPrefix': False,'decimals':6, 'step': 1e-1,'tip':'concentration of a target gas in ppmv'})
        self.addChild({'name': 'alpha @ 1ppmv', 'type': 'float', 'value':2.084e-7, 'suffix': 'cm-1', 'siPrefix': False,'decimals':8, 'step': 1e-3,'tip':'absorption coefficient for concentration of 1 ppmv'})
        self.addChild({'name': 'Power laser', 'type': 'float', 'value':3.9, 'suffix': 'mW', 'siPrefix': False,'decimals':3, 'step': 1e-1,'tip':'power laser'})
        self.addChild({'name': 'NNEA @ 1s', 'type': 'str', 'value': '...'})
        self.C = self.param('Conc LOD @ 1s')
        self.alpha = self.param('alpha @ 1ppmv')
        self.P = self.param('Power laser')
        self.NNEA = self.param('NNEA @ 1s')
        self.valueChanged()

        self.C.sigValueChanged.connect(self.valueChanged)
        self.alpha.sigValueChanged.connect(self.valueChanged)
        self.P.sigValueChanged.connect(self.valueChanged)

    def valueChanged(self):
        C = self.C.value()
        alpha=self.alpha.value()
        P=self.P.value()*1e-3
        NNEA = str(C*P*alpha)+' Wcm-1Hz-1/2'
        self.NNEA.setValue(NNEA)

params = [Modulation(name='Modulation')]+[Voltageconversion(name='Convert Volt')]+[WaterC(name='Water Concentration')]+[NNEA(name='NNEA @ 1s')]



## Create tree of Parameter objects
p = Parameter.create(name='params', type='group', children=params)

## If anything changes in the tree, print a message
def change(param, changes):
    print("tree changes:")
    for param, change, data in changes:
        path = p.childPath(param)
        if path is not None:
            childName = '.'.join(path)
        else:
            childName = param.name()
        print('  parameter: %s'% childName)
        print('  change:    %s'% change)
        print('  data:      %s'% str(data))
        print('  ----------')
    
p.sigTreeStateChanged.connect(change)
        

## Create two ParameterTree widgets, both accessing the same data
t = ParameterTree()
t.setParameters(p, showTop=False)
t.setWindowTitle('Calculatrice')

'''
win = QtGui.QWidget()
layout = QtGui.QGridLayout()
win.setLayout(layout)
layout.addWidget(t, 1, 0, 1, 1)
win.show()
win.resize(400,300)


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
'''

win = QtWidgets.QWidget()
layout = QtWidgets.QGridLayout()
win.setLayout(layout)
layout.addWidget(QtWidgets.QLabel("Calculatrice"), 0,  0, 1, 2) #widget added at row 0, column 0, with a row span of 1 and a column span of 2:
layout.addWidget(t, 1, 0, 1, 1)
win.show()
win.resize(500,600)




if __name__ == '__main__':
    pg.exec()
