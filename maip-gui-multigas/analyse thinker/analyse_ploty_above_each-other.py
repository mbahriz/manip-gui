import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import pandas as pd
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.figure_factory as ff
from plotly import tools
from plotly.graph_objs import Scatter
from plotly.tools import make_subplots

class CSVPlotterApp:
    def __init__(self, root):
        self.root = root
        self.root.title("CSV Plotter")

        self.file_path = tk.StringVar()
        self.column_names = []
        self.selected_x_columns = []
        self.selected_y_columns = []

        # File Selection
        tk.Label(root, text="CSV File:").grid(row=0, column=0, padx=10, pady=5)
        tk.Entry(root, textvariable=self.file_path, state="disabled", width=30).grid(row=0, column=1, padx=10, pady=5)
        tk.Button(root, text="Browse", command=self.browse_file).grid(row=0, column=2, padx=5, pady=5)

        # Column Selection
        tk.Label(root, text="Select Columns:").grid(row=1, column=0, padx=10, pady=5)

        self.column_dropdown_pairs = []
        self.add_column_dropdowns()

        # Add additional column selection buttons
        for i in range(2, 5):
            # tk.Label(root, text=f"Select X-Axis Column {i}:").grid(row=i, column=0, padx=10, pady=5)
            # tk.Label(root, text=f"Select Y-Axis Column {i+3}:").grid(row=i+3, column=0, padx=10, pady=5)
            self.add_column_dropdowns()

        # Plot Button
        tk.Button(root, text="Plot", command=self.plot).grid(row=8, column=1, pady=10)

    def browse_file(self):
        file_path = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")])
        if file_path:
            self.file_path.set(file_path)
            self.load_columns(file_path)

    def load_columns(self, file_path):
        df = pd.read_csv(file_path, delimiter=';')
        self.column_names = df.columns.tolist()
        self.update_column_dropdowns()

    def add_column_dropdowns(self):
        x_column_dropdown = ttk.Combobox(self.root, values=self.column_names, state="readonly")
        x_column_dropdown.grid(row=len(self.column_dropdown_pairs) + 1, column=1, padx=10, pady=5)
        
        y_column_dropdown = ttk.Combobox(self.root, values=self.column_names, state="readonly")
        y_column_dropdown.grid(row=len(self.column_dropdown_pairs) + 1, column=2, padx=10, pady=5)

        self.column_dropdown_pairs.append((x_column_dropdown, y_column_dropdown))

    def update_column_dropdowns(self):
        for x_dropdown, y_dropdown in self.column_dropdown_pairs:
            x_dropdown["values"] = self.column_names
            y_dropdown["values"] = self.column_names
           

    def plot(self):
        if not self.file_path.get() or not any(x.get() or y.get() for x, y in self.column_dropdown_pairs):
            tk.messagebox.showerror("Error", "Please select a CSV file and at least one column for plotting.")
            return
    
        self.selected_x_columns = [x.get() for x, _ in self.column_dropdown_pairs if x.get()]
        self.selected_y_columns = [y.get() for _, y in self.column_dropdown_pairs if y.get()]
    
        df = pd.read_csv(self.file_path.get(), delimiter=';')
        
        print("Selected X-Axis Columns:", self.selected_x_columns)
        print("Selected Y-Axis Columns:", self.selected_y_columns)
    
        fig = make_subplots(rows=len(self.selected_y_columns), cols=1, subplot_titles=self.selected_y_columns)
    
        for i, (x_col, y_col) in enumerate(zip(self.selected_x_columns, self.selected_y_columns), 1):
            scatter = go.Scatter(x=df[x_col], y=df[y_col], mode='markers', name=f"{x_col} vs {y_col}")
            fig.add_trace(scatter, row=i, col=1)
    
        fig.update_layout(
            height=600,
            showlegend=False,
            plot_bgcolor='white',  # Set the background color to white
        )
    
        for i, x_col in enumerate(self.selected_x_columns, 1):
            fig.update_xaxes(
                title_text=x_col,
                row=i,
                col=1,
                showgrid=True,
                gridcolor='black',
                zeroline=False,
                linecolor='black',  # Set axis line color to black
                linewidth=0.5,  # Set axis line width to a thin value
                gridwidth=0.5,  # Set grid line width to a thin value
            )
    
        for i, y_col in enumerate(self.selected_y_columns, 1):
            fig.update_yaxes(
                title_text=y_col,
                row=i,
                col=1,
                showgrid=True,
                gridcolor='black',
                zeroline=False,
                linecolor='black',  # Set axis line color to black
                linewidth=0.5,  # Set axis line width to a thin value
                gridwidth=0.5,  # Set grid line width to a thin value
            )
    
        # Embed the plot in Tkinter window
        fig.show(config={'displayModeBar': False, 'scrollZoom': False}, renderer='png')
        fig.write_image('temp_plot.png')  # Save the plot as a temporary image
        img = tk.PhotoImage(file='temp_plot.png')
        label = tk.Label(self.root, image=img)
        label.photo = img
        label.grid(row=9, column=1, padx=10, pady=10)



      

if __name__ == "__main__":
    root = tk.Tk()
    app = CSVPlotterApp(root)
    root.mainloop()
