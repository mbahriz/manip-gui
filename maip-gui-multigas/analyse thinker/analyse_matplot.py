import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import pandas as pd
import matplotlib.pyplot as plt

class CSVPlotterApp:
    def __init__(self, root):
        self.root = root
        self.root.title("CSV Plotter")

        self.file_path = tk.StringVar()
        self.column_names = []
        self.selected_x_columns = []
        self.selected_y_columns = []

        # File Selection
        tk.Label(root, text="CSV File:").grid(row=0, column=0, padx=10, pady=5)
        tk.Entry(root, textvariable=self.file_path, state="disabled", width=30).grid(row=0, column=1, padx=10, pady=5)
        tk.Button(root, text="Browse", command=self.browse_file).grid(row=0, column=2, padx=5, pady=5)

        # Column Selection
        tk.Label(root, text="Select Columns:").grid(row=1, column=0, padx=10, pady=5)

        self.column_dropdown_pairs = []
        self.add_column_dropdowns()

        # Add additional column selection buttons
        for i in range(2, 5):
            # tk.Label(root, text=f"Select X-Axis Column {i}:").grid(row=i, column=0, padx=10, pady=5)
            # tk.Label(root, text=f"Select Y-Axis Column {i+3}:").grid(row=i+3, column=0, padx=10, pady=5)
            self.add_column_dropdowns()

        # Plot Button
        tk.Button(root, text="Plot", command=self.plot).grid(row=8, column=1, pady=10)

    def browse_file(self):
        file_path = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")])
        if file_path:
            self.file_path.set(file_path)
            self.load_columns(file_path)

    def load_columns(self, file_path):
        df = pd.read_csv(file_path, delimiter=';')
        self.column_names = df.columns.tolist()
        self.update_column_dropdowns()

    def add_column_dropdowns(self):
        x_column_dropdown = ttk.Combobox(self.root, values=self.column_names, state="readonly")
        x_column_dropdown.grid(row=len(self.column_dropdown_pairs) + 1, column=1, padx=10, pady=5)
        
        y_column_dropdown = ttk.Combobox(self.root, values=self.column_names, state="readonly")
        y_column_dropdown.grid(row=len(self.column_dropdown_pairs) + 1, column=2, padx=10, pady=5)

        self.column_dropdown_pairs.append((x_column_dropdown, y_column_dropdown))

    def update_column_dropdowns(self):
        for x_dropdown, y_dropdown in self.column_dropdown_pairs:
            x_dropdown["values"] = self.column_names
            y_dropdown["values"] = self.column_names

    def plot(self):
        if not self.file_path.get() or not any(x.get() or y.get() for x, y in self.column_dropdown_pairs):
            tk.messagebox.showerror("Error", "Please select a CSV file and at least one column for plotting.")
            return

        self.selected_x_columns = [x.get() for x, _ in self.column_dropdown_pairs if x.get()]
        self.selected_y_columns = [y.get() for _, y in self.column_dropdown_pairs if y.get()]

        df = pd.read_csv(self.file_path.get(), delimiter=';')

        plt.figure(figsize=(8, 5))

        for x_col, y_col in zip(self.selected_x_columns, self.selected_y_columns):
            plt.scatter(df[x_col], df[y_col], label=f"{x_col} vs {y_col}", alpha=0.7)

        plt.title(f"Scatter Plots")
        plt.xlabel(', '.join(self.selected_x_columns))
        plt.ylabel(', '.join(self.selected_y_columns))
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15), ncol=len(self.selected_x_columns), fancybox=True, shadow=True)

        canvas = FigureCanvasTkAgg(plt.gcf(), master=self.root)
        canvas_widget = canvas.get_tk_widget()
        canvas_widget.grid(row=9, column=1, padx=10, pady=10)

        # Add navigation toolbar for zooming
        toolbar = NavigationToolbar2Tk(canvas, self.root)
        toolbar.update()
        toolbar.grid(row=10, column=1, pady=5, padx=10, sticky='ew')  # Place the toolbar below the canvas

if __name__ == "__main__":
    root = tk.Tk()
    app = CSVPlotterApp(root)
    root.mainloop()
