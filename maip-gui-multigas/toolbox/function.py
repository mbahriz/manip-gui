import numpy as np
import time
import pandas #to write csv file
import os  #to create folder
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import interpolate
import os, psutil 

def average(ARRAY,ns):
    '''
    Cut the long array list with all its sequence
    in array, one array by frequency sweep.
    And return only one array which is the average of all the array.
    --------
    ARRAY a list of number
    ns the number of sequence
    '''
    i=0
    nv = len(ARRAY)
    LIST_OF_ARRAY=[]
    while i <=ns-1:
        i+=1
        TEMP = ARRAY[int(nv/ns)*(i-1):int(nv/ns)*i]
        TEMP = TEMP[::(-1)**(i+1)]
        LIST_OF_ARRAY.append(TEMP)
    #print(LIST_OF_ARRAY)    
    MEAN = np.mean(LIST_OF_ARRAY,axis=0)
    #print(MEAN)
    return MEAN

def create_frequency_list(dico):
    '''
    Create a array with all the frequency use for the frequency sweep
    It take into account if many sequences have been asked
    '''
    dico['Data']['FREQ'] = np.linspace(
            dico['Frequency sweep']['acquisition']['freq min'],
            dico['Frequency sweep']['acquisition']['freq max'],
            dico['Frequency sweep']['acquisition']['nbr pts'])
    if dico['Frequency sweep']['acquisition']['nbr seqs'] == 1:
        return dico
    else:
        temp = dico['Data']['FREQ']
        i=0
        while i < int(dico['Frequency sweep']['acquisition']['nbr seqs'])-1:
            i+=1
            dico['Data']['FREQ'] = np.append(dico['Data']['FREQ'],temp[::(-1)**i])
        return dico

def find_nearest_index(array, a0):
    '''
    Return index of element in array closest to the scalar value a0

    Parameters
    ----------
    array : list
        A list of number
    a0 : flot
        The targeted value

    Return
    ------
    idx : integer
        The index of the closest value of a0
    '''
    array = np.asarray(array)
    idx = np.abs(array - a0).argmin()
    return idx

def find_nearest(array, a0):
    '''
    Return the element in an array closest to the scalar value a0

    Parameters
    ----------
    array : list
        A list of number
    a0 : flot
        The targeted value

    Return
    ------
    x : float
        The index of the closest value of a0
    '''
    x = min(array, key=lambda x:abs(x-a0))
    return x   

def reduce_data_range(X,Y,X_min,X_max):
    '''
    Allow to reduce the X range and change in consequence Y

    Parameters
    ----------
    X_min : float
        The new lower X value
    X_max : float
        The new higher X value

    Return
    ------
    Xc, Yc : array
        cutted version of X and Y
    '''
    if X_min < min(X):
        idx_min = 0
        # raise ImportError('the new minimum frequency is too small')
    else:    
        idx_min = find_nearest_index(X, X_min)
    if X_max > max(X):
        idx_max = len(X)-1
        # raise ImportError('the new maximum frequency is too big')
    else:    
        idx_max = find_nearest_index(X, X_max)
    # print('reduce_data_range',idx_min,idx_max)
    Xc = X[idx_min:idx_max]
    Yc = Y[idx_min:idx_max]
    return Xc, Yc

def ressources_decorator(orig_func):
    import time
    def wrapper(*args, **kwargs):
        t1 = time.time()
        mem_used_before = psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2    
        result = orig_func(*args, **kwargs)
        t2 = time.time() - t1
        mem_used_after = psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2
        print('# Function {} ran in: {:.0f}min {:.3f}sec'.format(orig_func.__name__, t2//60, t2%60))
        print('# Total memory used before process {:.3f} MB'.format(mem_used_before))
        print('# Total memory used after process {:.3f} MB'.format(mem_used_after))
        return result
    return wrapper

##############################################################################
##### LORENTZIAN 
##############################################################################
def capcitance(freq, C0):
    '''
    Capacitor admittance
    
    Prarameters
    -----------
    freq : Pulsation of the excitation
    C0 : capacitance    

    Return
    ------
    Return the complex value of the Lorentzian at the frequency freq       

    '''
    C = C0*2*np.pi*freq
    return C

def lorentzian(freq, amp, freq0, Q, C0):
    '''
    Lorentzina function use to fit the graph
    
    Prarameters
    -----------
    freq : Pulsation of the excitation
    amp : Amplitude = Force/m_eff/2Pi     
    freq0 : Resonance frequency
    Q : quality factor  
    C0 : resonator capacitance when it's not moving  

    Return
    ------
    Return the complex value of the Lorentzian at the frequency freq       

    '''
    # complex lorentzian function
    # Lorentzian expression from Aoust's thesis Amp = F/Meff
    L = amp/((freq0**2-freq**2)+1J*freq0*freq/Q)+C0*freq*2*np.pi
     # /!\ +Capa*freq*2*np.pi et non pas +1J*Capa*freq*2*np.pi depahsage entre la partie elec et meca demonstraiot navec le modele entiere elec (cf these Roman)
    return L

def lorentzianMAG(freq, amp, freq0, Q, C0):
    L = lorentzian(freq, amp, freq0, Q, C0)
    R = np.abs(L)
    return R

def lorentzianPHASE(freq, amp, freq0, Q, C0, Cste):
    L = lorentzian(freq, amp, freq0, Q, C0)
    Phi = np.angle(L,deg=True)+Cste
    return Phi

##############################################################################
##### GRAPH
##############################################################################

def load_mpl_params():
    # MATPLOTLIB PARAMETERS
    mpl.rcParams.update(mpl.rcParamsDefault)
    # mpl.rcParamsDefault.keys --> display all parameters available
    params = {'legend.fontsize': 10, #ATTENTION usually 16
            'figure.figsize': (10, 8),
            'axes.labelsize': 18,
            'axes.titlesize': 18,
            'xtick.labelsize': 14,#ATTENTION usually 18
            'ytick.labelsize': 14,#ATTENTION usually 18
            'figure.subplot.left': 0.15,
            'figure.subplot.right': 0.85,
            'figure.subplot.bottom': 0.15,
            'figure.subplot.top': 0.85,
            'xtick.direction': 'in',
            'ytick.direction': 'in',
            'xtick.major.size': 5,
            'xtick.major.width': 1.3,
            'xtick.minor.size': 3,
            'xtick.minor.width': 1,
            'xtick.major.pad': 8,
            'ytick.major.pad': 8,
            'lines.linewidth': 1,
            'axes.grid': True,
            'axes.grid.axis': 'both',
            'axes.grid.which': 'both',
            'grid.alpha': 0.5,
            'grid.color': '111111',
            'grid.linestyle': '--',
            'grid.linewidth': 0.8,
            'savefig.dpi': 300, }
    mpl.rcParams.update(params)  

##############################################################################
##### SAVE
##############################################################################
# def save_FS(param,courbes,courbes2):
#     # save function deidcated to Frequency Sweep 
#     today = time.strftime("%Y%m%d")
#     name = param.dico['Save']['sample name']
#     path_info = param.dico['Save']['path info']
#     sample_info = param.dico['Save']['sample info']
#     path = param.dico['Save']['path'] + '_'.join([today,name,path_info]) + '/'
#     freq = '%.0f-%.0fkHz'%(param.dico['Acquisition']['freq min']/1e3,param.dico['Acquisition']['freq max']/1e3)   
#     file_name = path + '_'.join(['FS_'+name,sample_info,freq]) 

#     # if don't exist create the directory
#     if not os.path.exists(path):
#         os.makedirs(path)

#     # save csv file with all the data
#     data = pandas.DataFrame({'Frequency (Hz)': courbes.curves['curve acq']['data']['Freq'],
#                 'x (Vrms)': courbes.curves['curve acq']['data']['X'],
#                 'y (Vrms)': courbes.curves['curve acq']['data']['Y'],
#                 'r (Vrms)': courbes.curves['curve acq']['data']['R'],
#                 'phase (deg)': courbes.curves['curve acq']['data']['Phi']})
#     data.to_csv(file_name +'.csv', sep=';', encoding='utf-8') 

#     if len(courbes.curves['curve fit']['data']['Freq']) > 1:
#         data = pandas.DataFrame({'Frequency (Hz)': courbes.curves['curve fit']['data']['Freq'],
#                     'r (Vrms)': courbes.curves['curve fit']['data']['R']})
#         data.to_csv(file_name +'_FIT_MAG.csv', sep=';', encoding='utf-8')
#     if len(courbes2.curves['curve fit']['data']['Freq']) > 1:
#         data = pandas.DataFrame({'Frequency (Hz)': courbes2.curves['curve fit']['data']['Freq'],
#                     'phase (deg)': courbes2.curves['curve fit']['data']['Phi']})
#         data.to_csv(file_name +'_FIT_PHASE.csv', sep=';', encoding='utf-8')

#     # save a txt file with all the parameter
#     info = open(file_name+'.txt','w')
#     info.write(time.strftime("%Y-%m-%d")+' '+time.strftime("%H:%M:%S")+ '\n')
#     info.write('\n')   
#     for k in param.dico:
#         info.write(k+'\n')
#         info.write(param.dico[k].child_list())
#     info.close()

#     # save graph
#     save_graph_FS(file_name,courbes,courbes2) 

# def save_PA(param,courbes):
#     # save function deidcated to Photo-Acoustic
#     today = time.strftime("%Y%m%d")
#     name = param.dico['Save']['sample name']
#     path_info = param.dico['Save']['path info']
#     sample_info = param.dico['Save']['sample info']
#     path = param.dico['Save']['path'] + '_'.join([today,name,path_info]) + '/'
#     curr = '%.0f-%.0fmA'%(param.dico['Laser Driver']['current min'],param.dico['Laser Driver']['current max'])
#     temperature = '{:.0f}C'.format(param.dico['Laser Driver']['temperature'])
#     harmonic = '{:.0f}f'.format(param.dico['Input']['harmonic'])
#     amplitude = '{:.0f}mV'.format(param.dico['Generator']['amplitude']*1e3)  
#     frequency = '{:.0f}kHz'.format(param.dico['Generator']['frequency']/1e3)   
#     file_name = path + '_'.join(['PA_'+name,sample_info,harmonic,amplitude,temperature,curr,frequency]) 

#     # if don't exist create the directory
#     if not os.path.exists(path):
#         os.makedirs(path)

#     # save csv file with all the data
#     data = pandas.DataFrame({'current (mA)': courbes.curves['curve acq']['data']['Current'],
#                 'x (Vrms)': courbes.curves['curve acq']['data']['X'],
#                 'y (Vrms)': courbes.curves['curve acq']['data']['Y'],
#                 'r (Vrms)': courbes.curves['curve acq']['data']['R'],
#                 'phase (deg)': courbes.curves['curve acq']['data']['Phi']})
#     data.to_csv(file_name +'.csv', sep=';', encoding='utf-8') 

#     # save a txt file with all the parameter
#     info = open(file_name+'.txt','w')
#     info.write(time.strftime("%Y-%m-%d")+' '+time.strftime("%H:%M:%S")+ '\n')
#     info.write('\n')   
#     for k in param.dico:
#         info.write(k+'\n')
#         info.write(param.dico[k].child_list())
#     info.close()
#     # save graph
#     save_graph_PA(file_name,courbes)

# def save_AOM(param,courbes):
#     # save function deidcated to Amplitude Of Modulation
#     today = time.strftime("%Y%m%d")
#     name = param.dico['Save']['sample name']
#     path_info = param.dico['Save']['path info']
#     sample_info = param.dico['Save']['sample info']
#     path = param.dico['Save']['path'] + '_'.join([today,name,path_info]) + '/'
#     amp = '%.0f-%.0fmVpk'%(param.dico['Generator']['amplitude min']*1e3,param.dico['Generator']['amplitude max']*1e3)   
#     file_name = path + '_'.join(['AOM_'+name,sample_info,amp]) 

#     # if don't exist create the directory
#     if not os.path.exists(path):
#         os.makedirs(path)

#     # save csv file with all the data
#     data = pandas.DataFrame({'amplitude of modualtion (Vpk)': courbes.curves['curve acq']['data']['Amplitude'],
#                 'x (Vrms)': courbes.curves['curve acq']['data']['X'],
#                 'y (Vrms)': courbes.curves['curve acq']['data']['Y'],
#                 'r (Vrms)': courbes.curves['curve acq']['data']['R'],
#                 'phase (deg)': courbes.curves['curve acq']['data']['Phi']})
#     data.to_csv(file_name +'.csv', sep=';', encoding='utf-8') 

#     # save a txt file with all the parameter
#     info = open(file_name+'.txt','w')
#     info.write(time.strftime("%Y-%m-%d")+' '+time.strftime("%H:%M:%S")+ '\n')
#     info.write('\n')   
#     for k in param.dico:
#         info.write(k+'\n')
#         info.write(param.dico[k].child_list())
#     info.close()
#     # save graph
#     save_graph_AOM(file_name,courbes)

# def save_AD(param,courbes):
#     # save function deidcated to Allan Deviation
#     today = time.strftime("%Y%m%d")
#     name = param.dico['Save']['sample name']
#     path_info = param.dico['Save']['path info']
#     sample_info = param.dico['Save']['sample info']
#     path = param.dico['Save']['path'] + '_'.join([today,name,path_info]) + '/'
#     acq_time = '%.0fs'%(param.dico['Allan Deviation']['acq. time'])
#     concentration = '%.0fppmv'%(param.dico['Allan Deviation']['concentration'])
#     time_cst = '%.0fms'%(param.dico['Input']['time constant']*param.dico['Allan Deviation']['waiting time']*1e3)
#     temperature = '{:.0f}C'.format(param.dico['Laser Driver']['temperature'])
#     harmonic = '{:.0f}f'.format(param.dico['Input']['harmonic'])
#     amplitude = '{:.0f}mV'.format(param.dico['Generator']['amplitude']*1e3)  
#     frequency = '{:.0f}kHz'.format(param.dico['Generator']['frequency']/1e3) 
#     current = '{:.0f}mA'.format(param.dico['Laser Driver']['current'])     
#     file_name = path + '_'.join(['AD_'+name,sample_info,harmonic,acq_time,concentration,time_cst,current,amplitude,frequency,temperature]) 

#     # if don't exist create the directory
#     if not os.path.exists(path):
#         os.makedirs(path)

#     # save csv file with all the data
#     data_1 = pandas.DataFrame({'time (s)': courbes.curves['curve acq']['data']['Time'],
#                 'x (Vrms)': courbes.curves['curve acq']['data']['X'],
#                 'y (Vrms)': courbes.curves['curve acq']['data']['Y'],
#                 'r (Vrms)': courbes.curves['curve acq']['data']['R'],
#                 'phase (deg)': courbes.curves['curve acq']['data']['Phi'],
#                 })
#     data_1.to_csv(file_name +'_MAG.csv', sep=';', encoding='utf-8') 
    
#     data_2 = pandas.DataFrame({'Tau-adev (s)': courbes.curves['curve acq']['data']['Tau-adev'],
#                 'Deviation-adev (Vrms)': courbes.curves['curve acq']['data']['Deviation-adev'],
#                 'Concetration-adev (ppmv)': courbes.curves['curve acq']['data']['Concentration-adev']})
#     data_2.to_csv(file_name +'_ADEV.csv', sep=';', encoding='utf-8') 

#     data_3 = pandas.DataFrame({'Tau-oadev (s)': courbes.curves['curve acq']['data']['Tau-oadev'],
#                 'Deviation-oadev (Vrms)': courbes.curves['curve acq']['data']['Deviation-oadev'],
#                 'Concetration-oadev (ppmv)': courbes.curves['curve acq']['data']['Concentration-oadev'],
#                 })
#     data_3.to_csv(file_name +'_OADEV.csv', sep=';', encoding='utf-8') 

#     # save a txt file with all the parameter
#     info = open(file_name+'.txt','w')
#     info.write(time.strftime("%Y-%m-%d")+' '+time.strftime("%H:%M:%S")+ '\n')
#     info.write('\n')   
#     for k in param.dico:
#         info.write(k+'\n')
#         info.write(param.dico[k].child_list())
#     info.close()
#     # save graph
#     save_graph_AD(file_name,courbes)

# def save_PV2I(param,courbes):
#     # save function deidcated to Frequency Sweep 
#     today = time.strftime("%Y%m%d")
#     name = param.dico['Save']['sample name']
#     path_info = param.dico['Save']['path info']
#     sample_info = param.dico['Save']['sample info']
#     path = param.dico['Save']['path'] + '_'.join([today,name,path_info]) + '/'
#     curr = '%.0f-%.0fmA'%(param.dico['Laser Driver']['current min'],param.dico['Laser Driver']['current max'])
#     temp = 'T{:.0f}C'.format(param.dico['Laser Driver']['temperature'])   
#     file_name = path + '_'.join(['PV2I_'+name,sample_info,curr,temp]) 

#     # if don't exist create the directory
#     if not os.path.exists(path):
#         os.makedirs(path)

#     # save csv file with all the data
#     data = pandas.DataFrame({'Current (mA)': courbes.curves['curve acq']['data']['Current'],
#                 'voltage (V)': courbes.curves['curve acq']['data']['Voltage'],
#                 'power (mW)': courbes.curves['curve acq']['data']['Power']
#                 })
#     data.to_csv(file_name +'.csv', sep=';', encoding='utf-8') 

#     # save a txt file with all the parameter
#     info = open(file_name+'.txt','w')
#     info.write(time.strftime("%Y-%m-%d")+' '+time.strftime("%H:%M:%S")+ '\n')
#     info.write('\n')   
#     for k in param.dico:
#         info.write(k+'\n')
#         info.write(param.dico[k].child_list())
#     info.close()
#     # save graph
#     save_graph_PV2I(file_name,temp,courbes)

# ## Conversion of the current value in wavelenght (nm) for laser Eblana 2
# def convert_wavelenght(current, temperature):
#     if temperature == 15:
#         return 1651.765+0.0145*current
#     elif temperature == 20:
#         return 1652.035+0.0155*current
#     elif temperature == 25:
#         return 1652.4075+0.01575*current
#     else:
#         return 1652.035+0.0155*current

##############################################################################
##### SAVE GRAPH MATPLOTLIB
############################################################################## 
# def load_mpl_params():
#     # MATPLOTLIB PARAMETERS
#     mpl.rcParams.update(mpl.rcParamsDefault)
#     # mpl.rcParamsDefault.keys --> display all parameters available
#     params = {'legend.fontsize': 12, #ATTENTION usually 16
#               'figure.figsize': (10, 8),
#               'axes.labelsize': 18,
#               'axes.titlesize': 18,
#               'xtick.labelsize': 14,#ATTENTION usually 18
#               'ytick.labelsize': 14,#ATTENTION usually 18
#               'figure.subplot.left': 0.15,
#               'figure.subplot.right': 0.85,
#               'figure.subplot.bottom': 0.15,
#               'figure.subplot.top': 0.85,
#               'xtick.direction': 'in',
#               'ytick.direction': 'in',
#               'xtick.major.size': 5,
#               'xtick.major.width': 1.3,
#               'xtick.minor.size': 3,
#               'xtick.minor.width': 1,
#               'xtick.major.pad': 8,
#               'ytick.major.pad': 8,
#               'lines.linewidth': 1,
#               'axes.grid': True,
#               'axes.grid.axis': 'both',
#               'axes.grid.which': 'both',
#               'grid.alpha': 0.5,
#               'grid.color': '111111',
#               'grid.linestyle': '--',
#               'grid.linewidth': 0.8,
#               'savefig.dpi': 300, }
#     mpl.rcParams.update(params)

# # For Frequency Sweep
# def save_graph_FS(name,courbes,courbes2):
#     # data
#     Freq = courbes.curves['curve acq']['data']['Freq']
#     R = courbes.curves['curve acq']['data']['R']
#     Phi = courbes.curves['curve acq']['data']['Phi']
#     Freq_fit = courbes.curves['curve fit']['data']['Freq']
#     R_fit = courbes.curves['curve fit']['data']['R']
#     Phi_fit = courbes2.curves['curve fit']['data']['Phi']

#     # color and graph param
#     load_mpl_params()
#     number_color = 20
#     cmap = plt.get_cmap('tab20')
#     colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

#     # magnitude
#     gridsize = (1, 1)
#     fig, _ = plt.subplots()
#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(Freq*1e-3, R, color=colors[0],
#       linewidth=2,label=r'data',marker='.')
#     if len(R_fit) > 1:
#         Q_mag = courbes.curves['curve fit']['data']['Q']
#         f0_mag = courbes.curves['curve fit']['data']['f0']
#         C0_mag = courbes.curves['curve fit']['data']['C0']
#         ax1.plot(Freq_fit*1e-3, R_fit, color=colors[1],
#           linewidth=1,label='Q={:.0f} f0={:.3f}kHz \n C0={:.2g}pF'.format(Q_mag,f0_mag/1e3,C0_mag*1e12))
#     ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
#     ax1.set_xlabel('frequency [kHz]', labelpad=4)
#     ax1.legend(loc=0)
#     fig.savefig(name+'_MAG.png')
#     plt.close(fig)

#     # phase
#     fig, _ = plt.subplots()
#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(Freq*1e-3, Phi, color=colors[0],
#       linewidth=2,label=r'data',marker='.')
#     if len(Phi_fit) > 1:
#         Q_phase = courbes2.curves['curve fit']['data']['Q']
#         f0_phase = courbes2.curves['curve fit']['data']['f0']
#         C0_phase = courbes2.curves['curve fit']['data']['C0']
#         ax1.plot(Freq_fit*1e-3, Phi_fit, color=colors[1],
#           linewidth=1,label='Q={:.0f} f0={:.3f}kHz \n C0={:.2g}pF'.format(Q_phase,f0_phase/1e3,C0_phase*1e12))
#     ax1.set_ylabel('phase [degrees]', labelpad=4)
#     ax1.set_xlabel('frequency [kHz]', labelpad=4)
#     ax1.legend(loc=0)
#     fig.savefig(name+'_PHASE.png')
#     plt.close(fig)

# # For laser PV2I
# def save_graph_PV2I(name,temperature,courbes):
#     # data
#     I = courbes.curves['curve acq']['data']['Current']
#     V = courbes.curves['curve acq']['data']['Voltage']
#     P = courbes.curves['curve acq']['data']['Power']

#     # color and graph param
#     load_mpl_params()
#     number_color = 20
#     cmap = plt.get_cmap('tab20')
#     colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

#     # graph
#     gridsize = (1, 1)
#     fig, _ = plt.subplots()

#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(I, V, color=colors[0],
#       linewidth=2,marker='.')
#     ax1.set_ylabel('voltage [V]', labelpad=4, color=colors[0])
#     ax1.tick_params(axis='y', labelcolor=colors[0])
#     #ax1.legend(loc=1)
#     ax1.set_xlabel('current [mA]', labelpad=4)
#     ax1.set_title(temperature.replace('T','Temp=').replace('C','°C'))
#     ax2 = ax1.twinx()
#     ax2.plot(I, P, color=colors[6],
#       linewidth=2,marker='.')
#     ax2.set_ylabel('power [mW]', labelpad=0, color=colors[6])
#     #ax2.legend(loc=3)
#     ax2.tick_params(axis='y', labelcolor=colors[6])
#     ax2.grid(b=False)

#     fig.savefig(name+'.png')
#     plt.close(fig)


# For PhotoAcoustic 
# def save_graph_PA(name,courbes):
#     # data
#     I = courbes.curves['curve acq']['data']['Current']
#     R = courbes.curves['curve acq']['data']['R']
#     Phi = courbes.curves['curve acq']['data']['Phi']

#     # color and graph param
#     load_mpl_params()
#     number_color = 20
#     cmap = plt.get_cmap('tab20')
#     colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

#     # magnitude
#     gridsize = (1, 1)
#     fig, _ = plt.subplots()
#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(I, R, color=colors[0],
#       linewidth=2,marker='.')
#     ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
#     ax1.set_xlabel('current [mA]', labelpad=4)
#     #ax1.legend(loc=0)
#     fig.savefig(name+'_MAG.png')
#     plt.close(fig)

#     # phase
#     fig, _ = plt.subplots()
#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(I, Phi, color=colors[0],
#       linewidth=2,marker='.')
#     ax1.set_ylabel('phase [degrees]', labelpad=4)
#     ax1.set_xlabel('current [mA]', labelpad=4)
#     #ax1.legend(loc=0)
#     fig.savefig(name+'_PHASE.png')
#     plt.close(fig)

# For Amplitude of modulation
# def save_graph_AOM(name,courbes):
#     # data
#     AOM = courbes.curves['curve acq']['data']['Amplitude']*1e3
#     R = courbes.curves['curve acq']['data']['R']
#     Amp_at_Rmax = courbes.curves['curve acq']['data']['Amplitude for R max']

#     # color and graph param
#     load_mpl_params()
#     number_color = 20
#     cmap = plt.get_cmap('tab20')
#     colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

#     # magnitude
#     gridsize = (1, 1)
#     fig, _ = plt.subplots()
#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(AOM, R, color=colors[0],linewidth=2,marker='.',label='Max for x={:.3f}mV'.format(Amp_at_Rmax*1e3))
#     ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
#     ax1.set_xlabel('amplitude of modulation [mVpk]', labelpad=4)
#     ax1.legend(loc=0)
#     fig.savefig(name+'_MAG.png')
#     plt.close(fig)

# # For Allan Deviation
# def save_graph_AD(name,courbes):
#     R = courbes.curves['curve acq']['data']['R']
#     Time = courbes.curves['curve acq']['data']['Time']
#     Mean = courbes.curves['curve acq']['data']['Mean']
#     Signal_to_noise = courbes.curves['curve acq']['data']['SNR']
#     Legend = 'Mean={:.3f}mVrms SNR={:.1f}'.format(Mean*1e3,Signal_to_noise)
#     Tadev = courbes.curves['curve acq']['data']['Tau-adev']
#     Cadev = courbes.curves['curve acq']['data']['Concentration-adev']
#     Toadev = courbes.curves['curve acq']['data']['Tau-oadev']
#     Coadev = courbes.curves['curve acq']['data']['Concentration-oadev']

#     # color and graph param
#     load_mpl_params()
#     number_color = 20
#     cmap = plt.get_cmap('tab20b')
#     colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

#     # magnitude
#     gridsize = (1, 1)
#     fig, _ = plt.subplots()
#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(Time, R, color=colors[0],linewidth=2,marker='.',label=Legend)
#     ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
#     ax1.set_xlabel('time [s]', labelpad=4)
#     ax1.legend(loc=0)
#     fig.savefig(name+'_TIME.png')
#     plt.close(fig)

#     #allandeviation
#     fig, _ = plt.subplots()
#     ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
#     ax1.plot(Tadev, Cadev, color=colors[0],linewidth=2,marker='.',label='Allan Deviation')
#     ax1.plot(Toadev, Coadev, color=colors[2],linewidth=2,marker='.',label='Overlapping Allan Deviation')

#     ax1.set_ylabel('concentration [ppmv]', labelpad=4)
#     ax1.set_xlabel('time constant [s]', labelpad=4)
#     ax1.set_xscale('log')
#     ax1.set_yscale('log')
#     ax1.legend(loc=0)
#     # find and plot the LOD @ 1s
#     f = interpolate.interp1d(Toadev, Coadev)
#     C1s = f(1)
#     print('LOD at 1s is {:.3f} ppmv'.format(C1s))
#     ax1.scatter(1, C1s, color='r', alpha=0.5)
#     ax1.text(1, C1s, 'LOD @ 1s {:.0f}ppmv'.format(C1s), rotation=15)
#     # find and plot the LOD @ 1s
#     if max(Toadev) > 10:
#         C10s = f(10)
#         print('LOD at 10s is {:.3f} ppmv'.format(C10s))
#         ax1.scatter(10, C10s, color='r', alpha=0.5)
#         ax1.text(10, C10s, 'LOD @ 10s {:.0f}ppmv'.format(C10s), rotation=15)
#     # find and plot the lowest LOD
#     x = np.linspace(min(Toadev),max(Toadev),1e4)
#     Con_LOD = min(f(x))
#     idx = find_nearest_index(f(x),Con_LOD)
#     Tau_LOD = x[idx]
#     print('LOD at {:.3f}s is {:.3f} ppmv'.format(Tau_LOD,Con_LOD))
#     ax1.scatter(Tau_LOD, Con_LOD, color='r', alpha=0.5)
#     ax1.text(Tau_LOD, Con_LOD, 'LOD @ {:.0f}s {:.0f}ppmv'.format(Tau_LOD,Con_LOD), rotation=15)
#     fig.savefig(name+'_ALLAN.png')
#     plt.close(fig)
