import time
from pathlib import Path


class Medisoft:
    def __init__(self):
        self.connect()
        self.path=self.connect()
        self.lines=[]
        self.timestamp=0
        self.index=0
        self.flow=0
        self.CO2=0
        
        self.R1 = 0
        self.R2 = 0
        self.R3 = 0
        self.R4 = 0
        
        self.file=open(self.path, 'r')
        
        self.file.seek(0, 2)
        self.flag=True
        

        
    def connect(self):
        # Get all .txt files in the folder
        txt_files = Path("C:\Expair\Logs\\").glob('*.txt')

        # Find the last modified .txt file
        loading_file= max(txt_files, key=lambda f: f.stat().st_mtime, default=None)
        # self.path=nini
        print("expair_path:",loading_file)
        # self.file = open(self.path, 'r')
        
        
        # self.path=loading_file
        # self.file=open(self.path, 'r')
        
        # self.file.seek(0, 2)
        # self.flag=True
        
        return loading_file
    

    def set_value(self,what,value): 
        self.connect()
        self.path=self.connect()
        self.lines=[]
        self.timestamp=0
        self.index=0
        self.volume=0
        self.flow=0
        self.CO2=0
        
        self.R1 = 0
        self.R2 = 0
        self.R3 = 0
        self.R4 = 0
        
        self.file=open(self.path, 'r')
        
        self.file.seek(0, 2)
        self.flag=True
        
        # pass
        # Open the file in read mode
         
    # def open_new_medisoft(self): 
    #     # Get all .txt files in the folder
    #     txt_files = Path("C:\Expair\Logs\\").glob('*.txt')

    #     # Find the last modified .txt file
    #     loading_file= max(txt_files, key=lambda f: f.stat().st_mtime, default=None)
    #     # self.path=nini
    #     # print(loading_file)
    #     # self.file = open(self.path, 'r')
    #     self.path=loading_file
        
    #     print("rrrrrrrrrrrrrrrconnected from file expaiiiiiiire")
    
    #     self.file=open(self.path, 'r')
        
    #     self.file.seek(0, 2)
        
        
    def receive(self):

        self.lines = self.file.readlines()
         # Check if there are new lines in the file
        try:      
            if self.lines:
                
                 
                 # Get the last line
                 # new_content = self.lines
                     
                 new_content = self.lines[-1]
         
                 # Split the line using semicolons
                 data = new_content.split(';')
         
                 # Extract the relevant values
                 timestamp_parts = data[1].strip().split('   ')
                 
                 self.timestamp = timestamp_parts[0]
                 
                 check_index_value=timestamp_parts[1] 
                 # self.index = timestamp_parts[1] 
                 # if len(timestamp_parts) > 1 else ""
         
                 self.flow = float(data[2].replace(',', '.').strip())
                 self.volume = float(data[3].replace(',', '.').strip())*-1
                 self.CO2 = float(data[4].replace(',', '.').strip())
                 
                 self.R1 = float(data[8].replace(',', '.').strip())
                 self.R2 = float(data[9].replace(',', '.').strip())
                 self.R3 = float(data[10].replace(',', '.').strip())
                 self.R4 = float(data[11].replace(',', '.').strip())
                 
                 
                 if self.flag==True:
                     self.index=check_index_value
                     self.flag=False
                     return self.timestamp, self.index, self.flow,self.volume,self.CO2,self.R1,self.R2,self.R3,self.R4
                
                 if check_index_value==self.index:
                     return None
                 else:
                     self.index=check_index_value
                     return self.timestamp, self.index, self.flow,self.volume,self.CO2,self.R1,self.R2,self.R3,self.R4
         
    
        
                # Return None if no new content is found
            return None
        
        
        
        except ValueError:
                 pass
    
    
    def disconnect(self):
        if self.file:
            self.file.close()
            print("Disconnected from file.")
        return 
   



if __name__ == "__main__":
    
    medi=Medisoft()
    print(medi.connect())
    # R=[]
    cnt=0
    while cnt==6:
        start_time = time.time()
        data=medi.receive()
        cnt=+1
        # time.sleep(0.1)
        
        if data is not None:
            # l, timestamp_value, index_value, flow_value, CO2_value = data
            timestamp_value, index_value, flow_value,volume, CO2_value = data
    
            # Use the extracted values as needed
            # print(f"l:{l}")
            print("Timestamp:", timestamp_value)
            print("Index:", index_value)
            print("Flow:", flow_value)
            print("CO2:", CO2_value)
            time.sleep(0.00001)
            elapsed_time = time.time() - start_time
            print(elapsed_time)
        
        
    medi.disconnect()
        
#         # Add a delay to avoid excessive file reads
#         time.sleep(1)
#         # R=medi.receive()
#         # print("R=",R[0],R[1],R[2],R[3])
        
        
   




"""
import csv
from io import StringIO


Test date;2023/03/31
Test time;15:48:25
Patient age (years);26.25
Patient height (cm);175
Patient weight (kg);70
Temperature (°C);22.1
Humidity (%);27.5
Barometric pressure (mmHg);771.2
Insp. BTPS;1.111
Exp. BTPS;1.038
Span O2 (mV/%);0.0
Span CO2 (mV/%);422.9
Response time O2 (ms);100
Response time CO2 (ms);160
Transport time O2 (ms);1940
Transport time CO2 (ms);440
;
Time (s);Index;Flow (ATP l/s);Volume (BTPS l);CO2 (%);O2 (%);Load (W);Step;Analog1;Analog2;CO;Analog4;
Data;15:49:54.930   1;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.931   2;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.940   3;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.992   4;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.992   5;0.000;0.000;0.000;20.000;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:54.999   6;0.000;0.000;0.000;20.000;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:55.005   7;0.000;0.000;0.000;20.000;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:55.013   8;0.000;0.000;0.000;80.606;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:55.019   9;0.000;0.000;0.000;80.606;0;Rest;0.083;0.029;0.073;0.000;
Data;15:49:55.054   10;0.000;0.000;0.000;80.606;0;Rest;0.083;0.029;0.073;0.000;

# Use StringIO to simulate reading from a file
csv_data = StringIO(data)

# Skip the header lines
for _ in range(19):
    next(csv_data)

# Read the CSV data
reader = csv.DictReader(csv_data, delimiter=';')
for row in reader:
    # Extract Flow (ATP l/s) and CO2 (%) values
    flow_atp = float(row['Flow (ATP l/s)'])
    co2_percent = float(row['CO2 (%)'])
    print(f"Flow (ATP l/s): {flow_atp}, CO2 (%): {co2_percent}")
"""