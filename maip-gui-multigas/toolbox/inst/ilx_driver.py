# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 10:06:00 2024

@author: user1
"""

import pyvisa

class Ilx_driver:
    def __init__(self, address="GPIB::1::INSTR"):
        self.rm = pyvisa.ResourceManager('C:/Windows/System32/visa32.dll')
        self.address = address
        self.connect()

    def connect(self):
        self.device = self.rm.open_resource(self.address)
        print(f"Connected to ILX Current Driver at {self.address}")

    # def set_current(self, current):
    #     self.device.write(f"LAS:LDI {current}")

    # def enable_output(self, enable=True):
    #     self.device.write(f"LAS:OUT {int(enable)}")

    def query_voltage(self):
        return float(self.device.query("LAS:LDV?"))

    def close(self):
        self.device.close()   
        print(f"Disconnected from ILX Current Driver at {self.address}")

    def poll(self,poll_length,x=None):
        current = float(self.device.query("LAS:LDI?"))
        t=0
        mod_freq=0
        voltage = float(self.device.query("LAS:LDV?"))
        # return current, voltage
        return [current,t,mod_freq,voltage]

    def set_value(self, what, value):
        if what == "current":
            
            self.device.write(f"LAS:LDI {value}")
        # elif what == "output":
        #     # self.enable_output(value)
            
        if 'on' in what:
            if value == True:
                self.device.write("LAS:OUT 1")             
            if value == False:
                self.device.write("LAS:OUT 0")