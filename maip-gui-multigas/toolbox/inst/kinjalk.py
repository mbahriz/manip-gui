# http://www.codestudyblog.com/cnb/0411121157.html

import serial
import sys
path = '/Users/michaelbahriz/Recherche/Manips/Codes/manip-gui'
sys.path.append(path) 
import toolbox.parameter_tree

##############################################################################
##### Toolbox function 
##############################################################################
clock_pico = 125e6 # in Hz
one_cycle = 1/clock_pico # in second

def give_clock_cycle():
    return one_cycle

def give_real_frequency(x,y):
    S0, S1 = logic_state(x,y)
    period = (S0 + S1)*one_cycle
    return 1/period

def logic_state(x, y):
    # return length in number of clock cycle of the true and false state
    true_state = 3*x-1
    false_state = 3*(y-x)+4
    return true_state, false_state

def XnY_modulator_frequency(frequency, duty=0.5):
    # the lowest frequency
    if frequency == 0:
        x, x_err = 443328, 0
        y, y_err = x, x_err
        x_round, y_round = x, y
    else:
        pulse = 1/frequency
        x = (pulse*duty/one_cycle+1)/3
        x_round = round(x)
        x_err = abs(x-x_round)
        y = (3*x-1)/3/duty-1
        y_round = round(y)
        y_err = abs(y-y_round)
    return [x, x_round, x_err], [y, y_round, y_err]     

def XnY_carrier_frequency(pulse_width, duty_cycle):
    # the highest frequency
    x = (pulse_width*1e-9/one_cycle+1)/3
    y = (3*x-1)/3/duty_cycle-1
    y_round = round(y)
    y_err = abs(y-y_round)
    return [x, x, 0], [y, y_round, y_err]  

def serial_ports_list():
    import serial.tools.list_ports
    port_list = list(serial.tools.list_ports.comports())
    print(port_list)
    if len(port_list) == 0:
        print(' no available serial port ')
    else:
        for i in range(0,len(port_list)):
            print(port_list[i])
    return port_list

def serial_ports_list_bis():
    import sys
    import glob
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    print(result)
    return result

##############################################################################
##### Instrument Class
##############################################################################
class Kinjalk_pulse_generator():
    def __init__(self):
        self.daq = None
        self.inst_init_values()
        self.connect()

    def connect(self):
        portx=self.find_pico_COM()
        bps=115200
        timex=None   # timeout setting, None： waiting for operation forever, 0 to return the result of the request immediately, the other values are waiting timeouts ( in seconds ）
        ser=serial.Serial(portx,bps,timeout=timex)
        self.daq=ser
        print('Kinjalk_pulse_generator is CONNECTED port={} baudrate={}'.format(ser.port,ser.baudrate))

    def disconnect(self):
        self.daq.close()# close the serial port 

    def find_pico_COM(self):
        port_list = serial_ports_list()
        for port, desc, hwid in sorted(port_list):
            print("{}: {} [{}]".format(port, desc, hwid))
            if 'SER=E6604430437B3E28' in hwid:
                port_COM = port
            else:
                port_COM = None
        return port_COM

    def inst_init_values(self):
        # should be init here as we need many parameters in the same time frequency and duty cycle
        dico = toolbox.parameter_tree.instrument_initials_values('Pulse module')
        self.pulse = dico['pulse width']
        self.freq = dico['frequency']
        self.carrier = dico['carrier frequency']
        self.duty = self.pulse*1e-9*self.carrier

    def poll(self,poll_length,x=None):
        pass

    def set_value(self,what,value):
        carrier_modified = False
        modulator_modified = False
        if 'carrier' in what: # in Hz
            self.carrier = value
            self.duty = self.pulse*1e-9*self.carrier
            carrier_modified = True
        if ('frequency' in what) and ('carrier' not in what):  #in Hz
            # should be at 0 Hz when initialize 
            self.freq = value
            modulator_modified = True
        if ('pulse' in what) and ('module' not in what): # in ns
            self.pulse = value
            self.duty = self.pulse*1e-9*self.carrier
            carrier_modified = True
        if carrier_modified:
            X_carrier, Y_carrier = XnY_carrier_frequency(self.pulse, self.duty)
            x = int(X_carrier[1])
            y = int(Y_carrier[1])
            print('Kinjalk CARRIER has CHANGED {} {} x={} y={}'.format(what,str(value),str(x),str(y)))
            self.write_serial('A'+str(x))
            self.write_serial('B'+str(y))
            carrier_modified = False
        if modulator_modified:
            X_modulator, Y_modulator = XnY_modulator_frequency(self.freq)
            x = int(X_modulator[1])
            y = int(Y_modulator[1])
            print('Kinjalk MODULATOR has CHANGED {} {} x={} y={}'.format(what,str(value),str(x),str(y)))
            self.write_serial('C'+str(x))
            self.write_serial('D'+str(y))
            modulator_modified = False

    def write_serial(self,message):
        result=self.daq.write(message.encode("gbk"))
        print(" write total bytes :",result)

if __name__ == "__main__":
    #Call of the classes
    mon_kinjalk = Kinjalk_pulse_generator()

    mon_kinjalk.set_value(what='pulse width',value=100e-9)
    mon_kinjalk.set_value(what='duty',value=0.01)
    mon_kinjalk.disconnect()


