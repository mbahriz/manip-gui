import pyvisa
import time
import numpy as np

class ThorlabsITC400xQCL():
    # laser Driver with USB connection
    def __init__(self,dev_num):
       
        self.dev = dev_num
 
        self.connect() 

    def connect(self):
        #identification de l'instrument
        rm = pyvisa.ResourceManager('C:/Windows/System32/visa32.dll')
        rm.list_resources()
        print(rm.list_resources())
        #print('****** Thorlabs Laser Driver : ',rm)
        
        
        if self.dev =='4005QCL_M00603635':
            self.daq = rm.open_resource('USB0::0x1313::0x804A::M00603635::INSTR')#4005_co

        if self.dev =='4005_M00440716':
            self.daq = rm.open_resource('USB0::0x1313::0x8049::M00440716::INSTR')#4005_No
        
        if self.dev =='4001QCL_M00468220':
            self.daq = rm.open_resource('USB0::0x1313::0x804A::M00468220::INSTR')#4001 DE lolo
            
        if self.dev =='4002QCL_M00907387':
            self.daq = rm.open_resource('USB0::0x1313::0x804A::M00907387::INSTR') #4002  (1) banc julien
          
        if self.dev =='4002QCL_M00560075':
            self.daq  = rm.open_resource('USB0::0x1313::0x804A::M00560075::INSTR')#4002  (2) banc fanny  actuellemnt au CHU
            
        if self.dev =='4002QCL_M00660255':
            self.daq  = rm.open_resource('USB0::0x1313::0x804A::M00660255::INSTR')#4002  (3) Alex isoprene 
                    
   
        
        # try:
    
        #     inst = rm.open_resource('USB0::0x1313::0x804A::M00603635::INSTR')#4005_co
        # except:   
        #     try:
        #         inst = rm.open_resource('USB0::0x1313::0x804A::M00560075::INSTR')#4002
                
        #     except:
        #         try:
        #             inst = rm.open_resource('USB0::0x1313::0x804A::M00660255::INSTR')
        #         except:
        #             try:
        #                 inst = rm.open_resource('USB0::0x1313::0x804A::M00468220::INSTR')#4001
        #             except:
        #                 inst = rm.open_resource('USB0::0x1313::0x804A::M00907387::INSTR') 
                
        # #print('Device Thorlabs-ITC4002QCL IDN is %s'%(self.IDN()))
        # return inst

    def IDN(self):
        time.sleep(0.1)
        self.daq.write("*CLS")
        return self.daq.query("*IDN?")

    def set_value(self,what,value):
        inst = self.daq
        if 'on' in what:
            if value == True:
                inst.write("OUTP ON")
            if value == False:
                inst.write("OUTP OFF")
        if 'current' in what:
            value = value/1000 #current given in mA
            inst.write("SOUR:CURR "+str(value))
            inst.write("SOUR:CURR "+str(value))
        if 'temperature' in what:
            if self.dev !='4005_NO':
                inst.write("SOUR2:TEMP "+str(value)+"C")
        if 'modulation' in what:
            if value == True:
                inst.write("SOUR:AM 1") #activate modulation of the laser
            if value == False:
                inst.write("SOUR:AM 0")#Desactivate the modulation
        if 'internal modulation (Square signal)' in what:
            if value == True:
                inst.write("SOUR:AM:SOUR INT") #set internat modulation
                inst.write("SOUR:AM:INT:SHAP SQU") #activate a square shape 
            if value == False:
                inst.write("SOUR:AM:SOUR EXT")
        if 'frequency modulation' in what:
            value = value*1000 #enter the frequency in kHz
            inst.write("SOUR:AM:INT:FREQ "+str(value)) #set the frequency modulation
        if 'amplitude modulation' in what:
            inst.write("SOUR:AM:INT "+str(value)) #/!\Value here is a pourcetage of the total DC current of the laser


    def poll(self,poll_length,x=None):
        inst = self.daq
        #x is only for the virtual instrument
        #time.sleep(0.1)
        a = float(inst.query("SOUR:CURR?"))*1000
        if self.dev !='4005_NO':
            t = float(inst.query("SOUR2:TEMP?"))
        else:
            t=None
        mod_freq = float(inst.query("SOUR:AM:INT:FREQ?")) #Mod frequency given in Hz
        v = float(inst.query("MEAS:VOLT?"))
        #Return the current in mA and the temperature in degC
        return [a,t,mod_freq,v]
    def disconnect(self):
        pass

class PowermeterThorlabsPM16401():
    def __init__(self):
        self.daq = None
        self.connect()

    def connect(self):
        rm = pyvisa.ResourceManager()
        rm.list_resources()
        self.daq = rm.open_resource('USB0::0x1313::0x807C::1909668::INSTR')
        
    def set_value(self,what,value):
        inst = self.daq
        if what in 'wavelength': 
            inst.write("SENS:CORR:WAV "+str(value))

    def poll(self,poll_length,x=None):
         return [float(self.daq.query("Read?")),np.nan,np.nan,np.nan]
     
        
if __name__ == "__main__":
    # Create an instance of the Arduino_GPIB class with the default resource address
    mon_PICO = ThorlabsITC400xQCL('4005_NO')
    mon_PICO.set_value('current',25)


