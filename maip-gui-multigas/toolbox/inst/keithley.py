import pyvisa
import numpy as np

class Keithley2010Multimeter():
    def __init__(self):
        self.daq = None
        self.connect()

    def connect(self):
        rm = pyvisa.ResourceManager()
        rm.list_resources()
        self.daq = rm.open_resource('GPIB0::16::INSTR')

    def set_value(self,what,value):
        pass
        #if what in "range":
        #    return value

    def poll(self,poll_length,x=None):
        return [float(self.daq.query(':DATA?')),np.nan,np.nan,np.nan]