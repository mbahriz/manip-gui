from pyfirmata import Arduino, util
import time
import serial.tools.list_ports
import numpy as np
import serial





import toolbox.RH as H2O
# import RH as H2O
##############################################################################
##### Instrument Class
##############################################################################
class Arduino_DHT():
    TERMINATOR = '\r'.encode('UTF8')  # '\r' is Carriage Return

    def __init__(self):
        self.ser = None
        # self.daq = None
        self.connect()
        self.humidity_value=0
        self.temperature_value=0
        self.water_conc_value=0
        
        self.flag=None

    def connect(self):
        # portx=self.find_arduino_port()
        portx='COM10'
        bps=9600
        timex=1   # timeout setting, None： waiting for operation forever, 0 to return the result of the request immediately, the other values are waiting timeouts ( in seconds ）
        self.ser=serial.Serial(portx,bps,timeout=timex)
        # self.daq=self.ser
        print('Arduino is CONNECTED port={} baudrate={}'.format(self.ser.port,self.ser.baudrate))
        time.sleep(2)


        # self.flag=True

    def disconnect(self):
        self.ser.close()
        self.ser= None
        # self.daq.close()# close the serial port 
        # self.daq = None
        print("Arduino connection closed.")
        

    def find_arduino_port(self):
        arduino_ports = [
            p.device
            for p in serial.tools.list_ports.comports()
            if 'Arduino' in p.description or 'VID:PID' in p.hwid
        ]
        return arduino_ports[0] if arduino_ports else None
        

    def inst_init_values(self):
       pass

    def poll(self,poll_length,x=None):
        pass

    

    def receive(self):
        # Rh=[]
        # T=[]
        
        for  a in range(0, 10, 1):#delay code 1ms ------lecture 10ligne fait 10ms-----lecture 50 ligne fait 5 ms
            self.data = self.ser.readline().decode('utf-8').rstrip()
            
        if self.data.startswith('H:') and ',' in self.data and 'T:' in self.data:
            humidity_str, temperature_str = self.data.split(',')
            self.humidity_value= float(humidity_str.split('H:')[1])
            self.temperature_value = float(temperature_str.split('T:')[1])
                
                # rh = float(humidity_str.split('H:')[1])
                # t = float(temperature_str.split('T:')[1])
                # Rh.append(rh)
                # T.append(t)
                
                # self.humidity_value=round(np.mean(Rh),2)
                # self.temperature_value=round(np.mean(T),2)
                
            CONC=H2O.Waterconc(self.humidity_value,self.temperature_value)
            self.water_conc_value=int(CONC.CH2O())
                
                
                
        return self.humidity_value,self.temperature_value,self.water_conc_value
        


        


    def set_value(self,what,value):
        pass
    
    
    


if __name__ == "__main__":
    # Call of the classes
    mon_PICO = Arduino_DHT()
    
    for a in range(0, 100, 1):
        # time.sleep(1)
    # while True:
        R=mon_PICO.receive()
    
        print(R[0],R[1],R[2])
        
    mon_PICO.disconnect()


'''
# CODE TO COPY ON THE PICO 

# main.py 

# as its name is main.py
# it will automaticaly run on the PICO when it is plugged

from machine import Pin
import utime

# POUR QUE CA MARCHE THONNY DOIT ETRE FERME
# ET AVOIR DEBRANCHE ET REBRANCHE LE PICO

# use onboard LED which is controlled by Pin 25
LED = machine.PWM(machine.Pin(25))
OUT = machine.PWM(machine.Pin(15))

# Turn the LED on
def pulse(frequency, duty_cycle): # feq in Hz duty in %
    convertion = 65535/100
    for gpio in [LED, OUT]:
        gpio.freq(int(frequency))
        gpio.duty_u16(int(duty_cycle*convertion))



'''

