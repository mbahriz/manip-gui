import time


class Medisoft:
    def _init_(self):
        self.connect()
        self.file=[]

        
    def connect(self):
        pass
    
    def set_value(self,what,file_path): 
        # Open the file in read mode
        
        
        if 'file_path' in what:
            with open(file_path, 'r') as self.file:
                # Move the file cursor to the end of the file
                self.file.seek(0, 2)
            

    def receive(self):
        
       line = self.file.readline()
       if line:

            # Split the line using semicolons
            data = line.split(';')
            
            # Extract the relevant values
            
            timestamp_parts = data[1].strip().split(' ')
            timestamp_part1 = timestamp_parts[0]
            # timestamp_part2 = timestamp_parts[1] if len(timestamp_parts) > 1 else ""
           
            flow = float(data[2].replace(',', '.').strip())
            CO2 = float(data[4].replace(',', '.').strip())
        
            return timestamp_part1,flow,CO2
    
    def disconnect(self):
        pass
   



if __name__ == "__main__":
    
    medi=Medisoft()
    medi.set_value('file_path',"C:\Expair\Logs\Logs_VO2_20231204_164316.txt")
    
    while True:
        R=medi.receive()
        print(R[0],R[1],R[2])
        time.sleep(1)
        
   




"""
import csv
from io import StringIO


Test date;2023/03/31
Test time;15:48:25
Patient age (years);26.25
Patient height (cm);175
Patient weight (kg);70
Temperature (°C);22.1
Humidity (%);27.5
Barometric pressure (mmHg);771.2
Insp. BTPS;1.111
Exp. BTPS;1.038
Span O2 (mV/%);0.0
Span CO2 (mV/%);422.9
Response time O2 (ms);100
Response time CO2 (ms);160
Transport time O2 (ms);1940
Transport time CO2 (ms);440
;
Time (s);Index;Flow (ATP l/s);Volume (BTPS l);CO2 (%);O2 (%);Load (W);Step;Analog1;Analog2;CO;Analog4;
Data;15:49:54.930   1;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.931   2;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.940   3;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.992   4;0.000;0.000;0.000;-40.606;0;Rest;0.083;0.034;0.073;0.000;
Data;15:49:54.992   5;0.000;0.000;0.000;20.000;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:54.999   6;0.000;0.000;0.000;20.000;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:55.005   7;0.000;0.000;0.000;20.000;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:55.013   8;0.000;0.000;0.000;80.606;0;Rest;0.083;0.031;0.073;0.000;
Data;15:49:55.019   9;0.000;0.000;0.000;80.606;0;Rest;0.083;0.029;0.073;0.000;
Data;15:49:55.054   10;0.000;0.000;0.000;80.606;0;Rest;0.083;0.029;0.073;0.000;

# Use StringIO to simulate reading from a file
csv_data = StringIO(data)

# Skip the header lines
for _ in range(19):
    next(csv_data)

# Read the CSV data
reader = csv.DictReader(csv_data, delimiter=';')
for row in reader:
    # Extract Flow (ATP l/s) and CO2 (%) values
    flow_atp = float(row['Flow (ATP l/s)'])
    co2_percent = float(row['CO2 (%)'])
    print(f"Flow (ATP l/s): {flow_atp}, CO2 (%): {co2_percent}")
"""