import numpy as np
import zhinst.ziPython
import time 
import matplotlib.pyplot as plt 

class ZurichMFLI():
    def __init__(self,dev_num):
        self.daq = None
        self.dev = dev_num
        self.connect()
        
        
    def connect(self):
        '''
        Use Device Discovery to find the ip adress and the port 
        of the device called dev
        '''
        d = zhinst.ziPython.ziDiscovery()
        d.find(self.dev)
        devProp = d.get(self.dev)
        print (devProp)
        print('Device Zurich-%s adress is %s'%(self.dev,devProp['serveraddress']))
        print('Device Zurich-%s port is %s'%(self.dev,devProp['serverport']))
        # Create a connection to the data server
        apilevel = 6 # allow timestamp support in poll (cf LabOneProgrammingManual page 16)
        #daq = data acquistion system adress
        self.daq = zhinst.ziPython.ziDAQServer(devProp['serveraddress'],devProp['serverport'], apilevel)
        # get ready for data acquisition
        self.poll_subscribe()


    def disconnect(self):
        pass
        # self.poll_unsubscribe()
        # self.daq.disconnect()
        # self.daq=None
        
    def poll_subscribe(self):
        # Subscribe to the demodulator's sample node path.
        # If not done the poll function return a empty data
        # Unsubscribe from all paths.
        # self.daq.unsubscribe('*')
        # demod_index_1 = 0
        # path1 = '/%s/demods/%d/sample' % (self.dev, demod_index_1)
        # self.daq.subscribe(path1)
        
        # demod_index_2 = 1
        # path2 = '/%s/demods/%d/sample' % (self.dev, demod_index_2)
        # self.daq.subscribe(path2)
        
        # demod_index_3 = 2
        # path3 = '/%s/demods/%d/sample' % (self.dev, demod_index_3)
        # self.daq.subscribe(path3)
        
        # demod_index_4 = 3
        # path4 = '/%s/demods/%d/sample' % (self.dev, demod_index_4)
        # self.daq.subscribe(path4)
        
        self.daq.unsubscribe('*')
        device = self.dev
        
        demod_index=[0,1,2,3]
        demod_path={}
        signal_paths = []
        
        for i in demod_index:
        # The list of signal paths that we would like to record in the module.
            demod_path[i] = '/{}/demods/{}/sample'.format(device,i)
            signal_paths.append(demod_path[i])
        self.daq.subscribe(signal_paths)
        
        
        
        # ### TEST PART
        # # Unsubscribe from all paths.
        # self.daq.unsubscribe('*')
        # # Asking poll to always return a value for a settings node
        # self.daq.getAsEvent('/dev4320/sigouts/0/amplitudes/3')
        # self.daq.subscribe('/dev4320/sigouts/0/amplitudes/3')

    def poll_unsubscribe(self):
        self.daq.unsubscribe('*')  

    def set_value(self,what,value):
        '''
        change value on zurich
        '''
        self.daq.setInt('/dev4199/demods/0/oscselect', 0)#attributue osc0 to channel 0
        self.daq.setInt('/dev4199/demods/0/adcselect', 0)#input channels (sigin1=0)
        

        
        self.daq.setInt('/dev4199/demods/1/oscselect', 1)#attributue osc1 to channel 1
        self.daq.setInt('/dev4199/demods/1/adcselect', 1)#input channels currin1=1

        
        self.daq.setInt('/dev4199/demods/2/oscselect', 2)#attributue osc2 to channel 2
        self.daq.setInt('/dev4199/demods/2/adcselect', 8)#input channel,Auxin1=8,Auxin2=9)
        
        self.daq.setInt('/dev4199/demods/3/oscselect', 3)#attributue osc3 to channel 3
        self.daq.setInt('/dev4199/demods/3/adcselect', 9)#input channel,Auxin2=9)
        
        
        self.daq.setInt('/dev4199/demods/0/enable', 1) #enable acquisition demod 0/1/2/3
        self.daq.setInt('/dev4199/demods/1/enable', 1) #enable acquisition demod 0/1/2/3
        self.daq.setInt('/dev4199/demods/2/enable', 1) #enable acquisition demod 0/1/2/3
        self.daq.setInt('/dev4199/demods/3/enable', 1) #enable acquisition demod 0/1/2/3
        
        # self.daq.setInt('/dev4199/sigouts/0/enables/0', 0)#off output  signout ch1
        self.daq.setInt('/dev4199/sigouts/0/enables/1', 0)#off output  signout ch2
        self.daq.setInt('/dev4199/sigouts/0/enables/2', 0)#off output  signout ch3
        self.daq.setInt('/dev4199/sigouts/0/enables/3', 0)#off output  signout ch4             
                        
        if 'time constant' in what:
            self.daq.setDouble('/%s/demods/0/timeconstant'% (self.dev), value)
            self.daq.setDouble('/%s/demods/1/timeconstant'% (self.dev), value) 
            self.daq.setDouble('/%s/demods/2/timeconstant'% (self.dev), value)
            self.daq.setDouble('/%s/demods/3/timeconstant'% (self.dev), value)
            
        if 'filter order' in what:
            self.daq.setInt('/%s/demods/0/order'% (self.dev), value)
            self.daq.setInt('/%s/demods/1/order'% (self.dev), value)
            self.daq.setInt('/%s/demods/2/order'% (self.dev), value)
            self.daq.setInt('/%s/demods/3/order'% (self.dev), value)
            
            
        if '1_sensitivity' in what:  
            self.daq.setDouble('/%s/sigins/0/range'% (self.dev), value)
            
        if '2_sensitivity' in what:  
            self.daq.setDouble('/%s/currins/0/range'% (self.dev), value)
            
            
            
        prefix_mapping = {
                        '1_': 0,
                        '2_': 1,
                        '3_': 2,
                        '4_': 3,
                    }  
       
            
        for prefix, index in prefix_mapping.items():
           if f'{prefix}frequency' in what:
            # self.daq.setInt(f'/%s/demods/{index}/oscselect'% (self.dev), f'{index}')#attributue osc0/1/2/3 to channel 0/1/2/3
            # self.daq.setInt(f'/%s/demods/{index}/enable'% (self.dev), 1) #enable acquisition demod 0/1/2/3
            self.daq.setDouble(f'/%s/oscs/{index}/freq'% (self.dev), value) 
            
           if f'{prefix}amplitude' in what:
            self.daq.setDouble(f'/%s/sigouts/0/amplitudes/{index}'% (self.dev), value) 
           if f'{prefix}on' in what: 
            if type(value) is not int: # 24/05/2022 replace in by is to avoid pb with 'harmonic'
                if value == True:
                    self.daq.setInt(f'/%s/sigouts/0/enables/{index}'% (self.dev), 1)
                if value == False:
                    self.daq.setInt(f'/%s/sigouts/0/enables/{index}'% (self.dev), 0)
           if f'{prefix}harmonic' in what:
            self.daq.setDouble(f'/%s/demods/{index}/harmonic'% (self.dev), value)          
                    
            
        # if what.startswith('1_'):

        #     if '1_frequency' in what:
        #         self.daq.setDouble('/%s/oscs/0/freq'% (self.dev), value)
    
        #     if '1_amplitude' in what:
        #         self.daq.setDouble('/%s/sigouts/0/amplitudes/0'% (self.dev), value)
        #     if '1_on' in what:
        #         if type(value) is not int: # 24/05/2022 replace in by is to avoid pb with 'harmonic'
        #             if value == True:
        #                 self.daq.setInt('/%s/sigouts/0/enables/0'% (self.dev), 1)
        #             if value == False:
        #                 self.daq.setInt('/%s/sigouts/0/enables/0'% (self.dev), 0)        
        #     if '1_sensitivity' in what:  
        #         self.daq.setDouble('/%s/sigins/0/range'% (self.dev), value)
        #     if '1_harmonic' in what:
        #         self.daq.setDouble('/%s/demods/0/harmonic'% (self.dev), value)
        #         # self.daq.setDouble('/%s/demods/1/harmonic'% (self.dev), value)

        

        # if what.startswith('2_'):
        #     if '2_frequency' in what:
        #         self.daq.setDouble('/%s/oscs/1/freq'% (self.dev), value)
        #     if '2_amplitude' in what:
        #         self.daq.setDouble('/%s/sigouts/0/amplitudes/1'% (self.dev), value)
        #     if '2_on' in what:
        #         if type(value) is not int: # 24/05/2022 replace in by is to avoid pb with 'harmonic'
        #             if value == True:
        #                 self.daq.setInt('/%s/sigouts/0/enables/1'% (self.dev), 1)
        #             if value == False:
        #                 self.daq.setInt('/%s/sigouts/0/enables/1'% (self.dev), 0)        
        #     if '2_sensitivity' in what:  
        #         self.daq.setDouble('/%s/currins/0/range'% (self.dev), value)
              
        #     if '2_harmonic' in what:
        #         self.daq.setDouble('/%s/demods/1/harmonic'% (self.dev), value)
        #         # self.daq.setDouble('/%s/demods/1/harmonic'% (self.dev), value)

        # if what.startswith('3_'):

        #     if '3_frequency' in what:
        #         self.daq.setDouble('/%s/oscs/2/freq'% (self.dev), value)
        #     if '3_amplitude' in what:
        #         self.daq.setDouble('/%s/sigouts/0/amplitudes/2'% (self.dev), value)
        #     if '3_on' in what:
        #         if type(value) is not int: # 24/05/2022 replace in by is to avoid pb with 'harmonic'
        #             if value == True:
        #                 self.daq.setInt('/%s/sigouts/0/enables/2'% (self.dev), 1)
        #             if value == False:
        #                 self.daq.setInt('/%s/sigouts/0/enables/2'% (self.dev), 0)              
        #     if '3_harmonic' in what:
        #         self.daq.setDouble('/%s/demods/2/harmonic'% (self.dev), value)
        #         # self.daq.setDouble('/%s/demods/1/harmonic'% (self.dev), value)  

        # if what.startswith('4_'):

        #     if '4_frequency' in what:
        #         self.daq.setDouble('/%s/oscs/3/freq'% (self.dev), value)
        #     if '4_amplitude' in what:
        #         self.daq.setDouble('/%s/sigouts/0/amplitudes/3'% (self.dev), value)
        #     if '4_on' in what:
        #         if type(value) is not int: # 24/05/2022 replace in by is to avoid pb with 'harmonic'
        #             if value == True:
        #                 self.daq.setInt('/%s/sigouts/0/enables/3'% (self.dev), 1)
        #             if value == False:
        #                 self.daq.setInt('/%s/sigouts/0/enables/3'% (self.dev), 0)           
        #     if '4_harmonic' in what:
        #         self.daq.setDouble('/%s/demods/3/harmonic'% (self.dev), value)
        #         # self.daq.setDouble('/%s/demods/1/harmonic'% (self.dev), value)

                

    def poll(self,poll_length,x=None):
        ##############################################################################
        #-                   Get data  - POLL function                              -#
        ##############################################################################
        # POLL Continuously check for value changes (by calling pollEvent) in all
        # subscribed nodes for the specified duration and return the data. If
        # no value change occurs in subscribed nodes before duration + timeout,
        # poll returns no data.
        # poll_length = 0.1 # recording time in [s]. The function will block during that time
        # poll_timeout = 500 # Timeout in [ms]. Recommanded 500 ms. The timeout parameter is only
               # relevant when communicating in a slow network. In this case it may be
               # set to a value larger than the expected round-trip time in the
               # network.
        # don't forget to use the function poll_subscribe() before
        # if not done the poll function return a empty data
        
        # reduce poll time out for allan deviation
        poll_timeout = 100
        
        demod_index0 = 0
        
        # get data
        data = self.daq.poll(poll_length, poll_timeout, True)
        
        x0 = data[self.dev]['demods'][str(demod_index0)]['sample']['x']
        y0 = data[self.dev]['demods'][str(demod_index0)]['sample']['y']
        # time_stamp = data[self.dev]['demods'][str(demod_index)]['sample']['timestamp']
        x0 = np.mean(x0)
        y0 = np.mean(y0)
        r0 = np.abs(x0 + 1j*y0)
        phi0 = np.angle(x0 + 1j*y0,deg=True)
        
        
        demod_index1 = 1
        # get data
        x1 = data[self.dev]['demods'][str(demod_index1)]['sample']['x']
        y1 = data[self.dev]['demods'][str(demod_index1)]['sample']['y']
        # time_stamp = data[self.dev]['demods'][str(demod_index)]['sample']['timestamp']
        x1 = np.mean(x1)
        y1 = np.mean(y1)
        r1 = np.abs(x1 + 1j*y1)
        phi1 = np.angle(x1 + 1j*y1,deg=True)
        
        demod_index2 = 2

        # get data
        x2 = data[self.dev]['demods'][str(demod_index2)]['sample']['x']
        y2 = data[self.dev]['demods'][str(demod_index2)]['sample']['y']
        # time_stamp = data[self.dev]['demods'][str(demod_index)]['sample']['timestamp']
        x2 = np.mean(x2)
        y2 = np.mean(y2)
        r2 = np.abs(x2 + 1j*y2)
        phi2 = np.angle(x2 + 1j*y2,deg=True)  
        
        
        demod_index3 = 3
        # get data
        
        x3 = data[self.dev]['demods'][str(demod_index3)]['sample']['x']
        y3 = data[self.dev]['demods'][str(demod_index3)]['sample']['y']
        # time_stamp = data[self.dev]['demods'][str(demod_index)]['sample']['timestamp']
        x3 = np.mean(x3)
        y3 = np.mean(y3)
        r3 = np.abs(x3 + 1j*y3)
        phi3 = np.angle(x3 + 1j*y3,deg=True) 
    
        
        return x0,y0,r0,phi0,x1,y1,r1,phi1,x2,y2,r2,phi2,x3,y3,r3,phi3

    # CODE FROM example_data_acquisition_continuous.py in zurich lib
    def poll_continuous(self,total_duration,do_plot):
        device = self.dev
   
        # # The list of signal paths that we would like to record in the module.
        # demod_path = '/{}/demods/0/sample'.format(device)
        # signal_paths = []
        
        
        demod_path={}
        signal_paths = []
        for i in range(1):
        # The list of signal paths that we would like to record in the module.
            demod_path[i] = '/{}/demods/{}/sample'.format(device,i)
            signal_paths.append(demod_path[i] + '.r')  # The demodulator R output.
        
        
        
        
        
        # # signal_paths.append(demod_path + '.x')  # The demodulator X output.
        # # signal_paths.append(demod_path + '.y')  # The demodulator Y output.
        # signal_paths.append(demod_path + '.r')  # The demodulator R output.
        # # It's also possible to add signals from other node paths:
        # signal_paths.append('/%s/demods/1/sample.r' % (device))
        # signal_paths.append('/%s/demods/2/sample.r' % (device))
        # signal_paths.append('/%s/demods/3/sample.r' % (device))
        # # Check the device has demodulators.
        # flags = ziListEnum.recursive | ziListEnum.absolute | ziListEnum.streamingonly
        # streaming_nodes = daq.listNodes('/{}'.format(device), flags)
        # if demod_path.upper() not in streaming_nodes:
        #     print("Device {} does not have demodulators. Please modify the example to specify".format(device),
        #         "a valid signal_path based on one or more of the following streaming nodes: ",
        #         "{}".format('\n'.join(streaming_nodes)))
        #     raise Exception("Demodulator streaming nodes unavailable - see the message above for more information.")
   
        # Defined the total time we would like to record data for and its sampling rate.
        # total_duration: Time in seconds: This examples stores all the acquired data in the `data` dict - remove this
        # continuous storing in read_data_update_plot before increasing the size of total_duration!
        module_sampling_rate = 1000  # Number of points/second
        burst_duration = 0.1  # Time in seconds for each data burst/segment.
        num_cols = int(np.ceil(module_sampling_rate*burst_duration))
        num_bursts = int(np.ceil(total_duration/burst_duration))
       
   
        # Create an instance of the Data Acquisition Module.
        daq_module = self.daq.dataAcquisitionModule()
   
        # Configure the Data Acquisition Module.
        # Set the device that will be used for the trigger - this parameter must be set.
        daq_module.set("device", device)
   
        # Specify continuous acquisition (type=0).
        daq_module.set("type", 0)
   
        # 'grid/mode' - Specify the interpolation method of
        #   the returned data samples.
        #
        # 1 = Nearest. If the interval between samples on the grid does not match
        #     the interval between samples sent from the device exactly, the nearest
        #     sample (in time) is taken.
        #
        # 2 = Linear interpolation. If the interval between samples on the grid does
        #     not match the interval between samples sent from the device exactly,
        #     linear interpolation is performed between the two neighbouring
        #     samples.
        #
        # 4 = Exact. The subscribed signal with the highest sampling rate (as sent
        #     from the device) defines the interval between samples on the DAQ
        #     Module's grid. If multiple signals are subscribed, these are
        #     interpolated onto the grid (defined by the signal with the highest
        #     rate, "highest_rate"). In this mode, duration is
        #     read-only and is defined as num_cols/highest_rate.
        daq_module.set("grid/mode", 2)
        # 'count' - Specify the number of bursts of data the
        #   module should return (if endless=0). The
        #   total duration of data returned by the module will be
        #   count*duration.
        daq_module.set("count", num_bursts)
        # 'duration' - Burst duration in seconds.
        #   If the data is interpolated linearly or using nearest neighbout, specify
        #   the duration of each burst of data that is returned by the DAQ Module.
        daq_module.set("duration", burst_duration)
        # 'grid/cols' - The number of points within each duration.
        #   This parameter specifies the number of points to return within each
        #   burst (duration seconds worth of data) that is
        #   returned by the DAQ Module.
        daq_module.set("grid/cols", num_cols)
   
        # if filename is not None:
        #     # 'save/fileformat' - The file format to use for the saved data.
        #     #    0 - Matlab
        #     #    1 - CSV
        #     daq_module.set('save/fileformat', 1)
        #     # 'save/filename' - Each file will be saved to a
        #     # new directory in the Zurich Instruments user directory with the name
        #     # filename_NNN/filename_NNN/
        #     daq_module.set('save/filename', filename)
        #     # 'save/saveonread' - Automatically save the data
        #     # to file each time read() is called.
        #     daq_module.set('save/saveonread', 1)
   
        data = {}
        # A dictionary to store all the acquired data.
        for signal_path in signal_paths:
            print("Subscribing to", signal_path)
            daq_module.subscribe(signal_path)
            data[signal_path] = []
   
        clockbase = float(self.daq.getInt("/{}/clockbase".format(device)))
        if do_plot:
            import matplotlib.pyplot as plt
            fig = plt.figure(1)
            fig.clf()
            plt.xlabel('Time ($s$)')
            plt.ylabel('Subscribed signals')
            # plt.xlim([0, total_duration])
            plt.ion() #Turn interactive mode on.
   
        ts0 = np.nan
        read_count = 0
        plot_count = 0
        my_data = {'Time_R':[],'R':[]}



   
        def read_data_update_plot(data, timestamp0):
            """
            Read the acquired data out from the module and plot it. Raise an
            AssertionError if no data is returned.
            """
            data_read = daq_module.read(True)
            returned_signal_paths = [signal_path.lower() for signal_path in data_read.keys()] #lower method returns a copy of the string in which all case-based characters have been lowercased.
            progress = daq_module.progress()[0]
            # Loop over all the subscribed signals:
            for signal_path in signal_paths:
                if signal_path.lower() in returned_signal_paths:
                    # Loop over all the bursts for the subscribed signal. More than
                    # one burst may be returned at a time, in particular if we call
                    # read() less frequently than the burst_duration.
                    for index, signal_burst in enumerate(data_read[signal_path.lower()]):
                        if np.any(np.isnan(timestamp0)):
                            # Set our first timestamp to the first timestamp we obtain.
                            timestamp0 = signal_burst['timestamp'][0, 0]
                        # Convert from device ticks to time in seconds.
                        t = (signal_burst['timestamp'][0, :] - timestamp0)/clockbase
                        value = signal_burst['value'][0, :]
                        # Fill my data
                        if 'sample.r' in signal_path:
                            my_data['Time_R'] = np.append(my_data['Time_R'],t)
                            my_data['R'] = np.append(my_data['R'],value)
                        if do_plot:
                            plt.plot(t, value)
                        num_samples = len(signal_burst['value'][0, :])
                        dt = (signal_burst['timestamp'][0, -1] - signal_burst['timestamp'][0, 0])/clockbase
                        ##data[signal_path].append(signal_burst)
                        print("Read: ", read_count, ", progress: {0:.2f}%".format(100*progress), ". Burst ", index, ": ",
                            signal_path, " contains ", num_samples, " spanning {0:.2f} s.".format(dt), sep="")
                else:
                    # Note: If we read before the next burst has finished, there may be no new data.
                    # No action required.
                    pass
   
            # Update the plot.
            if do_plot:
                plt.title("Progress of data acquisition: {0:.2f}%.".format(100*progress))
                plt.pause(0.01)
                fig.canvas.draw()
            return data, timestamp0
   
        # Start recording data.
        daq_module.execute()
   
        # Record data in a loop with timeout.
        timeout = 1.5*total_duration
        t0_measurement = time.time()
        # The maximum time to wait before reading out new data.
        t_update = 0.9*burst_duration
        while not daq_module.finished():
            t0_loop = time.time()
            if time.time() - t0_measurement > timeout:
                raise Exception("Timeout after {} s - recording not complete. Are the streaming nodes enabled? "
                                "Has a valid signal_path been specified?".format(timeout))
            _, ts0 = read_data_update_plot(data, ts0)
            read_count += 1
            plot_count += 1
            # to avoid overflow for the graph
            if plot_count > 200:
                plt.clf()
                plt.xlabel('Time ($s$)')
                plt.ylabel('Subscribed signals')
                plot_count = 0
            # We don't need to update too quickly.
            time.sleep(max(0, t_update - (time.time() - t0_loop)))
   
        # There may be new data between the last read() and calling finished().
        read_data_update_plot(data, ts0)
   
        # Before exiting, make sure that saving to file is complete (it's done in the background)
        # by testing the 'save/save' parameter.
        timeout = 1.5*total_duration
        t0 = time.time()
        while daq_module.getInt('save/save') != 0:
            time.sleep(0.1)
            if time.time() - t0 > timeout:
                raise Exception("Timeout after {} s before data save completed.".format(timeout))
   
        if not do_plot:
            print("Please run with `do_plot=True` to see dynamic plotting of the acquired signals.")
   
        plt.close()
        return my_data

if __name__ == "__main__":
    #Call of the classes
    #ma_zurich = ZurichMFLI(dev_num='dev4320')
    ma_zurich = ZurichMFLI(dev_num='dev4199')

    # x = ma_zurich.poll_continuous(total_duration=10, do_plot=True)
    # print(x)

# ma_zurich.set_value('4_filter order',8)
# ma_zurich.set_value('4_frequency',32000)
# ma_zurich.set_value('4_on',True)   
# ma_zurich.set_value('4_amplitude',1)  