import pyvisa
import time
import re
import serial
from cached_property import cached_property



# from mhs5200 import MHS5200
# import mhs5200.enums as WAVE

# Define enums
SINE = 0
SQUARE = 1
TRI = 2
UP = 3
DOWN = 4
ARB0 = 100
ARB1 = 101
ARB2 = 102
ARB3 = 103
ARB4 = 104
ARB5 = 105
ARB6 = 106
ARB7 = 107
ARB8 = 108
ARB9 = 109
ARB10 = 110
ARB11 = 111
ARB12 = 112
ARB13 = 113
ARB14 = 114
ARB15 = 115



class MHS5200:
    def __init__(self, port="COM4"):
        
        self.port = port
        # Create a list for the channels.
        self.channels = list()
        # Number of channels on the device.
        num_channels = 2
        # For each of the possible channels.
        for channel_number in range(num_channels):
            # Instantiate channel
            channel_obj = Channel(self, channel_number + 1)
            # Append to the channel list.
            self.channels.append(channel_obj)
            # Allow referencing channel by a parameter.
            setattr(self, "chan{}".format(channel_number + 1), channel_obj)

        # Send an empty string to flush buffers and set device to a known
        # state.
        self.send("")

    @cached_property
    def serial(self):
        # Serial configuration.
        cfg = dict()
        # Serial Basics
        cfg["port"] = self.port
        cfg["baudrate"] = 57600

        # Flow Control
        cfg["xonxoff"] = False
        cfg["rtscts"] = False
        cfg["dsrdtr"] = False

        # Timeouts.
        cfg["timeout"] = 0.5
        cfg["write_timeout"] = 0.5

        self.cfg = cfg
        # Open the serial port.
        self.serial = serial.Serial(**cfg)
        return self.serial

    def on(self):
        """Turn device output on."""
        self._set(1, "on", 1)

    def off(self):
        """Turn device output off."""
        self._set(1, "on", 0)

    @property
    def model(self):
        try:
            raw_model = self.send("r0c")
            return raw_model[4:]
        except:
            return ""

    def send(self, msg="", return_line=True):
        """
        Send message over the serial port to the MHS5200 device.

        Automatically adds the ':' prefix and CRLF to the message.
        """
        # Flush input and output buffers.
        self.serial.flushInput()
        self.serial.flushOutput()
        # Create the message string.
        cmd_str = f":{msg}\r\n"
        # Send the message out the serial bus.
        self.serial.write(cmd_str.encode())
        # If a return line is expected
        if return_line:
            # Read the line.
            data = self.serial.readline()
            # Decode and strip the CRLF.
            data_clean = data.decode().strip()
            # Return the data string.
            return data_clean

    def _read(self, channel, prop):
        """
        Read a channel's value.
        """
        cmd_str = "r{}{}".format(channel, cmd_map[prop])
        return self.send(cmd_str, return_line=True)

    def _set(self, channel, prop, value):
        """
        Set a channel's value
        """
        cmd_str = "s{}{}{}".format(channel, cmd_map[prop], value)
        response = self.send(cmd_str, return_line=True)
        assert response == "ok"

    def save(self, slot=0):
        """
        Save settings to a memory slot.

        Memory slot 0 is the default when device is powered on.
        """
        response = self.send(f"s{slot}u")
        assert response == "ok"

    def load(self, slot=0):
        """
        Load settings from a memory slot.
        """
        response = self.send(f"s{slot}v")
        assert response == "ok"

    def __enter__(self):
        """Enter from context management.

        """
        return self

    def __exit__(self, type, value, tb):
        """Exit from context management.

        """
        t1 = time.time()
        while self.serial.isOpen():
            if time.time() > t1 + 5:
                break
            self.serial.close()
            time.sleep(0.2)
############################################

#########################

"""
Mapping from the command name to the initialism used in the protocol.
"""
cmd_map = dict()
cmd_map["frequency"] = "f"
cmd_map["wave"] = "w"
cmd_map["duty_cycle"] = "d"
cmd_map["offset"] = "o"
cmd_map["phase"] = "p"
cmd_map["atten"] = "y"
cmd_map["amplitude"] = "a"
cmd_map["on"] = "b"




song_re = re.compile("([A-G]?#?)")

from typing import List


def get_freq(n=-21):
    """ Generate a frequency from an 'n'.

    Based on an equation:
      https://www.intmath.com/trigonometric-graphs/music.php
    """
    return 440.0 * 2.0 ** (n / 12.0)


def get_note_frequency(note, octave=0):
    """Get the frequency of a note:

    note: Musical note.
    octave: Setup so octave=0, A = 440 Hz.
    """
    n = notes.index(note) - 9.0 - octave * 12.0
    return get_freq(n)


def play_song(inst, song, octave=0):
    inst.on()
    for note in song:
        chan = inst.channels[1]
        chan.amplitude = 0
        # Between beats
        time.sleep(0.1)
        chan.frequency = get_note_frequency(note, octave)
        chan.amplitude = 1.0
        chan.offset = 0
        chan.wave = 0
        time.sleep(0.2)
    inst.off()


notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]


def play_scale(inst, octave=0):
    play_song(notes)


songs = {
    "twinkle": "CCGGAAGFFEEDDCGGFFEEDGGFFEEDCCGGAAGFFEEDDC",
    "baby_shark": "CDFFFFFFFFCDFFFFFFFFCDFFFFFFFFFFE",
}

###########################################

class Channel:
    def __init__(self, dds, num):
        """

        dds: signal generator class

        """
        self.dds = dds
        self.num = num

    def on(self):
        """ Turn channel off.

        (Both channels are turned on and off together).
        """
        self.dds.on()

    def off(self):
        """ Turn channel off.

        (Both channels are turned on and off together).
        """
        self.dds.off()

    @property
    def frequency(self):
        """ Get the frequency.

        """
        raw_value = self._frequency
        return float(raw_value) / 100

    @frequency.setter
    def frequency(self, value):
        """ Set the frequency of the channel. """

        # Accept strings with knows SI units.
        if isinstance(value, str):
            if value.endswith("MHz"):
                multiplier = 1000000
            elif value.endswith("kHz"):
                multiplier = 1000
            elif value.endswith("Hz"):
                multiplier = 1
            else:
                raise (Exception("Frequency does not end with known unit"))
            # Strip off the suffix
            value = value.strip("MkHz ")
            # Convert the value to an int and
            value = int(value) * multiplier

        raw_value = int(value * 100)

        self._frequency = raw_value

    @property
    def wave(self):
        raw_value = self._wave
        return raw_value

    @wave.setter
    def wave(self, value):
        raw_value = value
        self._wave = raw_value

    @property
    def duty_cycle(self):
        raw_value = self._duty_cycle
        return float(raw_value) / 10

    @duty_cycle.setter
    def duty_cycle(self, value):
        raw_value = int(value * 10)
        self._duty_cycle = raw_value

    @property
    def offset(self):
        raw_value = self._offset
        return raw_value - 120

    @offset.setter
    def offset(self, value):
        raw_value = int(value + 120)
        self._offset = raw_value

    @property
    def phase(self):
        raw_value = self._phase
        return raw_value

    @phase.setter
    def phase(self, value):
        raw_value = int(value)
        self._phase = raw_value

    @property
    def atten(self):
        raw_value = self._atten
        return raw_value

    @property
    def amplitude(self):
        raw_value = self._amplitude
        return float(raw_value) / 100

    @amplitude.setter
    def amplitude(self, value):
        raw_value = int(value * 100)
        self.dds._set(self, "amplitude", raw_value)

    def __str__(self):
        return f"{self.num}"

    def __repr__(self):
        return f"Channel<{self.num}, {self.amplitude}V@{self.frequency}Hz>"


# Function generator for get functions.


def getter_gen(parameter):
    def getter_fcn(self):
        cmd = cmd_map[parameter]
        raw_value = self.dds._read(self, parameter)
        value = raw_value.split(cmd)[1]
        return int(value)

    return getter_fcn


# Function generator for set functions.


def setter_gen(parameter):
    def setter_fcn(self, value):
        return self.dds._set(self, parameter, value)

    return setter_fcn


# Add each of the set & get methods to the Channel class.
for attribute, _ in cmd_map.items():
    setattr(
        Channel,
        f"_{attribute}",
        property(getter_gen(attribute), setter_gen(attribute)),
    )  # Add to the channel class  # Prefix the attribute as 'internal'.  # Add as a property.  # Define getter  # Define setter


signal_gen = MHS5200()
signal_gen.chan1 =signal_gen.channels[0]


signal_gen.on()
signal_gen.chan1.frequency = 23550 # Hz
signal_gen.chan1.amplitude = 0.05 # V-pp
signal_gen.chan1.offset = 0 # V
signal_gen.chan1.wave = SINE
time.sleep(2)
signal_gen.off()
signal_gen.serial.close()
