
#import time
import zhinst.ziPython as zi
# Log sequence recorded on 2023/07/22 02:39:55

device_id = '192.168.65.158'
device_props = device_id
daq = zi.ziDAQServer('192.168.65.158', 8004, 6)


daq.setDouble('/dev4199/oscs/0/freq', 20000)
daq.setDouble('/dev4199/oscs/1/freq', 30000)
daq.setDouble('/dev4199/oscs/2/freq', 50000)
daq.setDouble('/dev4199/oscs/3/freq', 60000)
