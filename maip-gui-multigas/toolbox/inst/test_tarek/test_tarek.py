from __future__ import print_function
#import time
import zhinst.ziPython as zi
# Log sequence recorded on 2023/07/22 02:39:55
import numpy as np



daq = zi.ziDAQServer('192.168.65.158', 8004, 6)

#oscillator commands frequency
daq.setDouble('/dev4199/oscs/0/freq', 20000)
daq.setDouble('/dev4199/oscs/1/freq', 30000)
daq.setDouble('/dev4199/oscs/2/freq', 50000)
daq.setDouble('/dev4199/oscs/3/freq', 60000)
#amplitude
daq.setDouble('/dev4199/sigouts/0/amplitudes/0', 0.02)

#filter order
daq.setInt('/dev4199/demods/0/order', 3)
daq.setInt('/dev4199/demods/1/order', 3)
daq.setInt('/dev4199/demods/2/order', 3)
daq.setInt('/dev4199/demods/3/order', 3)

#filter order time constant in seconds exemple: 0.1=100ms  0.2=200ms
daq.setDouble('/dev4199/demods/0/timeconstant', 0.1)
daq.setDouble('/dev4199/demods/1/timeconstant', 0.1)
daq.setDouble('/dev4199/demods/2/timeconstant', 0.2)
daq.setDouble('/dev4199/demods/3/timeconstant', 0.3)

#input channels (sigin1=0,currin1=1,Auxin1=8,Auxin2=9)
daq.setInt('/dev4199/demods/0/adcselect', 8)
daq.setInt('/dev4199/demods/1/adcselect', 8)
daq.setInt('/dev4199/demods/2/adcselect', 9)
daq.setInt('/dev4199/demods/3/adcselect', 9)

#attribiute demod 2 to osc 2
daq.setInt('/dev4199/demods/2/oscselect', 2)#attributue osc2 to channel 2


#acqusition (demods_off=0 demods_on=1)
daq.setInt('/dev4199/demods/0/enable', 0)
daq.setInt('/dev4199/demods/1/enable', 0)
daq.setInt('/dev4199/demods/2/enable', 0)
daq.setInt('/dev4199/demods/3/enable', 0)

#50ohm_signout(off=0,on=1)
daq.setInt('/dev4199/sigouts/0/on', 0)

#sign outputv+(/dev4199/sigouts/0)
#/enables/0 ,1  i.e pass signal for osc0
daq.setInt('/dev4199/sigouts/0/enables/0', 0)
#sesnitivity
daq.setDouble('/dev4199/sigins/0/range', 3)#3V
daq.setDouble('/dev4199/currins/0/range', 0.001)#1mA

#enable data acqui for demod 1
daq.setInt('/dev4199/demods/0/enable', 1)
daq.setInt('/dev4199/demods/1/enable', 1)
daq.setInt('/dev4199/demods/2/enable', 1)
daq.setInt('/dev4199/demods/3/enable', 1)


# -*- coding: utf-8 -*-
"""
Zurich Instruments LabOne Python API Example

Python API Example for the Data Acquisition Module. This example demonstrates
how to record data from an instrument continuously (without triggering).

Note: This example does not perform any device configuration. If the streaming
nodes corresponding to the signal_paths are not enabled, no data will be
recorded.
"""

# Copyright 2018 Zurich Instruments AG


