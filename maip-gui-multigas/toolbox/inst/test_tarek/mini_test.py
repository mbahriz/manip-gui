from mhs5200 import MHS5200
import mhs5200.enums as WAVE
import time



# rm = pyvisa.ResourceManager()
# rm.list_resources()

# signal_gen = MHS5200(port="COM3")






# signal_gen.chan1 =signal_gen.channels[0]


# signal_gen.on()
# signal_gen.chan1.frequency = 21550 # Hz
# signal_gen.chan1.amplitude = 0.02 # V-pp
# signal_gen.chan1.offset = 0 # V
# signal_gen.chan1.wave = WAVE.SINE
# time.sleep(2)
# signal_gen.off()
# signal_gen.serial.close()


class sharing:
    
    def __init__(self,daq):

        self.daq=self.connect()
        
    def connect(self):
        signal_gen = MHS5200(port="COM3") 
        print(signal_gen)
        return signal_gen
    


class Miniware_CH1():
   
    def __init__(self):

        self.daq=self.connect() 
        
    def connect(self):
        signal_gen = MHS5200(port="COM3") 
        print(signal_gen)
        return signal_gen
        
    def set_value(self,what,value):   
        
        
        self.daq.chan1 =self.daq.channels[0]
        self.daq.on()
        self.daq.chan1.offset = 0 # V
        self.daq.chan1.wave = WAVE.SINE

        if 'frequency' in what:
            self.daq.chan1.frequency = value #set the frequency modulation
            
        if 'amplitude' in what:
            ampli_Vpk=value
            ampli_Vpp=ampli_Vpk*2 #vpp
            self.daq.chan1.amplitude = ampli_Vpp

        
    def disconnect(self):   
        self.daq.off()
        self.daq.serial.close()
        
class Miniware_CH2():
   
     
    def set_value(self,what,value):   
        
        
        self.daq.chan2 =self.daq.channels[1]
        self.daq.on()
        self.daq.chan2.offset = 0 # V
        self.daq.chan2.wave = WAVE.SINE

        if 'frequency' in what:
            self.daq.chan2.frequency = value #set the frequency modulation
            
        if 'amplitude' in what:
            ampli_Vpk=value
            ampli_Vpp=ampli_Vpk*2 #vpp
            self.daq.chan2.amplitude = ampli_Vpp
        

        
    def disconnect(self):   
        self.daq.off()
        self.daq.serial.close()        


if __name__ == "__main__":
    
    mini=Miniware_CH1()
    mini.set_value('frequency', 32000)
    mini.set_value('amplitude', 0.08)
    mini2=Miniware_CH2()
    mini2.set_value('frequency',10000)
    mini2.set_value('amplitude', 0.3)
    
    time.sleep(20)
    mini.disconnect()