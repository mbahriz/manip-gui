#   20201001 add command to generate AM from driver laser need to connect th analog output BNC on an input entrance of the MFLI and set the ref oscillator as external to trigger on the laser driver mod (setup for Thorlabs FP laser)
# 21 septembre 2023 Update for pyqtgraph 0.13.1
'''
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

app = QtGui.QApplication([])
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree, ParameterItem, registerParameterType
'''


import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets

app = pg.mkQApp()
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree

import numpy as np

import sys
path = '/Users/michaelbahriz/Recherche/Manips/Codes/manip-gui'
sys.path.append(path) 
import toolbox.inst.kinjalk

import toolbox.inst.medisoft as medi


# import inst.medisoft as medi

# import medisoft_complet as medis

##############################################################################
##### GLOBAL 
##############################################################################
path_WINDOWS = 'C:\\Users\\user1\\Documents\\Data_QEPAS\\'
#'C:\\Users\\qepas2\\Documents\\Data_GUI\\'
#'C:\\Users\\user1\\Documents\\QEPAS\\DATA\\'
path_MACOS = '/Users/michaelbahriz/Recherche/Manips/Codes/manip-gui/data/'

path_EXPAIR="None"
#medi.Medisoft.connect('Medisoft') #'C:\\Expair\\Logs\\'



path = path_WINDOWS
amp_max = 200e-3 # amplitude of modulation max
amp_min_miniware=5e-3
amp_max_miniware= 200e-3 # amplitude of modulation max
freq_min_acq, freq_max_acq = 10e3, 20e3
freq_max_ametek = 250e3
freq_max_zurich = 500e3
keithley_time_cst_min = 10e-3

##############################################################################
##### INSTRUMENT INIT 
##############################################################################



input_init = {#'time constant' : 100e-3,
                'filter order' : 2,
                'poll length' : 0.002,
                'harmonic' : 1
                 } 


arduino_dht={'COM':10}

generator_ext_2 = {'frequency' : 16.3970e3,
                'amplitude' : 42.42e-3,
                'on':False
                
                }

generator_ext_3 = {'frequency' : 16.3804e3,
                'amplitude' : 70e-3,
                'on' : False
                
                }

generator_ext_4 = {'frequency' : 16.3787e3,
                'amplitude' : 15e-3,
                'on' : False
                }


generator_init_1 = {'1_frequency' : 32.7674e3,
                '1_amplitude' : 80.5e-3,
                '1_on' : False,
                '1_time constant' : 1,
                '1_harmonic' : 1,
                '1_sensitivity' : 1,
                '1_gain' : 20000,
                }





generator_init_2 = {'2_frequency' : 16.3970e3,
                '2_amplitude' : 42.42e-3,
                '2_time constant' : 1,
                '2_harmonic' : 2,
                '2_sensitivity' : 10e-3,
                '2_gain' : 1e6,}

generator_init_3 = {'3_frequency' : 16.3804e3,
                '3_amplitude' : 70e-3,
                '3_time constant' : 1,
                '3_harmonic' : 2 ,
                '3_gain' : 10000,
                }

generator_init_4 = {'4_frequency' : 16.3787e3,
                '4_amplitude' : 15e-3,
                '4_time constant' : 400e-3,
                '4_harmonic' : 2,
                '4_gain' : 50,
                }

laser_driver_init = {'on' : False,
                'current' : 2, #in mA
                'temperature' : 20} 


laser_driver_init_1 = {'on' : False,
                'current' : 2, #in mA
                'temperature' : 2} 

laser_driver_init_2 = {'on' : False,
                'current' : 2, #in mA
                'temperature' : 20} 

laser_driver_init_3 = {'on' : False,
                'current' : 2, #in mA
                'temperature' : 10} 

laser_driver_init_4 = {'on' : False,
                'current' : 2, #in mA
                'temperature' : 20} 

pulse_module_init = {'pulse width':40, # in ns
                'carrier frequency':500e3,
                'frequency':1000, # frequency = modulation frequency
                'duty cycle':10}   # in %

expair={'Expair_filename':path_EXPAIR
        
        
        
        }


all_instru_init = {'MFLI ch1' :generator_init_1,
                   'Generator' :generator_init_1,
                'Input' : input_init,
                'MFLI ch2' :generator_init_2,
                'MFLI ch3' :generator_init_3,                              
                'MFLI ch4' :generator_init_4,
                'Laser Driver_1' : laser_driver_init_1,
                'Laser Driver_2' : laser_driver_init_2,
                'Laser Driver_3' : laser_driver_init_3,
                'Laser Driver_4' : laser_driver_init_4,
                'Pulse module' : pulse_module_init,
                'Arduino' : arduino_dht,
                'Gen_ext_1' : generator_ext_2,
                'Gen_ext_2' : generator_ext_2,
                'Gen_ext_3' : generator_ext_3,
                'Gen_ext_4' : generator_ext_4,
                'Medisoft':expair
                }


def instrument_initials_values(who):
    return all_instru_init[who]

##############################################################################
##### LIST RESONATOR, LASER ....
##############################################################################
list_amplifier = ['TIA-B','TIA-C','TIA-1','TIA-2','TIA-3','TIA-4','FEMTO-100K-50M','FEMTO-40K-100M','Without','Unknow']
list_laser = ['QCL_Zayneb_10.9um','Norca4_2.4um','Eblana_1.3um_H2O','Without','Unknow']
list_gas = ['air','vacuum','acetone','Isoprene','CO','NO','ch4','c2h4','Unknow']

list_design = ['None','QTF','Acoustic_resonator','Unknow']
               # ,'H-Res_75um','H-Res_75um_V2','Hsquare-Res_75um','Hsquare-Res_75um_V3','Hsquare-Res_75um_V4','Hsquare-Res_100um','Acoustic_resonator','Menbrane','Cantilever','Unknow']

list_microphone = ['microphone_3V']
list_others = ['SOI_substrate_75um']
list_menbrane = ['Q7BM_2p5mm_eptot13um_q300','Q7BM_4mm_eptot13um_q300','Q7BM_3mm_eptot13um_q300','Q4MG1_2mm_eptot11um_q600nm','Q4MG_2mm_eptot12um_q600nm','Menbrane_2mm','Menbrane_1mm_10um','Q10_1_5mm_2um','Q9_2mm_8um','M_SiN500nm_1','M_SiN500nm_4','M_SiN500nm_5','M_SiN500nm_6','Unknow']
list_Acoustic_resonator = ['None','Bare','dural','3D_print_write']
                           # 'L066_1','L067_1','L068_1','L105','L112','L115','L116_1','L120_1','L122','L121_1','L121_2','L121_3','L121_4','L121_5','L133','L136_1','L145','L157_1','L157_2','L157_3','L157_4','L173','L185','L200_1','L215_1','L245_1','L245_2','L245_3','L230','L230_2','L230_3','L230_4','L230_5','L230_6','L230_7','L260','L260_2','Unknow']
list_Cantilever = ['CHS100A41N7_1','CHS100A41N7_5','CHS100A41N7_3','C_L580_e75um','C_L690_e75um','C_L1000_l1500_e75um','C_L1000_l1000_e75um','C_L1522_l990_e157um','C_L1330_l984_e157um','C_L1625_l988_e157um','C_L1428_l985_e157um','Unknow']
list_Acoustic_sensor = list_Cantilever + list_menbrane + list_microphone + list_others

list_QTF = ['QTF-NC15LF','QTF-china','QTF-210330','QTF-210330-A9','Unknow']
list_HRes_75um = ['H75R45F4.1','H75R45F4.2','H75R45F4.3','H75R45F4.4','H75R45F4.5','H75R15M500.1','H75R15M500.2','H75R15M500.3','H75R15M500.4','H75R65M500.1','H75R65M500.5','H75R65F4.1','H75R15F4.1','H75R15F4.5','Unknow']
list_HRes_75um_V2 = ['H75F4N5.2','H75F4N8C.1','H75F4N8.2','H75M500N8C.1','Unknow']
list_HRes_75um_device =['Unknow','10arms','9arms','8arms','7arms','6arms','5arms','4arms','3arms']
list_HRes_75um_V2_device =['Unknow','10arms','9arms','8arms','7arms','6arms','5arms','4arms','3arms']
list_Hsquare_Res_75um=['Unknow','HS75R15M350o1.1','HS75R15M350o4.1','HS75R15L350o2.1','HS75R15L430o1.1','HS75R15M200o1.1','HS75R15S700o1.1']
list_Hsquare_Res_75um_V3=['Unknow','HS75R15ls200.1','HS75R10ls100.1','HS75R10ls200.1','HS75R5ls200.1','HS75R20ls200.1']
list_Hsquare_Res_75um_V4=['Unknow','HS75R15d150a10.1','HS75R20d150a12.1','HS75R20d150a10.1','HS75R15d125.1']
list_Hsquare_Res_75um_device = ['Unknow','Top','Bottom left','Bottom right']
list_HSRes_100um = ['HS100A455N7C6.1','HS100A41N7C6.1','HS100A41N7C12.1','Unknow']
list_HSRes_100um_device = ['Unknow','Top','Bottom left','Bottom right']

# ATTENTION list should be the same in the ametek.py
list_sensitivity_LIA_Ametek = [2e-9,5e-9,10e-9,20e-9,50e-9,100e-9,200e-9,500e-9,
    1e-6,2e-6,5e-6,10e-6,20e-6,50e-6,100e-6,200e-6,500e-6,
    1e-3,2e-3,5e-3,10e-3,20e-3,50e-3,100e-3,200e-3,500e-3,1] # Ametek LIA works with list :-(
list_sensitivity_LIA_Zurich = [1e-3, 3e-3, 10e-3, 30e-3, 100e-3, 300e-3,1 ,3] 
list_time_constant_LIA = [10e-6,20e-6,50e-6,100e-6,200e-6,500e-6,
    1e-3,2e-3,5e-3,10e-3,20e-3,50e-3,100e-3,200e-3,500e-3,
    1,2,5]



##############################################################################
##### CLASS
##############################################################################
class MonParametre():
    # mother class use to transfert method to other class

    def child_list(self):
        # return a list with the name and value of all the children
        A = ''
        for i in self.children():
            A = A+i.name()+' : '+str(i.value())+'\n'
        return A

class Acquisition(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'freq min', 'type': 'float','value': freq_min_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'freq max', 'type': 'float','value': freq_max_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'nbr pts', 'type': 'int','value': 200, 'step': 100, 'limits': (10, 100e3),'siPrefix': False, 'tip':'number of points use for the frequecy sweep'})
        self.addChild({'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 1, 'tip':'number of sequences, number of time frequecy sweep is running'})
        self.addChild({'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor. Indeed, the Zurick don\'t send only one value but a list of value acquierd durint the waiting time'})
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'average', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'})

    def remaning_time(self,value):
        self.param('remaning time').setValue(value)


class AllPlotScalableGroup(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'group'
        opts['addText'] = "Add"
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'path', 'type': 'str', 'value': path})

    def addNew(self):
        new = OneCurve(name="Curve_%d" % (len(self.childs)))
        new.param('file name').setValue("Curve %d.csv" % (len(self.childs)))
        self.addChild(new,dict(removable=True))
        self.addChild({'name': 'sinusoidal fit', 'type': 'bool', 'value': False})

class Amplifier(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'amplifier', 'type': 'list', 'values': list_amplifier, 'value': 'Unknow'})
        self.addChild({'name': 'gain', 'type': 'float','value': 10e6, 'step': 10e6,'siPrefix': True, 'suffix': 'V/A','decimals':3})

        self.amplifier = self.param('amplifier')
        self.gain = self.param('gain')
        self.amplifier.sigValueChanged.connect(self.amplifier_changed)

    def amplifier_changed(self):
        # ['TIA-B','TIA-C','FEMTO-100K-50M','FEMTO-40K-100M','Unknow']
        if self.amplifier.value() in 'FEMTO-100K-50M':
            self.gain.setValue(50e6)
        if self.amplifier.value() in 'FEMTO-40K-100M':
            self.gain.setValue(100e6)
        if self.amplifier.value() in 'TIA-B':
            pass
        if self.amplifier.value() in 'TIA-C':
            pass

class AllanDeviation(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'acq. time', 'type': 'float','value': 30, 'step': 10, 'limits': (0.1, 3600),'siPrefix': True, 'suffix': 'min','decimals':6,'tip':'total acquisition time'})  
        self.addChild({'name': 'concentration', 'type': 'float','value': 1000, 'step': 100, 'limits': (1, 1e6),'siPrefix': True, 'suffix': 'ppmv','decimals':6,'tip':'concentration'})
        self.addChild({'name': 'type', 'type': 'list', 'values': ['adev','oadev'], 'value': 'adev', 'tip':'oadev = overlapping and adev = standarad allan deviation'})  
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'})
        
        
class Plotter(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        
        # self.addChild({'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor. Indeed, the Zurick don\'t send only one value but a list of value acquierd durint the waiting time'})
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'stop', 'type': 'action','tip':'stop and post treat log file, charging time 5s'})
        self.addChild({'name': 'clear all', 'type': 'action'})

    def remaning_time(self,value):
        self.param('remaning time').setValue(value)



class Arduino(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Arduino_DHT','Arduino_GPIB'], 'value': 'Virtual'})
        self.instru = self.param('instrument')

        self.instru.sigValueChanged.connect(self.instru_changed)
        # self.j=0 #Flag
    def instru_changed(self):
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])

        # if  'Arduino_DHT' in self.instru.value():  #and self.j==0:

        #     self.addChild({'name': 'COM', 'type': 'list', 'values': [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 'value': arduino_dht['COM']})
        #     self.removeChild(child_list[1])

      
class Medisoft_expair(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        path_info = ''
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Medisoft'], 'value': 'Virtual'})
        self.addChild({'name': 'Expair_filename', 'type': 'str','value':expair['Expair_filename']})
        self.instru = self.param('instrument')
       
        self.instru.sigValueChanged.connect(self.instru_changed)
        
        
    def instru_changed(self):
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])
        if  'Medisoft' in self.instru.value():
            path=medi.Medisoft.connect('Medisoft') 
            self.addChild({'name': 'Expair_filename', 'type': 'str','value':path})

        

class Generator_extern(pTypes.GroupParameter,MonParametre):   
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        opts['expanded'] = False
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual',"Tektronix_CH1","Tektronix_CH2",'Picoscope_3207B','Miniware_CH1','Miniware_CH2'], 'value': 'Virtual'})
        # self.addChild({'name': 'on', 'type': 'bool', 'value': generator_ext_1['on'], 'tip': "ON when it is checked"})
        self.instru = self.param('instrument')


        self.instru.sigValueChanged.connect(self.instru_changed)
        # self.j=0 #Flag
    def instru_changed(self):
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])

        if  'Picoscope_3207B' in self.instru.value():  #and self.j==0:
            self.addChild({'name': 'frequency', 'type': 'float','value': generator_ext_4['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6}) 
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_ext_4['amplitude'], 'step': 1e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_ext_4['on'], 'tip': "ON when it is checked"})
            
            
        if  'Tektronix_CH1' in self.instru.value():  #and self.j==0:
            self.addChild({'name': 'frequency', 'type': 'float','value': generator_ext_2['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6}) 
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_ext_2['amplitude'], 'step': 1e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_ext_2['on'], 'tip': "ON when it is checked"})
            
        if  'Tektronix_CH2' in self.instru.value():  #and self.j==0:
            self.addChild({'name': 'frequency', 'type': 'float','value': generator_ext_3['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6}) 
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_ext_3['amplitude'], 'step': 1e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_ext_3['on'], 'tip': "ON when it is checked"})

        
        if  'Miniware_CH1' in self.instru.value():
            # self.addChild({'name': 'connection', 'type': 'bool', 'value': miniware_CH1['connection'], 'tip': "ON when it is checked"})
            self.addChild({'name': 'frequency', 'type': 'float','value': generator_ext_3['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6}) 
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_ext_3['amplitude'], 'step': 1e-3, 'limits': (amp_min_miniware, amp_max_miniware),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': 'on', 'type': 'bool', 'value': generator_ext_3['on'], 'tip': "ON when it is checked"})

        if  'Miniware_CH2' in self.instru.value():
            self.addChild({'name': 'frequency', 'type': 'float','value': generator_ext_4['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6}) 
            self.addChild({'name': 'amplitude', 'type': 'float','value': generator_ext_4['amplitude'], 'step': 1e-3, 'limits': (amp_min_miniware, amp_max_miniware),'siPrefix': True, 'suffix': 'V'})

        
class QEPAS(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        
        # self.addChild({'name': 'QEPAS_1'})
        
        # self.instru = self.param('QEPAS_1')
    
    
    
class Generator1(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        opts['expanded'] = False
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Virtual','Zurich-MFLI_dev4199'], 'value':'None'})
        self.addChild({'name': '1_frequency', 'type': 'float','value': generator_init_1['1_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})  
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)
        
    def instru_changed(self):
        child_list = self.children()
        for i in range(2, len(child_list)):
            self.removeChild(child_list[i])        
        if  'Zurich' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': '1_time constant', 'type': 'float','value': generator_init_1['1_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '1_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_1['1_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '1_on', 'type': 'bool', 'value': generator_init_1['1_on'], 'tip': "ON when it is checked"})
            self.addChild({'name': '1_sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Zurich, 'value': generator_init_1['1_sensitivity'], 'tip':'in [V]'})
            self.addChild({'name': '1_gain', 'type': 'float','value': generator_init_1['1_gain'],'siPrefix': True, 'suffix': 'V/V','tip':'gain ch1'})
        
        
        
        if  'Ametek' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (1, freq_max_ametek),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            #self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if 'Kinjalk' in self.instru.value():
            self.addChild({'name': 'info', 'type': 'str', 'value': ''})
            self.addChild({'name': 'real frequency', 'type': 'str', 'value': ''})
            self.real_freq = self.param('real frequency')
            self.freq = self.param('frequency')
            self.freq.setValue(pulse_module_init['frequency'])
            self.info = self.param('info')
            self.ChangeDuty()
            self.freq.sigValueChanged.connect(self.ChangeDuty)
        if  'Virtual' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': '1_time constant', 'type': 'float','value': generator_init_1['1_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '1_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_1['1_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '1_on', 'type': 'bool', 'value': generator_init_1['1_on'], 'tip': "ON when it is checked"})
            self.addChild({'name': '1_sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Zurich, 'value': generator_init_1['1_sensitivity'], 'tip':'in [V]'})
            self.addChild({'name': '1_gain', 'type': 'float','value': generator_init_1['1_gain'],'siPrefix': True, 'suffix': 'V/V','tip':'gain ch1'})

    def ChangeDuty(self):
        X, Y = toolbox.inst.kinjalk.XnY_modulator_frequency(frequency=self.freq.value(), duty=0.5)
        x, x_round, x_err = X
        y, y_round, y_err = Y 
        true_state, false_state = toolbox.inst.kinjalk.logic_state(x=x_round, y=y_round) 
        text_info = 'x={:.0f}(err{:.3f}) y={:.0f}(err{:.3f}) 1->{:.0f}cycles 0->{:.0f}cycles'.format(x, x_err, y, y_err, true_state, false_state)
        self.info.setValue(text_info)
        text_real_freq = '{:.6f} kHz'.format(toolbox.inst.kinjalk.give_real_frequency(x_round,y_round)/1e3)
        self.real_freq.setValue(text_real_freq)


class Generator2(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        opts['expanded'] = False
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Virtual','Zurich-MFLI_dev4199'], 'value':'None'})
        # self.addChild({'name': '1_frequency', 'type': 'float','value': generator_init_1['1_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})  
        self.addChild({'name': '2_frequency', 'type': 'float','value': generator_init_2['2_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        # self.addChild({'name': '3_frequency', 'type': 'float','value': generator_init_3['3_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        # self.addChild({'name': '4_frequency', 'type': 'float','value': generator_init_4['4_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)
        
    def instru_changed(self):
        child_list = self.children()
        for i in range(2, len(child_list)):
            self.removeChild(child_list[i])        
        if  'Zurich' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            # self.addChild({'name': '2_amplitude', 'type': 'float','value': generator_init_2['2_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'}) 
            self.addChild({'name': '2_time constant', 'type': 'float','value': generator_init_2['2_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '2_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_2['2_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '2_sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Zurich, 'value': generator_init_2['2_sensitivity'], 'tip':'in [A]'})
            self.addChild({'name': '2_gain', 'type': 'float','value': generator_init_2['2_gain'],'siPrefix': True, 'suffix': 'V/A','tip':'gain ch2'})
        if  'Ametek' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (1, freq_max_ametek),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            #self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if 'Kinjalk' in self.instru.value():
            self.addChild({'name': 'info', 'type': 'str', 'value': ''})
            self.addChild({'name': 'real frequency', 'type': 'str', 'value': ''})
            self.real_freq = self.param('real frequency')
            self.freq = self.param('frequency')
            self.freq.setValue(pulse_module_init['frequency'])
            self.info = self.param('info')
            self.ChangeDuty()
            self.freq.sigValueChanged.connect(self.ChangeDuty)
        if  'Virtual' in self.instru.value():
            self.addChild({'name': '2_time constant', 'type': 'float','value': generator_init_2['2_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '2_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_2['2_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '2_sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Zurich, 'value': generator_init_2['2_sensitivity'], 'tip':'in [A]'})
            self.addChild({'name': '2_gain', 'type': 'float','value': generator_init_2['2_gain'],'siPrefix': True, 'suffix': 'V/A','tip':'gain ch2'})
            

    def ChangeDuty(self):
        X, Y = toolbox.inst.kinjalk.XnY_modulator_frequency(frequency=self.freq.value(), duty=0.5)
        x, x_round, x_err = X
        y, y_round, y_err = Y 
        true_state, false_state = toolbox.inst.kinjalk.logic_state(x=x_round, y=y_round) 
        text_info = 'x={:.0f}(err{:.3f}) y={:.0f}(err{:.3f}) 1->{:.0f}cycles 0->{:.0f}cycles'.format(x, x_err, y, y_err, true_state, false_state)
        self.info.setValue(text_info)
        text_real_freq = '{:.6f} kHz'.format(toolbox.inst.kinjalk.give_real_frequency(x_round,y_round)/1e3)
        self.real_freq.setValue(text_real_freq)


class Generator3(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        opts['expanded'] = False
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Virtual','Zurich-MFLI_dev4199'], 'value':'None'})
        # self.addChild({'name': '1_frequency', 'type': 'float','value': generator_init_1['1_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})  
        # self.addChild({'name': '2_frequency', 'type': 'float','value': generator_init_2['2_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': '3_frequency', 'type': 'float','value': generator_init_3['3_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        # self.addChild({'name': '4_frequency', 'type': 'float','value': generator_init_4['4_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)
        
    def instru_changed(self):
        child_list = self.children()
        for i in range(2, len(child_list)):
            self.removeChild(child_list[i])        
        if  'Zurich' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            # self.addChild({'name': '3_amplitude', 'type': 'float','value': generator_init_3['3_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': '3_time constant', 'type': 'float','value': generator_init_3['3_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '3_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_3['3_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '3_gain', 'type': 'float','value': generator_init_3['3_gain'],'siPrefix': True, 'suffix': 'V/V','tip':'gain ch3'})
        if  'Ametek' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (1, freq_max_ametek),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            #self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if 'Kinjalk' in self.instru.value():
            self.addChild({'name': 'info', 'type': 'str', 'value': ''})
            self.addChild({'name': 'real frequency', 'type': 'str', 'value': ''})
            self.real_freq = self.param('real frequency')
            self.freq = self.param('frequency')
            self.freq.setValue(pulse_module_init['frequency'])
            self.info = self.param('info')
            self.ChangeDuty()
            self.freq.sigValueChanged.connect(self.ChangeDuty)
        if  'Virtual' in self.instru.value():
            self.addChild({'name': '3_time constant', 'type': 'float','value': generator_init_3['3_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '3_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_3['3_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '3_gain', 'type': 'float','value': generator_init_3['3_gain'],'siPrefix': True, 'suffix': 'V/V','tip':'gain ch3'})
            
    def ChangeDuty(self):
        X, Y = toolbox.inst.kinjalk.XnY_modulator_frequency(frequency=self.freq.value(), duty=0.5)
        x, x_round, x_err = X
        y, y_round, y_err = Y 
        true_state, false_state = toolbox.inst.kinjalk.logic_state(x=x_round, y=y_round) 
        text_info = 'x={:.0f}(err{:.3f}) y={:.0f}(err{:.3f}) 1->{:.0f}cycles 0->{:.0f}cycles'.format(x, x_err, y, y_err, true_state, false_state)
        self.info.setValue(text_info)
        text_real_freq = '{:.6f} kHz'.format(toolbox.inst.kinjalk.give_real_frequency(x_round,y_round)/1e3)
        self.real_freq.setValue(text_real_freq)

class Generator4(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        opts['expanded'] = False
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Virtual','Zurich-MFLI_dev4199'], 'value':'None'})
        # self.addChild({'name': '1_frequency', 'type': 'float','value': generator_init_1['1_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})  
        # self.addChild({'name': '2_frequency', 'type': 'float','value': generator_init_2['2_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        # self.addChild({'name': '3_frequency', 'type': 'float','value': generator_init_3['3_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': '4_frequency', 'type': 'float','value': generator_init_4['4_frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)
        
    def instru_changed(self):
        child_list = self.children()
        for i in range(2, len(child_list)):
            self.removeChild(child_list[i])        
        if  'Zurich' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (0, freq_max_zurich),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            # self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            # self.addChild({'name': '2_amplitude', 'type': 'float','value': generator_init_2['2_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            # self.addChild({'name': '3_amplitude', 'type': 'float','value': generator_init_3['3_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            # self.addChild({'name': '4_amplitude', 'type': 'float','value': generator_init_4['4_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            self.addChild({'name': '4_time constant', 'type': 'float','value': generator_init_4['4_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '4_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_4['4_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '4_gain', 'type': 'float','value': generator_init_4['4_gain'],'siPrefix': True, 'suffix': 'V/V','tip':'gain ch4'})
        if  'Ametek' in self.instru.value():
            #self.addChild({'name': 'frequency', 'type': 'float','value': generator_init['frequency'], 'step': 1e3, 'limits': (1, freq_max_ametek),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
            self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
            #self.addChild({'name': 'on', 'type': 'bool', 'value': generator_init['on'], 'tip': "ON when it is checked"})
        if 'Kinjalk' in self.instru.value():
            self.addChild({'name': 'info', 'type': 'str', 'value': ''})
            self.addChild({'name': 'real frequency', 'type': 'str', 'value': ''})
            self.real_freq = self.param('real frequency')
            self.freq = self.param('frequency')
            self.freq.setValue(pulse_module_init['frequency'])
            self.info = self.param('info')
            self.ChangeDuty()
            self.freq.sigValueChanged.connect(self.ChangeDuty)
        if  'Virtual' in self.instru.value():
            self.addChild({'name': '4_time constant', 'type': 'float','value': generator_init_4['4_time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': '4_harmonic','type':'list','values':[1,2,3,4], 'value':generator_init_4['4_harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
            self.addChild({'name': '4_gain', 'type': 'float','value': generator_init_4['4_gain'],'siPrefix': True, 'suffix': 'V/V','tip':'gain ch4'})

    def ChangeDuty(self):
        X, Y = toolbox.inst.kinjalk.XnY_modulator_frequency(frequency=self.freq.value(), duty=0.5)
        x, x_round, x_err = X
        y, y_round, y_err = Y 
        true_state, false_state = toolbox.inst.kinjalk.logic_state(x=x_round, y=y_round) 
        text_info = 'x={:.0f}(err{:.3f}) y={:.0f}(err{:.3f}) 1->{:.0f}cycles 0->{:.0f}cycles'.format(x, x_err, y, y_err, true_state, false_state)
        self.info.setValue(text_info)
        text_real_freq = '{:.6f} kHz'.format(toolbox.inst.kinjalk.give_real_frequency(x_round,y_round)/1e3)
        self.real_freq.setValue(text_real_freq)



# Generator for electrical charaterisation for lower excitation tension
class GeneratorEC(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320'], 'value': 'Virtual'})
        self.addChild({'name': '1_frequency', 'type': 'float','value': generator_init_1['1_frequency'], 'step': 1e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
        self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 1e-6, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
        self.addChild({'name': '1_on', 'type': 'bool', 'value': generator_init_1['1_on'], 'tip': "ON when it is checked"})

# AOF For the code AmplitudeOfModulation
class GeneratorAOM(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320'], 'value': 'Virtual'})
        self.addChild({'name': '1_frequency', 'type': 'float','value': generator_init_1['1_frequency'], 'step': 1e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})        
        self.addChild({'name': '1_amplitude', 'type': 'float','value': generator_init_1['1_amplitude'], 'step': 50e-3, 'limits': (0, amp_max),'siPrefix': True, 'suffix': 'V'})
        self.addChild({'name': '1_on', 'type': 'bool', 'value': generator_init_1['1_on'], 'tip': "ON when it is checked"})
        self.addChild({'name': 'amplitude min', 'type': 'float','value': 1e-3, 'step': 1, 'limits': (0, 1),'siPrefix': True, 'suffix': 'V','decimals':6})
        self.addChild({'name': 'amplitude max', 'type': 'float','value': 5e-3, 'step': 1, 'limits': (0, 1),'siPrefix': True, 'suffix': 'V','decimals':6})
        self.addChild({'name': 'nbr pts', 'type': 'int','value': 100, 'step': 100, 'limits': (5, 100e3),'siPrefix': False, 'tip':'number of points use for the frequecy sweep'})
        self.addChild({'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 1, 'tip':'number of sequences, number of time frequecy sweep is running'})
        self.addChild({'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'})        
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'average', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'}) 

class GraphList(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'legend curve 1', 'type': 'str', 'value': ''})

class FitEC(pTypes.GroupParameter,MonParametre):
    # fir for electrical characterization
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'f0', 'type': 'float','value': 32e3, 'step': 10, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})     
        self.addChild({'name': 'Q', 'type': 'float','value': 10000, 'step': 10, 'limits': (1, 1e6),'siPrefix': False})
        self.addChild({'name': 'max res', 'type': 'float','value': 10000, 'step': 10,'siPrefix': False})
        self.addChild({'name': 'C0', 'type': 'float','value': 1e-12, 'step': 100, 'limits': (0, 1e-3), 'suffix': 'F','siPrefix': True,'tip':'parasitic capacitor'})
        self.addChild({'name': 'freq min', 'type': 'float','value': freq_min_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'freq max', 'type': 'float','value': freq_max_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'guess', 'type': 'action'})
        self.addChild({'name': 'fit', 'type': 'action'})
        self.addChild({'name': 'plot', 'type': 'action'})

class FitFS(pTypes.GroupParameter,MonParametre):
    # fir for frequency sweep
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        # self.addChild({'name': 'resonance frequency', 'type': 'float','value': 32e3, 'step': 10, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'initial guess for amp', 'type': 'float','value': 1e-3, 'step': 1,'siPrefix': True, 'suffix': 'Vrms'})    
        self.addChild({'name': 'initial guess for f0', 'type': 'float','value': 10000, 'step': 10, 'limits': (1, 1e6),'siPrefix': True, 'suffix': 'Hz'}) 
        self.addChild({'name': 'initial guess for Q', 'type': 'float','value': 300, 'step': 10, 'limits': (1, 1e6),'siPrefix': False})
        self.addChild({'name': 'value of C0', 'type': 'float','value': 1e-12, 'step': 0.1e-12, 'limits': (0, 1e-6), 'suffix': 'F','siPrefix': True,'tip':'parasitic capacitor'})
        self.addChild({'name': 'freq min', 'type': 'float','value': freq_min_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'freq max', 'type': 'float','value': freq_max_acq, 'step': 10e3, 'limits': (1, 500e3),'siPrefix': True, 'suffix': 'Hz','decimals':6})
        self.addChild({'name': 'full width', 'type': 'action'})
        self.addChild({'name': 'fit MAG', 'type': 'action'})
        self.addChild({'name': 'fit PHASE', 'type': 'action'})

class Input(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        #time_cst = 50e-3 #100e-3 # time cst use for the acquisition
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Virtual','Zurich-MFLI_dev4199'], 'value': 'None'})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)        

    def instru_changed(self):
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])     
        if  'Ametek' in self.instru.value():
            self.addChild({'name': 'time constant', 'type': 'list','value': input_init['time constant'], 'values':list_time_constant_LIA,'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': 'sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Ametek, 'value': input_init['sensitivity'], 'tip':'in [V]'})
            self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
        if 'Keithley' in self.instru.value():
            self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'], 'step': 100e-3, 'limits': (keithley_time_cst_min, 10),'siPrefix': True, 'suffix': 's'})
            self.addChild({'name': 'range Melles Griot', 'type': 'list', 'values': [3,10,30,100,300,1000,3000], 'value': 3,'tip':'connect the powermeter on the sense ohm of the Keithley. Range setting has to be done manually. ATTENTION Range 10 correspond to the value 3, then range 10 correspond to value 30 on the gear...'})
        if 'Virtual' in self.instru.value(): # harmonic is needed for sauvegarder.py
            self.addChild({'name': 'poll length', 'type': 'list','values': [0.002,0.005,0.01,0.05,0.1,0.5,1],'siPrefix': True, 'suffix': 's','value':input_init['poll length'],'tip':'use it to determine the temporal gap between successive points the recommeded min to use is 0.002 i.e 0.002*10=20ms'})
            # self.addChild({'name': 'time constant', 'type': 'float','value': 5e-3,'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})   
            # self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})
        if  'Zurich' in self.instru.value():
            # self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'],'siPrefix': True, 'suffix': 's','tip':'In [S] / For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
            self.addChild({'name': 'filter order','type':'list','values':[1,2,3,4,5,6,7,8], 'value':input_init['filter order'],'tip':'low pass filter order'})
            # NB : Poll length is not used in Allan code
            self.addChild({'name': 'poll length', 'type': 'list','values': [0.002,0.005,0.01,0.05,0.1,0.5,1],'siPrefix': True, 'suffix': 's','value':input_init['poll length'],'tip':'use it to determine the temporal gap between successive points the recommeded min to use is 0.002 i.e 0.002*10=20ms'})
            # self.addChild({'name': 'sensitivity', 'type': 'list', 'values': list_sensitivity_LIA_Zurich, 'value': input_init['sensitivity'], 'tip':'in [V]'})
            # self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})

# # Input Allan the same that input without poll length
# class InputAllan(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199','Zurich-MFLI_dev4320'], 'value': 'Virtual'})
#         self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'], 'step': 100e-3, 'limits': (time_cst_min, 10),'siPrefix': True, 'suffix': 's','tip':'For MFLI never use value bellow 50ms as the acquisition is at 10ms'})
#         self.addChild({'name': 'filter order','type':'list','values':[1,2,3,4,5,6,7,8], 'value':input_init['filter order'],'tip':'low pass filter order'})
#         self.addChild({'name': 'sensitivity', 'type': 'list', 'values': list_sensitivity_LIA, 'value': input_init['sensitivity']})
#         self.addChild({'name': 'harmonic','type':'list','values':[1,2,3,4], 'value':input_init['harmonic'],'tip':'The frequency has to be changed manualy. Do not forget to make a frequency sweep to find your resonance frequnce for the desire harmonic'})

# class InputPV2I(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         #time_cst = 50e-3 #ATTENTION don't use value below 10e-3 the recording time # time cst use for the acquisition
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Keithley_2010Multimeter'], 'value': 'Virtual'})
#         self.addChild({'name': 'time constant', 'type': 'float','value': input_init['time constant'], 'step': 100e-3, 'limits': (time_cst_min, 10),'siPrefix': True, 'suffix': 's'})
#         self.addChild({'name': 'range Melles Griot', 'type': 'list', 'values': [3,10,30,100,300,1000,3000], 'value': 3,'tip':'connect the powermeter on the sense ohm of the Keithley. Range setting has to be done manually. ATTENTION Range 10 correspond to the value 3, then range 10 correspond to value 30 on the gear...'})

class LaserDopplerVibrometer(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild({'name': 'decoder', 'type': 'list','values':['Unknow','VD06'],'value': 'Unknow'})
        self.addChild({'name': 'sensitivity', 'type': 'list','values':['Unknow','1mm/s/V (20kHz)',
                        '2mm/s/V LP (100kHz)','2mm/s/V (350kHz)',
                        '10mm/s/V LP (100kHz)','10mm/s/V (350kHz)',
                        '50mm/s/V LP (100kHz)','50mm/s/V (350kHz)'],
                        'value': 'Unknow'})

class LaserDriver(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        opts['expanded'] = False
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Ilx_driver','Thorlabs_ITC4002QCL_M00907387','Thorlabs_ITC4002QCL_M00560075','Thorlabs_ITC4002QCL_M00660255','Thorlabs_ITC4005QCL_M00603635','Thorlabs_LDC4005_M00440716','Thorlabs_ITC4001QCL_M00468220'], 'value': 'Virtual'})
        self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
        self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 750),'siPrefix': True, 'suffix': 'mA','decimals':6})
        
        
       
        # self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30,30),'siPrefix': True, 'suffix': '°C','decimals':3})
        # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed) 
        
    def instru_changed(self):
        child_list = self.children()
        for i in range(3, len(child_list)):
            self.removeChild(child_list[i]) 
            
        if  'Thorlabs_ITC4002QCL_M00907387' in self.instru.value(): 
            self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init_1['temperature'], 'step': 1, 'limits': (-35,35),'siPrefix': True, 'suffix': '°C','decimals':3})
            

        if  'Thorlabs_ITC4002QCL_M00660255' in self.instru.value():   
            
            self.addChild({'name': 'go up Ilaser 4002QCL_M00660255', 'type': 'action', 'tip': "go up to laser current"})
            self.addChild({'name': 'go down Ilaser 4002QCL_M00660255', 'type': 'action', 'tip': "go down from laser current"})
            self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init_1['temperature'], 'step': 1, 'limits': (-35,35),'siPrefix': True, 'suffix': '°C','decimals':3})
            
        if  'Thorlabs_LDC4005_M00440716' in self.instru.value():
             self.addChild({'name': 'go up Ilaser LDC4005_M00440716', 'type': 'action', 'tip': "go up to laser current"})
             self.addChild({'name': 'go down Ilaser LDC4005_M00440716', 'type': 'action', 'tip': "go down from laser current"})
         
        if  'Thorlabs_ITC4002QCL_M00560075' in self.instru.value():   
            
            self.addChild({'name': 'go up Ilaser 4002QCL_M00560075', 'type': 'action', 'tip': "go up to laser current"})
            self.addChild({'name': 'go down Ilaser 4002QCL_M00560075', 'type': 'action', 'tip': "go down from laser current"})
            self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init_3['temperature'], 'step': 1, 'limits': (-35,35),'siPrefix': True, 'suffix': '°C','decimals':3})
            
        if  'Thorlabs_ITC4005QCL_M00603635' in self.instru.value():   
            
            self.addChild({'name': 'go up Ilaser ITC4005QCL_M00603635', 'type': 'action', 'tip': "go up to laser current"})
            self.addChild({'name': 'go down Ilaser ITC4005QCL_M00603635', 'type': 'action', 'tip': "go down from laser current"})
            self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init_4['temperature'], 'step': 1, 'limits': (-35,35),'siPrefix': True, 'suffix': '°C','decimals':3})
    

# PA For photoacoustic
class LaserDriverPA(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):       
        MonParametre.__init__(self) # get method from MonParametre
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Ilx_driver','Thorlabs_ITC4002QCL_M00907387','Thorlabs_ITC4002QCL_M00560075','Thorlabs_ITC4002QCL_M00660255','Thorlabs_ITC4005QCL_M00603635','Thorlabs_LDC4005_M00440716','Thorlabs_ITC4001QCL_M00468220'], 'value': 'Virtual'})
        self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
        self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 750),'siPrefix': True, 'suffix': 'mA','decimals':6})
        self.addChild({'name': 'current min', 'type': 'float','value': 1, 'step': 1, 'limits': (1e-1, 750),'siPrefix': True, 'suffix': 'mA','decimals':6})
        self.addChild({'name': 'current max', 'type': 'float','value': 5, 'step': 1, 'limits': (1e-1, 750),'siPrefix': True, 'suffix': 'mA','decimals':6})
        self.addChild({'name': 'nbr pts', 'type': 'int','value': 5, 'step': 100, 'limits': (5, 100e3),'siPrefix': False, 'tip':'number of points use for the frequecy sweep'})
        self.addChild({'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 1, 'tip':'number of sequences, number of time frequecy sweep is running'})
        self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-35, 35),'siPrefix': True, 'suffix': '°C','decimals':3})
        # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})
        # self.addChild({'name': 'internal modulation (Square signal)', 'type': 'bool','value': False, 'tip': "ON to deliver a square signal modulation on Thorlabs laser"})
        # self.addChild({'name': 'internal frequency modulation', 'type': 'float','value': 100, 'step': 0.001, 'limits': (0.05,100),'siPrefix': True, 'suffix': 'kHz','decimals':6})
        # self.addChild({'name': 'internal amplitude modulation', 'type': 'float','value': 1, 'step': 1, 'limits': (1, 100),'siPrefix': True, 'suffix': '%','decimals':6})  
        self.addChild({'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'})
        self.addChild({'name': 'start', 'type': 'action'})
        self.addChild({'name': 'average', 'type': 'action'})
        self.addChild({'name': 'clear all', 'type': 'action'})

# # AOF For the code AmplitudeOfModulation
# class LaserDriverAOM(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Thorlabs_ITC4002QCL'], 'value': 'Virtual'})
#         self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
#         self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 400),'siPrefix': True, 'suffix': 'mA','decimals':6})
#         self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30, 30),'siPrefix': True, 'suffix': '°C','decimals':3})
#         # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})
#         # self.addChild({'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],'value': 3, 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'})

# # For AllanDeviation
# class LaserDriverAD(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Thorlabs_ITC4002QCL'], 'value': 'Virtual'})
#         self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
#         self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 400),'siPrefix': True, 'suffix': 'mA','decimals':6})
#         self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30, 30),'siPrefix': True, 'suffix': '°C','decimals':3})
#         # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})


# # FS For frequency sweep
# class LaserDriverFS(pTypes.GroupParameter,MonParametre):
#     def __init__(self, **opts):       
#         MonParametre.__init__(self) # get method from MonParametre
#         opts['type'] = 'bool'
#         opts['value'] = True
#         pTypes.GroupParameter.__init__(self, **opts)
#         self.addChild({'name': 'instrument', 'type': 'list', 'values': ['Virtual','Thorlabs_ITC4002QCL'], 'value': 'Virtual'})
#         self.addChild({'name': 'on', 'type': 'bool', 'value': laser_driver_init['on'], 'tip': "ON when it is checked"})
#         self.addChild({'name': 'current', 'type': 'float','value': laser_driver_init['current'] , 'step': 1, 'limits': (1e-1, 400),'siPrefix': True, 'suffix': 'mA','decimals':6})
#         self.addChild({'name': 'temperature', 'type': 'float','value': laser_driver_init['temperature'], 'step': 1, 'limits': (-30,30),'siPrefix': True, 'suffix': '°C','decimals':3})
#         # self.addChild({'name': 'modulation', 'type': 'bool','value': False, 'tip': "ON to activate the modualtion"})

class OneCurve(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        opts['removable'] = True
        opts['renamable'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'file name', 'type': 'str', 'value': 'curve 1'})
        self.addChild({'name': 'color', 'type': 'color', 'value': "FF0", 'tip': "This is a color button"})

class PulseModule(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        self.addChild({'name': 'instrument', 'type': 'list', 'values': ['None','Kinjalk_pulse_generator','PICO_pulse_generator'], 'value': 'None'})
        self.instru = self.param('instrument')
        self.instru.sigValueChanged.connect(self.instru_changed)        

    def instru_changed(self):
        #carrier_freq = [1/(nbr_cycle_per_periode*i*clock_cycle) for i in [4,2,1]]
        child_list = self.children()
        for i in range(1, len(child_list)):
            self.removeChild(child_list[i])        
        if 'Kinjalk' in self.instru.value():
            one_cycle =  toolbox.inst.kinjalk.give_clock_cycle()
            self.addChild({'name': 'pulse width', 'type': 'list','values': [(3*x-1)*one_cycle*1e9 for x in range(2,24,2) ] ,'siPrefix': False, 'suffix': 'ns', 'tip' : 'in ns'}) 
            self.addChild({'name': 'carrier frequency', 'type': 'float','value': pulse_module_init['carrier frequency'], 'limits': (10e3, 5e6) , 'step':5e3, 'siPrefix': True, 'suffix': 'Hz'}) 
            self.addChild({'name': 'duty cycle', 'type': 'str', 'value': 'nan'}) 
            #self.addChild({'name': 'duty cycle', 'type': 'list','values': [50, 1, 0.1, 0.01, 1e-3, 1e-4] ,'siPrefix': False, 'suffix': '%', 'tip':'in %'})         
            #self.addChild({'name': 'carrier frequency', 'type': 'str', 'value': '0 MHz'})
            self.addChild({'name': 'info', 'type': 'str', 'value': ''})
            self.pulse = self.param('pulse width')
            self.duty = self.param('duty cycle')
            self.carrier = self.param('carrier frequency')
            self.info = self.param('info')
            self.ChangeDuty()
            #self.modulation = self.param('modulation frequency')
            self.pulse.sigValueChanged.connect(self.ChangeDuty)
            self.carrier.sigValueChanged.connect(self.ChangeDuty)
        if 'PICO' in self.instru.value():
            self.addChild({'name': 'carrier frequency', 'type': 'float','value': pulse_module_init['carrier frequency'], 'limits': (1, 5e6) , 'step':5e3, 'siPrefix': True, 'suffix': 'Hz'}) 
            self.addChild({'name': 'duty cycle', 'type': 'float','value': pulse_module_init['duty cycle'], 'limits': (0, 100) , 'step':1, 'siPrefix': True, 'suffix': '%'}) 
            self.addChild({'name': 'pulse width', 'type': 'str', 'value': 'nan'}) 
            self.pulse = self.param('pulse width')
            self.duty = self.param('duty cycle')
            self.carrier = self.param('carrier frequency')
            self.ChangePulse()
            self.duty.sigValueChanged.connect(self.ChangePulse)
            self.carrier.sigValueChanged.connect(self.ChangePulse)

    def ChangeDuty(self):
        pulse = self.pulse.value()
        freq = self.carrier.value()
        duty = pulse*1e-9*freq
        text_duty = '{:.3g} %'.format(duty*100)
        X, Y = toolbox.inst.kinjalk.XnY_carrier_frequency(pulse_width=pulse, duty_cycle=duty)
        x, _, _ = X
        y, y_round, y_err = Y  
        true_state, false_state = toolbox.inst.kinjalk.logic_state(x=x, y=y_round) 
        text_info = 'x={:.0f} y={:.0f}(err{:.3f}) 1->{:.0f}cycles 0->{:.0f}cycles'.format(x, y, y_err,true_state, false_state)
        self.duty.setValue(text_duty)
        self.info.setValue(text_info)

    def ChangePulse(self):
        duty= self.duty.value()
        freq = self.carrier.value()
        pulse = 1/freq*duty/100
        text_pulse = '{:.3g} ns'.format(pulse*1e9)
        self.pulse.setValue(text_pulse)
        
        

    
    

class  Save(pTypes.GroupParameter,MonParametre):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True
        pTypes.GroupParameter.__init__(self, **opts)
        path_info = ''
        

        # print(flag_value)
        
        self.addChild({'name': 'setup description', 'type': 'text', 'value': 'Some text...'})
       
        self.addChild({'name': 'save', 'type': 'action', 'tip':'N.B: click on stop fisrt'})
        self.addChild({'name': 'path', 'type': 'list', 'values': [path_WINDOWS], 'value': path})
        # self.addChild({'name': 'path', 'type': 'list', 'values': [path_WINDOWS,path_MACOS], 'value': path})
        self.addChild({'name': 'path info', 'type': 'str', 'value': path_info})
        # self.addChild({'name': 'laser', 'type': 'list', 'values': list_laser, 'value': 'Unknow'})
        # self.addChild({'name': 'amplifier', 'type': 'list', 'values': list_amplifier, 'value': 'Unknow'})
        # self.addChild({'name': 'gas', 'type': 'list', 'values': list_gas, 'value': 'Unknow'})
        self.addChild({'name': 'design', 'type': 'list', 'values': list_design, 'value': 'QTF'})
        # self.addChild({'name': 'acoustic resonator', 'type': 'list', 'values': list_Acoustic_resonator, 'value': 'None'})            
        
        # self.addChild({'name': 'resonator', 'type': 'list', 'values': list_QTF, 'value': 'Unknow'})
        
        
        
        self.design = self.param('design')
        self.design.sigValueChanged.connect(self.design_changed)    
        


    def design_changed(self):
        child_list = self.children()
        for i in range(9, len(child_list)):
            self.removeChild(child_list[i])  
        if self.design.value() in 'QTF':
            self.addChild({'name': 'resonator', 'type': 'list', 'values': list_QTF, 'value': 'Unknow'})
        # if self.design.value() == 'H-Res_75um':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_HRes_75um, 'value': 'Unknow'})
        #     self.addChild({'name': 'device', 'type': 'list', 'values': list_HRes_75um_device, 'value': 'Unknow'})
        # if self.design.value() == 'H-Res_75um_V2':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_HRes_75um_V2, 'value': 'Unknow'})
        #     self.addChild({'name': 'device', 'type': 'list', 'values': list_HRes_75um_V2_device, 'value': 'Unknow'})            
        # if self.design.value() in 'Hsquare-Res_75um':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Hsquare_Res_75um, 'value': 'Unknow'})
        #     self.addChild({'name': 'device', 'type': 'list', 'values': list_Hsquare_Res_75um_device, 'value': 'Unknow'})
        # if self.design.value() in 'Hsquare-Res_75um_V3':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Hsquare_Res_75um_V3, 'value': 'Unknow'})
        #     self.addChild({'name': 'device', 'type': 'list', 'values': list_Hsquare_Res_75um_device, 'value': 'Unknow'})   
        # if self.design.value() in 'Hsquare-Res_75um_V4':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Hsquare_Res_75um_V4, 'value': 'Unknow'})
        #     self.addChild({'name': 'device', 'type': 'list', 'values': list_Hsquare_Res_75um_device, 'value': 'Unknow'})    
        # if self.design.value() in 'Hsquare-Res_100um':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_HSRes_100um, 'value': 'Unknow'})
        #     self.addChild({'name': 'device', 'type': 'list', 'values': list_HSRes_100um_device, 'value': 'Unknow'})
        # if self.design.value() in 'Acoustic_resonator':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Acoustic_resonator, 'value': 'Unknow'})
        #     self.addChild({'name': 'acoustic_sensor', 'type': 'list', 'values': list_Acoustic_sensor, 'value': 'Unknow'})
        # if self.design.value() in 'Menbrane':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_menbrane, 'value': 'Unknow'})            
        # if self.design.value() in 'Cantilever':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_Cantilever, 'value': 'Unknow'}) 
        # if self.design.value() in 'Cantilever':
        #     self.addChild({'name': 'resonator', 'type': 'list', 'values': list_others, 'value': 'Unknow'}) 
from PyQt5 import QtWidgets, QtGui, QtCore  # Add QtCore import          

class RedNameDelegate(QtWidgets.QStyledItemDelegate):
    def initStyleOption(self, option, index):
        super().initStyleOption(option, index)
        if index.column() == 0:  # Assuming the name column is the first column
            text = index.data()
           
                
                
            if text and 'QEPAS' in text:
                option.font.setPointSize(13)  # Set font size to 14
                option.font.setBold(True)
                option.palette.setColor(QtGui.QPalette.Text, QtGui.QColor('red'))
                # option.displayAlignment = QtCore.Qt.AlignCenter
                
            elif index.parent().isValid():
                option.font.setPointSize(12)  # Set font size to 10 for child items
                option.displayAlignment = QtCore.Qt.AlignLeft  # Align left for child items
                




##############################################################################
##############################################################################
## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    ## Create tree of Parameter objects
    
    params = [Arduino(name='Arduino'),
              
              Medisoft_expair(name='Medisoft'),
              
              Input(name='Input'),
              
              QEPAS(name='QEPAS_1:'),
              Generator1(name='Generator_1'),
              
              Generator2(name='Generator_2'),
              Generator3(name='Generator_3'),
              Generator4(name='Generator_4'),
              Generator_extern(name='Gen_ext_1'),
              AllPlotScalableGroup(name='Graph'),
              PulseModule(name='Pulse module'),
              Save(name='Save')
              
              ]
    
    
    

    p = Parameter.create(name='params', type='group', children=params)

    ## Create two ParameterTree widgets, both accessing the same data
    t = ParameterTree()
    t.setParameters(p, showTop=False)
    t.setWindowTitle('pyqtgraph example: Parameter Tree')
    
    
    # Apply the delegate
    delegate = RedNameDelegate(t)
    t.setItemDelegateForColumn(0, delegate)


    win = QtWidgets.QWidget()
    layout = QtWidgets.QGridLayout()
    win.setLayout(layout)
    layout.addWidget(t, 1, 0, 1, 1)  #widget added at row 0, column 0, with a row span of 1 and a column span of 2:
    win.show()
    win.resize(800,800)

    pg.exec()

    # import sys
    # if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    #     QtGui.QApplication.instance().exec_()

##############################################################################
##############################################################################


#### OLD VERSION #########

# def build_dico(dico):

#   '''
#   This dictionary define all the parametre tree use for the GUI 
#   (graphic utilisator interface)
#   The dico here is use for the inital value
#   '''


#   p_global = [{'name': 'Input', 'type': 'group',
#                'children': [{'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199'], 'value': dico['Input']['instrument']},
#                             {'name': 'time constant', 'type': 'float',
#                              'value': dico['Input']['time constant'], 'step': 100e-3, 'limits': (400e-9, 10),
#                              'siPrefix': True, 'suffix': 's'},
#                              {'name': 'recording time', 'type': 'float',
#                              'value': dico['Input']['recording time'], 'step': 10e-3, 'limits': (10e-3, dico['Input']['time constant']),
#                              'siPrefix': True, 'suffix': 's','tip':'data acquisition time, then averaged'},
#                              {'name': 'sensitivity', 'type': 'list', 'values': [1e-3,3e-3,1e-2,3e-2,1e-1,3e-1,1,3], 'value': dico['Input']['sensitivity']},
#                             ]},
#               {'name': 'Frequency sweep', 'type': 'group',
#                'children': [{'name': 'generator', 'type': 'group', 'children': [
#                             {'name': 'instrument', 'type': 'list', 'values': ['Virtual','Zurich-MFLI_dev4199'], 'value': dico['Frequency sweep']['generator']['instrument']},
#                             {'name': 'amplitude', 'type': 'float','value': dico['Frequency sweep']['generator']['amplitude'], 'step': 50e-3, 'limits': (0, 1),'siPrefix': True, 'suffix': 'V'},
#                             {'name': 'on', 'type': 'bool', 'value': dico['Frequency sweep']['generator']['on'], 'tip': "ON when it is checked"},
#                             ]},
#                             {'name': 'acquisition', 'type': 'group', 'children': [
#                             {'name': 'freq min', 'type': 'float',
#                              'value': dico['Frequency sweep']['acquisition']['freq min'], 'step': 10e3, 'limits': (1, 500e3),
#                              'siPrefix': True, 'suffix': 'Hz','decimals':6},
#                              {'name': 'freq max', 'type': 'float',
#                              'value': dico['Frequency sweep']['acquisition']['freq max'], 'step': 10e3, 'limits': (1, 500e3),
#                              'siPrefix': True, 'suffix': 'Hz','decimals':6},
#                              {'name': 'nbr pts', 'type': 'int',
#                              'value': dico['Frequency sweep']['acquisition']['nbr pts'], 'step': 100, 'limits': (10, 100e3),
#                              'siPrefix': False, 'tip':'number of points use for the frequecy sweep'},
#                              {'name': 'nbr seqs', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],
#                              'value': dico['Frequency sweep']['acquisition']['nbr seqs'], 'tip':'number of sequences, number of time frequecy sweep is running'},
#                              {'name': 'waiting time', 'type': 'list','values':[1,2,3,4,5,6,7,8,9,10],
#                              'value': dico['Frequency sweep']['acquisition']['waiting time'], 'tip':'Time time between two measurements. It is equal to time cosntant multiply by this factor'},
#                              {'name': 'start', 'type': 'action'},
#                              {'name': 'average', 'type': 'action'},
#                              {'name': 'clear all', 'type': 'action'}
#                              ]}
#                           ]},
#               {'name': 'Save', 'type': 'group',
#                'children': [{'name': 'path', 'type': 'str', 'value': dico['Save']['path']},
#                             {'name': 'path info', 'type': 'str', 'value': dico['Save']['path info']},
#                             {'name': 'sample name', 'type': 'str', 'value': dico['Save']['sample name']},
#                             {'name': 'sample info', 'type': 'str', 'value': dico['Save']['sample info']},
#                             {'name': 'save', 'type': 'action'}                 
#                           ]}
#               ]
#   return p_global
