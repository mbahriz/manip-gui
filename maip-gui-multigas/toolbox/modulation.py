import pandas
from pathlib import Path, PureWindowsPath
import numpy as np
from scipy import interpolate
import toolbox.function

class Convertion4002():
    def __init__(self):
        f = self.interpolated_curve()
        self.freq = np.linspace(self.freq_min,self.freq_max,int(1e5))
        self.H2V = f(self.freq)
        self.H2I = 1/self.H2V

    def get_calibration_file(self):
        data_folder = Path("./toolbox/calibration/mod_ampl_calibration_Thorlabs_Zurich")
        file_to_open = data_folder / "231023_ITC4002_100mA_100mVpk_1f_1987Hz_1-300kHz.txt"
        # Convert path to Windows format
        file_to_open_on_windows = PureWindowsPath(file_to_open)
        # print(file_to_open_on_windows)
        # Zurich data
        zd_100 = pandas.read_csv(file_to_open,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
        zd_100['I'] = 2*np.sqrt(2)*zd_100['Vrms']/10.2
        zd_100['Inormalized'] = zd_100['I']/100e-3
        self.freq_min = min(zd_100['Freq'])
        self.freq_max = max(zd_100['Freq'])
        return zd_100['Freq'], zd_100['Inormalized']

    def interpolated_curve(self):
        dataX, dataY = self.get_calibration_file()
        f = interpolate.interp1d(dataX, dataY)
        return f
    
    def give_I(self,frequency,tension):
        idx = toolbox.function.find_nearest_index(self.freq,frequency)
        current = self.H2V[idx]*tension
        return current

    def give_U(self,frequency,current):
        idx = toolbox.function.find_nearest_index(self.freq,frequency)
        tension = self.H2I[idx]*current
        return tension

class Convertion4005():
    def __init__(self):
        f = self.interpolated_curve()
        self.freq = np.linspace(self.freq_min,self.freq_max,int(1e5))
        self.H2V = f(self.freq)
        self.H2I = 1/self.H2V

    def get_calibration_file(self):
        data_folder = Path("./toolbox/calibration/mod_ampl_calibration_Thorlabs_Zurich")
        file_to_open = data_folder / "231023_ITC4005_100mA_100mVpK_1f_5974Hz_5-300kHz.txt"
        # Convert path to Windows format
        file_to_open_on_windows = PureWindowsPath(file_to_open)
        # print(file_to_open_on_windows)
        # Zurich data
        zd_100 = pandas.read_csv(file_to_open,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
        zd_100['I'] = 2*np.sqrt(2)*zd_100['Vrms']/10.2
        zd_100['Inormalized'] = zd_100['I']/100e-3
        self.freq_min = min(zd_100['Freq'])
        self.freq_max = max(zd_100['Freq'])
        return zd_100['Freq'], zd_100['Inormalized']

    def interpolated_curve(self):
        dataX, dataY = self.get_calibration_file()
        f = interpolate.interp1d(dataX, dataY)
        return f
    
    def give_I(self,frequency,tension):
        idx = toolbox.function.find_nearest_index(self.freq,frequency)
        current = self.H2V[idx]*tension
        return current

    def give_U(self,frequency,current):
        idx = toolbox.function.find_nearest_index(self.freq,frequency)
        tension = self.H2I[idx]*current
        return tension

