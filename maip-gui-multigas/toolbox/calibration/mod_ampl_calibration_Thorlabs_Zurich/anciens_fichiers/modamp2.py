import pandas
import matplotlib.pyplot as plt
import numpy as np

file_name = '25mVpk_freq_sweep_32kHz_1-300kHz_current_modlation.txt'
# Zurich data
zd_25 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_25['Normalized'] = zd_25['Vrms']/25e-3

file_name = '50mVpk_freq_sweep__1-300kHz_current_modlation.txt'
# Zurich data
zd_50 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_50['Normalized'] = zd_50['Vrms']/50e-3

file_name = '75mVpk_freq_sweep__1-300kHz_current_modlation.txt'
# Zurich data
zd_75 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_75['Normalized'] = zd_75['Vrms']/75e-3

file_name = '100mVpk_freq_sweep__1-300kHz_current_modlation.txt'
# Zurich data
zd_100 = pandas.read_csv(file_name,sep=';',skiprows=[0,1,2,3,4],names=['Freq','Vrms'])
zd_100['Normalized'] = zd_100['Vrms']/100e-3

fig, _ = plt.subplots()
gridsize = (1, 1)
ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
ax1.plot(zd_25['Freq']*1e-3,2*np.sqrt(2)/10*zd_25['Vrms'],label='Zurich output 25mVpk')
ax1.plot(zd_50['Freq']*1e-3,2*np.sqrt(2)/10*zd_50['Vrms'],label='Zurich output 50mVpk')
ax1.plot(zd_75['Freq']*1e-3,2*np.sqrt(2)/10*zd_75['Vrms'],label='Zurich output 75mVpk')
ax1.plot(zd_100['Freq']*1e-3,2*np.sqrt(2)/10*zd_100['Vrms'],label='Zurich output 100mVpk')
ax1.set_xlabel('frequency [kHz]', labelpad=0)
ax1.set_ylabel(r'Current [Appk]', labelpad=0)
ax1.legend(loc=0)


fig.savefig('mod_ampl_calibration_Thorlabs_Zurich2.png')

plt.show()