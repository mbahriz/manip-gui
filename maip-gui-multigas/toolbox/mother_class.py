import toolbox.instrument
import toolbox.parameter_tree


# import time

# import instrument
# import parameter_tree

import matplotlib.pyplot as plt 
import numpy as np
from pyqtgraph.Qt import QtWidgets
import scipy 

class MotherMainWindow():
    def closeEvent(self, event):
        result = QtWidgets.QMessageBox.question(self,
                                                "Confirm Exit...",
                                                "Do you want to exit ?",
                                                (QtWidgets.QMessageBox.Yes |
                                                 QtWidgets.QMessageBox.No))
        if result == QtWidgets.QMessageBox.Yes:
            # permet d'ajouter du code pour fermer proprement
            # close all instrument
            MotherInstru.close_all_inst()
            event.accept()
        else:
            event.ignore()
        pass

    def color_map(self,number_color):
        number_color
        cmap = plt.get_cmap('terrain')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]
        return colors 

    def count_the_number_of_ghost_curves(self):
        # count number of ghost
        # fit has been added for the fit curves of the frequency sweep
        graph_1 = self.obj_graph['plot']['curve']
        nbr_ghost = 0
        for k in graph_1.curves:
            if ('ghost' in k) and ('fit' not in k):
                nbr_ghost += 1
        return nbr_ghost 

    def message_box(self,text):
        QtWidgets.QMessageBox.information(self,'message',text,QtWidgets.QMessageBox.Ok)
        return

    def stop_timer(self):
        """
        methode pour arrêter le timer s'il est en route
        """
        # si l'attribut 'my_timer' n'est pas None
        if self.my_timer is not None:
            # alors on arrête le timer
            self.killTimer(self.my_timer)
            self.my_timer = None


class MotherInstru():
    def set_value(self,inst,what,value):
        self.connect_inst(inst)
        self.use[inst].set_value(what,value)
        

    def connect_inst(self,inst):
        # check if the instrument is already connected if not connect it
        if inst not in self.connected:
            print('%s has not been created'%inst)
            # create instrument
            self.use[inst] = toolbox.instrument.Instrument(name=inst)
            # add its named in the list of created instrument
            self.connected.append(inst)
            print('Instrument connected :%s'%(self.connected))

    # def update(self,who,inst):
    #     # update value from the parameter tree to the instrument
    #     self.connect_inst(inst)
    #     A = self.param_tree.give_all_inst_value(who)
    #     for k in A:
    #         self.set_value(inst,k[0],k[1])
    #     pass

    def update(self,who,inst):
        # update value from the parameter tree to the instrument
        self.connect_inst(inst)
        dico = toolbox.parameter_tree.instrument_initials_values(who)
        for k in dico:
            self.set_value(inst,k,dico[k])
        pass

    def get_data(self,inst,poll_length):
        return self.get_X_Y_R_Phi(inst,poll_length)

    def get_X_Y_R_Phi(self,inst,poll_length,freq=0):
        self.connect_inst(inst)
        return self.use[inst].poll(poll_length=poll_length,x=freq)    
    
    def receive(self,inst):
        return self.use[inst].receive()
    
    def close_all_inst(self):
          dico = self.connected
          for k in dico:
              self.use[k].disconnect()
              
    # def close_medisoft(self):
    #    self.use['Medisoft'].disconnect()
    # def open_new_medisoft(self):
    #     self.use['Medisoft']
        
         
        

class MotherParam():
    def give(self,who,what):
        # return the value of 'what' from 'who' in the parameter tree
        return self.dico[who].param(what).value()

    def give_inst(self,who):
        # return the name of the instrument selected in the parameter tree
        return self.dico[who].param('instrument').value()


class MotherCourbe():
    def clear_data(self,curve_id):
        for k in self.curves[curve_id]['data']:
            self.curves[curve_id]['data'][k] = []     

    def copy_curve(self,mother,daughter):
        # mother = curve_id of the mother
        # daughter = curve_id of the daughter 
        # create a daughter curve which a copy of the mother curve
        # You cannot copy a dictionary simply by typing dict2 = dict1, because: dict2 will only be a reference to dict1, and changes made in dict1 will automatically also be made in dict2.
        # There are ways to make a copy, one way is to use the built-in Dictionary method copy().
        self.curves[daughter]['data'] = self.curves[mother]['data'].copy()

    def interpolate(self,curve_id,Xname,Yname):
        # inporlation de la fonction f -> Yname=f(Xname)
        dataX = self.curves[curve_id]['data'][Xname]
        dataY = self.curves[curve_id]['data'][Yname]
        # print('interpoalte X :',dataX)
        # print('interpoalte Y :',dataY)
        f = scipy.interpolate.interp1d(dataX, dataY)
        x = np.linspace(min(dataX),max(dataX),10000)
        # self.curves[curve_id]['data'][Xname+' interpolate'] = x
        # self.curves[curve_id]['data'][Yname+' interpolate'] = f(x)
        return x, f(x), f

    def remove_curve(self, curve_id):
        curve_id = str(curve_id)
        if curve_id in self.curves:
            self.plot_widget.removeItem(self.curves[curve_id]['plot'])
            del self.curves[curve_id]

    def remove_ghost(self):
        ghost_list = []
        for k in self.curves:
            if 'ghost' in k:
                ghost_list.append(k)
        # to avoid error we add a second loop too remove item 'RuntimeError: dictionary changed size during iteration'
        for k in ghost_list:
            self.remove_curve(k)

    def save_parameters(self,curve_id,param):
        dico = {}
        for k in param.dico:
            dico[k] = {}
            for i in param.dico[k].children():
                dico[k][i.name()]= i.value()
        self.curves[curve_id]['data']['parameters'] = dico.copy()

    def save_parameters_sauvegarder(self,param,curve_id='curve acq'):
        # useful to update the save parameters just before the button Save has been pressed
        for k in ['Graph','Save']:
            dico = {}
            for i in param.dico[k].children():
                dico[i.name()]= i.value()
            self.curves[curve_id]['data']['parameters'][k] = dico.copy()


    def set_values(self, curve_id, data_x, data_y):
        curve = self.curves[curve_id]['plot']
        curve.setData(data_x, data_y)    