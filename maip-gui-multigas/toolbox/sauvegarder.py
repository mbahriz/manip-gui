import time
import os
import pandas
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import toolbox.function
from scipy import interpolate

def load_mpl_params():
    # MATPLOTLIB PARAMETERS
    mpl.rcParams.update(mpl.rcParamsDefault)
    # mpl.rcParamsDefault.keys --> display all parameters available
    params = {'legend.fontsize': 10, #ATTENTION usually 16
            'figure.figsize': (10, 8),
            'axes.labelsize': 18,
            'axes.titlesize': 18,
            'xtick.labelsize': 14,#ATTENTION usually 18
            'ytick.labelsize': 14,#ATTENTION usually 18
            'figure.subplot.left': 0.15,
            'figure.subplot.right': 0.85,
            'figure.subplot.bottom': 0.15,
            'figure.subplot.top': 0.85,
            'xtick.direction': 'in',
            'ytick.direction': 'in',
            'xtick.major.size': 5,
            'xtick.major.width': 1.3,
            'xtick.minor.size': 3,
            'xtick.minor.width': 1,
            'xtick.major.pad': 8,
            'ytick.major.pad': 8,
            'lines.linewidth': 1,
            'axes.grid': True,
            'axes.grid.axis': 'both',
            'axes.grid.which': 'both',
            'grid.alpha': 0.5,
            'grid.color': '111111',
            'grid.linestyle': '--',
            'grid.linewidth': 0.8,
            'savefig.dpi': 300, }
    mpl.rcParams.update(params)  

class Save():
    def __init__(self,what,curve1,curve2=None):
        self.origin = what
        self.courbe1 = curve1
        self.courbe2 = curve2
        self.create_curve_list() # create attribut courbe1_list/courbe1_list/courbe1_prefix
        # sauvegarde
        self.file_name = []
        self.path = self.fabricate_path()
        self.create_path()
        self.update_legend_for_all_curve()
        for num_curve in range(0,len(self.courbe1_list)):
            self.file_name.append(self.fabricate_file_name(num_curve))
            self.save_param(num_curve)
            self.save_data(num_curve)
        self.save_graph()

    def clean_up_curve_list(self,curve_list):
        # remove fit curve
        for k in curve_list:
            if 'fit' in k:
                curve_list.remove(k)
        # remove the last ghost which is empty
        curve_list = curve_list[:-1] 
        # move 'curve acq' at the end
        # if 'MultiGaz_MD'not in self.origin: 
            
        #     curve_list.remove('curve acq')
        # curve_list.append('curve acq')
        if'MultiGaz_MD'not in self.origin and 'Conc_Gas_MD' not in self.origin: 
            
            curve_list.remove('curve acq')
        
        curve_list.append('curve acq')
        return curve_list

    def create_curve_list(self):
        curve_list = list(self.courbe1.keys())
        self.courbe1_list = self.clean_up_curve_list(curve_list)
        if self.courbe2 is not None:
            curve_list = list(self.courbe2.keys())
            self.courbe2_list = self.clean_up_curve_list(curve_list)
        self.create_curve_prefix()            

    def create_curve_prefix(self):
        myList = self.courbe1_list
        A = np.linspace(1,len(myList)+1,len(myList)+1)
        if 'Allan deviation' in self.origin:
            self.courbe1_prefix = ['AD{:0>2}'.format(int(k)) for k in A]
        if 'Amplitude of modulation' in self.origin:
            self.courbe1_prefix = ['AOM{:0>2}'.format(int(k)) for k in A]
        if 'Electrical characterization' in self.origin:
            self.courbe1_prefix = ['EC{:0>2}'.format(int(k)) for k in A]
        if 'Frequency sweep' in self.origin:
            self.courbe1_prefix = ['FS{:0>2}'.format(int(k)) for k in A]
        if 'Continous Acquisition' in self.origin:
            self.courbe1_prefix = ['CA{:0>2}'.format(int(k)) for k in A]
        if 'Conc_Gas_MD' in self.origin:
            self.courbe1_prefix = ['Conc_MD{:0>2}'.format(int(k)) for k in A]
        if 'MultiGaz_MD' in self.origin:
            self.courbe1_prefix = ['MD{:0>2}'.format(int(k)) for k in A]
        if 'Laser PV2I' in self.origin:
            self.courbe1_prefix = ['PV2I{:0>2}'.format(int(k)) for k in A]
        if 'Photoacoustic' in self.origin:
            self.courbe1_prefix = ['PA{:0>2}'.format(int(k)) for k in A]

    def create_path(self):
        # if don't exist create the directory
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def fabricate_file_name(self,num):
        curve_name = self.courbe1_list[num]
        prefix = self.courbe1_prefix[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        legend = parameters['Graph']['legend curve '+str(num+1)]
        if legend is not '':
            legend = '_'+legend
        if 'Allan deviation' in self.origin:
            acq_time = '%.0fmin'%(parameters['Allan Deviation']['acq. time'])
            concentration = '%.0fppmv'%(parameters['Allan Deviation']['concentration'])
            time_cst = '%.0fms'%(parameters['Input']['time constant']*1e3)
            temperature = '{:.0f}C'.format(parameters['Laser Driver']['temperature'])
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            amplitude = '{:.0f}mV'.format(parameters['Generator']['amplitude']*1e3)  
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3) 
            current = '{:.0f}mA'.format(parameters['Laser Driver']['current'])   
            #Mean = '{:.0f}mVrms'.format(self.courbe1[curve_name]['data']['Mean']*1e3)
            signal_to_noise = 'SNR{:.0f}'.format(self.courbe1[curve_name]['data']['SNR'])            
            file_name = self.path + prefix + legend + '_'  + '_'.join([harmonic,signal_to_noise,acq_time,concentration,
                                time_cst,current,amplitude,frequency,temperature])
        if 'Amplitude of modulation' in self.origin:
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            amp = '%.0f-%.0fmVpk'%(parameters['Generator']['amplitude min']*1e3,parameters['Generator']['amplitude max']*1e3) 
            temperature = '{:.0f}C'.format(parameters['Laser Driver']['temperature']) 
            current = '{:.0f}mA'.format(parameters['Laser Driver']['current']) 
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3)     
            file_name = self.path + prefix + legend + '_'  + '_'.join([frequency,harmonic,amp,current,temperature]) 
        if 'Electrical characterization' in self.origin:
            ampli = '{}'.format(parameters['Amplifier']['amplifier'])
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            freq = '%.0f-%.0fkHz'%(parameters['Acquisition']['freq min']/1e3,parameters['Acquisition']['freq max']/1e3)   
            file_name = self.path + prefix + legend + '_' +'_'.join([harmonic,freq,ampli])
        if 'Frequency sweep' in self.origin:
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            freq0 = '{:.0f}Hz'.format(self.courbe1[curve_name]['data']['f0'])
            freq = '%.0f-%.0fkHz'%(parameters['Acquisition']['freq min']/1e3,parameters['Acquisition']['freq max']/1e3)   
            file_name = self.path + prefix + legend + '_' +'_'.join([harmonic,freq0,freq])
        if 'Continous Acquisition' in self.origin:
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3)
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            time_cst = '%.0fms'%(parameters['Input']['time constant']*1e3)
            file_name = self.path + prefix + legend + '_' +'_'.join([harmonic,frequency,time_cst])
            
        if 'MultiGaz_MD' in self.origin or 'Conc_Gas_MD' in self.origin:
            time_cst = '%.0fms'%(parameters['Input']['poll length']*10e3)
            file_name = self.path + prefix + legend + '_' +'_'.join([time_cst])  
            
        
            
        if 'Laser PV2I' in self.origin:
            curr = '%.0f-%.0fmA'%(parameters['Laser Driver']['current min'],parameters['Laser Driver']['current max'])
            temp = 'T{:.0f}C'.format(parameters['Laser Driver']['temperature'])   
            file_name = self.path + prefix + legend + '_'  + '_'.join([curr,temp]) 
        if 'Photoacoustic' in self.origin:
            curr = '%.0f-%.0fmA'%(parameters['Laser Driver']['current min'],parameters['Laser Driver']['current max'])
            temperature = '{:.0f}C'.format(parameters['Laser Driver']['temperature'])
            harmonic = '{:.0f}f'.format(parameters['Input']['harmonic'])
            amplitude = '{:.0f}mV'.format(parameters['Generator']['amplitude']*1e3)  
            frequency = '{:.0f}kHz'.format(parameters['Generator']['frequency']/1e3)   
            file_name = self.path + prefix + legend + '_'  + '_'.join([harmonic,amplitude,temperature,curr,frequency]) 
        return file_name   

    def fabricate_title_name(self):
        # curve_name = self.courbe1_list[0]
        # parameters = self.courbe1[curve_name]['data']['parameters']
        # legend = parameters['Graph']['legend curve 1']
        # if legend is not '':
        #     legend = '_'+legend
        # file_name = self.file_name[0].replace(legend,'')
        # prefix = self.courbe1_prefix[0]
        # file_name = file_name.replace(prefix,prefix.replace('01',''))
        curve_name = self.courbe1_list[0]
        parameters = self.courbe1[curve_name]['data']['parameters']
        path_info = parameters['Save']['path info']
        prefix = self.courbe1_prefix[0]
        today = time.strftime("%Y%m%d")
        if path_info is '':
            file_name = '_'.join([prefix.replace('01',''),today])
        else: 
            file_name = '_'.join([prefix.replace('01',''),today,path_info])
        return self.path+file_name       

    def fabricate_path(self):
        curve_name = 'curve acq'
        parameters = self.courbe1[curve_name]['data']['parameters']
        today = time.strftime("%Y%m%d")
        if 'MultiGaz_MD' not in self.origin and 'Conc_Gas_MD' not in self.origin:
          path_laser = parameters['Save']['laser']
        path_design = parameters['Save']['design']
        
        
        
        if 'Unknow' in path_design:
            path_resonator = 'Unkonw'
        elif 'MultiGaz_MD' not in self.origin and 'Conc_Gas_MD' not in self.origin:
            path_resonator = parameters['Save']['resonator']

            
        path_info = parameters['Save']['path info']
        
        if path_info is '':
            path_info = today
        else:
            path_info = today+'_'+path_info
        if 'C:' in parameters['Save']['path']:
            slash = '\\'
        else:
            slash = '/'
        if 'device' in parameters['Save'].keys():
            path_device = parameters['Save']['device']
            path = parameters['Save']['path'] + slash.join([path_design,path_resonator,path_device]) + slash
        else:
            # path = parameters['Save']['path'] + slash.join([path_design,path_resonator]) + slash
            # print('device False',path)
            if 'acoustic_sensor' in parameters['Save'].keys():
                path_acoustic_sensor = parameters['Save']['acoustic_sensor']
                path = parameters['Save']['path'] + slash.join([path_design,path_resonator,path_acoustic_sensor]) + slash
            elif 'MultiGaz_MD' not in self.origin and 'Conc_Gas_MD' not in self.origin:
                path = parameters['Save']['path'] + slash.join([path_design,path_resonator]) + slash
            
            
                
            elif 'MultiGaz_MD'  in self.origin:   
                path = parameters['Save']['path'] + slash.join([path_design]) + slash
                
            elif 'Conc_Gas_MD' in self.origin:   
                path = parameters['Save']['path'] + slash.join([path_design]) + slash
                
        if 'Allan deviation' in self.origin:
            path = path + slash.join(['allan_deviation',path_info]) + slash
        if 'Amplitude of modulation' in self.origin:
            path = path + slash.join(['amplitude_of_modulation',path_info]) + slash
        if 'Electrical characterization' in self.origin:
            path = path + slash.join(['electrical_characterization',path_info]) + slash            
        if 'Frequency sweep' in self.origin:
            path = path + slash.join(['frequency_sweep',path_info]) + slash
        if 'Continous Acquisition' in self.origin:
            path = path + slash.join(['Continous Acquisition',path_info]) + slash
        if 'MultiGaz_MD' in self.origin:
            path = path + slash.join(['MultiGaz_MD',path_info]) + slash
        if 'Conc_Gas_MD' in self.origin:
            path = path + slash.join(['Conc_Gas_MD',path_info]) + slash
        if 'Laser PV2I' in self.origin:
            path = parameters['Save']['path'] + slash.join(['Laser',path_laser]) + slash
            path = path + slash.join(['pv2i',path_info]) + slash
        if 'Photoacoustic' in self.origin:
            path = path + slash.join(['photoacoustic',path_info]) + slash
        return path  

    def update_legend_for_all_curve(self):
        # juste before savinng the last legend has been saved on 'curve acq'
        # so we update all the other curve
        for k in ['Graph','Save']:
            dico = self.courbe1['curve acq']['data']['parameters'][k]
            for curve_name in self.courbe1_list:
                self.courbe1[curve_name]['data']['parameters'][k] = dico

    def save_param(self,num):
        curve_name = self.courbe1_list[num]
        parameters = self.courbe1[curve_name]['data']['parameters']
        # save a txt file with all the parameter
        info = open(self.file_name[num]+'.txt','w')
        info.write(time.strftime("%Y-%m-%d")+' '+time.strftime("%H:%M:%S")+ '\n')
        info.write('\n')   
        for k in parameters:
            info.write(k+'\n')
            for j in parameters[k]:
                texte = '  * '+str(j)+' : '+str(parameters[k][j])+'\n'
                info.write(texte)      
        info.close()

    def save_data(self,num):
        # save csv file with all the data
        curve_name = self.courbe1_list[num]
        if 'Allan deviation' in self.origin:
            data_1 = pandas.DataFrame({'time (s)': self.courbe1[curve_name]['data']['Time'],
                        'time wanted (s)': self.courbe1[curve_name]['data']['Time wanted'],
                        #'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                        #'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                        'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                        #'phase (deg)': self.courbe1[curve_name]['data']['Phi'],
                        })
            data_1.to_csv(self.file_name[num] +'_MAG.csv', sep=';', encoding='utf-8') 
            
            data_2 = pandas.DataFrame({'Tau-adev (s)': self.courbe1[curve_name]['data']['Tau-adev'],
                        'Deviation-adev (Vrms)': self.courbe1[curve_name]['data']['Deviation-adev'],
                        'Concetration-adev (ppmv)': self.courbe1[curve_name]['data']['Concentration-adev']})
            data_2.to_csv(self.file_name[num] +'_ADEV.csv', sep=';', encoding='utf-8') 

            data_3 = pandas.DataFrame({'Tau-oadev (s)': self.courbe1[curve_name]['data']['Tau-oadev'],
                        'Deviation-oadev (Vrms)': self.courbe1[curve_name]['data']['Deviation-oadev'],
                        'Concetration-oadev (ppmv)': self.courbe1[curve_name]['data']['Concentration-oadev'],
                        })
            data_3.to_csv(self.file_name[num] +'_OADEV.csv', sep=';', encoding='utf-8') 
        if 'Amplitude of modulation' in self.origin:
            # save csv file with all the data
            data = pandas.DataFrame({'amplitude of modualtion (Vpk)': self.courbe1[curve_name]['data']['Amplitude'],
                        'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                        'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                        'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                        'phase (deg)': self.courbe1[curve_name]['data']['Phi']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')
        if 'Electrical characterization' in self.origin:
            data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name]['data']['Freq'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi'],
                'Vexcitation (Vrms)': self.courbe1[curve_name]['data']['Vexc'],
                'Gain': self.courbe1[curve_name]['data']['Gain'],
                'Vout/Vexc': self.courbe1[curve_name]['data']['Vout/Vexc'],
                'Admittance (1/Ohm)': self.courbe1[curve_name]['data']['Admittance']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8') 
            if len(self.courbe1[curve_name+' fit']['data']['Freq']) > 1:
                data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name+' fit']['data']['Freq'],
                            'Admittance (1/Ohm)': self.courbe1[curve_name+' fit']['data']['Admittance']})
                data.to_csv(self.file_name[num] +'_FIT_ADM.csv', sep=';', encoding='utf-8')
        if 'Frequency sweep' in self.origin:
            data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name]['data']['Freq'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8') 
            if len(self.courbe1[curve_name+' fit']['data']['Freq']) > 1:
                data = pandas.DataFrame({'Frequency (Hz)': self.courbe1[curve_name+' fit']['data']['Freq'],
                            'r (Vrms)': self.courbe1[curve_name+' fit']['data']['R']})
                data.to_csv(self.file_name[num] +'_FIT_MAG.csv', sep=';', encoding='utf-8')
            if len(self.courbe2[curve_name+' fit']['data']['Freq']) > 1:
                data = pandas.DataFrame({'Frequency (Hz)': self.courbe2[curve_name+' fit']['data']['Freq'],
                            'phase (deg)': self.courbe2[curve_name+' fit']['data']['Phi']})
                data.to_csv(self.file_name[num] +'_FIT_PHASE.csv', sep=';', encoding='utf-8')
                
                
                
        if 'Continous Acquisition' in self.origin:
            data = pandas.DataFrame({'Time (s)': self.courbe1[curve_name]['data']['tim'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')
            
            


            
            
            
        if 'MultiGaz_MD' in self.origin:
            #in case that arrays do not have the same length
            
            data_dict = {
            
                'Time (s)': self.courbe1[curve_name]['data']['tim'],
                self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)': self.courbe1[curve_name]['data']['R1'],
                self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)': self.courbe1[curve_name]['data']['R2'],
                self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)': self.courbe1[curve_name]['data']['R3'],
                self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)': self.courbe1[curve_name]['data']['R4'],
                'RH (%)': self.courbe1[curve_name]['data']['RH'],
                'T (degC)': self.courbe1[curve_name]['data']['T'],
                'H2O (ppm)': self.courbe1[curve_name]['data']['H2O'],
                'medisoft_time(-)':self.courbe1[curve_name]['data']['time_stamp_medisoft'],
                'medisoft_index(-)':self.courbe1[curve_name]['data']['index_medisoft'],
                # 'medisoft_step(-)':self.courbe1[curve_name]['data']['step_medisoft'],
                'flow(ATP l/s)':self.courbe1[curve_name]['data']['flow_medisoft'],
                'CO2(%)':self.courbe1[curve_name]['data']['CO2_medisoft'],
                
                'time_increm_medisoft_complet (s)':self.courbe1[curve_name]['data']['time_increm_medisoft_complet'],
                'time_clock_medisoft_complet (s)':self.courbe1[curve_name]['data']['time_clock_medisoft_complet'],
                'time_stamp_medisoft_complet':self.courbe1[curve_name]['data']['time_stamp_medisoft_complet'],
                'index_medisoft_complet (-)':self.courbe1[curve_name]['data']['index_medisoft_complet'],
                'volume_medisoft_complet':self.courbe1[curve_name]['data']['volume_medisoft_complet'],
                'flow_medisoft_complet (ATP l/s)':self.courbe1[curve_name]['data']['flow_medisoft_complet'],
                'CO2_medisoft_complet (%)':self.courbe1[curve_name]['data']['CO2_medisoft_complet'],
                self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R1_medisoft_complet'],
                self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R2_medisoft_complet'],
                self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R3_medisoft_complet'],
                self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R4_medisoft_complet']
                
                
                
                }
            
            # Find the maximum length among all arrays
            max_length = max(map(len, data_dict.values()))
            
            # Create a list of dictionaries
            data_list = []
            for i in range(max_length):
                row = {
                   'Time (s)': data_dict['Time (s)'][i] if i < len(data_dict['Time (s)']) else None,
                    self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)':data_dict[self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)':data_dict[self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)']) else None,
                    self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)':data_dict[self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)':data_dict[self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)']) else None,
                    'RH (%)': data_dict['RH (%)'][i] if i < len(data_dict['RH (%)']) else None,
                    'T (degC)': data_dict['T (degC)'][i] if i < len(data_dict['T (degC)']) else None,
                    'H2O (ppm)': data_dict['H2O (ppm)'][i] if i < len(data_dict['H2O (ppm)']) else None,
                    'medisoft_time(-)': data_dict['medisoft_time(-)'][i] if i < len(data_dict['medisoft_time(-)']) else None,
                    'medisoft_index(-)': data_dict['medisoft_index(-)'][i] if i < len(data_dict['medisoft_index(-)']) else None,
                    # 'medisoft_step(-)': data_dict['medisoft_step(-)'][i] if i < len(data_dict['medisoft_step(-)']) else None,
                    
                    'flow(ATP l/s)': data_dict['flow(ATP l/s)'][i] if i < len(data_dict['flow(ATP l/s)']) else None,
                    'CO2(%)': data_dict['CO2(%)'][i] if i < len(data_dict['CO2(%)']) else None,
                    '': '',  # Empty column
                    'time_increm_medisoft_complet (s)': data_dict['time_increm_medisoft_complet (s)'][i] if i < len(data_dict['time_increm_medisoft_complet (s)']) else None,
                    'time_clock_medisoft_complet (s)': data_dict['time_clock_medisoft_complet (s)'][i] if i < len(data_dict['time_clock_medisoft_complet (s)']) else None,
                    'time_stamp_medisoft_complet': data_dict['time_stamp_medisoft_complet'][i] if i < len(data_dict['time_stamp_medisoft_complet']) else None,
                    'index_medisoft_complet (-)': data_dict['index_medisoft_complet (-)'][i] if i < len(data_dict['index_medisoft_complet (-)']) else None,
                    'volume_medisoft_complet': data_dict['volume_medisoft_complet'][i] if i < len(data_dict['volume_medisoft_complet']) else None,
                    'flow_medisoft_complet (ATP l/s)': data_dict['flow_medisoft_complet (ATP l/s)'][i] if i < len(data_dict['flow_medisoft_complet (ATP l/s)']) else None,
                    'CO2_medisoft_complet (%)': data_dict['CO2_medisoft_complet (%)'][i] if i < len(data_dict['CO2_medisoft_complet (%)']) else None,
                    
                    self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)']) else None,
                    
                    self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)']) else None,
                    
                
                
                
                
                }
                data_list.append(row)
            
            # Create a DataFrame from the list of dictionaries
            data = pandas.DataFrame(data_list)
            
           
            #replace é
            # data['medisoft_step(-)']=data['medisoft_step(-)'].str.replace('é','e')
            # data['step_medisoft_complet']=data['step_medisoft_complet'].str.replace('é','e')
            
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')
            
            
        if 'Conc_Gas_MD' in self.origin:
            #in case that arrays do not have the same length
            
            data_dict = {
            
                'Time (s)': self.courbe1[curve_name]['data']['tim'],
                self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)': self.courbe1[curve_name]['data']['R1'],
                self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)': self.courbe1[curve_name]['data']['R2'],
                self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)': self.courbe1[curve_name]['data']['R3'],
                self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)': self.courbe1[curve_name]['data']['R4'],
                'RH (%)': self.courbe1[curve_name]['data']['RH'],
                'T (degC)': self.courbe1[curve_name]['data']['T'],
                'H2O (ppm)': self.courbe1[curve_name]['data']['H2O'],
                'medisoft_time(-)':self.courbe1[curve_name]['data']['time_stamp_medisoft'],
                'medisoft_index(-)':self.courbe1[curve_name]['data']['index_medisoft'],
                'medisoft_volume(-)':self.courbe1[curve_name]['data']['volume_medisoft'],
                'flow(ATP l/s)':self.courbe1[curve_name]['data']['flow_medisoft'],
                'CO2(%)':self.courbe1[curve_name]['data']['CO2_medisoft'],
                
                'time_increm_medisoft_complet (s)':self.courbe1[curve_name]['data']['time_increm_medisoft_complet'],
                'time_clock_medisoft_complet (s)':self.courbe1[curve_name]['data']['time_clock_medisoft_complet'],
                'time_stamp_medisoft_complet':self.courbe1[curve_name]['data']['time_stamp_medisoft_complet'],
                'index_medisoft_complet (-)':self.courbe1[curve_name]['data']['index_medisoft_complet'],
                'volume_medisoft_complet':self.courbe1[curve_name]['data']['volume_medisoft_complet'],
                'flow_medisoft_complet (ATP l/s)':self.courbe1[curve_name]['data']['flow_medisoft_complet'],
                'CO2_medisoft_complet (%)':self.courbe1[curve_name]['data']['CO2_medisoft_complet'],
                self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R1_medisoft_complet'],
                self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R2_medisoft_complet'],
                self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R3_medisoft_complet'],
                self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)':self.courbe1[curve_name]['data']['R4_medisoft_complet'],
                
                self.courbe1[curve_name]['data']['title1']+'_'+'Conc1_medisoft_complet (ppm)':self.courbe1[curve_name]['data']['Conc1_medisoft_complet'],
                self.courbe1[curve_name]['data']['title2']+'_'+'Conc2_medisoft_complet (ppm)':self.courbe1[curve_name]['data']['Conc2_medisoft_complet'],
                self.courbe1[curve_name]['data']['title3']+'_'+'Conc3_medisoft_complet (ppm)':self.courbe1[curve_name]['data']['Conc3_medisoft_complet'],
                self.courbe1[curve_name]['data']['title4']+'_'+'Conc4_medisoft_complet (ppm)':self.courbe1[curve_name]['data']['Conc4_medisoft_complet']
                
                
                
                }
            
            # Find the maximum length among all arrays
            max_length = max(map(len, data_dict.values()))
            
            # Create a list of dictionaries
            data_list = []
            for i in range(max_length):
                row = {
                   'Time (s)': data_dict['Time (s)'][i] if i < len(data_dict['Time (s)']) else None,
                    self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)':data_dict[self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title1']+'_R1 (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)':data_dict[self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title2']+'_R2 (Arms)']) else None,
                    self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)':data_dict[self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title3']+'_R3 (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)':data_dict[self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title4']+'_R4 (Vrms)']) else None,
                    'RH (%)': data_dict['RH (%)'][i] if i < len(data_dict['RH (%)']) else None,
                    'T (degC)': data_dict['T (degC)'][i] if i < len(data_dict['T (degC)']) else None,
                    'H2O (ppm)': data_dict['H2O (ppm)'][i] if i < len(data_dict['H2O (ppm)']) else None,
                    'medisoft_time(-)': data_dict['medisoft_time(-)'][i] if i < len(data_dict['medisoft_time(-)']) else None,
                    'medisoft_index(-)': data_dict['medisoft_index(-)'][i] if i < len(data_dict['medisoft_index(-)']) else None,
                    'medisoft_volume(-)': data_dict['medisoft_volume(-)'][i] if i < len(data_dict['medisoft_volume(-)']) else None,
                    
                    'flow(ATP l/s)': data_dict['flow(ATP l/s)'][i] if i < len(data_dict['flow(ATP l/s)']) else None,
                    'CO2(%)': data_dict['CO2(%)'][i] if i < len(data_dict['CO2(%)']) else None,
                    '': '',  # Empty column
                    'time_increm_medisoft_complet (s)': data_dict['time_increm_medisoft_complet (s)'][i] if i < len(data_dict['time_increm_medisoft_complet (s)']) else None,
                    'time_clock_medisoft_complet (s)': data_dict['time_clock_medisoft_complet (s)'][i] if i < len(data_dict['time_clock_medisoft_complet (s)']) else None,
                    'time_stamp_medisoft_complet': data_dict['time_stamp_medisoft_complet'][i] if i < len(data_dict['time_stamp_medisoft_complet']) else None,
                    'index_medisoft_complet (-)': data_dict['index_medisoft_complet (-)'][i] if i < len(data_dict['index_medisoft_complet (-)']) else None,
                    'volume_medisoft_complet': data_dict['volume_medisoft_complet'][i] if i < len(data_dict['volume_medisoft_complet']) else None,
                    'flow_medisoft_complet (ATP l/s)': data_dict['flow_medisoft_complet (ATP l/s)'][i] if i < len(data_dict['flow_medisoft_complet (ATP l/s)']) else None,
                    'CO2_medisoft_complet (%)': data_dict['CO2_medisoft_complet (%)'][i] if i < len(data_dict['CO2_medisoft_complet (%)']) else None,
                    
                    self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title1']+'_'+'R1_medisoft_complet (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title2']+'_'+'R2_medisoft_complet (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title3']+'_'+'R3_medisoft_complet (Vrms)']) else None,
                    self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)':data_dict[self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title4']+'_'+'R4_medisoft_complet (Vrms)']) else None,
                    
                    self.courbe1[curve_name]['data']['title1']+'_'+'Conc1_medisoft_complet (ppm)':data_dict[self.courbe1[curve_name]['data']['title1']+'_'+'Conc1_medisoft_complet (ppm)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title1']+'_'+'Conc1_medisoft_complet (ppm)']) else None,
                    self.courbe1[curve_name]['data']['title2']+'_'+'Conc2_medisoft_complet (ppm)':data_dict[self.courbe1[curve_name]['data']['title2']+'_'+'Conc2_medisoft_complet (ppm)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title2']+'_'+'Conc2_medisoft_complet (ppm)']) else None,
                    self.courbe1[curve_name]['data']['title3']+'_'+'Conc3_medisoft_complet (ppm)':data_dict[self.courbe1[curve_name]['data']['title3']+'_'+'Conc3_medisoft_complet (ppm)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title3']+'_'+'Conc3_medisoft_complet (ppm)']) else None,
                    self.courbe1[curve_name]['data']['title4']+'_'+'Conc4_medisoft_complet (ppm)':data_dict[self.courbe1[curve_name]['data']['title4']+'_'+'Conc4_medisoft_complet (ppm)'][i] if i < len(data_dict[self.courbe1[curve_name]['data']['title4']+'_'+'Conc4_medisoft_complet (ppm)']) else None,
                    
                
                
                
                }
                data_list.append(row)
            
            # Create a DataFrame from the list of dictionaries
            data = pandas.DataFrame(data_list)
            #replace é
            # data['medisoft_step(-)']=data['medisoft_step(-)'].str.replace('é','e')
            # data['step_medisoft_complet']=data['step_medisoft_complet'].str.replace('é','e')
            
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')
            
            
        
                    
                    
        if 'Laser PV2I' in self.origin:
            data = pandas.DataFrame({'Current (mA)': self.courbe1[curve_name]['data']['Current'],
                'voltage (V)': self.courbe1[curve_name]['data']['Voltage'],
                'power (mW)': self.courbe1[curve_name]['data']['Power']
                })
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')
        if 'Photoacoustic' in self.origin:
            data = pandas.DataFrame({'current (mA)': self.courbe1[curve_name]['data']['Current'],
                'x (Vrms)': self.courbe1[curve_name]['data']['X'],
                'y (Vrms)': self.courbe1[curve_name]['data']['Y'],
                'r (Vrms)': self.courbe1[curve_name]['data']['R'],
                'phase (deg)': self.courbe1[curve_name]['data']['Phi']})
            data.to_csv(self.file_name[num] +'.csv', sep=';', encoding='utf-8')  

    def save_graph(self):
        # color and graph param
        load_mpl_params()
        if 'Allan deviation' in self.origin:
            self.save_graph_Allan()
        if 'Amplitude of modulation' in self.origin:
            self.save_graph_Amplitude_Of_Modulation()
        if 'Electrical characterization' in self.origin:
            self.save_graph_Electrical_Characterization()
        if 'Frequency sweep' in self.origin:
            self.save_graph_Frequency_Sweep()
            
        if 'Continous Acquisition' in self.origin:
            self.save_graph_Continous_Acquisition()
        if 'MultiGaz_MD' in self.origin:
            self.save_graph_Continous_Acquisition_MD()
        if 'Conc_Gas_MD' in self.origin:
            self.save_graph_Conc_MultiGaz_MD()
                
        if 'Laser PV2I' in self.origin:
            self.save_graph_Laser_PV2I()
        if 'Photoacoustic' in self.origin:
            self.save_graph_PhotoAcoustic()

    def save_graph_Allan(self):
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            curve_name = self.courbe1_list[num]
            R = self.courbe1[curve_name]['data']['R']
            Time = self.courbe1[curve_name]['data']['Time']
            Mean = self.courbe1[curve_name]['data']['Mean']
            signal_to_noise = self.courbe1[curve_name]['data']['SNR']
            step_time = self.courbe1[curve_name]['data']['Step time']
            step_time_dev = self.courbe1[curve_name]['data']['Step time dev']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            legend = legend +' Mean={:.3f}mVrms SNR={:.1f} step={:.1f}+/-{:.3f}ms'.format(Mean*1e3,signal_to_noise,step_time*1e3,step_time_dev*1e3)
            ax1.plot(Time, R, color=colors[2*num],linewidth=2,marker='.',label=legend)

        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)        
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_TIME.png')
        plt.close(fig)

        # ALLAN DEVIATION
        def add_plot_allan(num):
            curve_name = self.courbe1_list[num]
            Tadev = self.courbe1[curve_name]['data']['Tau-adev']
            Cadev = self.courbe1[curve_name]['data']['Concentration-adev']
            Toadev = self.courbe1[curve_name]['data']['Tau-oadev']
            Coadev = self.courbe1[curve_name]['data']['Concentration-oadev']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            ax1.plot(Tadev, Cadev, color=colors[2*num],linewidth=2,marker='.',label=legend+' (adev)')
            ax1.plot(Toadev, Coadev, color=colors[2*num+1],linewidth=2,marker='.',label=legend+' (oadev)')
            # add value on the curve
            # find and plot the LOD @ 1s
            C1s = self.courbe1[curve_name]['data']['LOD 1s']
            print('LOD at 1s is {:.3f} ppmv'.format(C1s))
            ax1.scatter(1, C1s, color='r', alpha=0.5)
            ax1.text(1, C1s, '{:.0f}ppmv @ 1s'.format(C1s), rotation=15,color=colors[2*num])
            # find and plot the LOD @ 10s
            if max(Toadev) > 10:
                C10s = self.courbe1[curve_name]['data']['LOD 10s']
                print('LOD at 10s is {:.3f} ppmv'.format(C10s))
                ax1.scatter(10, C10s, color='r', alpha=0.5)
                ax1.text(10, C10s, '{:.0f}ppmv @ 10s'.format(C10s), rotation=15,color=colors[2*num])
            # find and plot the lowest LOD
            Tau_LOD, Con_LOD = self.courbe1[curve_name]['data']['LOD']
            print('LOD at {:.3f}s is {:.3f} ppmv'.format(Tau_LOD,Con_LOD))
            ax1.scatter(Tau_LOD, Con_LOD, color='r', alpha=0.5)
            ax1.text(Tau_LOD, Con_LOD, '{:.0f}ppmv @ {:.1f}s '.format(Con_LOD,Tau_LOD), rotation=15,color=colors[2*num])

        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_allan(num_curve) 
        ax1.set_ylabel('concentration [ppmv]', labelpad=4)
        ax1.set_xlabel('time constant [s]', labelpad=4)
        ax1.set_xscale('log')
        ax1.set_yscale('log')
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_ALLAN.png')
        plt.close(fig)

        ## TIME STEP
        def add_plot_time_step(num):
            curve_name = self.courbe1_list[num]
            Time = self.courbe1[curve_name]['data']['Time']
            Mean = self.courbe1[curve_name]['data']['Mean']
            signal_to_noise = self.courbe1[curve_name]['data']['SNR']
            step_time = self.courbe1[curve_name]['data']['Step time']
            step_time_dev = self.courbe1[curve_name]['data']['Step time dev']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            legend = legend +' Mean={:.3f}mVrms SNR={:.1f} step={:.1f}+/-{:.3f}ms'.format(Mean*1e3,signal_to_noise,step_time*1e3,step_time_dev*1e3)
            if len(Time) > 5000:
                divisor = 1000
                step = int(len(Time)/divisor)
            else:
                step = 1
            legend = legend + '\nThe number of points has been divided by {}'.format(step)    
            ax1.plot(Time[0:-1:step], np.diff(Time)[::step]*1e3, color=colors[2*num],linewidth=1,label=legend)
            

        gridsize = (1, 1)

        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_time_step(num_curve)        
        ax1.set_ylabel('step time [ms]', labelpad=4)
        ax1.set_xlabel('time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_TIME_STEP.png')
        plt.close(fig)


    def save_graph_Amplitude_Of_Modulation(self):
        number_color = 10
        cmap = plt.get_cmap('tab10')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        def add_plot_mag(num):
            curve_name = self.courbe1_list[num]
            Amp = self.courbe1[curve_name]['data']['Amplitude']*1e3
            R = self.courbe1[curve_name]['data']['R']
            Amp_at_Rmax = self.courbe1[curve_name]['data']['Amplitude for R max']
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            ax1.plot(Amp, R, color=colors[num],linewidth=2,marker='.',label=legend+' max for x={:.3f}mV'.format(Amp_at_Rmax*1e3))

        # magnitude
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)         
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('amplitude of modulation [mVpk]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)


    def save_graph_Electrical_Characterization(self):
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # ADMITTANCE
        def add_plot_adm(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            R = self.courbe1[curve_name]['data']['Admittance']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            R_fit = self.courbe1[curve_name+' fit']['data']['Admittance']
            ax1.plot(Freq*1e-3, R, color=colors[2*num],linewidth=2,label=legend,marker='.')
            if len(R_fit) > 1:
                Q_mag = self.courbe1[curve_name+' fit']['data']['Q']
                f0_mag = self.courbe1[curve_name+' fit']['data']['f0']
                C0_mag = self.courbe1[curve_name+' fit']['data']['C0']
                ax1.plot(Freq_fit*1e-3, R_fit, color=colors[2*num+1],
                linewidth=1,label='Q={:.0f} f0={:.3f}kHz \n C0={:.4f}pF'.format(Q_mag,f0_mag/1e3,C0_mag*1e12))

        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_adm(num_curve)
        ax1.set_ylabel(r'admittance [1/$\Omega$]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_ADM.png')
        plt.close(fig)

        # MAGNITUDE
        def add_plot_mag(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            R = self.courbe1[curve_name]['data']['R']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            R_fit = self.courbe1[curve_name+' fit']['data']['R']
            ax1.plot(Freq*1e-3, R, color=colors[2*num],linewidth=2,label=legend,marker='.')
            if len(R_fit) > 1:
                Q_mag = self.courbe1[curve_name+' fit']['data']['Q']
                f0_mag = self.courbe1[curve_name+' fit']['data']['f0']
                C0_mag = self.courbe1[curve_name+' fit']['data']['C0']
                ax1.plot(Freq_fit*1e-3, R_fit, color=colors[2*num+1],
                linewidth=1,label='Q={:.0f} f0={:.3f}kHz \n C0={:.2g}pF'.format(Q_mag,f0_mag/1e3,C0_mag*1e12))

        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)

        # PHASE
        def add_plot_phase(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            Phi = self.courbe1[curve_name]['data']['Phi']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            Phi_fit = self.courbe1[curve_name+' fit']['data']['Phi']
            ax1.plot(Freq*1e-3, Phi, color=colors[2*num],
            linewidth=2,label=legend,marker='.')

        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_phase(num_curve)
        ax1.set_ylabel('phase [degrees]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_PHASE.png')
        plt.close(fig)

    def save_graph_Frequency_Sweep(self):
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            R = self.courbe1[curve_name]['data']['R']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            R_fit = self.courbe1[curve_name+' fit']['data']['R']
            ax1.plot(Freq*1e-3, R, color=colors[2*num],linewidth=2,label=legend,marker='.')
            if len(R_fit) > 1:
                Q_mag = self.courbe1[curve_name+' fit']['data']['Q']
                Q_sigma = self.courbe1[curve_name+' fit']['data']['Q sigma']
                f0_mag = self.courbe1[curve_name+' fit']['data']['f0']
                f0_sigma = self.courbe1[curve_name+' fit']['data']['f0 sigma']
                C0_mag = self.courbe1[curve_name+' fit']['data']['C0']
                label_text = 'f0={:.3f}kHz(+/-){:.3f}Hz \nQ={:.0f}(+/-){:.1f} \nC0={:.2g}pF'.format(f0_mag/1e3, f0_sigma, Q_mag, Q_sigma, C0_mag*1e12)
                ax1.plot(Freq_fit*1e-3, R_fit, color=colors[2*num+1],linewidth=1,label=label_text)

        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)

        # PHASE
        def add_plot_phase(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Freq = self.courbe1[curve_name]['data']['Freq']
            Phi = self.courbe1[curve_name]['data']['Phi']
            Freq_fit = self.courbe1[curve_name+' fit']['data']['Freq']
            Phi_fit = self.courbe2[curve_name+' fit']['data']['Phi']
            ax1.plot(Freq*1e-3, Phi, color=colors[2*num],
            linewidth=2,label=legend,marker='.')
            if len(Phi_fit) > 1:
                Q_phase = self.courbe2[curve_name+' fit']['data']['Q']
                Q_sigma = self.courbe2[curve_name+' fit']['data']['Q sigma']
                f0_phase = self.courbe2[curve_name+' fit']['data']['f0']
                f0_sigma = self.courbe2[curve_name+' fit']['data']['f0 sigma']
                C0_phase = self.courbe2[curve_name+' fit']['data']['C0']
                label_text = 'f0={:.3f}kHz(+/-){:.3f}Hz \nQ={:.0f}(+/-){:.1f} \nC0={:.2g}pF'.format(f0_phase/1e3, f0_sigma, Q_phase, Q_sigma, C0_phase*1e12)
                ax1.plot(Freq_fit*1e-3, Phi_fit, color=colors[2*num+1],
                linewidth=1,label=label_text)
        fig, ax1 = plt.subplots()
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_phase(num_curve)
        ax1.set_ylabel('phase [degrees]', labelpad=4)
        ax1.set_xlabel('frequency [kHz]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_PHASE.png')
        plt.close(fig)
        
    def save_graph_Continous_Acquisition(self):
        
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Tim = self.courbe1[curve_name]['data']['tim']
            R = self.courbe1[curve_name]['data']['R']
            ax1.plot(Tim, R, color=colors[2*num],linewidth=2,label=legend,marker='.')
            

        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)

        # PHASE
        def add_plot_phase(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            Tim= self.courbe1[curve_name]['data']['tim']
            Phi = self.courbe1[curve_name]['data']['Phi']
            ax1.plot(Tim, Phi, color=colors[2*num],
            linewidth=2,label=legend,marker='.')
           
        fig, ax1 = plt.subplots()
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_phase(num_curve)
        ax1.set_ylabel('phase [degrees]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_PHASE.png')
        plt.close(fig)
        
    def save_graph_Continous_Acquisition_MD(self):
        
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]
    
        # MAGNITUDE 1
        def add_plot_mag1(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title1']
            Tim = self.courbe1[curve_name]['data']['tim']
            R1 = self.courbe1[curve_name]['data']['R1']
            ax1.plot(Tim, R1, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag1(num_curve)
        ax1.set_ylabel('magnitude_1 [Vrms]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG_1.png')
        plt.close(fig)
        
        # MAGNITUDE 2
        def add_plot_mag2(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title2']
            Tim = self.courbe1[curve_name]['data']['tim']
            R2 = self.courbe1[curve_name]['data']['R2']
            ax1.plot(Tim, R2, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag2(num_curve)
        ax1.set_ylabel('magnitude_2 [Arms]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG_2.png')
        plt.close(fig)   
        
        # MAGNITUDE 3
        def add_plot_mag3(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title3']
            Tim = self.courbe1[curve_name]['data']['tim']
            R3 = self.courbe1[curve_name]['data']['R3']
            ax1.plot(Tim, R3, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag3(num_curve)
        ax1.set_ylabel('magnitude_3 [Vrms]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG_3.png')
        plt.close(fig)   
        
        # MAGNITUDE 4
        def add_plot_mag4(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title4']
            Tim = self.courbe1[curve_name]['data']['tim']
            R4 = self.courbe1[curve_name]['data']['R4']
            ax1.plot(Tim, R4, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag4(num_curve)
        ax1.set_ylabel('magnitude_4 [Vrms]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG_4.png')
        plt.close(fig)   
        
        def add_plot_H2O(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title5']
            Tim = self.courbe1[curve_name]['data']['tim']
            H2O = self.courbe1[curve_name]['data']['H2O']
            ax1.plot(Tim, H2O, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_H2O(num_curve)
        ax1.set_ylabel('H2O [ppm]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_water.png')
        plt.close(fig)   
        
        
        def add_plot_flow(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title6']
            Tim = self.courbe1[curve_name]['data']['tim']
            H2O = self.courbe1[curve_name]['data']['flow_medisoft']
            ax1.plot(Tim, H2O, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_flow(num_curve)
        ax1.set_ylabel('flow [ATP l/s]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_flow_medisoft.png')
        plt.close(fig)  
        
        
        def add_plot_CO2(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title7']
            Tim = self.courbe1[curve_name]['data']['tim']
            H2O = self.courbe1[curve_name]['data']['CO2_medisoft']
            ax1.plot(Tim, H2O, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            

    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_CO2(num_curve)
        ax1.set_ylabel('CO2 [%]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_CO2_medisoft.png')
        plt.close(fig)  
        

    def save_graph_Conc_MultiGaz_MD(self):
        number_color = 20
        cmap = plt.get_cmap('tab20')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]
    
        # MAGNITUDE 1
        def add_plot_conc1(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title1']
            Tim = self.courbe1[curve_name]['data']['time_increm_medisoft_complet']
            C1 = self.courbe1[curve_name]['data']['Conc1_medisoft_complet']
            ax1.plot(Tim, C1, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_conc1(num_curve)
        ax1.set_ylabel('Gas 1 [ppm]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_isoprene.png')
        plt.close(fig)
        
        # MAGNITUDE 2
        def add_plot_conc2(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title2']
            Tim = self.courbe1[curve_name]['data']['time_increm_medisoft_complet']
            C2 = self.courbe1[curve_name]['data']['Conc2_medisoft_complet']
            ax1.plot(Tim, C2, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_conc2(num_curve)
        ax1.set_ylabel('Gas 2 [ppm]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_NO.png')
        plt.close(fig)   
        
        # MAGNITUDE 3
        def add_plot_conc3(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title3']
            Tim = self.courbe1[curve_name]['data']['time_increm_medisoft_complet']
            C3 = self.courbe1[curve_name]['data']['Conc3_medisoft_complet']
            ax1.plot(Tim, C3, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_conc3(num_curve)
        ax1.set_ylabel('Gas 3 [ppm]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_Acetone.png')
        plt.close(fig)   
        
        # MAGNITUDE 4
        def add_plot_conc4(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title4']
            Tim = self.courbe1[curve_name]['data']['time_increm_medisoft_complet']
            C4 = self.courbe1[curve_name]['data']['Conc4_medisoft_complet']
            ax1.plot(Tim, C4, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_conc4(num_curve)
        ax1.set_ylabel('Gas 4 [ppm]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_CO.png')
        plt.close(fig)   
        
        def add_plot_H2O(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title5']
            Tim = self.courbe1[curve_name]['data']['tim']
            H2O = self.courbe1[curve_name]['data']['H2O']
            ax1.plot(Tim, H2O, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_H2O(num_curve)
        ax1.set_ylabel('H2O [ppm]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_water.png')
        plt.close(fig)   
        
        
        
        def add_plot_volume(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title8']
            Tim = self.courbe1[curve_name]['data']['time_increm_medisoft_complet']
            volume = self.courbe1[curve_name]['data']['volume_medisoft_complet']
            ax1.plot(Tim, volume, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_volume(num_curve)
        ax1.set_ylabel('volume [l]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_volume_medisoft.png')
        plt.close(fig)  
        
        
        
        
        
        
        
        
        def add_plot_flow(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title6']
            Tim = self.courbe1[curve_name]['data']['time_increm_medisoft_complet']
            flow = self.courbe1[curve_name]['data']['flow_medisoft_complet']
            ax1.plot(Tim, flow, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            
    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_flow(num_curve)
        ax1.set_ylabel('flow [ATP l/s]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_flow_medisoft.png')
        plt.close(fig)  
        
        
        def add_plot_CO2(num):
            # data
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            title=self.courbe1[curve_name]['data']['title7']
            Tim = self.courbe1[curve_name]['data']['time_increm_medisoft_complet']
            CO2 = self.courbe1[curve_name]['data']['CO2_medisoft_complet']
            ax1.plot(Tim, CO2, color=colors[2*num],linewidth=2,label=title+'_'+legend,marker='.')
            

    
        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_CO2(num_curve)
        ax1.set_ylabel('CO2 [%]', labelpad=4)
        ax1.set_xlabel('Time [s]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_CO2_medisoft.png')
        plt.close(fig)  
        
        
        
    def save_graph_Laser_PV2I(self):
        number_color = 20
        cmap = plt.get_cmap('tab20b')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        def add_plot_pv2i(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            temperature = ' T{:.0f}C'.format(parameters['Laser Driver']['temperature']) 
            I = self.courbe1[curve_name]['data']['Current']
            V = self.courbe1[curve_name]['data']['Voltage']
            P = self.courbe1[curve_name]['data']['Power']
            ax1.plot(I, V, color=colors[num],linewidth=2,marker='.',label=legend+temperature)
            ax2.plot(I, P, color=colors[12+num],linewidth=2,marker='.')

        # graph
        gridsize = (1, 1)
        
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        
        ax1.set_ylabel('voltage [V]', labelpad=4, color=colors[0])
        ax1.tick_params(axis='y', labelcolor=colors[0])
        ax1.set_xlabel('current [mA]', labelpad=4)
        #ax1.set_title(temperature.replace('T','Temp=').replace('C','°C'))
        ax2 = ax1.twinx()
        ax2.set_ylabel('power [mW]', labelpad=0, color=colors[12])
        ax2.tick_params(axis='y', labelcolor=colors[12])
        ax2.grid(b=False)
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_pv2i(num_curve)
        ax1.legend(loc=1)
        #ax2.legend(loc=3)
        fig.savefig(self.fabricate_title_name()+'.png')
        plt.close(fig)

    def save_graph_PhotoAcoustic(self):
        number_color = 10
        cmap = plt.get_cmap('tab10')
        colors = [cmap(i) for i in np.linspace(0, 1, number_color)]

        # MAGNITUDE
        def add_plot_mag(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)]  
            I = self.courbe1[curve_name]['data']['Current']
            R = self.courbe1[curve_name]['data']['R']
            ax1.plot(I, R, color=colors[num],linewidth=2,marker='.',label=legend)

        gridsize = (1, 1)
        # fig, _ = plt.subplots()
        # ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        
        fig, ax1 = plt.subplots()
        
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_mag(num_curve)
        ax1.set_ylabel('magnitude [Vrms]', labelpad=4)
        ax1.set_xlabel('current [mA]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_MAG.png')
        plt.close(fig)

        # PHASE
        def add_plot_phase(num):
            curve_name = self.courbe1_list[num]
            parameters = self.courbe1[curve_name]['data']['parameters']
            legend = parameters['Graph']['legend curve '+str(num+1)] 
            I = self.courbe1[curve_name]['data']['Current']
            Phi = self.courbe1[curve_name]['data']['Phi']
            ax1.plot(I, Phi, color=colors[num],linewidth=2,marker='.',label=legend)

        fig, _ = plt.subplots()
        ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
        for num_curve in range(0,len(self.courbe1_list)):
            add_plot_phase(num_curve)        
        ax1.set_ylabel('phase [degrees]', labelpad=4)
        ax1.set_xlabel('current [mA]', labelpad=4)
        ax1.legend(loc=0)
        fig.savefig(self.fabricate_title_name()+'_PHASE.png')
        plt.close(fig)


