import pandas as pd

from pathlib import Path
from openpyxl.workbook import Workbook
from datetime import datetime
# Rest of your code using openpyxl...

class Medisoft_complet:
    def __init__(self,timestamp_0,timestamp_end):
        self.timestamp_0= timestamp_0# taux d'humitité relative indique le pourcentage de vapeur d’eau rapporté au maximum possible[%]
        self.timestamp_end= timestamp_end
        self.lines=[]
        self.open_file()
        
    def open_file(self):  
        # Get all .txt files in the folder
        txt_files = Path("C:\Expair\Logs\\").glob('*.txt')
        
        # Find the last modified .txt file
        loading_file= max(txt_files, key=lambda f: f.stat().st_mtime, default=None)
        # self.path=nini
        print(loading_file)
        # self.file = open(self.path, 'r')
        # print(self.timestamp_0,"toooooooooooooooooooo",self.timestamp_end)
        # print("complet:",loading_file)
        
        # Specify the file path
        file_path = loading_file

        with open(file_path, 'r') as self.file:
        
            self.lines = self.file.readlines()
        
    def get_info_patient(self):
     info_patient=self.lines[0:17]
     return info_patient     
        
        
    def get_lists(self):
        def time_string_to_seconds(time_str):
            time_obj = datetime.strptime(time_str, "%H:%M:%S.%f")
            total_seconds = (time_obj.hour * 3600 + time_obj.minute * 60 + time_obj.second) + time_obj.microsecond / 1e6
            return total_seconds

        lines = self.lines[18:]
        
        
        # Split each line into columns
        data_lines= [line.strip() for line in lines]

        times=[]
        indices=[]
        flows=[]
        volume_values=[]
        CO2_values=[]
        t_creat_list=[]
        deltat_list=[]
        deltat_list_h=[]
        
        
        R1_list=[]
        R2_list=[]
        R3_list=[]
        R4_list=[]
        
        
       
        for new_content in data_lines:
            data = new_content.split(';')
        
            # Extract the relevant values
            timestamp_parts = data[1].strip().split('   ')
            timestamp = timestamp_parts[0]
            index_m = timestamp_parts[1] 
            # print(index_m)
            # if len(timestamp_parts) > 1 else ""
            
            flow = float(data[2].replace(',', '.').strip())
            volume= float(data[3].replace(',', '.').strip())*-1
            CO2 = float(data[4].replace(',', '.').strip())
            
            
            R1 = float(data[8].replace(',', '.').strip())
            R2 = float(data[9].replace(',', '.').strip())
            R3 = float(data[10].replace(',', '.').strip())
            R4 = float(data[11].replace(',', '.').strip())
            
            
            
            
            #create _lists
            times.append(timestamp)
            indices.append(index_m)
            flows.append(flow)
            CO2_values.append(CO2)
            volume_values.append(volume)
            R1_list.append(R1)
            R2_list.append(R2)
            R3_list.append(R3)
            R4_list.append(R4)
            
        #find and match the first timestamp  when start the continous acquisition code    
        # a='15:29:13.402'
        # timestamp_0 =a
        ind = times.index(self.timestamp_0)
        ind2=times.index(self.timestamp_end)
        times=times[ind:(ind2+1)]
        indices=indices[ind:(ind2)]
        flows=flows[ind:(ind2)]
        CO2_values=CO2_values[ind:(ind2)]
        volume_values=volume_values[ind:(ind2)]
        R1_list=R1_list[ind:(ind2)]
        R2_list=R2_list[ind:(ind2)]
        R3_list=R3_list[ind:(ind2)]
        R4_list=R4_list[ind:(ind2)]
        
        
        t0_h=time_string_to_seconds(self.timestamp_0)
        t0=0
        t=float(0)
        # create a time_list
        for  i in range(0,len(times)-1):
        
            # t_creat=round(i*0.01,2)
            # t_creat_list.append(t_creat)
            # print(t_creat_list[i],times[i])
            
            # don forget for len(times)-1
               t_h=time_string_to_seconds(times[i+1])
               deltat_h=t_h-t0_h
               deltat_list_h.append(deltat_h)
              
               t=t+0.01
               deltat=t-t0
               deltat_list.append(deltat)
        
        # print(t_creat_list,times,indices,flows,CO2_values)   
        # return t_creat_list,times,indices,flows,CO2_values
        
        return deltat_list,deltat_list_h,times,indices,flows,volume_values,CO2_values,R1_list,R2_list,R3_list,R4_list
        # print(times)
        # print(ind)
        # print(len(times))
        # print(t_creat_list) 
        
    def disconnect(self):
       pass
        # if self.file:
        #     self.file.close()
        #     print("Disconnected from file.")
       
        
        
if __name__ == "__main__":
    
    medi=Medisoft_complet('16:23:27.377','16:23:27.618')
    t_creat_list,times,indices,flows,volume_values,CO2_values,R1_list,R2_list,R3_list,R4_list=medi.get_lists()
    print(t_creat_list,times,indices,flows,volume_values,CO2_values,R1_list,R2_list,R3_list,R4_list)
    # print(medi.get_info_patient())
    