void setup() {
  // Initialize Serial communication
  Serial.begin(9600);
}

void loop() {
  // Read analog value from pin A0
  int sensorValue = analogRead(A0);

  // Print the analog value to the Serial Monitor
  Serial.print("Analog Value: ");
  Serial.println(sensorValue);

  // Delay for a short time to make the output readable
  delay(5000);
}
